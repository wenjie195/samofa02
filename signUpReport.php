<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/SignUpProduct.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/phoneFormat.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$signUpDetails =  getSignUpProduct($conn," WHERE referrer_id = ? ",array("referrer_id"),array($_SESSION['uid']),"s");

$signUpAdminDetails =  getSignUpProduct($conn);

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/signUpReport.php" />
    
    <meta property="og:title" content="<?php echo _HEADERBEFORELOGIN_SIGNUP_PACKAGE ?> | Samofa 莎魔髪" />
    <title><?php echo _HEADERBEFORELOGIN_SIGNUP_PACKAGE ?> | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/signUpReport.php" />
	<?php include 'css.php'; ?> 
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 menu-distance75 min-height-with-flower">
	<h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _HEADERBEFORELOGIN_SIGNUP_PACKAGE ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

    <div class="overflow-scroll-div same-padding">
        
        <table class="table-css">
            <thead>
                <tr>
                <?php
                for($i= 0;$i < count($userRows) ;$i++)
                {
                    if($userRows[$i]->getUserType()==0)
                    {
                    ?>
                        <th><?php echo _PRODUCTDETAILS_NO ?></th>
                        <th><?php echo _PRODUCTDETAILS_NAME ?></th>
                        <th><?php echo _HEADERBEFORELOGIN_PACKAGE ?></th>
                        <th><?php echo _HEADERBEFORELOGIN_PACKAGE_DETAILS ?></th>
                        <th><?php echo _VIEW_CART_TOTAL ?> (RM)</th>
                        <th><?php echo _PURCHASE_DATE ?></th>
                        <th><?php echo _HEADERBEFORELOGIN_PURCHASE_INVOICE ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($signUpAdminDetails) {

                    for($cnt = 0;$cnt < count($signUpAdminDetails) ;$cnt++)
                        {
                            $productId = $signUpAdminDetails[$cnt]->getProduct();
                                $productDetails = getProduct($conn, "WHERE id =?",array("id"),array($productId), "s");
                                $productName = $productDetails[0]->getName();
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo $signUpAdminDetails[$cnt]->getReferrerName();?></td>
                                    <td><?php echo $signUpAdminDetails[$cnt]->getReferralName();?></td>
                                    <td><?php echo $productName ?></td>
                                    <td><?php echo $signUpAdminDetails[$cnt]->getTotal() ?></td>
                                    <td>
                                        <?php echo $date = date("d/m/Y",strtotime($signUpAdminDetails[$cnt]->getDateCreated()));?>
                                    </td>
                                    <td>
                                        <form method="POST" action="invoice.php" class="hover1">
                                            <button class="clean hover1 img-btn transparent-button pink-hover-text" type="submit" name="signup_id" value="<?php echo $signUpAdminDetails[$cnt]->getId();?>">
                                            <img src="img/details1.png" class="edit-icon1 hover1a" alt="<?php echo _ADMIN_DETAILS ?>" title="<?php echo _ADMIN_DETAILS ?>">
                                            <img src="img/details2.png" class="edit-icon1 hover1b" alt="<?php echo _ADMIN_DETAILS ?>" title="<?php echo _ADMIN_DETAILS ?>">
                                            </button>
                                        </form>
                                    </td>
                            </tr>
                        <?php

                        }
                        $conn->close();
                }else{
                    ?>
                    <td class="no-data-td" colspan="6"><?php echo _ADMIN_NO_SIGN_UP_USER ?></td>
                    <?php
                }
                        
                        ?>
                </tbody>
                    <?php
                    }
                    else if($userRows[$i]->getUserType()==1)
                    {
                        if($signUpDetails)
                        {
                        ?>
                            <thead>
                                <tr>
                                    <th><?php echo _PRODUCTDETAILS_NO ?></th>
                                    <th><?php echo _PRODUCTDETAILS_NAME ?></th>
                                    <th><?php echo _JS_SIGNUP ?></th>
                                    <th><?php echo _JS_SIGNUP." "._HEADER_ORDER ?></th>
                                    <th><?php echo _VIEW_CART_TOTAL ?> (RM)</th>
                                    <th><?php echo _PURCHASE_DATE ?></th>
                                    <th><?php echo _HEADERBEFORELOGIN_PURCHASE_INVOICE ?></th>
                                </tr>
                            </thead>
                            <?php
                            for($cnt = 0;$cnt < count($signUpDetails) ;$cnt++)
                            {   
                                $productId = $signUpDetails[$cnt]->getProduct();
                                $productDetails = getProduct($conn, "WHERE id =?",array("id"),array($productId), "s");
                                $productName = $productDetails[0]->getName();
                            ?>  
                            <tbody>  
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo $userRows[$i]->getUsername();?></td>
                                    <td><?php echo $signUpDetails[$cnt]->getReferralName();?></td>
                                    <td><?php echo $productName ?></td>
                                    <td><?php echo $signUpDetails[$cnt]->getTotal() ?></td>
                                    <td>
                                        <?php echo $date = date("d/m/Y",strtotime($signUpDetails[$cnt]->getDateCreated()));?>
                                    </td><td>
                                        <form method="POST" action="invoice.php" class="hover1">
                                            <button class="clean hover1 img-btn transparent-button pink-hover-text" type="submit" name="signup_id" value="<?php echo $signUpAdminDetails[$cnt]->getId();?>">
                                            <img src="img/details1.png" class="edit-icon1 hover1a" alt="<?php echo _ADMIN_DETAILS ?>" title="<?php echo _ADMIN_DETAILS ?>">
                                            <img src="img/details2.png" class="edit-icon1 hover1b" alt="<?php echo _ADMIN_DETAILS ?>" title="<?php echo _ADMIN_DETAILS ?>">
                                            </button>
                                        </form>
                                    </td>
                                    <!-- <td>
                                        <form method="POST" action="#" class="hover1">
                                            <button class="clean hover1 img-btn transparent-button pink-hover-text" type="submit" name="order_id" value="">
                                                <//?php echo _PURCHASE_VIEW ?>
                                            </button>
                                        </form>
                                    </td> -->
                                </tr>
                            </tbody>
                            <?php
                            }
                        }
                        else
                        { ?>
                            </br>
                            <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMIN_NO_SIGNUP_PACKAGE ?></h1>
                        <?php    
                        }
                    }
                }
                ?>   
            </tbody>
        </table>

    </div>
</div>    

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>