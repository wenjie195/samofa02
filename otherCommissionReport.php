<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/OtherBonus.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$bonusDetails = getOtherBonus($conn, " WHERE receive_uid =? ", array("receive_uid"), array($uid), "s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/otherCommissionReport.php" />
    <meta property="og:title" content="<?php echo _ADMIN_OTHER_COMMISSION_REPORT ?> | Samofa 莎魔髪" />
    <title><?php echo _ADMIN_OTHER_COMMISSION_REPORT ?> | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/otherCommissionReport.php" />
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
	<?php include 'css.php'; ?>

</head>

<body class="body">

<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h2 class="dark-pink-text hi-title text-center modal-h1 big-header-color"><a href="commissionReport.php" class="link-a"><?php echo _HEADERBEFORELOGIN_COMMISSION_REPORT ?></a> | <?php echo _ADMIN_OTHER_COMMISSION_REPORT ?></h2>

    <div class="width100 container-div1">

        <div class="overflow-scroll-div same-padding">
            <table class="table-css">
                <thead>
                    <tr>
                        <th><?php echo _PRODUCTDETAILS_NO ?></th>
                        <th><?php echo _JS_USERNAME ?></th>
                        <th><?php echo _ADMIN_AMOUNT ?></th>
                        <th><?php echo _ADMIN_BONUS_TYPE ?></th>
                        <th><?php echo _ADMIN_SIGN_UP_USER ?></th>
                        <th><?php echo _USERDASHBOARD_STATUS ?></th>
                        <th><?php echo _PURCHASE_DATE ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($bonusDetails)
                    {
                        for($cnt = 0;$cnt < count($bonusDetails) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $bonusDetails[$cnt]->getReceiveUsername();?></td>
                                <td><?php echo $bonusDetails[$cnt]->getBonus();?></td>
                                <td><?php echo $bonusDetails[$cnt]->getBonusType();?></td>
                                <td><?php echo $bonusDetails[$cnt]->getNewUser() ?></td>
                                <td><?php echo $bonusDetails[$cnt]->getStatus();?></td>
                                <td><?php echo date('d/m/Y',strtotime($bonusDetails[$cnt]->getDateCreated()));?></td>
                            </tr>
                        <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>

    </div>

    <div class="clear"></div>

</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>
