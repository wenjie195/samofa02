<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Orders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$orderCheckers = getOrders($conn, " WHERE status = 'PENDING' AND payment_references is not null ORDER BY date_created DESC");
// $soundfile = "notification_sound.mp3";

if($orderCheckers)
{  
    $totalOrderCheckers = count($orderCheckers);
    // echo "<embed src =\"$soundfile\" hidden=\"true\" autostart=\"true\"></embed>";
?>
    <a href="adminViewBill.php">    
        <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color">
            有（<?php echo $totalOrderCheckers;?>）待批订单， 点击查看！
        </h1>
    </a>  
<?php
}
else
{   }
$conn->close();
?>