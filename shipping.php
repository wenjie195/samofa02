<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/timezone.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/Shipping.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
// require_once dirname(__FILE__) . '/classes/Product.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();
$date = date("Y-m-d");
$time = date("h:i a");
$time24Hour = date('Hi');
$totalProduct = 0;

$id = getOrders($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1 ",array("uid"),array($uid),"s");
$orderUid = $id[0]->getId();

$productOrders = getProductOrders($conn);

$userOrder = getOrders($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$orderDetails = $userOrder[0];

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$rank= $userDetails->getRank();
$currentWallet = $userDetails->getSales();
$username = $userDetails->getUsername();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $id = $orderUid;

    $productOrdersDetails = getProductOrders($conn, "WHERE order_Id =?",array("order_Id"),array($id), "s");
    for ($m=0; $m <count($productOrdersDetails) ; $m++) {
        $totalProduct += $productOrdersDetails[$m]->getQuantity();
        $productId = $productOrdersDetails[$m]->getProductId();
        // $productType = $productDetails[0]->getProductType();
        $productDetails = getProduct($conn, "WHERE id =?",array("id"),array($productOrdersDetails[$m]->getProductId()), "s");
        $productName[] = $productId;
        $productNames[] = $productDetails[0]->getName();
        $productQuantity[] = $productOrdersDetails[$m]->getQuantity();
    }

    $productNameImp = implode(",",$productName);
    $productNamesImp = implode(",",$productNames);
    $productQuantityImp = implode(",",$productQuantity);
    $productDetails = getProduct($conn, "WHERE id =?",array("id"),array($productId), "s");
    $productType = $productDetails[0]->getProductType();

    $name = rewrite($_POST["insert_name"]);
    //$_SESSION['name'] = $name;
    $email = rewrite($_POST["insert_email"]);
    $contactNo = rewrite($_POST["insert_contactNo"]);
    //$_SESSION['contact'] = $contactNo;
    $address_1 = rewrite($_POST["insert_address_1"]);
    $address_2 = rewrite($_POST["insert_address_2"]);
    $city = rewrite($_POST["insert_city"]);
    $zipcode = rewrite($_POST["insert_zipcode"]);
    $state = rewrite($_POST["insert_state"]);
    $country = rewrite($_POST["insert_country"]);
    $subtotal = ($_POST["subtotal"]);
    $total = $subtotal;

    if(isset($_POST['shippingSubmit']))
    {
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";

        //echo "save to database";
        if($name)
        {
            array_push($tableName,"recipient_firstname");
            array_push($tableValue,$name);
            $stringType .=  "s";
        }

        if($contactNo)
        {
            array_push($tableName,"contact");
            array_push($tableValue,$contactNo);
            $stringType .=  "s";
        }

        if($address_1)
        {
            array_push($tableName,"address_line_1");
            array_push($tableValue,$address_1);
            $stringType .=  "s";
        }

        if($address_2)
        {
            array_push($tableName,"address_line_2");
            array_push($tableValue,$address_2);
            $stringType .=  "s";
        }

        if($city)
        {
            array_push($tableName,"city");
            array_push($tableValue,$city);
            $stringType .=  "s";
        }

        if($zipcode)
        {
            array_push($tableName,"zipcode");
            array_push($tableValue,$zipcode);
            $stringType .=  "s";
        }

        if($state)
        {
            array_push($tableName,"state");
            array_push($tableValue,$state);
            $stringType .=  "s";
        }

        if($country)
        {
            array_push($tableName,"country");
            array_push($tableValue,$country);
            $stringType .=  "s";
        }

        if($total)
        {
            array_push($tableName,"total");
            array_push($tableValue,$total);
            $stringType .=  "d";
        }

        if($subtotal)
        {
            array_push($tableName,"subtotal");
            array_push($tableValue,$subtotal);
            $stringType .=  "d";
        }
        if($productType)
        {
            array_push($tableName,"product_type");
            array_push($tableValue,$productType);
            $stringType .=  "s";
        }
        if($totalProduct)
        {
            array_push($tableName,"total_product");
            array_push($tableValue,$totalProduct);
            $stringType .=  "s";
        }
        if($totalProduct > 5)
        {
            $totalRebate = $totalProduct - 5;
            array_push($tableName,"rebate_done");
            array_push($tableValue,$totalRebate);
            $stringType .=  "s";
        }
        if($productNameImp)
        {
            array_push($tableName,"product_id");
            array_push($tableValue,$productNameImp);
            $stringType .=  "s";
        }
        if($productQuantityImp)
        {
            array_push($tableName,"quantity");
            array_push($tableValue,$productQuantityImp);
            $stringType .=  "s";
        }
        if($username)
        {
            array_push($tableName,"username");
            array_push($tableValue,$username);
            $stringType .=  "s";
        }

        // echo $uid. "<br>";
        // echo $name. "<br>";
        // echo $email. "<br>";
        // echo $contactNo. "<br>";
        // echo $address_1. "<br>";
        // echo $address_2. "<br>";
        // echo $city ."<br>";
        // echo $zipcode ."<br>";
        // echo $state."<br>";
        // echo $country ."<br>";
        // echo count($tableValue);

        array_push($tableValue,$id);
        $stringType .=  "s";
        $updateOrderDetails = updateDynamicData($conn,"orders"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        if($updateOrderDetails)
        {
            echo "<script>alert('Successfully added shipping details!');</script>";
        }
        else
        {
            echo "<script>alert('Fail to add shipping details!');window.location='../checkout.php'</script>";
        }
    }
    else
    {
        echo "<script>alert('ERROR 2');window.location='../checkout.php'</script>";
    }
}

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
// $userDetails = $userRows[0];

// $userOrder = getOrders($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
// $orderDetails = $userOrder[0];

// $rank= $userDetails->getRank();
// $currentWallet = $userDetails->getSales();
// $username = $userDetails->getUsername();

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'])
{
    $productListHtml = getShoppingCart($conn,$rank,2);
}
else
{}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/shipping.php" />
    <meta property="og:title" content="Payment Method | Samofa 莎魔髪" />
    <title>Payment Method | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/shipping.php" />
    <?php include 'css.php'; ?>
    <style>
    td {
        text-align: center;
        vertical-align: middle;
    }
    </style>
</head>

<body class="body">

<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-menu-distance75 same-padding">

    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _SHIPPING_PAYMENT ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

        <!-- <form method="POST" action="utilities/orderInformation.php" enctype="multipart/form-data"> -->
        <!-- <form method="POST" action="#" enctype="multipart/form-data"> -->

        <p class="info-title dark-pink-text"><b><?php echo _SHIPPING_SHIP_TO ?></b></p>

        <input class="clean white-input two-box-input" type="hidden" id="order_id" name="order_id" value="<?php echo $id;?>">
        <!-- <input class="clean white-input two-box-input" type="hidden" id="insert_username" name="insert_username" value="<?php echo $username;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_bankname" name="insert_bankname" value="<?php echo $bankName;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_bankaccountholder" name="insert_bankaccountholder" value="<?php echo $bankAccountHolder;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_bankaccountnumber" name="insert_bankaccountnumber" value="<?php echo $bankAccountNo;?>"> -->
        <input class="clean white-input two-box-input" type="hidden" id="insert_name" name="insert_name" value="<?php echo $name;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_contactNo" name="insert_contactNo" value="<?php echo $contactNo;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_address_1" name="insert_address_1" value="<?php echo $address_1;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_address_2" name="insert_address_2" value="<?php echo $address_2;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_city" name="insert_city" value="<?php echo $city;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_zipcode" name="insert_zipcode" value="<?php echo $zipcode;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_state" name="insert_state" value="<?php echo $state;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_country" name="insert_country" value="<?php echo $country;?>">

        <p class="info-title dark-pink-text"><b><?php echo _SHIPPING_PAYMENT ?></b></p>
        <p class="smaller-text pink-text"><?php echo _SHIPPING_ALL_TRANS ?></p>
        <?php
            if($userDetails)
            {
                $conn = connDB();
                $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
            ?>

                <div class="dual-input">
                    <p class="input-top-text"><?php echo _SHIPPING_BANK ?></p>
                    <p class="fake-input-p">CIMB</p>
                </div>

                <div class="dual-input second-dual-input">
                    <p class="input-top-text"><?php echo _SHIPPING_ACC_NO ?></p>
                    <p class="fake-input-p">XXXXXXXXXXXXX</p>
                </div>

                <div class="clear"></div>

                <div class="dual-input">
                    <p class="input-top-text"><?php echo _SHIPPING_BANK_HOLDER ?></p>
                    <p class="fake-input-p">Account Holder</p>
                </div>
            <?php
            }
            ?>
        <!-- ---------------------------------------------------Imp--------------------------------------------------------- -->
        <div style="display: none;" class="white-input-div payment-white-div">
            <p class="payment-input-p">
                <!-- Payment Method -->
                <div class="clean edit-profile-input payment-input" type="hidden" id="payment_method" name="payment_method">
                    <input type="hidden" id="billPlz" value="<?php echo 'CreateABill.php?amount='.$_SESSION['total'] ?>" name="Online Banking">
                </div>
            </p>
        </div>
        <!-- ---------------------------------------------------Imp--------------------------------------------------------- -->

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _SHIPPING_PAYMENT_METHOD ?></p>
            <select class="clean de-input" id="paymentMethod">
                <option><?php echo _SHIPPING_SELECT_PAYMENT_METHOD ?></option>
                <option value="Online Banking and Cards">Online Banking and Cards</option>
                <option value="Commission">Commission (Wallet) - RM <?php echo $currentWallet ?></option>
            </select>
            <!-- <p class="fake-input-p">BllPlz</p> -->
        </div>

        <div class="clear"></div>

        <div style="display: none" id="showLater">
            <div class="dual-input">
                <p class="input-top-text"><?php echo _SHIPPING_AMOUNT ?></p>
                <input class="clean de-input" required type="text" placeholder="0.00"  id="payment_amount" name="payment_amount">
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text"><?php echo _SHIPPING_BANK_REFERENCE ?></p>
                <input class="clean de-input" required type="text" placeholder="<?php echo _SHIPPING_BANK_REFERENCE ?>"  id="payment_bankreference" name="payment_bankreference">
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-text"><?php echo _SHIPPING_BANK_IN_DATE ?></p>
                <input class="clean de-input" required type="date" id="payment_date" name="payment_date" value="<?php echo $date ?>">
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text"><?php echo _SHIPPING_BANK_IN_TIME ?></p>
                <input class="clean de-input"  required type="time" id="payment_time" name="payment_time" value="<?php echo $time ?>">
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-text"><?php echo _SHIPPING_UPLOAD_RECEIPT ?></p>
                <input class="hidden-input" type="file" name="file" />
            </div>
        </div>

        <div class="clear"></div>

        <input type="hidden" value="PENDING" class="clean edit-profile-input payment-input" id="payment_status" name="payment_status">

        <div class="clear"></div>

        <div class="width100 top-bottom-spacing">
            <table class="table-css">
                <thead>
                    <th style="text-align:center;" width="20%"><?php echo _HEADERBEFORELOGIN_PRODUCT ?></th>
                    <th style="text-align:center;" width="20%"><?php echo _VIEW_CART_QUANTITY ?></th>
                    <th style="text-align:center;" width="40%"><?php echo _PRODUCTDETAILS_NAME ?></th>
                    <th style="text-align:center;" width="20%"><?php echo _PRODUCTDETAILS_PRICE ?></th>
                </thead>
            </table>
        </div>
        <?php echo $productListHtml; ?>



        <div class="clear"></div>

        <div class="width100 text-center">
            <!-- <div class="width100 text-center top-bottom-spacing">
            <button class="dark-pink-button border0 clean black-button add-to-cart-btn checkout-btn continue2 add-to-cart-btn2" name="register"><?php echo _CHECKOUT_NEXT ?></button>
            </div> -->

            <div class="width100 text-center top-bottom-spacing">
                <form method="post" action="billplzpost.php">
                    <input type="hidden" name="name" value="<?php echo $username ?>">
                    <input type="hidden" name="email" value="<?php echo $email ?>">
                    <input type="hidden" name="mobile" value="<?php echo $contactNo ?>">
                    <input type="hidden" name="reference_1_label" value="Product">
                    <input type="hidden" name="reference_1" value="<?php echo $productNamesImp ?>">
                    <input type="hidden" name="amount" value="<?php echo $total * 100 ?>">
                    <input type="hidden" name="reference_2" value="<?php echo $orderUid ?>">
                    <button type="submit" style="display: none" id="completeOrder" class="dark-pink-button border0 clean black-button add-to-cart-btn" name="register"><?php echo _CHECKOUT_NEXT ?></button>
                </form>
                <input id="currentWallet" type="hidden" name="wallet" value="<?php echo $currentWallet ?>">
                <input type="hidden" id="time24Hour" value="<?php echo $time24Hour ?>">
                <?php
                    $_SESSION['name'] = $name;
                    $_SESSION['email'] = $email;
                    // $items = array();
                    // if($productOrders){
                    //     for($cnt = 0;$cnt < count($productOrders) ;$cnt++){
                    //             if($productOrders[$cnt]->getOrderId()==$orderUid){
                    //                 $item = $productOrders[$cnt]->getProductId();
                    //                 $items[] = $item;
                    //             }
                    //     }
                    // }
                    // print_r($items);
                ?>
                <input id="name" type="hidden" name="name" value="<?php echo $_SESSION['name'] ?>">
                <input id="email" type="hidden" name="email" value="<?php echo $_SESSION['email'] ?>">
                <input id="item" type="hidden" name="item" value="<?php echo $_SESSION['item'] ?>">
                <!-- <form action="utilities/paymentMethodFunction.php" method="post"> -->
                    <input id="orderId" type="hidden" name="order_id" value="<?php echo $id ?>">
                    <input id="total" type="hidden" name="total" value="<?php echo $_SESSION['total'] ?>">
                    <button type="submit" style="display: none" value="<?php echo $uid ?>" id="completeOrderCommission" class="dark-pink-button border0 clean black-button add-to-cart-btn" name="uid"><?php echo _JS_PLACEORDER ?></button>
                <!-- </form> -->
            </div>

            <div class="clear"></div>

            <div class="width100 text-center margin-bottom30">
                <p class="continue-shopping pointer continue2"><a href="checkout.php" class="pink-text opacity-hover back-text"><img src="img/back.png" class="checkout-back-btn" alt="<?php echo _VIEW_CART_BACK ?>" title="<?php echo _VIEW_CART_BACK ?>" > <?php echo _VIEW_CART_BACK ?></a></p>
            </div>
        </div>

    <!-- </form> -->

</div>

<?php include 'js.php'; ?>

<script>
function la(src)
{
    window.location=src;
}
</script>

<script type="text/javascript">
    $(document).ready(function(){
        // var time24Hour = $("#time24Hour").val();
        // // alert(time24Hour);
        // if (time24Hour > 2300) {
        //     $("#completeOrder").text("Temporary Close");
        // }

    $("#paymentMethod").change(function(){
        if ($(this).val() == 'Online Banking and Cards') {
            // alert("Billplz");
            $("#completeOrder").show();
            $("#completeOrderCommission").hide();
        }else
        if ($(this).val() == 'Commission') {
            // alert("Commission");
            $("#completeOrder").hide();
            $("#completeOrderCommission").show();
        }
    });
    $("#completeOrderCommission").click(function(){
        var orderId = $("#orderId").val();
        var uid = $(this).attr('value');
        var total = $("#total").val();
        var name = $("#name").val();
        var wallet = $("#currentWallet").val(); // your wallet
        var poleTest = wallet - total;
        // alert(orderId);
        // alert(uid);
        // alert(total);
        // alert(poleTest);
        if (poleTest >= 0) {
            $.ajax({
                url: 'utilities/paymentMethodFunction.php',
                data: {orderId:orderId,uid:uid,total:total,name:name},
                type: 'post',
                dataType: 'json',
                success:function(response){
                    alert('success');
                    location.href = "userDashboard.php";
                }
            });
        }else if(poleTest < 0){
            alert("Your Balance Not Enough.");
        }
    });
    });
</script>

</body>
</html>
