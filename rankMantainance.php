<?php 

require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Announcement.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Orders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$mantainanceMonth = 3;
$todayDate = date('Y-m-d H:i');

$userDetails = getUser($conn, "WHERE user_rank='Partner' OR user_rank='Director'");
for ($i=0; $i <count($userDetails) ; $i++) { 
	$uid = $userDetails[$i]->getUid();
	$username = $userDetails[$i]->getUsername();
	$ordersDetails = getOrders($conn, "WHERE uid=? AND status = 'APPROVED' ORDER BY date_created DESC LIMIT 1",array("uid"),array($uid), "s");
	if ($ordersDetails) {
		$dateCreated = $ordersDetails[0]->getDateUpdated();
		$dateExpired = date('Y-m-d H:i',strtotime($dateCreated. "+".$mantainanceMonth." Month"));
	}else {
		$dateCreated = $userDetails[$i]->getDateCreated();
		$dateExpired = date('Y-m-d H:i',strtotime($dateCreated. "+".$mantainanceMonth." Month"));
	}
	// echo $username." : ".$dateExpired."<br>";
	if ($todayDate > $dateExpired) {
		echo $username." : ".$dateExpired."<br>";
		$expiredUser = insertDynamicData($conn,"expired_user",array("uid","expired_date"),
          array($uid,$dateExpired),"ss");
		
	}
}
 ?>