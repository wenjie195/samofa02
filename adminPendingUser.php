<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/ReferralPending.php';
require_once dirname(__FILE__) . '/classes/UserPending.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/phoneFormat.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$allUser = getPendingUser($conn, " WHERE user_type = ? AND user_status = 'PENDING' ", array("user_type"), array(1), "i");


function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/adminPendingUser.php" />
    <meta property="og:title" content="All Member | Samofa 莎魔髪" />
    <title><?php echo _ADMIN_NEW_REGISTRATION ?> | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/adminPendingUser.php" />
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
	<?php include 'css.php'; ?>
    
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMIN_NEW_REGISTRATION ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    <div class="width100 overflow same-padding margin-bottom30">
    	<div class="search-input-div">
    		<img src="img/search.png" class="search-png" alt="<?php echo _ADMIN_SEARCH ?>" title="<?php echo _ADMIN_SEARCH ?>">
            <input class="clean fake-line-input" type="text" placeholder="<?php echo _ADMIN_SEARCH ?> <?php echo _JS_FULLNAME ?>" id="myInput" onkeyup="myFunction()">
        </div>
    </div>
    <div class="width100 same-padding container-div1">

    <div class="overflow-scroll-div">
        <table class="table-css" id="myTable">
            <thead>
                <tr>
                    <th><?php echo _PRODUCTDETAILS_NO ?></th>
                    <th><?php echo _JS_FULLNAME ?></th>
                    <th><?php echo _USERDASHBOARD_SPONSOR ?></th>
                    <th><?php echo _JS_EMAIL ?></th>
                    <th><?php echo _JS_PHONE ?></th>
                    <th><?php echo _JS_COUNTRY ?></th>
                    <th><?php echo _ADMIN_REQUESTED_DATE ?></th>
                    <th><?php echo _ADMIN_ACTION ?></th>
                    <th><?php echo _ADMIN_ACTION ?> (2)</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($allUser)
                {
                    for($cnt = 0;$cnt < count($allUser) ;$cnt++)
                    {
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>

                            <td><?php echo $allUser[$cnt]->getLastName().$allUser[$cnt]->getFirstName();?></td>
                            <td>
                                <?php 
                                	$referralPendingDetails = getPendingReferral($conn, "WHERE referral_id =?",array("referral_id"),array($allUser[$cnt]->getUid()), "s");
                                	$referrerId = $referralPendingDetails[0]->getReferrerId();
                                	$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($referrerId), "s");
                                	echo $userDetails[0]->getUsername();
                                 ?>
                            </td>
                            <td><?php echo $allUser[$cnt]->getEmail();?></td>
                            <td><?php echo phone_number_format($allUser[$cnt]->getPhoneNo());?></td>
                            <td><?php echo $allUser[$cnt]->getCountry();?></td>
                            <td><?php echo date('d/m/Y',strtotime($allUser[$cnt]->getDateCreated())) ?></td>
                            <td>
                            	<form action="utilities/userApprovalFunction.php" method="post">
                            		<input type="hidden" name="uid" value="<?php echo $allUser[$cnt]->getUid()?>">
                            		<button class="clean hover1 transparent-button pointer" name="accept" type="submit">
                            			<img src="img/tick.png" class="edit-icon1 hover1a">
                                    	<img src="img/tick2.png" class="edit-icon1 hover1b">
                            		</button>
                            		<button class="clean hover1 transparent-button pointer" name='reject' type="submit">
                            			<img src="img/delete1.png" class="edit-icon1 hover1a">
                                    	<img src="img/delete2.png" class="edit-icon1 hover1b">
                            		</button>
                            	</form>
                            </td>

                            <td>
                            	<form action="utilities/userApprovalAdditionalFunction.php" method="post">
                            		<input type="hidden" name="uid" value="<?php echo $allUser[$cnt]->getUid()?>">
                            		<button class="clean hover1 transparent-button pointer" name="accept" type="submit">
                            			<img src="img/tick.png" class="edit-icon1 hover1a">
                                    	<img src="img/tick2.png" class="edit-icon1 hover1b">
                            		</button>
                            		<!-- <button class="clean hover1 transparent-button pointer" name='reject' type="submit">
                            			<img src="img/delete1.png" class="edit-icon1 hover1a">
                                    	<img src="img/delete2.png" class="edit-icon1 hover1b">
                            		</button> -->
                            	</form>
                            </td>
                        </tr>
                    <?php
                    }
                }else{
                	?>
                	<td class="no-data-td" colspan="9"><?php echo _ADMIN_NO_NEW_PENDING ?></td>
                	<?php
                    $conn->close();
                }
                ?> 
            </tbody>
        </table>
    </div>

    </div>
</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>