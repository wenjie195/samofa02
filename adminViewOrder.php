<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/Product.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$orderDetails = getOrders($conn, " WHERE pickup_type = '' AND status ='APPROVED'");

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/adminViewOrder.php" />
    <meta property="og:title" content="<?php echo _ADMIN_CURRENT_ORDER ?> | Samofa 莎魔髪" />
    <title><?php echo _ADMIN_CURRENT_ORDER ?> | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/adminViewOrder.php" />
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
	<?php include 'css.php'; ?>
    
</head>

<body class="body">

<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMIN_CURRENT_ORDER ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    
    <div class="width100 container-div1">
   
        <div class="overflow-scroll-div same-padding">
            <table class="table-css">
                <thead>
                    <tr>
                        <th><?php echo _PRODUCTDETAILS_NO ?></th>
                        <th><?php echo _JS_USERNAME ?></th>
                        <th><?php echo _ADMINHEADER_PRODUCTS ?></th>
                        <th><?php echo _ADMIN_PAYMENT_METHOD ?></th>
                        <th><?php echo _ADMIN_REQUESTED_DATE ?></th>
                        <th><?php echo _ADMIN_DETAILS ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($orderDetails)
                    {
                        for($cnt = 0;$cnt < count($orderDetails) ;$cnt++)
                        {

							$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($orderDetails[$cnt]->getUid()), "s");
							$userData = $userDetails[0];

							$ProductOrdersDetails = getProductOrders($conn, "WHERE order_id =?",array("order_id"),array($orderDetails[$cnt]->getId()), "s");
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $userData->getUsername();?></td>
                                <td>
                                	<?php if ($ProductOrdersDetails) {
                                	for ($i=0; $i <count($ProductOrdersDetails) ; $i++) { 
                                		$productDetails = getProduct($conn, "WHERE id =?",array("id"),array($ProductOrdersDetails[$i]->getProductId()),"s");
                                		echo $productDetails[0]->getName()."<br>";
                                	}
                                } ?>
                                </td>

                                <td><?php echo $orderDetails[$cnt]->getPaymentMethod();?></td>
                                <td><?php echo date('d/m/Y',strtotime($orderDetails[$cnt]->getDateCreated()));?></td>

                                <td>
                                    <form method="POST" action="userOrdersDetails.php" class="hover1">
                                        <button class="clean hover1 transparent-button pointer" type="submit" name="id" value="<?php echo $orderDetails[$cnt]->getId();?>">
                                            <img src="img/details1.png" class="edit-icon1 hover1a" alt="<?php echo _ADMIN_DETAILS ?>" title="<?php echo _ADMIN_DETAILS ?>">
                                            <img src="img/details2.png" class="edit-icon1 hover1b" alt="<?php echo _ADMIN_DETAILS ?>" title="<?php echo _ADMIN_DETAILS ?>">
                                        </button>
                                    </form>                  
                                </td>
                            </tr>
                        <?php
                        }
                    }
                    $conn->close();
                    ?> 
                </tbody>
            </table>
        </div>

    </div>    

    <div class="clear"></div>

</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>
