<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ExpiredUser.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$expiredUserDetails = getExpiredUser($conn,"WHERE status = 'PENDING'");

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://dxforextrade88.com/userDashboard.php" /> -->
    <meta property="og:title" content="All Member | Samofa 莎魔髪" />
    <title>All Member | Samofa 莎魔髪</title>
    <!-- <link rel="canonical" href="https://dxforextrade88.com/userDashboard.php" /> -->
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
	<?php include 'css.php'; ?>
    
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMINHEADER_ALL_EXPIRED_MEMBER ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    <div class="width100 overflow same-padding margin-bottom30">
    	<div class="search-input-div">
    		<img src="img/search.png" class="search-png" alt="<?php echo _ADMIN_SEARCH ?>" title="<?php echo _ADMIN_SEARCH ?>">
            <input class="clean fake-line-input" type="text" placeholder="<?php echo _ADMIN_SEARCH ?>" id="myInput" onkeyup="myFunction()">
        </div>
    </div>
    <div class="width100 same-padding container-div1">

    <div class="overflow-scroll-div">
        <table class="table-css" id="myTable">
            <thead>
                <tr>
                    <th><?php echo _PRODUCTDETAILS_NO ?></th>
                    <th><?php echo _JS_SPONSOR_ID ?></th>
                    <th><?php echo _JS_USERNAME ?></th>
                    <th><?php echo _USERDASHBOARD_RANK ?></th>
                    <th><?php echo "Expired Date" ?></th>
                    <th><?php echo _ADMIN_ACTION ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($expiredUserDetails)
                {
                    for($cnt = 0;$cnt < count($expiredUserDetails) ;$cnt++)
                    {
                    	$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($expiredUserDetails[$cnt]->getUid()), "s");
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $userDetails[0]->getMemberId() ?></td>
                            <td><?php echo $userDetails[0]->getUsername();?></td>
                            <td><?php echo $userDetails[0]->getUserRank();?></td>
                            <td><?php echo date('d/m/Y',strtotime($expiredUserDetails[$cnt]->getExpiredDate()));?></td>
                            <td>
                            	<form action="utilities/expiredUserFunction.php" method="post">
                            		<input type="hidden" name="uid" value="<?php echo $userDetails[0]->getUid() ?>">
                            		<button class="clean inactive-btn transparent-button dark-pink-button" type="submit" name="submitBtn">Inactive</button>
                            	</form>
                            </td>
                        </tr>
                    <?php
                    }
                }else{
                	?>
                	<td class="no-data-td" colspan="6">No Expired User.</td>
                	<?php
                }
				$conn->close();
                ?> 
            </tbody>
        </table>
    </div>

    </div>
</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>