<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/MilesStones.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/Product.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();
$mantainanceMonth = 3;
$groupSalesPackage = 0;
$addMember = 0;
$directDownlineLevel = 0;

$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userData = $userDetails[0];

// $userMilestone = getMilesStones($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userMilestoneOne = getMilesStones($conn, " WHERE uid = ? AND status = 'APPROVED' and bonus = 1 ", array("uid"), array($uid), "s");
$userMilestoneTwo = getMilesStones($conn, " WHERE uid = ? AND status = 'APPROVED' and bonus = 2 ", array("uid"), array($uid), "s");
$userMilestoneThree = getMilesStones($conn, " WHERE uid = ? AND status = 'APPROVED' and bonus = 3 ", array("uid"), array($uid), "s");
$userMilestoneFour = getMilesStones($conn, " WHERE uid = ? AND status = 'APPROVED' and bonus = 4 ", array("uid"), array($uid), "s");
$userMilestoneFive = getMilesStones($conn, " WHERE uid = ? AND status = 'APPROVED' and bonus = 5 ", array("uid"), array($uid), "s");
$userMilestoneSix = getMilesStones($conn, " WHERE uid = ? AND status = 'APPROVED' and bonus = 6 ", array("uid"), array($uid), "s");
$userMilestoneSeven = getMilesStones($conn, " WHERE uid = ? AND status = 'APPROVED' and bonus = 7 ", array("uid"), array($uid), "s");
$userMilestoneEight = getMilesStones($conn, " WHERE uid = ? AND status = 'APPROVED' and bonus = 8 ", array("uid"), array($uid), "s");

// //--------------- last order and check maintainence (start) ---------------//
// $presidentLastOrder = getOrders($conn, "WHERE uid= ? AND status = 'APPROVED' ORDER BY date_created DESC LIMIT 1",array("uid"),array($uid), "s");

// $presidentAboveLastOrder = getOrders($conn, "WHERE uid= ? AND status = 'APPROVED' AND product_type='Package' ORDER BY date_created DESC LIMIT 1",array("uid"),array($uid), "s");
// //--------------- last order and check maintainence (end) ---------------//

$ordersDetails = getOrders($conn, "WHERE uid=? AND status = 'APPROVED' AND product_type='Package' ORDER BY date_created DESC LIMIT 1",array("uid"),array($uid), "s");

$referralHistory = getReferralHistory($conn, "WHERE referrer_id=? ORDER BY date_created DESC LIMIT 1",array("referrer_id"),array($uid), "s");

if ($ordersDetails) {
    $dateCreated = $ordersDetails[0]->getDateUpdated();
    $dateExpiredOrders = date('Y-m-d H:i',strtotime($dateCreated. "+".$mantainanceMonth." Month"));
    $dateExpiredReferral = 0;
}
elseif ($referralHistory) {
    $currentLevel = $referralHistory[0]->getCurrentLevel();
    $directDownlineLevel  = $currentLevel + 1;
    $dateCreated = $referralHistory[0]->getDateCreated();
    $dateExpiredReferral = date('Y-m-d H:i',strtotime($dateCreated. "+".$mantainanceMonth." Month"));
    $dateExpiredOrders = 0;
}
else {
    $dateCreated = $userData->getDateCreated();
    $dateExpiredUser = date('Y-m-d H:i',strtotime($dateCreated. "+".$mantainanceMonth." Month"));
    $dateExpiredOrders = 0;
    $dateExpiredReferral = 0;
}
if ($dateExpiredOrders > $dateExpiredReferral) {
    $dateExpired = $dateExpiredOrders;
}elseif ($dateExpiredOrders < $dateExpiredReferral) {
    $dateExpired = $dateExpiredReferral;
}elseif($dateExpiredOrders < $dateExpiredReferral){
    $dateExpired = $dateExpiredOrders;
}else{
    $dateExpired = $dateExpiredUser;
}

$userRHDetails = getReferralHistory($conn, " WHERE referral_id =? ", array("referral_id"), array($uid), "s");
$userLevel = $userRHDetails[0];

$withdrawalAmount = getWithdrawal($conn, " WHERE uid = ? AND status = 'APPROVED' ", array("uid"), array($uid), "s");

$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);
if ($getWho)
{
    $groupMember = count($getWho);
    for ($i=0; $i <count($getWho) ; $i++) {
    if ($getWho[$i]->getCurrentLevel() == $directDownlineLevel) {
      $addMember += 1;
    }
    $downlineUserOrders = getOrders($conn, "WHERE uid =?",array("uid"),array($getWho[$i]->getReferralId()), "s");
    if ($downlineUserOrders) {
      for ($j=0; $j <count($downlineUserOrders) ; $j++) {
        $downlineUserProductOrders = getProductOrders($conn, "WHERE order_id =?",array("order_id"),array($downlineUserOrders[$j]->getId()), "s");
        if ($downlineUserProductOrders) {
          for ($k=0; $k <count($downlineUserProductOrders) ; $k++) {
            $downlineUserProduct = getProduct($conn, "WHERE id =?",array("id"),array($downlineUserProductOrders[$k]->getProductId()), "s");
            if ($downlineUserProduct[0]->getProductType() == 'Package') {
              $groupSalesPackage += $downlineUserProductOrders[$k]->getQuantity();
            }
          }
        }
      }
    }
    }
}
else
{
    $groupMember = 0;
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/userDashboard.php" />
    <meta property="og:title" content="<?php echo _USERDASHBOARD_DASHBOARD ?> | Samofa 莎魔髪" />
    <title><?php echo _USERDASHBOARD_DASHBOARD ?> | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/userDashboard.php" />
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
	<?php include 'css.php'; ?>

</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _USERDASHBOARD_PERSONAL_MILESTONES ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
	<div class="width100 same-padding container-div1">
    	<!-- if achieved, add the class achieved1 besides eight-div-->

        <?php
        if(!$userMilestoneOne)
        {
        ?>
            <div class="eight-div">
                <div class="standard-div">
                    RM500
                </div>
                <div class="circle-line01"></div>
                <div class="circle01">1</div>

                <p class="eight-div-p">
                    <b><?php echo _MILESTONE_BONUS1 ?></b><br>
                    <?php echo _MILESTONE_BONUS1_DESC1 ?><br><?php echo _MILESTONE_BONUS1_DESC2 ?>
                </p>
            </div>
        <?php
        }
        else
        {
        ?>
            <div class="eight-div achieved1">
                <div class="standard-div">
                    RM500
                </div>
                <div class="circle-line01"></div>
                <div class="circle01">1</div>

                <p class="eight-div-p">
                    <b><?php echo _MILESTONE_BONUS1 ?></b><br>
                    <?php echo _MILESTONE_BONUS1_DESC1 ?><br><?php echo _MILESTONE_BONUS1_DESC2 ?>
                </p>
            </div>
        <?php
        }
        ?>

        <?php
        if(!$userMilestoneTwo)
        {
        ?>
            <div class="eight-div">
                <div class="standard-div">
                    RM1,000
                </div>
                <div class="circle-line01"></div>
                <div class="circle01">2</div>

                <p class="eight-div-p">
                    <b><?php echo _MILESTONE_BONUS2 ?></b><br>
                    <?php echo _MILESTONE_BONUS2_DESC ?>
                </p>
            </div>
        <?php
        }
        else
        {
        ?>
            <div class="eight-div achieved1">
                <div class="standard-div">
                    RM1,000
                </div>
                <div class="circle-line01"></div>
                <div class="circle01">2</div>

                <p class="eight-div-p">
                    <b><?php echo _MILESTONE_BONUS2 ?></b><br>
                    <?php echo _MILESTONE_BONUS2_DESC ?>
                </p>
            </div>
        <?php
        }
        ?>

        <?php
        if(!$userMilestoneThree)
        {
        ?>
            <div class="eight-div">
                <div class="standard-div">
                    RM2,000
                </div>
                <div class="circle-line01"></div>
                <div class="circle01">3</div>

                <p class="eight-div-p">
                    <b><?php echo _MILESTONE_BONUS3 ?></b><br>
                    <?php echo _MILESTONE_BONUS3_DESC ?>
                </p>
            </div>
        <?php
        }
        else
        {
        ?>
            <div class="eight-div achieved1">
                <div class="standard-div">
                    RM2,000
                </div>
                <div class="circle-line01"></div>
                <div class="circle01">3</div>

                <p class="eight-div-p">
                    <b><?php echo _MILESTONE_BONUS3 ?></b><br>
                    <?php echo _MILESTONE_BONUS3_DESC ?>
                </p>
            </div>
        <?php
        }
        ?>


        <?php
        if(!$userMilestoneFour)
        {
        ?>
            <div class="eight-div">
                <div class="standard-div">
                    RM3,500
                </div>
                <div class="circle-line01"></div>
                <div class="circle01">4</div>

                <p class="eight-div-p">
                    <b><?php echo _MILESTONE_BONUS4 ?></b><br>
                    <?php echo _MILESTONE_BONUS4_DESC ?>
                </p>
            </div>
        <?php
        }
        else
        {
        ?>
            <div class="eight-div achieved1">
                <div class="standard-div">
                    RM3,500
                </div>
                <div class="circle-line01"></div>
                <div class="circle01">4</div>

                <p class="eight-div-p">
                    <b><?php echo _MILESTONE_BONUS4 ?></b><br>
                    <?php echo _MILESTONE_BONUS4_DESC ?>
                </p>
            </div>
        <?php
        }
        ?>

        <?php
        if(!$userMilestoneFive)
        {
        ?>
            <div class="eight-div">
                <div class="standard-div">
                    RM5,000
                </div>
                <div class="circle-line01"></div>
                <div class="circle01">5</div>

                <p class="eight-div-p">
                    <b><?php echo _MILESTONE_BONUS5 ?></b><br>
                    <?php echo _MILESTONE_BONUS5_DESC ?>
                </p>
            </div>
        <?php
        }
        else
        {
        ?>
            <div class="eight-div achieved1">
                <div class="standard-div">
                    RM5,000
                </div>
                <div class="circle-line01"></div>
                <div class="circle01">5</div>

                <p class="eight-div-p">
                    <b><?php echo _MILESTONE_BONUS5 ?></b><br>
                    <?php echo _MILESTONE_BONUS5_DESC ?>
                </p>
            </div>
        <?php
        }
        ?>

        <?php
        if(!$userMilestoneSix)
        {
        ?>
            <div class="eight-div">
                <div class="standard-div">
                    RM6,000
                </div>
                <div class="circle-line01"></div>
                <div class="circle01">6</div>

                <p class="eight-div-p">
                    <b><?php echo _MILESTONE_BONUS6 ?></b><br>
                    <?php echo _MILESTONE_BONUS6_DESC ?>
                </p>
            </div>
        <?php
        }
        else
        {
        ?>
            <div class="eight-div achieved1">
                <div class="standard-div">
                    RM6,000
                </div>
                <div class="circle-line01"></div>
                <div class="circle01">6</div>

                <p class="eight-div-p">
                    <b><?php echo _MILESTONE_BONUS6 ?></b><br>
                    <?php echo _MILESTONE_BONUS6_DESC ?>
                </p>
            </div>
        <?php
        }
        ?>

        <?php
        if(!$userMilestoneSeven)
        {
        ?>
            <div class="eight-div">
                <div class="standard-div">
                    RM8,000
                </div>
                <div class="circle-line01"></div>
                <div class="circle01">7</div>

                <p class="eight-div-p">
                    <b><?php echo _MILESTONE_BONUS7 ?></b><br>
                    <?php echo _MILESTONE_BONUS7_DESC ?>
                </p>
            </div>
        <?php
        }
        else
        {
        ?>
            <div class="eight-div achieved1">
                <div class="standard-div">
                    RM8,000
                </div>
                <div class="circle-line01"></div>
                <div class="circle01">7</div>

                <p class="eight-div-p">
                    <b><?php echo _MILESTONE_BONUS7 ?></b><br>
                    <?php echo _MILESTONE_BONUS7_DESC ?>
                </p>
            </div>
        <?php
        }
        ?>

        <?php
        if(!$userMilestoneEight)
        {
        ?>
            <div class="eight-div">
                <div class="standard-div">
                    0.5%<?php echo _MILESTONE_GROUP_SALES ?>
                </div>
                <div class="circle-line01"></div>
                <div class="circle01">8</div>

                <p class="eight-div-p">
                    <b><?php echo _MILESTONE_BONUS8 ?></b><br>
                    <?php echo _MILESTONE_BONUS8_DESC ?>
                </p>
            </div>
        <?php
        }
        else
        {
        ?>
            <div class="eight-div achieved1">
                <div class="standard-div">
                    0.5%<?php echo _MILESTONE_GROUP_SALES ?>
                </div>
                <div class="circle-line01"></div>
                <div class="circle01">8</div>

                <p class="eight-div-p">
                    <b><?php echo _MILESTONE_BONUS8 ?></b><br>
                    <?php echo _MILESTONE_BONUS8_DESC ?>
                </p>
            </div>
        <?php
        }
        ?>

        <div class="clear"></div>
		<p class="note-p eight-div-p"><?php echo _MILESTONE_BONUS8_DESC2 ?></p>
        <p class="note-p eight-div-p opacity-hover pointer big-header-color"><a href="pdf/SAMOFA-2020-System-Marketing-Plan.pdf" class="big-header-color" target="_blank" download><?php echo _MILESTONE_SAMOFA_MARKETING ?><img src="img/download2.png" class="download-icon" alt="<?php echo _INDEX_DOWNLOAD ?>" title="<?php echo _INDEX_DOWNLOAD ?>"></a> &nbsp; | &nbsp; <a href="pdf/price-list.pdf" class="big-header-color" target="_blank" download><?php echo _MILESTONE_PRODUCT_PRICE ?><img src="img/download2.png" class="download-icon" alt="<?php echo _INDEX_DOWNLOAD ?>" title="<?php echo _INDEX_DOWNLOAD ?>"></a></p>

    </div>

    <div class="clear"></div>

    <div class="width100 overflow same-padding">

        <h1 class="dark-pink-text text-center big-header-color"><?php echo _USERDASHBOARD_RANK ?>: <?php echo $userCurrentRanking = $userData->getUserRank() ?></h1>
        <p class="small-hp dark-pink-text">

            <?php
            $conn = connDB();
            //--------------- last order and check maintainence (start) ---------------//
            $presidentBelowLastOrder = getOrders($conn, "WHERE uid= ? AND status = 'APPROVED' ORDER BY date_created DESC LIMIT 1",array("uid"),array($uid), "s");
            // $presidentBelowLO = $presidentBelowLastOrder[0]->getDateCreated();

            $presidentAboveLastOrder = getOrders($conn, "WHERE uid= ? AND status = 'APPROVED' AND product_type='Package' ORDER BY date_created DESC LIMIT 1",array("uid"),array($uid), "s");
            // $presidentAboveLO = $presidentAboveLastOrder[0]->getDateCreated();

            $M1 = '01';
            $M2 = '02';
            $M3 = '03';
            $M4 = '04';
            $M5 = '05';
            $M6 = '06';
            $M7 = '07';
            $M8 = '08';
            $M9 = '09';
            $M10 = '10';
            $M11 = '11';
            $M12 = '12';

            // echo $currentMonth = date('m');
            // echo "<br>";
            // echo $currentYear = date('Y');
            // echo "<br>";
            // echo $extraYear = $currentYear + 1;
            // echo "<br>";

            $currentMonth = date('m');
            $currentYear = date('Y');
            $extraYear = $currentYear + 1;

            $Q1Date = '31-3';
            $Q2Date = '30-6';
            $Q3Date = '30-9';
            $Q4Date = '31-12';

            $dash = '-';
            $Q1Merge = $Q1Date.$dash.$currentYear;
            $Q2Merge = $Q2Date.$dash.$currentYear;
            $Q3Merge = $Q3Date.$dash.$currentYear;
            $Q4Merge = $Q4Date.$dash.$currentYear;
            $Q1MergeExtra = $Q1Date.$dash.$extraYear;
            $Q4MergeExtra = $Q4Date.$dash.$extraYear;

            //--------------- last order and check maintainence (end) ---------------//

            if($userCurrentRanking == 'Member')
            {
                // echo " - ";
                // echo "<br>";
                if(!$presidentBelowLastOrder)
                {
                    // echo " No Order Yet ";
                ?>
                    <?php echo _USER_NO_ORDER_YET ?>
                <?php
                }
                else
                {
                    // echo " - ";
                    // echo "$presidentBelowLO";
                ?>
                    <?php echo _USER_LAST_SUC ?>: <?php echo $presidentBelowLO = $presidentBelowLastOrder[0]->getDateCreated(); ?>
                    <?php echo "<br>"; ?>
                    <?php 
                        // $presidentBelowLO = $presidentBelowLastOrder[0]->getDateCreated();
                        $orderYear = date('Y',strtotime($presidentBelowLO = $presidentBelowLastOrder[0]->getDateCreated()));
                        if($currentYear == $orderYear)
                        {
                        ?>
                            <?php echo _USER_NEXT_MAIN ?>: <?php echo $Q4MergeExtra; ?>
                        <?php
                        }
                        else
                        {}
                    ?>
                <?php
                }

            }
            elseif($userCurrentRanking == 'Agent')
            {
                // echo " Maintainence Within A Year ";
                // echo "<br>";
                // echo "$presidentBelowLO";
            ?>

                <?php
                if(!$presidentBelowLastOrder)
                {
                ?>
                    <?php echo _USER_NO_ORDER_YET ?>
                <?php
                }
                else
                {
                ?>

                    <?php echo _USER_LAST_SUC ?>: <?php echo $presidentBelowLO = $presidentBelowLastOrder[0]->getDateCreated(); ?>
                    <?php echo "<br>"; ?>
                    <?php 
                        $orderYear = date('Y',strtotime($presidentBelowLO = $presidentBelowLastOrder[0]->getDateCreated()));
                        if($currentYear == $orderYear)
                        {
                        ?>
                            <?php echo _USER_NEXT_MAIN ?>: <?php echo $Q4MergeExtra; ?>
                        <?php
                        }
                        else
                        {
                        ?>
                            <?php echo _USER_PLEASE_PERFORM ?>: <?php echo $Q4Merge; ?>
                        <?php
                        }
                    ?>

                <?php
                }
                ?>

            <?php
            }
            elseif($userCurrentRanking == 'President')
            {
                // echo " Maintainence Within A Quarter (Any Product) ";
                // echo "<br>";
                // echo "$presidentBelowLO";
            ?>
            
                <?php
                if(!$presidentBelowLastOrder)
                {
                ?>
                    <?php echo _USER_NO_ORDER_YET ?>
                <?php
                }
                else
                {
                ?>

                    <?php echo _USER_LAST_SUC ?>:  <?php echo $presidentBelowLO = $presidentBelowLastOrder[0]->getDateCreated(); ?>
                    <?php echo "<br>"; ?>
                    <?php 
                        $orderMonth = date('M',strtotime($presidentBelowLO = $presidentBelowLastOrder[0]->getDateCreated()));
                        // if($orderMonth)
                        if($currentMonth == $M1 || $currentMonth == $M2 || $currentMonth == $M3) 
                        {
                            if($orderMonth == $M1 || $orderMonth == $M2 || $orderMonth == $M3) 
                            {
                            ?>
                                <?php echo _USER_NEXT_MAIN ?>: <?php echo $Q2Merge; ?>
                            <?php
                            }
                            else
                            {
                            ?>
                                <?php echo _USER_PLEASE_PERFORM ?>: <?php echo $Q1Merge; ?>
                            <?php
                            }
                        }
                        elseif($currentMonth == $M4 || $currentMonth == $M5 || $currentMonth == $M6) 
                        {
                            if($orderMonth == $M4 || $orderMonth == $M5 || $orderMonth == $M6) 
                            {
                            ?>
                                <?php echo _USER_NEXT_MAIN ?>: <?php echo $Q3Merge; ?>
                            <?php
                            }
                            else
                            {
                            ?>
                                <?php echo _USER_PLEASE_PERFORM ?>: <?php echo $Q4Merge; ?>
                            <?php
                            }
                        }
                        elseif($currentMonth == $M7 || $currentMonth == $M8 || $currentMonth == $M9) 
                        {
                            if($orderMonth == $M7 || $orderMonth == $M8 || $orderMonth == $M9) 
                            {
                            ?>
                                <?php echo _USER_NEXT_MAIN ?>: <?php echo $Q4Merge; ?>
                            <?php
                            }
                            else
                            {
                            ?>
                                <?php echo _USER_PLEASE_PERFORM ?>: <?php echo $Q3Merge; ?>
                            <?php
                            }
                        }
                        elseif($currentMonth == $M10 || $currentMonth == $M11 || $currentMonth == $M12)
                        {
                            if($orderMonth == $M10 || $orderMonth == $M11 || $orderMonth == $M12) 
                            {
                            ?>
                                <?php echo _USER_NEXT_MAIN ?>: <?php echo $Q1MergeExtra; ?>
                            <?php
                            }
                            else
                            {
                            ?>
                                <?php echo _USER_PLEASE_PERFORM ?>: <?php echo $Q4Merge; ?>
                            <?php
                            }
                        }
                    ?>

                <?php
                }
                ?>

            <?php
            }
            else
            {
                // echo " Maintainence Within A Quarter (Package Only) ";
                // echo "<br>";
                // echo "$presidentAboveLO";
            ?>

                <?php
                if(!$presidentAboveLastOrder)
                {
                ?>
                    <?php echo _USER_NO_ORDER_YET ?>
                <?php
                }
                else
                {
                ?>

                    <?php echo _USER_LAST_SUC ?>:  <?php echo $presidentAboveLO = $presidentAboveLastOrder[0]->getDateCreated(); ?>
                    <?php echo "<br>"; ?>
                    <?php 
                        $orderMonth = date('M',strtotime($presidentAboveLO = $presidentAboveLastOrder[0]->getDateCreated()));
                        // if($orderMonth)
                        if($currentMonth == $M1 || $currentMonth == $M2 || $currentMonth == $M3) 
                        {
                            if($orderMonth == $M1 || $orderMonth == $M2 || $orderMonth == $M3) 
                            {
                            ?>
                                <?php echo _USER_NEXT_MAIN ?>: <?php echo $Q2Merge; ?>
                            <?php
                            }
                            else
                            {
                            ?>
                                <?php echo _USER_PLEASE_PERFORM ?>: <?php echo $Q1Merge; ?>
                            <?php
                            }
                        }
                        elseif($currentMonth == $M4 || $currentMonth == $M5 || $currentMonth == $M6) 
                        {
                            if($orderMonth == $M4 || $orderMonth == $M5 || $orderMonth == $M6) 
                            {
                            ?>
                                <?php echo _USER_NEXT_MAIN ?>: <?php echo $Q3Merge; ?>
                            <?php
                            }
                            else
                            {
                            ?>
                                <?php echo _USER_PLEASE_PERFORM ?>: <?php echo $Q4Merge; ?>
                            <?php
                            }
                        }
                        elseif($currentMonth == $M7 || $currentMonth == $M8 || $currentMonth == $M9) 
                        {
                            if($orderMonth == $M7 || $orderMonth == $M8 || $orderMonth == $M9) 
                            {
                            ?>
                                <?php echo _USER_NEXT_MAIN ?>: <?php echo $Q4Merge; ?>
                            <?php
                            }
                            else
                            {
                            ?>
                                <?php echo _USER_PLEASE_PERFORM ?>: <?php echo $Q3Merge; ?>
                            <?php
                            }
                        }
                        elseif($currentMonth == $M10 || $currentMonth == $M11 || $currentMonth == $M12)
                        {
                            if($orderMonth == $M10 || $orderMonth == $M11 || $orderMonth == $M12) 
                            {
                            ?>
                                <?php echo _USER_NEXT_MAIN ?>: <?php echo $Q1MergeExtra; ?>
                            <?php
                            }
                            else
                            {
                            ?>
                                <?php echo _USER_PLEASE_PERFORM ?>: <?php echo $Q4Merge; ?>
                            <?php
                            }
                        }
                    ?>

                <?php
                }
                ?>

            <?php
            }
            ?>

        </p>

    </div>

    <div class="clear"></div>

    <div class="width100 overflow same-padding dashboard-big-div">
    	<div class="three-div-width dashboard-box">
        	<img src="img/icon1.png" alt="<?php echo _USERDASHBOARD_PERFORMANCE ?>" title="<?php echo _USERDASHBOARD_PERFORMANCE ?>">
            <p class="box-p first-box-p second-first-p"><b><?php echo _USERDASHBOARD_PERFORMANCE ?></b></p>
            <p class="box-p">0 <?php echo _USERDASHBOARD_PACKAGES ?></p>
        </div>
    	<div class="three-div-width mid-three-div-width-margin dashboard-box second-third-box">
        	<img src="img/icon2.png" alt="<?php echo _USERDASHBOARD_GROUP_PERFORMANCE ?>" title="<?php echo _USERDASHBOARD_GROUP_PERFORMANCE ?>">
            <p class="box-p first-box-p second-first-p"><b><?php echo _USERDASHBOARD_GROUP_PERFORMANCE ?></b></p>
            <p class="box-p">0 <?php echo _USERDASHBOARD_PACKAGES ?></p>
        </div>
        <a href="myTeam.php">
    	<div class="three-div-width dashboard-box opacity-hover pointer">
        	<img src="img/icon3.png" alt="<?php echo _USERDASHBOARD_MY_GROUP ?>" title="<?php echo _USERDASHBOARD_MY_GROUP ?>">
            <p class="box-p first-box-p third-first-p"><b><?php echo _USERDASHBOARD_MY_GROUP ?></b></p>
            <!-- <p class="box-p">50</p> -->
            <p class="box-p"><?php echo $groupMember ;?></p>
        </div>
        </a>
		<div class="tempo-three-clear2"></div>
    	<div class="three-div-width dashboard-box second-third-box">
        	<img src="img/icon4.png" alt="<?php echo _USERDASHBOARD_TOTAL_COMMISSION ?>" title="<?php echo _USERDASHBOARD_TOTAL_COMMISSION ?>">
            <p class="box-p third-first-p"><b><?php echo _USERDASHBOARD_TOTAL_COMMISSION ?></b></p>
            <p class="box-p"><?php echo $userData->getBonus() ?></p>
        </div>
    </div>

    <div class="clear"></div>

</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>
