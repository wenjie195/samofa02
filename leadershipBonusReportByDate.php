<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/OtherBonus.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

if (isset($_POST['startDate'])) {
$dateStart = $_POST['startDate'];
}else {
$dateStart = "";
}

if (isset($_POST['endDate'])) {
$dateEnd = $_POST['endDate'];
}else {
$dateEnd = "";
}
// $bookingDate = date('Y-m-d',strtotime($_POST['bookingDate']));
if ($dateStart) {
    $dateStartN = str_replace("/","-",$dateStart);
    $bookingStartDate = date('Y-m-d',strtotime($dateStartN));
}else{
    $bookingStartDate = '1970-01-01';
}
if ($dateEnd) {
    $dateEndN = str_replace("/","-",$dateEnd);
    $bookingEndDate = date('Y-m-d',strtotime($dateEndN."+ 1 days"));
}else{
    $bookingEndDate = date('Y-m-d');
}

$conn = connDB();

$userDetails = getUser($conn);
// $conn->close();
$no = 0;

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
            <table class="table-css">
                <thead>
                    <tr>
                        <th><?php echo _PRODUCTDETAILS_NO ?></th>
                        <th><?php echo _JS_USERNAME ?></th>
                        <th><?php echo _ADMIN_AMOUNT ?></th>
                        <th>Bonus Type</th>
                        <th><?php echo _ADMINHEADER_REMARKS ?></th>
                        <th><?php echo _USERDASHBOARD_STATUS ?></th>
                        <th><?php echo _PURCHASE_DATE ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if ($bookingStartDate && $bookingEndDate) {
                            $bonusDetails = getOtherBonus($conn, "WHERE date_created >=? AND date_created <=?", array("date_created","date_created"),array($bookingStartDate,$bookingEndDate), "ss");
                        }else{
                            $bonusDetails = getOtherBonus($conn);
                        }
                        if($bonusDetails)
                        {
                            for($cnt = 0;$cnt < count($bonusDetails) ;$cnt++)
                            {
                              $no += 1;
                              $referralDetails = getUser($conn, "WHERE uid =?",array("uid"),array($bonusDetails[$cnt]->getUid()), "s");
                            ?>
                                <tr>
                                    <td><?php echo $no?></td>
                                    <td><?php echo $bonusDetails[$cnt]->getReceiveUsername();?></td>
                                    <td><?php echo $bonusDetails[$cnt]->getBonus();?></td>
                                    <td><?php echo $bonusDetails[$cnt]->getBonusType();?></td>
                                    <?php
                                    if ($bonusDetails[$cnt]->getNewUser() == 'Buy Product') {
                                      ?><td><?php echo $bonusDetails[$cnt]->getNewUser()." (#".$bonusDetails[$cnt]->getInvoiceNo().")";?></td><?php
                                    }else{
                                      ?><td><?php echo $bonusDetails[$cnt]->getNewUser();?></td><?php
                                    }
                                     ?>
                                    <td><?php echo $bonusDetails[$cnt]->getStatus();?></td>
                                    <td><?php echo date('d/m/Y',strtotime($bonusDetails[$cnt]->getDateCreated()));?></td>
                                </tr>
                            <?php
                            }
                        }else{
                            ?>
                            <td colspan="7" class="no-data-td">No Record Found.</td>
                            <?php
                        }
                    ?>
                </tbody>
            </table>
