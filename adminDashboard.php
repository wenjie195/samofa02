<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';
require_once dirname(__FILE__) . '/classes/UserPending.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Bonus.php';
require_once dirname(__FILE__) . '/classes/OtherBonus.php';
require_once dirname(__FILE__) . '/classes/SignUpProduct.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();
$totalCommission = 0;
$totalRevenue = 0;
$totalWithdrawalPrice = 0;

$allUser = getUser($conn, " WHERE user_type = ? ", array("user_type"), array(1), "s");
$allWithdrawal = getWithdrawal($conn, " WHERE status = ? ", array("status"), array("PENDING"), "s");
$withdrawalDetails = getWithdrawal($conn,"WHERE status='APPROVED'");
$orderDetails = getOrders($conn, "WHERE pickup_type = '' AND status = 'APPROVED'");

$orderCheckers = getOrders($conn, " WHERE status = 'PENDING' AND payment_references is not null ORDER BY date_created DESC");
// $soundfile = "notification_sound.mp3";

$orderRevenueDetails = getOrders($conn, "WHERE status='APPROVED'");
// $bonusDetails = getBonusDetails($conn, "WHERE status='APPROVED'");
// $otherBonusDetails = getOtherBonus($conn, "WHERE status='APPROVED'");
$signupDetails = getSignUpProduct($conn);
$pendingUserDetails = getPendingUser($conn, "WHERE user_status = 'PENDING'");
$productDetails = getProduct($conn);
if ($productDetails) {
    $totalProduct = count($productDetails);
}else{
    $totalProduct = 0;
}

// if ($bonusDetails) {
//     for ($i=0; $i <count($bonusDetails) ; $i++) { 
//         $totalCommission += $bonusDetails[$i]->getAmount();
//     }
// }
// if ($otherBonusDetails) {
//     for ($i=0; $i <count($otherBonusDetails) ; $i++) { 
//         $totalCommission += $otherBonusDetails[$i]->getBonus();
//     }
// }
if ($signupDetails) {
    for ($i=0; $i <count($signupDetails) ; $i++) { 
        $totalRevenue += $signupDetails[$i]->getPrice();
    }
}
if ($orderRevenueDetails) {
    for ($i=0; $i <count($orderRevenueDetails) ; $i++) { 
        $totalRevenue += $orderRevenueDetails[$i]->getSubTotal();
    }
}
if ($withdrawalDetails) {
    for ($i=0; $i <count($withdrawalDetails) ; $i++) { 
        $totalWithdrawalPrice += $withdrawalDetails[$i]->getFinalAmount();
    }
}

if ($pendingUserDetails) {
    $pendingApproval = count($pendingUserDetails);
}else {
    $pendingApproval = 0;
}
if ($orderDetails) {
    $orderTotal = count($orderDetails);
}else{
    $orderTotal = 0;
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/adminDashboard.php" />
    <meta property="og:title" content="Admin Dashboard | Samofa 莎魔髪" />
    <title>Admin Dashboard | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/adminDashboard.php" />
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
	<?php include 'css.php'; ?>
    
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMIN_DASHBOARD ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    
    <div id="divOrderChecker">
    </div>

    <div class="width100 same-padding">
		<a href="adminViewOrder.php">
        <div class="four-div dashboard-box">
            <img src="img/icons1.png" alt="<?php echo _ADMIN_CURRENT_ORDER ?>" title="<?php echo _ADMIN_CURRENT_ORDER ?>">
            <p class="box-p first-p1"><b><?php echo _ADMIN_CURRENT_ORDER ?></b></p>
            <p class="box-p"><?php echo $orderTotal ?></p>
        </div>      
        </a>
        <a href="adminViewWithdrawal.php">
            <div class="four-div dashboard-box second-four-div opacity-hover pointer four-mid">
                <img src="img/icon6.png" alt="<?php echo _ADMIN_WITHDRAWAL_REQUEST ?>" title="<?php echo _ADMIN_WITHDRAWAL_REQUEST ?>">
                <p class="box-p first-p1"><b><?php echo _ADMIN_WITHDRAWAL_REQUEST ?></b></p>
                <?php
                if($allWithdrawal)
                {   
                    $totalWithdrawal = count($allWithdrawal);
                }
                else
                {   $totalWithdrawal = 0;   }
                ?>
                <!-- <p class="box-p">5</p> -->
                <p class="box-p"><?php echo $totalWithdrawal;?></p>
            </div>   
        </a>     
		<a href="adminPendingUser.php">
            <div class="four-div dashboard-box third-four-div">
            <img src="img/icons3.png" alt="<?php echo _ADMIN_NEW_REGISTRATION ?>" title="<?php echo _ADMIN_NEW_REGISTRATION ?>">
            <p class="box-p first-p1"><b><?php echo _ADMIN_NEW_REGISTRATION ?></b></p>
            <p class="box-p"><?php echo $pendingApproval ?></p>
            </div>      
        </a>     
        <a href="adminAllMember.php">
             <div class="four-div dashboard-box forth-div opacity-hover pointer">
                <img src="img/icon3.png" alt="<?php echo _ADMIN_TOTAL_MEMBERS ?>" title="<?php echo _ADMIN_TOTAL_MEMBERS ?>">
                <p class="box-p first-p1"><b><?php echo _ADMIN_TOTAL_MEMBERS ?></b></p>
                <?php
                if($allUser)
                {   
                    $totalUser = count($allUser);
                }
                else
                {   $totalUser = 0;   }
                ?>
                <!-- <p class="box-p">500</p> -->
                <p class="box-p"><?php echo $totalUser;?></p>
            </div>        
        </a> 
        <a href="adminViewCurrentProduct.php">   
		<div class="four-div dashboard-box four-mid opacity-hover pointer">
        	<img src="img/icons5.png" alt="<?php echo _ADMIN_TOTAL_PACKAGE ?>" title="<?php echo _ADMIN_TOTAL_PACKAGE ?>">
            <p class="box-p second-p2"><b><?php echo _ADMIN_TOTAL_PACKAGE ?></b></p>
            <p class="box-p"><?php echo $totalProduct ?></p>
        </div>
        </a>
            <div class="four-div dashboard-box second-four-div">
                <img src="img/icon5.png" alt="<?php echo _ADMIN_TOTAL_REVENUE ?>" title="<?php echo _ADMIN_TOTAL_REVENUE ?>">
                <p class="box-p second-p2"><b><?php echo _ADMIN_TOTAL_REVENUE ?></b></p>
                <p class="box-p">RM <?php echo $totalRevenue ?></p>
            </div>         
		<div class="four-div dashboard-box third-four-div">
        	<img src="img/icons2.png" alt="<?php echo _ADMIN_TOTAL_COMMISSION ?>" title="<?php echo _ADMIN_TOTAL_COMMISSION ?>">
            <p class="box-p second-p2 third-p3"><b><?php echo _ADMIN_TOTAL_COMMISSION ?></b></p>
            <p class="box-p">RM <?php //echo $totalCommission ?></p>
        </div>    
        <a href="adminWithdrawalReport.php">       
		<div class="four-div dashboard-box four-mid forth-div opacity-hover pointer">
        	<img src="img/icons4.png" alt="<?php echo _ADMIN_TOTAL_COMMISSION_WITHDRAWN ?>" title="<?php echo _ADMIN_TOTAL_COMMISSION_WITHDRAWN ?>">
            <p class="box-p second-p2 third-p3"><b><?php echo _ADMIN_TOTAL_COMMISSION_WITHDRAWN ?></b></p>
            <p class="box-p">RM <?php echo $totalWithdrawalPrice ?></p>
        </div>
        </a>        
    </div>
</div>
<div class="clear"></div>
<div class="width100 same-padding">
        <p class="note-p eight-div-p opacity-hover pointer big-header-color"><a href="pdf/SAMOFA-2020-System-Marketing-Plan.pdf" class="big-header-color" target="_blank" download><?php echo _MILESTONE_SAMOFA_MARKETING ?><img src="img/download2.png" class="download-icon" alt="<?php echo _INDEX_DOWNLOAD ?>" title="<?php echo _INDEX_DOWNLOAD ?>"></a></p>
        <p class="note-p eight-div-p opacity-hover pointer big-header-color"><a href="pdf/price-list.pdf" class="big-header-color" target="_blank" download><?php echo _MILESTONE_PRODUCT_PRICE ?><img src="img/download2.png" class="download-icon" alt="<?php echo _INDEX_DOWNLOAD ?>" title="<?php echo _INDEX_DOWNLOAD ?>"></a></p> 
</div>
<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>

<script type="text/javascript">
$(document).ready(function()
{
    $("#divOrderChecker").load("adminOrderChecker.php");
setInterval(function()
{
    $("#divOrderChecker").load("adminOrderChecker.php");
}, 10000);
});
</script>


</body>
</html>