<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = null;

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/resetPassword.php" />
    <meta property="og:title" content="Reset Password | Samofa 莎魔髪" />
    <title>Reset Password | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/resetPassword.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="width100 same-padding min-height-with-flower menu-distance75">

<h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _MAINJS_INDEX_RESET_PASSWORD ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

    <form action="utilities/resetPasswordFunction.php" method="POST">

        <input type="hidden" name="checkThat" value="<?php if(isset($_GET['uid'])){echo $_GET['uid'] ;}?>">

        <div class="dual-input">
            <p class="input-top-text"><?php echo _MAINJS_INDEX_VERIFY_CODE ?></p>
            <input class="clean de-input" type="text" placeholder="<?php echo _MAINJS_INDEX_VERIFY_CODE ?>" id="verify_code" name="verify_code" required>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-text"><?php echo _JS_NEW_PASSWORD ?></p>
            <input class="clean de-input" type="text" placeholder="<?php echo _JS_NEW_PASSWORD ?>" id="new_password" name="new_password" required>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _JS_RETYPE_NEW_PASSWORD ?></p>
            <input class="clean de-input" type="text" placeholder="<?php echo _JS_RETYPE_NEW_PASSWORD ?>" id="retype_new_password" name="retype_new_password" required>
        </div>

        <div class="clear"></div>

        <div class="width100 text-center top-bottom-distance">
            <button class="clean button-width transparent-button dark-pink-button" name="submit"><?php echo _JS_SUBMIT ?></button>
        </div>

    </form>

</div>

	<div class="clear"></div>
    <img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">
    <div class="clear"></div>

<?php include 'js.php'; ?>

<script type="text/javascript">
    var x = location.search.replace('?', '').split('=');
    if (x[1] == 1) {
        alert("Success");
    }else if(x[1] == 2){
        alert("Error");
    }
</script>

</body>
</html>