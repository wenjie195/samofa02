<?php
class Orders {
    /* Member variables */
    var $id,$uid,$username,$recipientFirstname,$recipientLastname,$contact,$email,$address_line1,$address_line2,$address_line3,$city,$zipcode,$state,$country,$productType,$totalProduct,$productExcludeRebate,$rebate,$rebateDone,$productId,$quantity,$rebateTotal,$subtotal,
            $total,$paymentMethod,$paymentAmount,$paymentReferences,$pickupType,$pickupName,$trackingNo,$date,$time,$status,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }
  
    /**
     * @return mixed
     */
    public function getRecipientFirstname()
    {
        return $this->recipientFirstname;
    }

    /**
     * @param mixed $recipientFirstname
     */
    public function setRecipientFirstname($recipientFirstname)
    {
        $this->recipientFirstname = $recipientFirstname;
    }

    /**
     * @return mixed
     */
    public function getRecipientLastname()
    {
        return $this->recipientLastname;
    }

    /**
     * @param mixed $recipientLastname
     */
    public function setRecipientLastname($recipientLastname)
    {
        $this->recipientLastname = $recipientLastname;
    }

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAddressLine1()
    {
        return $this->address_line1;
    }

    /**
     * @param mixed $address_line1
     */
    public function setAddressLine1($address_line1)
    {
        $this->address_line1 = $address_line1;
    }

    /**
     * @return mixed
     */
    public function getAddressLine2()
    {
        return $this->address_line2;
    }

    /**
     * @param mixed $address_line2
     */
    public function setAddressLine2($address_line2)
    {
        $this->address_line2 = $address_line2;
    }

    /**
     * @return mixed
     */
    public function getAddressLine3()
    {
        return $this->address_line3;
    }

    /**
     * @param mixed $address_line3
     */
    public function setAddressLine3($address_line3)
    {
        $this->address_line3 = $address_line3;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param mixed $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * @param mixed $productType
     */
    public function setProductType($productType)
    {
        $this->productType = $productType;
    }

    /**
     * @return mixed
     */
    public function getTotalProduct()
    {
        return $this->total_product;
    }

    /**
     * @param mixed $country
     */
    public function setTotalProduct($totalProduct)
    {
        $this->total_product = $totalProduct;
    }

    /**
     * @return mixed
     */
    public function getProductExcludeRebate()
    {
        return $this->product_exclude_rebate;
    }

    /**
     * @param mixed $country
     */
    public function setProductExcludeRebate($productExcludeRebate)
    {
        $this->product_exclude_rebate = $productExcludeRebate;
    }

    /**
     * @return mixed
     */
    public function getRebate()
    {
        return $this->rebate;
    }

    /**
     * @param mixed $country
     */
    public function setRebate($rebate)
    {
        $this->rebate = $rebate;
    }

    /**
     * @return mixed
     */
    public function getRebateDone()
    {
        return $this->rebate_done;
    }

    /**
     * @param mixed $country
     */
    public function setRebateDone($rebateDone)
    {
        $this->rebate_done = $rebateDone;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

     /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getRebateTotal()
    {
        return $this->rebate_total;
    }

    /**
     * @param mixed $country
     */
    public function setRebateTotal($rebateTotal)
    {
        $this->rebate_total = $rebateTotal;
    }

    /**
     * @return mixed
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * @param mixed $subtotal
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param mixed $paymentMethod
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return mixed
     */
    public function getPaymentAmount()
    {
        return $this->paymentAmount;
    }

    /**
     * @param mixed $paymentAmount
     */
    public function setPaymentAmount($paymentAmount)
    {
        $this->paymentAmount = $paymentAmount;
    }

    /**
     * @return mixed
     */
    public function getPaymentReferences()
    {
        return $this->paymentReferences;
    }

    /**
     * @param mixed $paymentReferences
     */
    public function setPaymentReferences($paymentReferences)
    {
        $this->paymentReferences = $paymentReferences;
    }

    /**
     * @return mixed
     */
    public function getPickupType()
    {
        return $this->pickupType;
    }

    /**
     * @param mixed $paymentReferences
     */
    public function setPickupType($pickupType)
    {
        $this->pickupType = $pickupType;
    }

    /**
     * @return mixed
     */
    public function getPickupName()
    {
        return $this->pickupName;
    }

    /**
     * @param mixed $paymentReferences
     */
    public function setPickupName($pickupName)
    {
        $this->pickupName = $pickupName;
    }

    /**
     * @return mixed
     */
    public function getTrackingNo()
    {
        return $this->trackingNo;
    }

    /**
     * @param mixed $paymentReferences
     */
    public function setTrackingNo($trackingNo)
    {
        $this->trackingNo = $trackingNo;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getOrders($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","username","recipient_firstname","recipient_lastname","contact","email","address_line_1","address_line_2","address_line_3","city",
                            "zipcode","state","country","product_type","total_product","product_excl_rebate","rebate","rebate_done","product_id","quantity","rebate_total","subtotal","total","payment_method","payment_amount","payment_references","pickup_type","pickup_name","tracking_no","date","time","status","date_created",
                            "date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"orders");
    if($whereClause){
        $sql .= $whereClause;
    }

    // echo $whereClause;

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$username,$recipientFirstname,$recipientLastname,$contact,$email,$address_line1,$address_line2,$address_line3,$city,$zipcode,$state,
                            $country,$productType,$totalProduct,$productExcludeRebate,$rebate,$rebateDone,$productId,$quantity,$rebateTotal,$subtotal,$total,$paymentMethod,$paymentAmount,$paymentReferences,$pickupType,$pickupName,$trackingNo,$date,$time,$status,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Orders();
            $class->setId($id);
            $class->setUid($uid);
            $class->setUsername($username);

            $class->setRecipientFirstname($recipientFirstname);
            $class->setRecipientLastname($recipientLastname);

            $class->setContact($contact);
            $class->setEmail($email);
            $class->setAddressLine1($address_line1);
            $class->setAddressLine2($address_line2);
            $class->setAddressLine3($address_line3);
            $class->setCity($city);
            $class->setZipcode($zipcode);
            $class->setState($state);
            $class->setCountry($country);
            $class->setProductType($productType);
            $class->setTotalProduct($totalProduct);
            $class->setProductExcludeRebate($productExcludeRebate);
            $class->setRebate($rebate);
            $class->setRebateDone($rebateDone);
            $class->setProductId($productId);
            $class->setQuantity($quantity);
            $class->setRebateTotal($rebateTotal);
            $class->setSubtotal($subtotal);
            $class->setTotal($total);

            $class->setPaymentMethod($paymentMethod);
            $class->setPaymentAmount($paymentAmount);
            $class->setPaymentReferences($paymentReferences);
            $class->setPickupType($pickupType);
            $class->setPickupName($pickupName);
            $class->setTrackingNo($trackingNo);
            $class->setDate($date);
            $class->setTime($time);
            $class->setStatus($status);

            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}