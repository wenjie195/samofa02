<?php
class ProductImage{
    /* Member variables */
    var $id,$name,$price,$rankOri,$rankA,$rankB,$rankC,$stock,$buyStock,$totalPruchaseValue,$display,$type,$description,$images,$productType,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getRankOri()
    {
        return $this->rankOri;
    }

    /**
     * @param mixed $rankOri
     */
    public function setRankOri($rankOri)
    {
        $this->rankOri = $rankOri;
    }

    /**
     * @return mixed
     */
    public function getRankA()
    {
        return $this->rankA;
    }

    /**
     * @param mixed $rankA
     */
    public function setRankA($rankA)
    {
        $this->rankA = $rankA;
    }

    /**
     * @return mixed
     */
    public function getRankB()
    {
        return $this->rankB;
    }

    /**
     * @param mixed $rankB
     */
    public function setRankB($rankB)
    {
        $this->rankB = $rankB;
    }

    /**
     * @return mixed
     */
    public function getRankC()
    {
        return $this->rankC;
    }

    /**
     * @param mixed $rankC
     */
    public function setRankC($rankC)
    {
        $this->rankC = $rankC;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param mixed $id
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return mixed
     */
    public function getBuyStock()
    {
        return $this->buyStock;
    }

    /**
     * @param mixed $buyStock
     */
    public function setBuyStock($buyStock)
    {
        $this->buyStock = $buyStock;
    }

    /**
     * @return mixed
     */
    public function getTotalPurchaseValue()
    {
        return $this->totalPruchaseValue;
    }

    /**
     * @param mixed $totalPruchaseValue
     */
    public function setTotalPurchaseValue($totalPruchaseValue)
    {
        $this->totalPruchaseValue = $totalPruchaseValue;
    }

    /**
     * @return mixed
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * @param mixed $display
     */
    public function setDisplay($display)
    {
        $this->display = $display;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param mixed $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return mixed
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * @param mixed $productType
     */
    public function setProductType($productType)
    {
        $this->productType = $productType;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getProductImage($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","name","price","rankOri","rankA","rankB","rankC","stock","buy_stock","total_price","display","type","description","images","product_type",
                                "date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"product");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$name,$price,$rankOri,$rankA,$rankB,$rankC,$stock,$buyStock,$totalPruchaseValue,$display,$type,$description,$images,$productType,
                                $dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new ProductImage();

            $class->setId($id);
            $class->setName($name);
            $class->setPrice($price);
            $class->setRankOri($rankOri);
            $class->setRankA($rankA);
            $class->setRankB($rankB);
            $class->setRankC($rankC);
            $class->setStock($stock);
            $class->setBuyStock($buyStock);
            $class->setTotalPurchaseValue($totalPruchaseValue);
            $class->setDisplay($display);
            $class->setType($type);
            $class->setDescription($description);
            $class->setImages($images);

            $class->setProductType($productType);

            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}