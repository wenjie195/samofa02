<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Announcement.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/adminUpdateAnnoucement.php" />
    <meta property="og:title" content="Admin Update Announcement | Samofa 莎魔髪" />
    <title>Admin Update Announcement | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/adminUpdateAnnoucement.php" />
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
	<?php include 'css.php'; ?>
    
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMIN_UPDATE_ANNOUNCEMENT ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    <div class="width100 same-padding">

    <form method="POST" action="utilities/adminUpdateAnnoucementFunction.php">
        <?php
            if(isset($_POST['announcement_uid']))
            {
                $conn = connDB();
                $annouceDetails = getAnnouncement($conn,"WHERE uid = ? ", array("uid") ,array($_POST['announcement_uid']),"s");
            ?>

                <div class="width100">
                    <p class="input-top-text"><?php echo _ADMIN_TITLE ?></p>
                    <input class="clean de-input" type="text" placeholder="<?php echo _ADMIN_TITLE ?>" value="<?php echo $annouceDetails[0]->getTitle();?>" id="update_title" name="update_title" required>
                </div>

                <div class="clear"></div>

                <div class="width100">
                    <p class="input-top-text"><?php echo _ADMIN_CONTENT ?></p>
                    <textarea class="clean de-input de-textarea" type="text" placeholder="<?php echo _ADMIN_CONTENT ?>" value="<?php echo $annouceDetails[0]->getContent();?>" id="update_content" name="update_content" required></textarea>
                </div>

                <div class="clear"></div>

                <input class="clean de-input" type="hidden" value="<?php echo $annouceDetails[0]->getUid();?>" id="announcement_uid" name="announcement_uid" readonly>

                <div class="width100 text-center top-bottom-distance">
                    <button class="clean button-width transparent-button dark-pink-button" name="submit"><?php echo _JS_SUBMIT ?></button>
                </div>

            <?php
        }
        ?>
    </form>

    </div>
</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>
