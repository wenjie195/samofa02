<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/phoneFormat.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$productsOrders =  getOrders($conn," WHERE uid = ? AND status='APPROVED'",array("uid"),array($_SESSION['uid']),"s");

$adminProductView =  getOrders($conn, "WHERE status='APPROVED'");

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/salesReport.php" />
    
    <meta property="og:title" content="<?php echo _ADMINHEADER_SALES_REPORT ?> | Samofa 莎魔髪" />
    <title><?php echo _ADMINHEADER_SALES_REPORT ?> | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/salesReport.php" />
    <?php include 'css.php'; ?> 
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMINHEADER_SALES_REPORT ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

    <div class="overflow-scroll-div same-padding">
        
        <table class="table-css">
            <thead>
                <tr>
                <?php
                for($i= 0;$i < count($userRows) ;$i++)
                {
                    if($userRows[$i]->getUserType()==0)
                    {
                    ?>
                        <th><?php echo _PRODUCTDETAILS_NO ?></th>
                        <th><?php echo _ADMIN_INVOICE_NO ?></th>
                        <th><?php echo _ADMIN_PACKAGE_PRODUCT ?></th>
                        <th><?php echo _JS_USERNAME ?></th>
                        <th><?php echo _PURCHASE_DATE ?></th>
                        <!-- <th><?php //echo _INDEX_CONTACT_NO ?></th> -->
                        <th><?php echo _SHIPPING_PAYMENT_METHOD ?></th>
                        <th><?php echo _SHIPPING_AMOUNT ?> (RM)</th>
                        <th><?php echo _PURCHASE_DETAILS ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                        for($cnt = 0;$cnt < count($adminProductView) ;$cnt++)
                        {
                            $userPurchaseDetails = getUser($conn, "WHERE uid =?",array("uid"),array($adminProductView[$cnt]->getUid()), "s");
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo "#".$adminProductView[$cnt]->getId()?></td>
                                <td><?php 
                                $productId = $adminProductView[$cnt]->getProductId();
                                $productIdExp = explode(",",$productId);
                                for ($i=0; $i <count($productIdExp) ; $i++) { 
                                    $productDetails = getProduct($conn, "WHERE id =?",array("id"),array($productIdExp[$i]), "s");
                                    echo $productDetails[0]->getName()."<br>";
                                }
                                 ?>    
                                </td>
                                <td><?php echo $userPurchaseDetails[0]->getUsername();?></td>
                                <td>
                                    <?php echo $date = date("d/m/Y",strtotime($adminProductView[$cnt]->getDateCreated()));?>
                                </td>
                                <!-- <td><?php //echo phone_number_format($adminProductView[$cnt]->getContact());?></td> -->
                                <td><?php echo $adminProductView[$cnt]->getPaymentMethod() ?></td>
                                <td><?php echo $adminProductView[$cnt]->getTotal();?></td>
                                <td>
                                    <form method="POST" action="#" class="hover1">
                                        <button class="clean hover1 img-btn transparent-button pink-hover-text" type="submit" name="order_id" value="<?php echo $adminProductView[$cnt]->getId();?>">
                                            <img src="img/details1.png" class="edit-icon1 hover1a" alt="<?php echo _ADMIN_DETAILS ?>" title="<?php echo _ADMIN_DETAILS ?>">
                                            <img src="img/details2.png" class="edit-icon1 hover1b" alt="<?php echo _ADMIN_DETAILS ?>" title="<?php echo _ADMIN_DETAILS ?>">
                                        </button>
                                    </form>
                                </td> 
                            </tr>
                        <?php
                        }
                        $conn->close();
                        ?>
                </tbody>
                    <?php
                    }
                    else if($userRows[$i]->getUserType()==1)
                    {
                        if($productsOrders)
                        {
                        ?>
                            <thead>
                                <tr>
                                    <th><?php echo _PRODUCTDETAILS_NO ?></th>
                                    <th><?php echo _ADMIN_INVOICE_NO ?></th>
                                    <th><?php echo _ADMIN_PACKAGE_PRODUCT ?></th>
                                    <th><?php echo _JS_USERNAME ?></th>
                                    <th><?php echo _PURCHASE_DATE ?></th>
                                    <!-- <th><?php //echo _INDEX_CONTACT_NO ?></th> -->
                                    <th><?php echo _SHIPPING_PAYMENT_METHOD ?></th>
                                    <th><?php echo _SHIPPING_AMOUNT ?> (RM)</th>
                                    <th><?php echo _PURCHASE_DETAILS ?></th>
                                </tr>
                            </thead>
                            <?php
                            for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                            {
                            ?>  
                            <tbody>  
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo "#".$productsOrders[$cnt]->getId() ?></td>
                                    <td><?php 
                                    $productId = $productsOrders[$cnt]->getProductId();
                                    $productIdExp = explode(",",$productId);
                                    for ($i=0; $i <count($productIdExp) ; $i++) { 
                                        $productDetails = getProduct($conn, "WHERE id =?",array("id"),array($productIdExp[$i]), "s");
                                        echo $productDetails[0]->getName()."<br>";
                                    }
                                     ?>    
                                    </td>
                                    <td><?php echo $userRows[0]->getUsername();?></td>
                                    <td>
                                        <?php echo $date = date("d/m/Y",strtotime($productsOrders[$cnt]->getDateCreated()));?>
                                    </td>
                                    <!-- <td><?php //echo phone_number_format($productsOrders[$cnt]->getContact());?></td> -->
                                    <td><?php echo $productsOrders[$cnt]->getPaymentMethod() ?></td>
                                    <td><?php echo $productsOrders[$cnt]->getTotal();?></td>
                                    <td>
                                        <form method="POST" action="invoice.php" class="hover1">
                                            <button class="clean hover1 img-btn transparent-button pink-hover-text" type="submit" name="order_id" value="<?php echo $productsOrders[$cnt]->getId();?>">
                                            <img src="img/details1.png" class="edit-icon1 hover1a" alt="<?php echo _ADMIN_DETAILS ?>" title="<?php echo _ADMIN_DETAILS ?>">
                                            <img src="img/details2.png" class="edit-icon1 hover1b" alt="<?php echo _ADMIN_DETAILS ?>" title="<?php echo _ADMIN_DETAILS ?>">
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            </tbody>
                            <?php
                            }
                        }
                        else
                        { ?>
                            </br>
                            <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMIN_NO_PURCHASE_ORDER ?></h1>
                        <?php    
                        }
                    }
                }
                ?>   
            </tbody>
        </table>

    </div>
</div>    

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>