<?php
if(isset($_SESSION['uid']))
{
?>

    <?php
    if($_SESSION['usertype'] == 0)
    {
    ?>

        <header id="header" class="header header--fixed same-padding header1 menu-color admin-header" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php"><img src="img/samofa.png" class="logo-img" alt="Logo" title="Logo"></a>
                    </div>



                    <div class="right-menu-div float-right logged-in-menu">

                        <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">

                            <?php echo _ADMINHEADER_NETWORK ?>
                                        <img src="img/dropdown.png" class="dropdown-img hover1a" >
                                        <img src="img/dropdown2.png" class="dropdown-img hover1b" >


                            <div class="dropdown-content yellow-dropdown-content">
								<p class="dropdown-p"><a href="adminDashboard.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMIN_DASHBOARD ?></a></p>
								<p class="dropdown-p"><a href="adminViewAnnouncement.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMIN_ANNOUNCEMENT ?></a></p>
                <p class="dropdown-p"><a href="adminViewWithdrawal.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMIN_WITHDRAWAL_REQUEST ?></a></p>
                <p class="dropdown-p"><a href="adminViewMilesStones.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMIN_MILESTONES_BONUS ?></a></p>
                            </div>
                        </div>
                        <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">

                            <?php echo _ADMINHEADER_MEMBERS ?>
                                        <img src="img/dropdown.png" class="dropdown-img hover1a" >
                                        <img src="img/dropdown2.png" class="dropdown-img hover1b" >

                            <div class="dropdown-content yellow-dropdown-content">

                                <p class="dropdown-p"><a href="adminAllMember.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_ALLMEMBER ?></a></p>
                                <p class="dropdown-p"><a href="adminExpiredUser.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_ALL_EXPIRED_MEMBER ?></a></p>
                            </div>
                        </div>
                        <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">

                            <?php echo _JS_SIGNUP ?>
                                        <img src="img/dropdown.png" class="dropdown-img hover1a" >
                                        <img src="img/dropdown2.png" class="dropdown-img hover1b" >

                            <div class="dropdown-content yellow-dropdown-content">

                                <p class="dropdown-p"><a href="register.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMIN_REGISTER_NEW_MEMBER ?></a></p>
                            </div>
                        </div>
                        <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">

                            <?php echo _ADMINHEADER_PRODUCTS ?>
                                        <img src="img/dropdown.png" class="dropdown-img hover1a" >
                                        <img src="img/dropdown2.png" class="dropdown-img hover1b" >

                            <div class="dropdown-content yellow-dropdown-content">


                                <p class="dropdown-p"><a href="adminAddNewPackage.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_ADD_NEW_PACKAGE ?></a></p>
                                <p class="dropdown-p"><a href="adminAddNewProduct.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_ADD_NEW_PRODUCT ?></a></p>
                                <p class="dropdown-p"><a href="adminAddNewSalesAids.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_ADD_NEW_SALESAIDS ?></a></p>
                                <p class="dropdown-p"><a href="adminViewCurrentProduct.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_CURRENT_PRODUCT ?></a></p>
                            </div>
                        </div>
                        <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">

                            <?php echo _ADMINHEADER_ORDERS ?>
                                        <img src="img/dropdown.png" class="dropdown-img hover1a" >
                                        <img src="img/dropdown2.png" class="dropdown-img hover1b" >

                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="adminViewBill.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo AVB_ORDER_CHECKER ?></a></p>
                                <p class="dropdown-p"><a href="adminAllOrders.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_ALL_ORDERS ?></a></p>
                                <p class="dropdown-p"><a href="adminViewOrder.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMIN_CURRENT_ORDER ?></a></p>
                                <p class="dropdown-p"><a href="adminViewCompOrder.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMIN_COMPLETED_ORDER ?></a></p>
                            </div>
                        </div>
                        <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">

                            <?php echo _ADMINHEADER_PROFILE_MANAGEMENT ?>
                                        <img src="img/dropdown.png" class="dropdown-img hover1a" >
                                        <img src="img/dropdown2.png" class="dropdown-img hover1b" >

                            <div class="dropdown-content yellow-dropdown-content">
                            <p class="dropdown-p"><a href="adminEditPassword.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _MAINJS_INDEX_RESET_PASSWORD ?></a></p>
                                <p class="dropdown-p"><a href="adminViewAllMembers.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_MEMBER_PROFILE ?></a></p>
                                <p class="dropdown-p"><a href="adminViewForgot.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMIN_RESET_USER_PASSWORD ?></a></p>
                            </div>
                        </div>

                        <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">

                            <?php echo _HEADERBEFORELOGIN_REPORT ?>
                                        <img src="img/dropdown.png" class="dropdown-img hover1a" >
                                        <img src="img/dropdown2.png" class="dropdown-img hover1b" >

                            <div class="dropdown-content yellow-dropdown-content">


                                <p class="dropdown-p"><a href="salesReport.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_SALES_REPORT ?></a></p>
                                <p class="dropdown-p"><a href="payoutReport.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_PAYOUT_REPORT ?></a></p>
                                <p class="dropdown-p"><a href="packageReport.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_PACKAGE_REPORT ?></a></p>
                                <p class="dropdown-p"><a href="salesPackageReport.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_SALES_PACKAGE_REPORT ?></a></p>
                                <p class="dropdown-p"><a href="customerSalesReport.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_CUSTOMER_REPORT ?></a></p>
                                <p class="dropdown-p"><a href="purchaseReport.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_CUSTOMER_HISTORY ?></a></p>
                                <p class="dropdown-p"><a href="signUpReport.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _HEADERBEFORELOGIN_SIGNUP_PACKAGE ?></a></p>
                                <p class="dropdown-p"><a href="adminWithdrawalReport.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _HEADERBEFORELOGIN_WITHDRAWAL_REPORT ?></a></p>

                            </div>
                        </div>
                        <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">

                            Language / 语言
                                        <img src="img/dropdown.png" class="dropdown-img hover1a" alt="Language / 语言" title="Language / 语言">
                                        <img src="img/dropdown2.png" class="dropdown-img hover1b" alt="Language / 语言" title="Language / 语言">
                                        <!-- <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                            <div class="dropdown-content yellow-dropdown-content">
                                <!-- <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                                <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                                <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">English</a></p>
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">中文</a></p>
                            </div>
                        </div>
                        <a href="logout.php" class="pink-hover-text menu-item">
                            <?php echo _MAINJS_ALL_LOGOUT ?>
                        </a>
                                    <div id="dl-menu" class="dl-menuwrapper logged-in-dl">
                                        <button class="dl-trigger">Open Menu</button>
                                        <ul class="dl-menu">
                                        <li><a href="adminDashboard.php"><?php echo _ADMIN_DASHBOARD ?></a></li>
                                        <li><a href="adminViewAnnouncement.php"><?php echo _ADMIN_ANNOUNCEMENT ?></a></li>
                                        <li><a href="adminAllMember.php"><?php echo _ADMINHEADER_ALLMEMBER ?></a></li>
                                        <li><a href="adminExpiredUser.php"><?php echo _ADMINHEADER_ALL_EXPIRED_MEMBER ?></a></li>
                                        <li><a href="register.php"><?php echo _ADMIN_REGISTER_NEW_MEMBER ?></a></li>
                                        <li><a href="adminEditPassword.php"><?php echo _MAINJS_INDEX_RESET_PASSWORD ?></a></li>
                                        <li><a href="adminViewForgot.php"><?php echo _ADMIN_RESET_USER_PASSWORD ?></a></li>
                                        <li><a href="adminViewAllMembers.php"><?php echo _ADMINHEADER_MEMBER_PROFILE ?></a></li>
                                        <li><a href="adminViewWithdrawal.php"><?php echo _ADMIN_WITHDRAWAL_REQUEST ?></a></li>
                                        <li><a href="adminViewMilesStones.php"><?php echo _ADMIN_MILESTONES_BONUS ?></a></li>
                                        <li><a href="adminAddNewPackage.php"><?php echo _ADMINHEADER_ADD_NEW_PACKAGE ?></a></li>
                                        <li><a href="adminAddNewProduct.php"><?php echo _ADMINHEADER_ADD_NEW_PRODUCT ?></a></li>
                                        <li><a href="adminViewCurrentProduct.php"><?php echo _ADMINHEADER_CURRENT_PRODUCT ?></a></li>
                                        <li><a href="salesReport.php"><?php echo _ADMINHEADER_SALES_REPORT ?></a></li>
                                        <li><a href="payoutReport.php"><?php echo _ADMINHEADER_PAYOUT_REPORT ?></a></li>
                                        <li><a href="packageReport.php"><?php echo _ADMINHEADER_PACKAGE_REPORT ?></a></li>
                                        <li><a href="salesPackageReport.php"><?php echo _ADMINHEADER_SALES_PACKAGE_REPORT ?></a></li>
                                        <li><a href="customerSalesReport.php"><?php echo _ADMINHEADER_CUSTOMER_HISTORY ?></a></li>
                                        <!-- <li><a href="purchaseReport.php"><?php echo _PURCHASE_ORDER_HISTORY ?></a></li> -->
                                        <li><a href="signUpReport.php"><?php echo _HEADERBEFORELOGIN_SIGNUP_PACKAGE ?></a></li>
                                        <li><a href="adminWithdrawalReport.php"><?php echo _HEADERBEFORELOGIN_WITHDRAWAL_REPORT ?></a></li>
                                        <li><a href="<?php $link ?>?lang=en">Switch to English</a></li>
                                        <li><a href="<?php $link ?>?lang=ch">转换成中文</a></li>
                                        <li><a href="logout.php" ><?php echo _MAINJS_ALL_LOGOUT ?></a></li>

                                        </ul>
                                    </div><!-- /dl-menuwrapper -->
                    </div>
                </div>

        </header>

    <?php
    }
    elseif($_SESSION['usertype'] == 1)
    {
    ?>

        <header id="header" class="header header--fixed same-padding header1 menu-color member-menu" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php"><img src="img/samofa.png" class="logo-img" alt="Logo" title="Logo"></a>
                    </div>



                    <div class="right-menu-div float-right logged-in-menu">
                        <a href="userDashboard.php" class="pink-hover-text menu-margin-right menu-item">
                            <?php echo _USERDASHBOARD_DASHBOARD ?>
                        </a>
                        <a href="announcement.php" class="pink-hover-text menu-margin-right menu-item">
                            <?php echo _HEADERBEFORELOGIN_ANNOUNCEMENT ?>
                        </a>
                        <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">

                            <?php echo _HEADERBEFORELOGIN_MY_TEAM ?>
                                        <img src="img/dropdown.png" class="dropdown-img hover1a" >
                                        <img src="img/dropdown2.png" class="dropdown-img hover1b" >

                            <div class="dropdown-content yellow-dropdown-content">


                                <p class="dropdown-p"><a href="myTeam.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _HEADERBEFORELOGIN_TEAM_MEMBER ?></a></p>
                                <p class="dropdown-p"><a href="register.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _HEADERBEFORELOGIN_REGISTER ?></a></p>

                            </div>
                        </div>
                        <a href="productDetails.php" class="pink-hover-text menu-margin-right menu-item">
                            <?php echo _HEADER_ORDER ?>
                        </a>

                        <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">

                            <?php echo _HEADERBEFORELOGIN_PROFILE ?>
                                        <img src="img/dropdown.png" class="dropdown-img hover1a" >
                                        <img src="img/dropdown2.png" class="dropdown-img hover1b" >


                            <div class="dropdown-content yellow-dropdown-content">

                                <p class="dropdown-p"><a href="userProfile.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _HEADERBEFORELOGIN_MY_PROFILE ?></a></p>
                                <p class="dropdown-p"><a href="editPassword.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _HEADERBEFORELOGIN_EDIT_PASSWORD ?></a></p>
                                <p class="dropdown-p"><a href="addEpin.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _HEADERBEFORELOGIN_EDIT_EPIN ?></a></p>
                            </div>
                        </div>
                        <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">

                            <?php echo _HEADERBEFORELOGIN_REPORT ?>
                                        <img src="img/dropdown.png" class="dropdown-img hover1a" >
                                        <img src="img/dropdown2.png" class="dropdown-img hover1b" >
                                        <!-- <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                            <div class="dropdown-content yellow-dropdown-content">
                                <!-- <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                                <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                                <p class="dropdown-p"><a href="commissionReport.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _HEADERBEFORELOGIN_COMMISSION_REPORT ?></a></p>
                                <p class="dropdown-p"><a href="withdrawalReport.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _HEADERBEFORELOGIN_WITHDRAWAL_REPORT ?></a></p>
                                <p class="dropdown-p"><a href="salesReport.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_SALES_REPORT ?></a></p>
                                <p class="dropdown-p"><a href="signUpReport.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _HEADERBEFORELOGIN_SIGNUP_PACKAGE ?></a></p>
                            </div>
                        </div>
                        <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">

                            Language / 语言
                                        <img src="img/dropdown.png" class="dropdown-img hover1a" alt="Language / 语言" title="Language / 语言">
                                        <img src="img/dropdown2.png" class="dropdown-img hover1b" alt="Language / 语言" title="Language / 语言">
                                        <!-- <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                            <div class="dropdown-content yellow-dropdown-content">
                                <!-- <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                                <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                                <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">English</a></p>
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">中文</a></p>
                            </div>
                        </div>
                        <a href="logout.php" class="pink-hover-text menu-item">
                            <?php echo _MAINJS_ALL_LOGOUT ?>
                        </a>
                                    <div id="dl-menu" class="dl-menuwrapper logged-in-dl">
                                        <button class="dl-trigger">Open Menu</button>
                                        <ul class="dl-menu">
                                        <li><a href="userDashboard.php"><?php echo _USERDASHBOARD_DASHBOARD ?></a></li>
                                        <li><a href="announcement.php"><?php echo _HEADERBEFORELOGIN_ANNOUNCEMENT ?></a></li>
                                        <li><a href="myTeam.php"><?php echo _HEADERBEFORELOGIN_TEAM_MEMBER ?></a></li>
                                        <li><a href="register.php"><?php echo _HEADERBEFORELOGIN_REGISTER ?></a></li>
                                        <li><a href="productDetails.php"><?php echo _HEADER_ORDER ?></a></li>


                                        <li><a href="userProfile.php"><?php echo _HEADERBEFORELOGIN_MY_PROFILE ?></a></li>
                                        <li><a href="editPassword.php"><?php echo _HEADERBEFORELOGIN_EDIT_PASSWORD ?></a></li>
                                        <li><a href="addEpin.php"><?php echo _HEADERBEFORELOGIN_EDIT_EPIN ?></a></li>
                                        <li><a href="commissionReport.php"><?php echo _HEADERBEFORELOGIN_COMMISSION_REPORT ?></a></li>
                                        <li><a href="withdrawalReport.php"><?php echo _HEADERBEFORELOGIN_WITHDRAWAL_REPORT ?></a></li>
                                        <li><a href="purchaseReport.php"><?php echo _PURCHASE_ORDER_HISTORY ?></a></li>
                                        <li><a href="signUpReport.php"><?php echo _HEADERBEFORELOGIN_SIGNUP_PACKAGE ?></a></li>
                                        <li><a href="<?php $link ?>?lang=en">Switch to English</a></li>
                                        <li><a href="<?php $link ?>?lang=ch">转换成中文</a></li>
                                        <li><a href="logout.php" ><?php echo _MAINJS_ALL_LOGOUT ?></a></li>

                                        </ul>
                                    </div><!-- /dl-menuwrapper -->
                    </div>
                </div>

        </header>

    <?php
    }
    ?>

<?php
}
else
{
?>

<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <a href="index.php"><img src="img/samofa.png" class="logo-img" alt="Logo" title="Logo"></a>
            </div>



            <div class="right-menu-div float-right before-header-div">

            	<a class="pink-hover-text menu-margin-right menu-item open-login ow-login-color">
                	<?php echo _HEADERBEFORELOGIN_LOGIN ?>
                </a>
            	<a href="index.php" class="pink-hover-text menu-margin-right menu-item">
                	<?php echo _HEADERBEFORELOGIN_HOME ?>
                </a>
                <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect">

                    <?php echo _HEADERBEFORELOGIN_OUR_PRODUCT ?>
                            	<img src="img/dropdown.png" class="dropdown-img hover1a" alt="<?php echo _HEADERBEFORELOGIN_OUR_PRODUCT ?>" title="<?php echo _HEADERBEFORELOGIN_OUR_PRODUCT ?>">
                                <img src="img/dropdown2.png" class="dropdown-img hover1b" alt="<?php echo _HEADERBEFORELOGIN_OUR_PRODUCT ?>" title="<?php echo _HEADERBEFORELOGIN_OUR_PRODUCT ?>">

                	<div class="dropdown-content yellow-dropdown-content menu-item">
                        <!-- <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                        <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                        <p class="dropdown-p"><a href="magicBloca.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Magic BloCA<sup class="pink-hover-text">TM</sup> 脂糖宝</a></p>
                        <p class="dropdown-p"><a href="hairSerum.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _INDEX_SERUM2 ?></a></p>
                        <p class="dropdown-p"><a href="hairShampoo.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _INDEX_PURI ?></a></p>
                	</div>
                </div>

                <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">

                    Language / 语言
                            	<img src="img/dropdown.png" class="dropdown-img hover1a" alt="Language / 语言" title="Language / 语言">
                                <img src="img/dropdown2.png" class="dropdown-img hover1b" alt="Language / 语言" title="Language / 语言">
                                <!-- <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                	<div class="dropdown-content yellow-dropdown-content menu-item">
                        <!-- <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                        <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                        <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">English</a></p>
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">中文</a></p>
                	</div>
                </div>
                <a href="./charitysales/index.php" class="pink-hover-text menu-item">
                	<?php echo NEW_CHARITY_PAGE ?>
                </a>
                           	<div id="dl-menu" class="dl-menuwrapper before-dl">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
                                  <li><a class="open-login"><?php echo _HEADERBEFORELOGIN_LOGIN ?></a></li>
                                  <li><a href="index.php"><?php echo _HEADERBEFORELOGIN_HOME ?></a></li>
                                  <li><a href="magicBloca.php">Magic BloCA 脂糖宝</a></li>
                                  <li><a href="hairSerum.php"><?php echo _INDEX_HAIR_SERUM_SHORT ?></a></li>
                                  <li><a href="<?php $link ?>?lang=en">Switch to English</a></li>
                                  <li><a href="<?php $link ?>?lang=ch">转换成中文</a></li>
                                  <li><a href="./charitysales/index.php"><?php echo NEW_CHARITY_PAGE ?></a></li>

                                </ul>
							</div><!-- /dl-menuwrapper -->

            </div>
        </div>

</header>

<?php
}
?>
