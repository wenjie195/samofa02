<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Announcement.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userData = $userDetails[0];

// $allAnnoucement = getAnnouncement($conn);
$allAnnoucement = getAnnouncement($conn, " WHERE status = 'Available' AND type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/announcement.php" />
    <meta property="og:title" content="Announcement | Samofa 莎魔髪" />
    <title>Announcement | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/announcement.php" />
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
	<?php include 'css.php'; ?>
    
</head>

<body class="body">

<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _HEADERBEFORELOGIN_ANNOUNCEMENT ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    
    <div class="width100 same-padding container-div1">
   
        <div class="overflow-scroll-div">
            <table class="table-css">
                <thead>
                    <tr>
                        <th><?php echo _PRODUCTDETAILS_NO ?></th>
                        <th><?php echo _ADMIN_TITLE ?></th>
                        <th><?php echo _ADMIN_CONTENT ?></th>
                        <th><?php echo _ADMIN_DATE ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($allAnnoucement)
                    {
                        for($cnt = 0;$cnt < count($allAnnoucement) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $allAnnoucement[$cnt]->getTitle();?></td>
                                <td><?php echo $allAnnoucement[$cnt]->getContent();?></td>
                                <td><?php echo $allAnnoucement[$cnt]->getDateCreated();?></td>
                            </tr>
                        <?php
                        }
                    }
                    ?> 
                </tbody>
            </table>
        </div>

    </div>    

    <div class="clear"></div>

</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>
