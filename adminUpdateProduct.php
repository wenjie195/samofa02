<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/adminUpdateProduct.php" />
    <meta property="og:title" content="Admin Update Product | Samofa 莎魔髪" />
    <title>Admin Update Product | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/adminAddNewProduct.php" />
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
	<?php include 'css.php'; ?>
    
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMINHEADER_UPDATE_PRODUCT_DETAILS ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    <div class="width100 same-padding">

        <?php
        if(isset($_POST['product_id']))
        {
            $conn = connDB();
            $productDetails = getProduct($conn,"WHERE id = ? ", array("id") ,array($_POST['product_id']),"s");
            ?>

                <form method="POST" action="utilities/adminUpdateProductFunction.php" enctype="multipart/form-data">

                    <div class="dual-input">
                        <p class="input-top-text"><?php echo _PRODUCTDETAILS_NAME ?></p>
                        <input class="clean de-input" type="text" placeholder="<?php echo _PRODUCTDETAILS_NAME ?>" id="product_name" name="product_name" value="<?php echo $productDetails[0]->getName();?>" required>
                    </div>

                    <div class="dual-input second-dual-input">
                        <p class="input-top-text"><?php echo _PRODUCTDETAILS_ORI_PRICE ?> (RM)</p>
                        <input class="clean de-input" type="text" placeholder="<?php echo _PRODUCTDETAILS_ORI_PRICE ?> (RM)" id="product_price" name="product_price" value="<?php echo $productDetails[0]->getPrice();?>" required>
                    </div>

                    <div class="clear"></div>

                    <div class="dual-input">
                        <p class="input-top-text"><?php echo _PRODUCTDETAILS_RANK_PRICE ?> (RM)</p>
                        <input class="clean de-input" type="text" placeholder="<?php echo _PRODUCTDETAILS_RANK_PRICE ?> (RM)" id="product_member_price" name="product_member_price" value="<?php echo $productDetails[0]->getRankOri();?>">
                    </div>

                    <div class="dual-input second-dual-input">
                        <p class="input-top-text"><?php echo _USERDASHBOARD_RANK ?> A <?php echo _PRODUCTDETAILS_PRICE ?>(RM)</p>
                        <input class="clean de-input" type="text" placeholder="<?php echo _USERDASHBOARD_RANK ?> A <?php echo _PRODUCTDETAILS_PRICE ?>(RM)" id="rank_a_price" name="rank_a_price" value="<?php echo $productDetails[0]->getRankA();?>">
                    </div>

                    <div class="clear"></div>

                    <div class="dual-input">
                        <p class="input-top-text"><?php echo _USERDASHBOARD_RANK ?> B <?php echo _PRODUCTDETAILS_PRICE ?>(RM)</p>
                        <input class="clean de-input" type="text" placeholder="<?php echo _USERDASHBOARD_RANK ?> B <?php echo _PRODUCTDETAILS_PRICE ?>(RM)" id="rank_b_price" name="rank_b_price" value="<?php echo $productDetails[0]->getRankB();?>">
                    </div>

                    <div class="dual-input second-dual-input">
                        <p class="input-top-text"><?php echo _USERDASHBOARD_RANK ?> C <?php echo _PRODUCTDETAILS_PRICE ?>(RM)</p>
                        <input class="clean de-input" type="text" placeholder="<?php echo _USERDASHBOARD_RANK ?> C <?php echo _PRODUCTDETAILS_PRICE ?>(RM)" id="rank_c_price" name="rank_c_price" value="<?php echo $productDetails[0]->getRankC();?>">
                    </div>

                    <div class="clear"></div>

                    <div class="width100">
                        <p class="input-top-text"><?php echo _PRODUCTDETAILS_DESCRIPTION ?></p>
                        <textarea class="clean de-input de-textarea" type="text" placeholder="<?php echo _PRODUCTDETAILS_DESCRIPTION ?>" id="product_description" name="product_description" required><?php echo $productDetails[0]->getDescription();?></textarea>
                    </div>

                    <div class="clear"></div>

                    <div class="width100">
                        <p class="input-top-text"><?php echo _PRODUCTDETAILS_IMAGE ?> Current Image : <a href="ProductImages/<?php echo $productDetails[0]->getImages();?>" class="blue-link2" target="_blank">View</a></p>
                        <input type="file" name="image_one" id="image_one" accept="image/*">
                    </div>

                    <input class="clean de-input" type="hidden"  value="<?php echo $productDetails[0]->getImages();?>" name="ori_fileone" id="ori_fileone" readonly> 
                    <input class="clean de-input" type="hidden"  value="<?php echo $productDetails[0]->getId();?>" name="product_id" id="product_id" readonly> 

                    <div class="clear"></div>

                    <div class="width100 text-center top-bottom-distance">
                        <button class="clean button-width transparent-button dark-pink-button" name="submit"><?php echo _JS_SUBMIT ?></button>
                    </div>

                </form>

            <?php
        }
        ?>

    </div>
</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>
