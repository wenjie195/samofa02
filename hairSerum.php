<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/hairSerum.php" />
    <meta property="og:title" content="<?php echo _INDEX_SERUM2 ?> | Samofa 莎魔髪" />
    <title><?php echo _INDEX_SERUM2 ?> | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/hairSerum.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="overflow width100 menu-distance min-height-with-menu-distance2">
    <div class="top-left-flower-div top-left-flower-div2">
        <img src="img/top-left-flower.png" class="top-left-flower" alt="Samofa 莎魔髪" title="Samofa 莎魔髪">
    </div>
    <div class="width100 overflow same-padding product-page-div">
    	<div class="left-product">
        	<img src="img/serum2.jpg" class="product-page-product" alt="<?php echo _INDEX_SERUM2 ?>" title="<?php echo _INDEX_SERUM2 ?>">
        </div>
        <div class="right-product-content">
            <h2 class="product-title big-header-color ow-no-margin-top"><?php echo _INDEX_SERUM2 ?></h2>
            <h3 class="mild-pink-text desc-h3"><?php echo _INDEX_SERUM_DESC ?></h3>
   	
         	<!-- Unhide it when you are ready for the add to cart function 
            <div class="width100 overflow product-quantity">
            	<p class="quantity-p dark-pink-text"><?php echo _INDEX_QUANTITY ?>:</p> <div class="numbers-row numbers-row-css"></div>
        	</div>
            <div class="border-btn add-to-cart-btn"><div class="white-bg"><?php echo _INDEX_ADD_TO_CART ?> | RM XXXX</div></div>  
            -->      
        </div>	
    </div>
    
    
    <div class="bottom-right-flower-div bottom-right-flower-div2">
        <img src="img/bottom-right-flower.png" class="bottom-right-flower" alt="Samofa 莎魔髪" title="Samofa 莎魔髪">
    </div>
    <div class="clear"></div>
    <div class="after-flower-info width100 same-padding">
    	<div class="row-div">
        	<div class="row-title-div option-heading">
            	
                <h3 class="big-header-color row-h3"><img src="img/hair.png" alt="<?php echo _INDEX_STEPS ?>" class="row-img"> <?php echo _INDEX_STEPS ?></h3>
            </div>

            <div class="option-content is-hidden row-content">
                
            	<p class="td-p dark-pink-text">1. <?php echo _HAIR_SERUM_STEP1 ?></p>
            	<p class="inside-td-p mild-pink-text inside-td-p3"><img src="img/flower3.png" alt="<?php echo _INDEX_STEPS ?>" class="td-img star-png2"> <?php echo _HAIR_SERUM_STEP1_DESC ?></p> 
                
            	<p class="td-p dark-pink-text">2. <?php echo _HAIR_SERUM_STEP2 ?></p>
            	<p class="inside-td-p mild-pink-text inside-td-p3"><img src="img/flower3.png" alt="<?php echo _INDEX_STEPS ?>" class="td-img star-png2"> <?php echo _HAIR_SERUM_STEP2_DESC ?></p> 
                
            	<p class="td-p dark-pink-text">3. <?php echo _HAIR_SERUM_STEP3 ?></p>
            	<p class="inside-td-p mild-pink-text inside-td-p3"><img src="img/flower3.png" alt="<?php echo _INDEX_STEPS ?>" class="td-img star-png2"> <?php echo _HAIR_SERUM_STEP3_DESC ?></p> 
            </div>
		</div>
    	<div class="row-div">
        	<div class="row-title-div option-heading">
            	
                <h3 class="big-header-color row-h3"><img src="img/benefit.png" alt="<?php echo _INDEX_FUNCTION ?>" class="row-img"> <?php echo _INDEX_SERUM_EFFECT ?></h3>
                
            </div>

            <div class="option-content is-hidden row-content">
            	<table class="transparent-table row-table">
                	<tbody>
                        <tr>
                            <td>1 ♥</td>
                            <td><?php echo _HAIR_SERUM_E1 ?></td>
                        </tr>
                        <tr>
                            <td>2 ♥</td>
                            <td><?php echo _HAIR_SERUM_E2 ?></td>
                        </tr>
                        <tr>
                            <td>3 ♥</td>
                            <td><?php echo _HAIR_SERUM_E3 ?></td>
                        </tr> 
                        <tr>
                            <td>4 ♥</td>
                            <td><?php echo _HAIR_SERUM_E4 ?></td>
                        </tr> 
                        <tr>
                            <td>5 ♥</td>
                            <td><?php echo _HAIR_SERUM_E5 ?></td> 
                        </tr> 
                        <tr>
                            <td>6 ♥</td>
                            <td><?php echo _HAIR_SERUM_E6 ?></td>
                        </tr>  
                        <tr>
                            <td>7 ♥</td>
                            <td><?php echo _HAIR_SERUM_E7 ?></td>
                        </tr>  
                        <tr>
                            <td>8 ♥</td>
                            <td><?php echo _HAIR_SERUM_E8 ?></td>
                        </tr> 
                        <tr>
                            <td>9 ♥</td>
                            <td><?php echo _HAIR_SERUM_E9 ?></td>
                        </tr> 
                        <tr>
                            <td>10 ♥</td>
                            <td><?php echo _HAIR_SERUM_E10 ?></td> 
                        </tr> 
                        <tr>
                            <td>11 ♥</td>
                            <td><?php echo _HAIR_SERUM_E11 ?></td>
                        </tr>  
                        <tr>
                            <td>12 ♥</td>
                            <td><?php echo _HAIR_SERUM_E12 ?></td>
                        </tr>                        
                                      
                       </tbody>                                                                                                                          
            	</table>
            </div>
		</div>

    	<div class="row-div">
        	<div class="row-title-div option-heading">
            	
                <h3 class="big-header-color row-h3"><img src="img/love.png" alt="<?php echo _HAIR_SERUM_LOVE_CREATION ?>" class="row-img"> <?php echo _HAIR_SERUM_LOVE_CREATION ?></h3>
            </div>

            <div class="option-content is-hidden row-content">
            	<table class="transparent-table row-table">
                	<tbody>
                        <tr>
                            <td><img src="img/safe.png" alt="<?php echo _HAIR_SERUM_LOVE_CREATION ?>" class="td-img"></td>
                            <td><?php echo _HAIR_SERUM_LOVE_CREATION_DESC ?></td>
                        </tr>
                    </tbody>
             	</table>
            </div>
        </div>

    	<div class="row-div last-row">
        	<div class="row-title-div option-heading">
            	
                <h3 class="big-header-color row-h3"><img src="img/ingredients2.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="row-img"> <?php echo _INDEX_MAIN_INGREDIENTS ?></h3>
            </div>

            <div class="option-content is-hidden row-content">
             	<table class="transparent-table row-table">
                	<tbody>
                        <tr>
                            <td><img src="img/star2.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="td-img star-png"></td>
                            <td><?php echo _HAIR_SERUM_SWEET_ALMOND ?></td>
                        </tr>
                        <tr>
                            <td><img src="img/star2.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="td-img star-png"></td>
                            <td><?php echo _HAIR_SERUM_MONA ?></td>
                        </tr>      
                        <tr>
                            <td><img src="img/star2.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="td-img star-png"></td>
                            <td><?php echo _HAIR_SERUM_TEATREE ?></td>
                        </tr>                        
                        <tr>
                            <td><img src="img/star2.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="td-img star-png"></td>
                            <td><?php echo _HAIR_SERUM_LONG ?></td>
                        </tr>                                          
                 </tbody>               
				</table>
				<p class="td-p pink-text"><?php echo _HAIR_SERUM_4 ?></p>
            </div>
        </div>
            
        </div>
    
    </div>
</div>
<?php include 'js.php'; ?>
</body>
</html>