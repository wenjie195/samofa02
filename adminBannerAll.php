<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Slider.php';
// require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$allSlider = getSlider($conn, "WHERE status = 'Show' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
    <?php include 'meta.php'; ?>
    <meta property="og:title" content="All Banner | Samofa 莎魔髪" />
    <title>All Banner | Samofa 莎魔髪</title>   
    <?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>


<div class="clear"></div>
<div class="width100 menu-distance75 min-height-with-flower">
    

    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color">
        Banner<img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>">
    </h1>
    

	<div class="width100 same-padding text-center top-bottom-distance">
    	<a href="adminBannerAdd.php" class="clean button-width transparent-button dark-pink-button" >Add Banner</a>
    </div>

    <div class="width100 same-padding container-div1">

        <div class="overflow-scroll-div">
            <table class="table-css">
                <thead>
                    <th>No.</th>
                    <th>Img Name</th>
                    <th>Update</th>
                    <th>Delete</th>
                </thead>

                <?php
                if($allSlider)
                {
                    for($cnt = 0;$cnt < count($allSlider) ;$cnt++)
                    {
                    ?>    
                        <tr>
                            <td><?php echo ($cnt+1)?></td>

                            <td>
                                <div class="table-review-comment">
                                    <img src="uploadsSlider/<?php echo $allSlider[$cnt]->getImgName();?>" class="product-img" >
                                </div>
                            </td>

                            <td>
                                <form action="adminBannerEdit.php" method="POST">
                                    <button class="transparent-button clean green-link hover-effect" type="submit" name="item_uid" value="<?php echo $allSlider[$cnt]->getUid();?>">
                                        Update
                                    </button>
                                </form>
                            </td>

                            <td>
                                <form action="utilities/adminBannerDeleteFunction.php" method="POST" class="claim-css decision">
                                    <button class="transparent-button clean red-link hover-effect" type="submit" name="item_uid" value="<?php echo $allSlider[$cnt]->getUid();?>">
                                        Delete
                                    </button>
                                </form>
                            </td>
                            
                        </tr>
                    <?php
                    }
                }
                ?>  

            </table>
        </div>
    </div>

</div>

<div class="clear"></div>
<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">
<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Banner Updated !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Fail To Update Banner !"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "ERROR !";
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "Banner Added !"; 
        }
        elseif($_GET['type'] == 5)
        {
            $messageType = "Fail To Add Banner !"; 
        }
        elseif($_GET['type'] == 6)
        {
            $messageType = "Banner Deleted !"; 
        }
        elseif($_GET['type'] == 7)
        {
            $messageType = "Fail To Delete Banner !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>