<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-172739670-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-172739670-1');
</script>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta property="og:type" content="website" />
<meta property="og:image" content="https://samofa.my/img/samofa-meta.jpg" />
<meta name="author" content="Samofa 莎魔髪">
<meta property="og:description" content="SAMOFA 成立于2017年，努力通过专业经营与美容保健项目阐释现代人的理念。公司在经营规模、业务领域取得长足发展，已从最初的髪之深品牌快速成长为自创品牌、微营销、互联网创业等业务于一体的综合公司。" />
<meta name="description" content="SAMOFA 成立于2017年，努力通过专业经营与美容保健项目阐释现代人的理念。公司在经营规模、业务领域取得长足发展，已从最初的髪之深品牌快速成长为自创品牌、微营销、互联网创业等业务于一体的综合公司。" />
<meta name="keywords" content="Samofa 莎魔髪,samofa,脂糖宝,MagicBloca, Magic Bloca, Hair shampoo, hair serum, Purifying Balancing Shampoo, Serum Ultimate Elixir, 净化平衡洗发露, 升级版护发神器,  etc">


