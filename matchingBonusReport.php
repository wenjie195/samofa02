<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/OtherBonus.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn);
// $conn->close();
$no = 0;

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/otherCommissionReport.php" />
    <meta property="og:title" content="Commission Report | Samofa 莎魔髪" />
    <title>Commission Report | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/otherCommissionReport.php" />
    <!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
    <!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<?php include 'css.php'; ?>

</head>

<body class="body">

<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h2 class="dark-pink-text hi-title text-center modal-h1 big-header-color"><a href="payoutReport.php" class="link-a"><?php echo "Direct Bonus" ?></a> | <a href="leadershipBonusReport.php" class="link-a"><?php echo "Leadership Bonus" ?></a> | <?php echo "Matching Bonus" ?></h2>

    <div class="width100 container-div1">

        <div class="overflow-scroll-div same-padding">

        <div class="dual-input">
            <input class="clean de-input" type="text" placeholder="<?php echo "Start Date" ?>" id="startDate" name="register_firstname" autocomplete="off">
        </div>
        <div class="dual-input second-dual-input">
        	<input class="clean de-input" type="text" placeholder="<?php echo "End Date" ?>" id="endDate" name="register_lastname" autocomplete="off">
        </div>
        <div class="clear"></div><br>
        <a href="excel/matchingBonusReportExcel.php" class="btn pull-right" style="font-size: 12px; background-color: pink">Export Excel</a><br><br>

            <table id="myTable" class="table-css">
                <thead>
                    <tr>
                        <th><?php echo _PRODUCTDETAILS_NO ?></th>
                        <th><?php echo _JS_USERNAME ?></th>
                        <th><?php echo _ADMIN_AMOUNT ?></th>
                        <th>Bonus Type</th>
                        <th><?php echo _ADMINHEADER_REMARKS ?></th>
                        <th><?php echo _USERDASHBOARD_STATUS ?></th>
                        <th><?php echo _PURCHASE_DATE ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($userDetails) {
                        for ($i=0; $i <count($userDetails) ; $i++) {
                            $bonusDetails = getOtherBonus($conn, " WHERE receive_uid =? AND bonus_type = 'Matching'", array("receive_uid"), array($userDetails[$i]->getUid()), "s");
                        if($bonusDetails)
                        {
                            for($cnt = 0;$cnt < count($bonusDetails) ;$cnt++)
                            {
                              $no += 1;
                              $referralDetails = getUser($conn, "WHERE uid =?",array("uid"),array($bonusDetails[$cnt]->getUid()), "s");
                            ?>
                                <tr>
                                    <td><?php echo $no?></td>
                                    <td><?php echo $bonusDetails[$cnt]->getReceiveUsername();?></td>
                                    <td><?php echo $bonusDetails[$cnt]->getBonus();?></td>
                                    <td><?php echo $bonusDetails[$cnt]->getBonusType();?></td>
                                    <?php
                                    if ($bonusDetails[$cnt]->getNewUser() == 'Buy Product') {
                                      ?><td><?php echo $bonusDetails[$cnt]->getNewUser()." (#".$bonusDetails[$cnt]->getInvoiceNo().")";?></td><?php
                                    }else{
                                      ?><td><?php echo $bonusDetails[$cnt]->getNewUser();?></td><?php
                                    }
                                     ?>
                                    <td><?php echo $bonusDetails[$cnt]->getStatus();?></td>
                                    <td><?php echo date('d/m/Y',strtotime($bonusDetails[$cnt]->getDateCreated()));?></td>
                                </tr>
                            <?php
                            }
                        }
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>

    </div>

    <div class="clear"></div>

</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>
<script>
$("#startDate,#endDate").datepicker({
    dateFormat : 'dd/mm/yy'
});
$("#startDate,#endDate").on("change", function(){
    var startDate = $("#startDate").val();
    var endDate = $("#endDate").val();
    
    $.ajax({
        url : 'matchingBonusReportByDate.php',
        data : {startDate:startDate,endDate:endDate},
        type : 'post',
        success:function(data){
            $("#myTable").empty();
            $("#myTable").html(data);
        }
    });
});
</script>
<?php include 'js.php'; ?>
</body>
</html>
