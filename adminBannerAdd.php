<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Slider.php';
// require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
    <?php include 'meta.php'; ?>
    <meta property="og:title" content="Add Banner | Samofa 莎魔髪" />
    <title>Add Banner | Samofa 莎魔髪</title>   
    <?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="clear"></div>

<div class="width100 menu-distance75 min-height-with-flower">


    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color">
        Add Banner<img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>">
    </h1>
    
    <div class="width100 same-padding">

        <!-- <form method="POST" action="utilities/adminBannerAddFunction.php" enctype="multipart/form-data">  -->
        <form method="POST" action="utilities/adminBannerAddFunction.php" enctype="multipart/form-data">
            <!-- <div class="width100">
                <p class="input-top-text">Link</p>
                <input class="clean de-input" type="text" placeholder="https://" id="link" name="link" required>  
            </div> -->
    
            <div class="width100">
                <p class="input-top-text">Cover Image (Less than 1.8MB)</p>
                <!-- <input type="file" name="image_one" id="image_one" accept="image/*" required> -->
                <input type="file" name="image_one" id="image_one" accept="image/*" required>
            </div>

            <div class="width100 text-center top-bottom-distance">
                <button class="clean button-width transparent-button dark-pink-button" name="submit"><?php echo _JS_SUBMIT ?></button>
            </div>
        </form>

    </div>

</div>

<div class="clear"></div>
<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">
<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>