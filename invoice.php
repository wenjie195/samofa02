<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/SignUpProduct.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/ringgitMalaysia.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';


$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{

}

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/invoice.php" />
    
    <meta property="og:title" content="Invoice | Samofa 莎魔髪" />
    <title>Invoice | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/invoice.php" />
    <?php include 'cssInvoice.php'; ?> 
</head>
<style type="text/css">
    *{
    font-family: 'SF Compact Text Heavy';
    }
</style>
<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php if (isset($_POST['order_id'])) {

    $orderDetails = getOrders($conn," WHERE id = ? ",array("id"),array($_POST['order_id']),"s");

    ?>
    <div class="print-div">


        <div class="header-row">
            <div class="header-left">
                <img src="img/samofa.png" class="supreme-img" alt="Samofa" title="Samofa">
            </div>
            <div class="header-right">
                <h1 class="dck-title">SASA SUCCESS RESOURCES <span class="dck-span">(PG0405956-P)</span></h1>
                <p class="dck-address-p">11 Lorong Jaya 3, Taman Impian Jaya, 14000 Bukit Mertajam, Penang, Malaysia.</p>
                <!-- <p class="dck-address-p">Tel: +6012-461 6966  &nbsp; &nbsp;   Fax: 604-3235651</p> -->
                <p class="dck-address-p">Tel: +6012-461 6966</p>
            </div>
        </div>
        <div class="clear"></div>
        <div class="first-row">
            <div class="first-column">
                <p class="invoice-p"><b>Attn:</b> <?php echo $orderDetails[0]->getUsername() ?></p>
                <p class="invoice-p"><b>Collection Method</b></p>

                    <?php
                    if($orderDetails[0]->getPickupType() == 'Self-Collect')
                    { ?>
                        <p class="invoice-p">
                            <b>Self Collect Location: </b><br>
                                Penang
                        </p>   
                    <?php
                    }
                    else if($orderDetails[0]->getPickupType() == 'Courier')
                    {
                    ?>
                        <p class="invoice-p">
                            <b>Delivery Address: </b><br>
                                <?php 
                                if ($orderDetails[0]->getAddressLine1()) {
                                    echo $orderDetails[0]->getAddressLine1().","."<br>";
                                }
                                if ($orderDetails[0]->getAddressLine2()) {
                                    echo $orderDetails[0]->getAddressLine2().","."<br>";
                                }
                                if ($orderDetails[0]->getAddressLine3()) {
                                    echo $orderDetails[0]->getAddressLine3().","."<br>";
                                }
                                echo $orderDetails[0]->getZipcode()." ".$orderDetails[0]->getCity().","."<br>".$orderDetails[0]->getState(); ?>
                        </p>   
                    <?php
                    }else{
                        ?>
                        <p class="invoice-p">
                            <b>Not Update Yet.</b><br>
                        </p>   
                    <?php
                    }
                    ?>

            </div>
        </div>

            <div class="second-column">
            <h1 class="invoice-h1">INVOICE</h1>
            
                <table class="invoice-table">
                    <tr>
                        <td><p class="invoice-p"><b>No.</b></p></td>
                        <td><p class="invoice-p"><b>:</b></p></td>
                        <td>
                            <p class="invoice-p">
                                <b>
                                    SAM/<?php echo date('d/m/Y',strtotime($orderDetails[0]->getDateCreated()))."/".$orderDetails[0]->getId() ?>
                                </b>
                            </p>
                        </td>
                    </tr>

                    <tr>
                        <td><p class="invoice-p">D/O No.</p></td>
                        <td><p class="invoice-p">:</p></td>
                        <td><p class="invoice-p"></p></td>
                    </tr>        

                    <tr>
                        <td><p class="invoice-p">Your P/O</p></td>
                        <td><p class="invoice-p">:</p></td>
                        <td><p class="invoice-p"></p></td>
                    </tr>     

                    <tr>
                        <td><p class="invoice-p">Terms</p></td>
                        <td><p class="invoice-p">:</p></td>
                        <td><p class="invoice-p">COD</p></td>
                    </tr>  

                    <tr>
                        <td><p class="invoice-p">Date</p></td>
                        <td><p class="invoice-p">:</p></td>
                        <td><p class="invoice-p"><?php echo date('d/m/Y',strtotime($orderDetails[0]->getDateCreated())) ?></p></td>
                    </tr> 

                    <tr>
                        <!-- <td><p class="invoice-p">Page</p></td> -->
                        <!-- <td><p class="invoice-p">:</p></td> -->
                        <!-- <td><p class="invoice-p">1 of 1</p></td> -->
                    </tr>                                                                   
                </table>
                
                <p class="invoice-p"><b></b></p>

                <button class="clean edit-anc-btn hover1 print" onclick="window.print()">
                    <img src="img/print.png" class="edit-announcement-img hover1a" alt="Print" title="Print">
                    <img src="img/print2.png" class="edit-announcement-img hover1b" alt="Print" title="Print">
                </button>

            </div>
  

        <div class="clear"></div>

        <div class="second-row">
            <table class="invoice-item-table">

                <thead>
                    <tr class="th-tr">
                        <th>No.</th>
                        <th>Item Code</th>
                        <th>Description</th>
                        <th>Qty</th>
                        <!-- <th>UOM</th> -->
                        <th>Price/Unit</th>
                        <th class="last-th">Amount</th>
                    </tr>
                </thead>
                 <?php $productOrders = getProductOrders($conn, "WHERE order_id =?",array("order_id"),array($_POST['order_id']), "s");
                 if ($productOrders) {
                      for ($i=0; $i <count($productOrders) ; $i++) { 
                        $productId = $productOrders[$i]->getProductId();
                        $productDetails = getProduct($conn, "WHERE id =?",array("id"),array($productId), "s");
                        $productName = $productDetails[0]->getName();
                        $description = $productDetails[0]->getDescription();
                        $priceUnit = number_format($productDetails[0]->getPrice(),2);
                          ?>
                          <tr>
                            <td><?php echo $i + 1 ?></td>
                            <td><?php echo $productName ?></td>
                            <td><?php echo $description ?></td>
                            <td><?php echo $productOrders[$i]->getQuantity(); ?></td>
                            <!-- <td>BTLS</td> -->
                            <td><?php echo $priceUnit ?></td>
                            <!-- <td></td> -->
                            <td><?php echo number_format($productOrders[$i]->getFinalPrice(),2); ?></td>
                        </tr>
                          <?php
                      }
                    $conn->close();
                  } ?>

            </table>
        </div>

        <div class="third-row">
            <div class="left-price-div">
                <p class="invoice-price-p">
                    <?php 
                    $priceFormat = number_format($orderDetails[0]->getTotal(),2);
                    $priceFormatRemoveComma = str_replace(",", "", $priceFormat);
                    $split = explode(".",$priceFormatRemoveComma);
                    $whole = convertNumber($split[0].".0");
                    $cents = convertNumber($split[1].".0");
                    if ($split[1] == '.00') {
                    echo mb_strtoupper($whole." Ringgit ".$cents." only");
                    }else {
                    echo mb_strtoupper($whole." Ringgit and ".$cents." cents only");
                    } ?>     
                </p>
                <p class="invoice-note-p">Notes:</p>
                <ol class="price-ol">
                    <li>All cheques should be crossed and made payable to <br><b>SASA SUCCESS RESOURCES</b></li>
                    <li>Goods sold are neither returnable nor refundable.</li>
                </ol>
            </div>
            <div style="margin-top: 15px" class="right-price-div">
                <table class="gross-table">
                    <tr>
                        <td><b>Shipping (RM)</b></td>
                        <td class="text-right"><b><?php echo number_format($orderDetails[0]->getTotal() - $orderDetails[0]->getSubTotal(),2) ?></b></td>
                    </tr>
                    <tr>
                        <td><b>Total (RM)</b></td>
                        <td class="total-price-td text-right"><b><?php echo number_format($orderDetails[0]->getTotal(),2) ?></b></td>
                    </tr>                             
                </table>
            </div>
        </div><br>

    <div class="clear"></div>
    
    <div class="four-row">
        <div class="left-sign">
            <div class="border-div"></div>
            <p class="invoice-last-p">Received by</p>
        </div>
        <div class="right-sign">
            <div class="border-div"></div>
            <p class="invoice-last-p">Authorised Signature</p>
        </div>        
    </div>

</div>
    <?php
}elseif (isset($_POST['signup_id'])) {
    $signupDetails = getSignUpProduct($conn, "WHERE id =?",array("id"),array($_POST['signup_id']), "s");

    ?>
    <div class="print-div">


        <div class="header-row">
            <div class="header-left">
                <img src="img/samofa.png" class="supreme-img" alt="Samofa" title="Samofa">
            </div>
            <div class="header-right">
                <h1 class="dck-title">SASA SUCCESS RESOURCES <span class="dck-span">(PG0405956-P)</span></h1>
                <p class="dck-address-p">11 Lorong Jaya 3, Taman Impian Jaya, 14000 Bukit Mertajam, Penang, Malaysia.</p>
                <!-- <p class="dck-address-p">Tel: +6012-461 6966  &nbsp; &nbsp;   Fax: 604-3235651</p> -->
                <p class="dck-address-p">Tel: +6012-461 6966</p>
            </div>
        </div>
        <div class="clear"></div>
        <div class="first-row">
            <div class="first-column">
                <p class="invoice-p"><b>Attn:</b> <?php echo $signupDetails[0]->getReferralName() ?></p>
                <p class="invoice-p"><b>Collection Method</b></p>

                    <?php
                    // if($orderDetails[0]->getPickupType() == 'Self-Collect')
                    // { ?>
                        <!-- <p class="invoice-p"> -->
                            <!-- <b>Self Collect Location: </b><br> -->
                                <!-- Penang -->
                        <!-- </p>    -->
                    <?php
                    // }
                    // else if($orderDetails[0]->getPickupType() == 'Courier')
                    // {
                    ?>
                        <!-- <p class="invoice-p"> -->
                            <!-- <b>Delivery Address: </b><br> -->
                                <?php 
                                // if ($orderDetails[0]->getAddressLine1()) {
                                    // echo $orderDetails[0]->getAddressLine1().","."<br>";
                                // }
                                // if ($orderDetails[0]->getAddressLine2()) {
                                    // echo $orderDetails[0]->getAddressLine2().","."<br>";
                                // }
                                // if ($orderDetails[0]->getAddressLine3()) {
                                    // echo $orderDetails[0]->getAddressLine3().","."<br>";
                                // }
                                // echo $orderDetails[0]->getZipcode()." ".$orderDetails[0]->getCity().","."<br>".$orderDetails[0]->getState(); ?>
                        <!-- </p>    -->
                    <?php
                    // }else{
                        ?>
                        <p class="invoice-p">
                            <b>Not Update Yet.</b><br>
                        </p>   
                    <?php
                    // }
                    ?>

            </div>
        </div>

            <div class="second-column">
            <h1 class="invoice-h1">INVOICE</h1>
            
                <table class="invoice-table">
                    <tr>
                        <td><p class="invoice-p"><b>No.</b></p></td>
                        <td><p class="invoice-p"><b>:</b></p></td>
                        <td>
                            <p class="invoice-p">
                                <b>
                                    SAM/<?php echo date('d/m/Y',strtotime($signupDetails[0]->getDateCreated()))."/".$signupDetails[0]->getId() ?>
                                </b>
                            </p>
                        </td>
                    </tr>

                    <tr>
                        <td><p class="invoice-p">D/O No.</p></td>
                        <td><p class="invoice-p">:</p></td>
                        <td><p class="invoice-p"></p></td>
                    </tr>        

                    <tr>
                        <td><p class="invoice-p">Your P/O</p></td>
                        <td><p class="invoice-p">:</p></td>
                        <td><p class="invoice-p"></p></td>
                    </tr>     

                    <tr>
                        <td><p class="invoice-p">Terms</p></td>
                        <td><p class="invoice-p">:</p></td>
                        <td><p class="invoice-p">COD</p></td>
                    </tr>  

                    <tr>
                        <td><p class="invoice-p">Date</p></td>
                        <td><p class="invoice-p">:</p></td>
                        <td><p class="invoice-p"><?php echo date('d/m/Y',strtotime($signupDetails[0]->getDateCreated())) ?></p></td>
                    </tr> 

                    <tr>
                        <!-- <td><p class="invoice-p">Page</p></td> -->
                        <!-- <td><p class="invoice-p">:</p></td> -->
                        <!-- <td><p class="invoice-p">1 of 1</p></td> -->
                    </tr>                                                                   
                </table>
                
                <p class="invoice-p"><b></b></p>

                <button class="clean edit-anc-btn hover1 print" onclick="window.print()">
                    <img src="img/print.png" class="edit-announcement-img hover1a" alt="Print" title="Print">
                    <img src="img/print2.png" class="edit-announcement-img hover1b" alt="Print" title="Print">
                </button>

            </div>
  

        <div class="clear"></div>

        <div class="second-row">
            <table class="invoice-item-table">

                <thead>
                    <tr class="th-tr">
                        <th>No.</th>
                        <th>Item Code</th>
                        <th>Description</th>
                        <th>Qty</th>
                        <!-- <th>UOM</th> -->
                        <th>Price/Unit</th>
                        <th class="last-th">Amount</th>
                    </tr>
                </thead>
                 <?php 
                        $productDetails = getProduct($conn, "WHERE id =?",array("id"),array($signupDetails[0]->getProduct()), "s");
                        $productName = $productDetails[0]->getName();
                        $description = $productDetails[0]->getDescription();
                        $priceUnit = number_format($signupDetails[0]->getTotal(),2);
                          ?>
                          <tr>
                            <td><?php echo 1 ?></td>
                            <td><?php echo $productName ?></td>
                            <td><?php echo $description ?></td>
                            <td><?php echo $signupDetails[0]->getQuantity(); ?></td>
                            <!-- <td>BTLS</td> -->
                            <td><?php echo $priceUnit ?></td>
                            <!-- <td></td> -->
                            <td><?php echo number_format($signupDetails[0]->getTotal(),2); ?></td>
                        </tr>
                          <?php
                      
                    $conn->close();
                   ?>

            </table>
        </div>

        <div class="third-row">
            <div class="left-price-div">
                <p class="invoice-price-p">
                    <?php 
                    $priceFormat = number_format($signupDetails[0]->getTotal(),2);
                    $priceFormatRemoveComma = str_replace(",", "", $priceFormat);
                    $split = explode(".",$priceFormatRemoveComma);
                    $whole = convertNumber($split[0].".0");
                    $cents = convertNumber($split[1].".0");
                    if ($split[1] == '.00') {
                    echo mb_strtoupper($whole." Ringgit ".$cents." only");
                    }else {
                    echo mb_strtoupper($whole." Ringgit and ".$cents." cents only");
                    } ?>     
                </p>
                <p class="invoice-note-p">Notes:</p>
                <ol class="price-ol">
                    <li>All cheques should be crossed and made payable to <br><b>SASA SUCCESS RESOURCES</b></li>
                    <li>Goods sold are neither returnable nor refundable.</li>
                </ol>
            </div>
            <div style="margin-top: 15px" class="right-price-div">
                <table class="gross-table">
                    <tr>
                        <td><b>Shipping (RM)</b></td>
                        <td class="text-right"><b><?php echo number_format($signupDetails[0]->getTotal() - $signupDetails[0]->getTotal(),2) ?></b></td>
                    </tr>
                    <tr>
                        <td><b>Total (RM)</b></td>
                        <td class="total-price-td text-right"><b><?php echo number_format($signupDetails[0]->getTotal(),2) ?></b></td>
                    </tr>                             
                </table>
            </div>
        </div><br>

    <div class="clear"></div>
    
    <div class="four-row">
        <div class="left-sign">
            <div class="border-div"></div>
            <p class="invoice-last-p">Received by</p>
        </div>
        <div class="right-sign">
            <div class="border-div"></div>
            <p class="invoice-last-p">Authorised Signature</p>
        </div>        
    </div>

</div>
    <?php
} ?>

<?php include 'js.php'; ?>

<style>
p.a {
  font-style: normal;
}

p.b {
  font-style: italic;
}

p.c {
  font-style: oblique;
}
</style>

<script>
function goBack() {
  window.history.back();
}
</script>

</body>
</html>