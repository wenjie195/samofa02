<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Slider.php';
// require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
    <?php include 'meta.php'; ?>
    <meta property="og:title" content="Edit Banner | Samofa 莎魔髪" />
    <title>Edit Banner | Samofa 莎魔髪</title>   
    <?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="clear"></div>

<div class="width100 menu-distance75 min-height-with-flower">

    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color">
        Edit Banner<img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>">
    </h1>

    <div class="width100 same-padding container-div1">

    <?php
    if(isset($_POST['item_uid']))
    {
    $conn = connDB();
    $adsDetails = getSlider($conn,"WHERE uid = ? ", array("uid") ,array($_POST['item_uid']),"s");
    ?>

        <form method="POST" action="utilities/adminBannerUpdateFunction.php" enctype="multipart/form-data">

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Current Image</p>
                <!-- <img src="uploadsSlider/<?php //echo $imgName;?>" class="width100"> -->

                <div class="width100 overflow">
                    <img src="uploadsSlider/<?php echo $adsDetails[0]->getImgName();?>" width="100%" style="margin-top:48px;">
                </div>

            </div>    

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Upload New Image (Less than 1.8mb)</p>
                <!-- <input class="clean"  type="file" >   -->
                <input class="clean" type="file" name="image_one" id="image_one" accept="image/*">
            </div>        

            <input class="tele-input clean" type="hidden" value="<?php echo $adsDetails[0]->getUid();?>" id="uid" name="uid" readonly>  
            <input class="tele-input clean" type="hidden" value="<?php echo $adsDetails[0]->getImgName();?>" id="ori_img" name="ori_img" readonly>  
            
            <div class="width100 text-center top-bottom-distance">
                <button class="clean button-width transparent-button dark-pink-button" name="submit"><?php echo _JS_SUBMIT ?></button>
            </div>

        </form>

    <?php
    }
    ?>

    </div>

</div>

<div class="clear"></div>
<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">
<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>