<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/editPassword.php" />

    <meta property="og:title" content="Edit Password | Samofa 莎魔髪" />
    <title>Edit Password | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/editPassword.php" />
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
	<?php include 'css.php'; ?>
    
</head>

<body class="body">

<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _HEADERBEFORELOGIN_EDIT_PASSWORD ?></h1>

        <div class="width100 same-padding">

            <form action="utilities/editPasswordFunction.php" method="POST">
            
                <div class="dual-input">
                    <p class="input-top-text"><?php echo _JS_CURRENT_PASSWORD ?></p>
                    <div class="fake-input-bg">
                        <input class="clean de-input password-input" type="password" placeholder="<?php echo _JS_CURRENT_PASSWORD ?>" id="current_password" name="current_password">
                        <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="<?php echo _JS_VIEW_PASSWORD ?>" title="<?php echo _JS_VIEW_PASSWORD ?>">
                    </div>
                </div>
                
                <div class="clear"></div>

                <div class="dual-input">
                    <p class="input-top-text"><?php echo _JS_NEW_PASSWORD ?></p>
                    <div class="fake-input-bg">
                        <input class="clean de-input password-input" type="password" placeholder="<?php echo _JS_NEW_PASSWORD ?>"  id="new_password" name="new_password">
                        <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="<?php echo _JS_VIEW_PASSWORD ?>" title="<?php echo _JS_VIEW_PASSWORD ?>">
                    </div>
                </div>

                <div class="dual-input second-dual-input">
                    <p class="input-top-text"><?php echo _JS_RETYPE_NEW_PASSWORD ?></p>
                    <div class="fake-input-bg">
                        <input class="clean de-input password-input" type="password" placeholder="<?php echo _JS_RETYPE_NEW_PASSWORD ?>" id="retype_new_password" name="retype_new_password">
                        <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionC()" alt="<?php echo _JS_VIEW_PASSWORD ?>" title="<?php echo _JS_VIEW_PASSWORD ?>">
                    </div>
                </div>

                <div class="clear"></div>

                <div class="width100 text-center top-bottom-distance">
                    <button class="clean button-width transparent-button dark-pink-button" name="submit"><?php echo _JS_SUBMIT ?></button>
                </div>
                
            </form>

        </div>
</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<?php include 'js.php'; ?>

<script>
function myFunctionA()
{
    var x = document.getElementById("current_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionC()
{
    var x = document.getElementById("retype_new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

</body>
</html>