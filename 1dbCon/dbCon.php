<?php
// DB SQL Connection
function connDB(){
    // Create connection
    $conn = new mysqli("localhost", "root", "","vidatech_samofa");//for localhost
    // $conn = new mysqli("localhost", "vidatech_usersamofa", "w7gUxWWFJ4iQ","vidatech_samofa"); //for testing
    // $conn = new mysqli("localhost", "ichibang_usersmf", "Z3FWugZRPMOu","ichibang_samofa"); //for testing
    // $conn = new mysqli("localhost", "ichibang_smftest", "gNVGP2GPuZL5","ichibang_samofa_test"); //for ichiban testing
    // $conn = new mysqli("localhost", "samofamy_user", "pR8m5XOhJADl","samofamy_live"); //for bigdomain testing


    mysqli_set_charset($conn,'UTF8');//because cant show chinese characters so need to include this to show
    //for when u insert chinese characters inside, because if dont include this then it will become unknown characters in the database
    mysqli_query($conn,"SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'")or die(mysqli_error($conn));

// Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    return $conn;
}
