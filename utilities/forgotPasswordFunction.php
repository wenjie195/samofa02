<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/ForgotUser.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';


function generateRandomString($length = 10) 
{
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();
     $username = rewrite($_POST['username']);
     $forgotPassword_email = rewrite($_POST['forgot_password']);
     $forgotPassword_email = filter_var($forgotPassword_email, FILTER_SANITIZE_EMAIL);
     
     if ($forgotPassword_email) {
          if(filter_var($forgotPassword_email, FILTER_VALIDATE_EMAIL))
     {
          $tempPass = generateRandomString();

          $tempPass = hash('sha256',$tempPass);
          $salt = substr(sha1(mt_rand()), 0, 100);
          $finalPassword = hash('sha256', $salt.$tempPass);

          $passwordUpdated = updateDynamicData($conn,"user"," WHERE email = ? ",array("password","salt"),array($finalPassword,$salt,$forgotPassword_email),"sss");
          // var_dump($passwordUpdated);

          // echo $forgotPassword_email;

          if($passwordUpdated)
          {
               $user = getUser($conn," WHERE email = ? ",array("email"),array($forgotPassword_email),"s");
               // var_dump($user);
               $verifyUser_debugMode = 2;
               $verifyUser_host = "mail.samofa.my";
               $verifyUser_usernameThatSendEmail = "noreply@samofa.my";                   // Sender Acc Username

               $verifyUser_password = "i7emkaibNTAN";                                                      // Sender Acc Password

               $verifyUser_smtpSecure = "ssl";                                                           // SMTP type
               $verifyUser_port = 465;                                                                   // SMTP port no
               $verifyUser_sentFromThisEmailName = "noreply@samofa.my";                                       // Sender Username
               $verifyUser_sentFromThisEmail = "noreply@samofa.my";                          // Sender Email
               $verifyUser_sendToThisEmailName = $user[0]->getUsername();                                // Recipient Username
               $verifyUser_sendToThisEmail = $forgotPassword_email;                                      // Recipient Email
               $verifyUser_isHtml = true;                                                                // Set To Html
               $verifyUser_subject = "Reset Password";  
     
               $verifyUser_body = "<p>Please reset your password in this ";
               $verifyUser_body .="<a href='http://samofa.my/resetPassword.php?uid=".$user[0]->getUid()."'>link</a>  ";
               $verifyUser_body .="using this key code below</p>";
               $verifyUser_body .="<p>Link to Reset Password  =  <a href='http://samofa.my/resetPassword.php?uid=".$user[0]->getUid()."'>http://samofa.my/resetPassword.php?uid=".$user[0]->getUid()."</a></p>";
               $verifyUser_body .="<p>Key Code = ".$tempPass."</p>";

               sendMailTo(
                    null,
                    $verifyUser_host,
                    $verifyUser_usernameThatSendEmail,
                    $verifyUser_password,
                    $verifyUser_smtpSecure,
                    $verifyUser_port, 
                    $verifyUser_sentFromThisEmailName,
                    $verifyUser_sentFromThisEmail,
                    $verifyUser_sendToThisEmailName,
                    $verifyUser_sendToThisEmail,
                    $verifyUser_isHtml,
                    $verifyUser_subject,
                    $verifyUser_body,
                    null
               );
               // $_SESSION['messageType'] = 1;
               // header('Location: ../index.php?type=8');
               // //echo "// forgot email success send email ";
               echo "<script>alert('reset password link has been sent to your email');window.location='../index.php'</script>"; 
          }
          else 
          {
               // echo "no user with ths email ";
               echo "<script>alert('no user with ths email !');window.location='../index.php'</script>"; 
          }

     }
     else 
     {
          // echo "wrong email format ";
          echo "<script>alert('wrong email format  !');window.location='../index.php'</script>"; 
     }
     }
     elseif($username){
          $userDetails = getUser($conn, "WHERE username =?",array("username"),array($username), "s");
          if ($userDetails) {
               $uid = $userDetails[0]->getUid();
               $username = $userDetails[0]->getUsername();
               $forgotUserDetails = getForgotUser($conn, "WHERE username =? AND status = 'PENDING'",array("username"), array($username), "s");
               if (!$forgotUserDetails) {
                    $forgotUserSend = insertDynamicData($conn,"forgot",array("uid","username"),array($uid,$username),"ss");
                    if ($forgotUserSend) {
                         echo "<script>alert('Request been sent to admin !');window.location='../index.php'</script>";
                    }else{
                         echo "<script>alert('Error. Please Contact Admin !');window.location='../index.php'</script>";
                    }
               }else{
                    echo "<script>alert('request on pending !');window.location='../index.php'</script>";
               }
          }else{
               echo "<script>alert('no user with this username !');window.location='../index.php'</script>";
          }
     }
}
else 
{
     header('Location: ../index.php');
}
?>