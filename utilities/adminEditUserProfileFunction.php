<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $username = rewrite($_POST["update_username"]);
    // $icno = rewrite($_POST["update_icno"]);
    $firstname = rewrite($_POST["update_firstname"]);
    $lastname = rewrite($_POST["update_lastname"]);
    $email = rewrite($_POST["update_email"]);
    $phone = rewrite($_POST["update_phone"]);
    $bankName = rewrite($_POST["update_bank_name"]);
    $bankAccountHolder = rewrite($_POST["update_bank_account_holder"]);
    $bankAccountNo = rewrite($_POST["update_bank_account_no"]);

    $userUid = rewrite($_POST["user_uid"]);

    // // FOR DEBUGGING 
    // echo "<br>";
    // echo $username."<br>";
    // echo $firstname."<br>";

    $user = getUser($conn," uid = ? ",array("uid"),array($userUid),"s");    

    if(!$user)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($username)
        {
            array_push($tableName,"username");
            array_push($tableValue,$username);
            $stringType .=  "s";
        }
        // if($icno)
        // {
        //     array_push($tableName,"icno");
        //     array_push($tableValue,$icno);
        //     $stringType .=  "s";
        // }
        if($firstname)
        {
            array_push($tableName,"firstname");
            array_push($tableValue,$firstname);
            $stringType .=  "s";
        }
        if($lastname)
        {
            array_push($tableName,"lastname");
            array_push($tableValue,$lastname);
            $stringType .=  "s";
        }
        if($email)
        {
            array_push($tableName,"email");
            array_push($tableValue,$email);
            $stringType .=  "s";
        }
        if($phone)
        {
            array_push($tableName,"phone_no");
            array_push($tableValue,$phone);
            $stringType .=  "s";
        }
        if($phone)
        {
            array_push($tableName,"phone_no");
            array_push($tableValue,$phone);
            $stringType .=  "s";
        }
        if($bankName)
        {
            array_push($tableName,"bank_name");
            array_push($tableValue,$bankName);
            $stringType .=  "s";
        }
        if($bankAccountHolder)
        {
            array_push($tableName,"bank_account_holder");
            array_push($tableValue,$bankAccountHolder);
            $stringType .=  "s";
        }
        if($bankAccountNo)
        {
            array_push($tableName,"bank_account_no");
            array_push($tableValue,$bankAccountNo);
            $stringType .=  "s";
        }

        array_push($tableValue,$userUid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            echo "<script>alert('Update Profile success !');window.location='../adminViewAllMembers.php'</script>";
        }
        else
        {
            echo "<script>alert('Fail to update profile!');window.location='../adminViewAllMembers.php'</script>";
        }
    }
    else
    {
        echo "<script>alert('This user is not available ');window.location='../adminViewAllMembers.php'</script>";
    }

}
else 
{
    header('Location: ../index.php');
}
?>
