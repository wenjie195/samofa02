<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ReferralPending.php';
require_once dirname(__FILE__) . '/../classes/SignUpProductPending.php';
require_once dirname(__FILE__) . '/../classes/UserPending.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Bonus.php';
require_once dirname(__FILE__) . '/../classes/OtherBonus.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewUser($conn,$uid,$memberID,$username,$email,$firstname,$lastname,$country,$phoneNo,$finalPassword,$salt,$dateCreated,$dateUpdated)
{
     if(insertDynamicData($conn,"user",array("uid","member_id","username","email","firstname","lastname","country","phone_no","password","salt","date_created","date_updated"),
          array($uid,$memberID,$username,$email,$firstname,$lastname,$country,$phoneNo,$finalPassword,$salt,$dateCreated,$dateUpdated),"ssssssssssss") === null)
     {
          // echo "gg";
     }
     else
     {    }
     return true;
}
function referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid,$dateCreated,$dateUpdated)
{
     if(insertDynamicData($conn,"referral_history",array("referrer_id","referral_id","referral_name","current_level","top_referrer_id","date_created","date_updated"),
     array($referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid,$dateCreated,$dateUpdated),"sssisss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
function SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$productPrice,$totalProductPrice,$phoneNo)
{
     if(insertDynamicData($conn,"signup_product",array("referrer_id","referrer_name","referral_id","referral_name","referral_fullname","product","price","total","contact"),
     array($referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$productPrice,$totalProductPrice,$phoneNo),"sssssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

$conn = connDB();
$uid = $_POST['uid'];
$bonusDetails = getBonusDetails($conn, "WHERE referral_id =?",array("referral_id"),array($uid), "s");
$otherBonusDetails = getOtherBonus($conn, "WHERE uid =?",array("uid"),array($uid), "s");

// for ($i=0; $i <count($bonusDetails) ; $i++) {

// 		echo "Bonus :".$bonusDetails[$i]->getReferralName()."<br>";
// 	}

// 	for ($j=0; $j <count($otherBonusDetails) ; $j++) {

// 		echo "Other Bonus :".$otherBonusDetails[$j]->getNewUser()."<br>";
// 	}

if (isset($_POST['accept'])) {
	echo "accept";
	$action = "APPROVED";
	$pendingUserDetails = getPendingUser($conn, "WHERE uid =?",array("uid"),array($uid), "s");
	$memberID = $pendingUserDetails[0]->getMemberId();
	$username = $pendingUserDetails[0]->getUsername();
	$firstname = $pendingUserDetails[0]->getFirstName();
	$lastname = $pendingUserDetails[0]->getLastName();
	$email = $pendingUserDetails[0]->getEmail();
	$phoneNo = $pendingUserDetails[0]->getPhoneNo();
	$country = $pendingUserDetails[0]->getCountry();
	$finalPassword = $pendingUserDetails[0]->getPassword();
	$dateCreated = $pendingUserDetails[0]->getDateCreated();
	$dateUpdated = $pendingUserDetails[0]->getDateUpdated();
	$salt = $pendingUserDetails[0]->getSalt();
	$referralFullname = $lastname.$firstname;

	$pendingReferral = getPendingReferral($conn, "WHERE referral_id =?",array("referral_id"),array($uid),"s");
	$referrerUid = $pendingReferral[0]->getReferrerId();
	$referralName = $pendingReferral[0]->getReferralName();
	$currentLevel = $pendingReferral[0]->getCurrentLevel();
	$topReferrerUid = $pendingReferral[0]->getTopReferrerId();
	$dateCreatedReferral = $pendingReferral[0]->getDateCreated();
	$dateUpdatedReferral = $pendingReferral[0]->getDateUpdated();

	// $referralDetails = getPendingSignUpProduct($conn, "WHERE referral_id =?",array("referral_id"),array($uid), "s");
	// $referrerName = $referralDetails[0]->getReferrerName();
	// $referralFullname = $referralDetails[0]->getReferralFullname();
	// $product = $referralDetails[0]->getProduct();
	// $subtotal = $referralDetails[0]->getPrice();
	// $total = $subtotal;

	$userReferrerDetails = getUser($conn, "WHERE uid = ?",array("uid"),array($referrerUid),"s");
	$currentdownline = $userReferrerDetails[0]->getDownlineNo();
	$currentRank = $userReferrerDetails[0]->getUserRank();
	$newDownlineNo = $currentdownline + 1;

	if($currentdownline >= 7 && $currentRank == 'Member') // if your member 7 and above
	{
		$rankUpdated = 'Agent';
	}else
	{
		$rankUpdated = $currentRank;
	}

	$userPendingUpdated = updateDynamicData($conn,"user_pending"," WHERE uid = ? ",array("user_status"),array($action,$uid),"ss");
	$referrerUserUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("downline_no","user_rank"),array($newDownlineNo,$rankUpdated,$referrerUid),"sss");
	$referralUpdated = updateDynamicData($conn,"referral_pending"," WHERE referral_id = ? ",array("status"),array($action,$uid),"ss");
	// $signUpProductUpdated = updateDynamicData($conn,"signup_product_pending"," WHERE referral_id = ? ",array("status"),array($action,$uid),"ss");

	// if ($userPendingUpdated && $referralUpdated && $signUpProductUpdated) 
	if ($userPendingUpdated && $referralUpdated) 
	{
		if (registerNewUser($conn,$uid,$memberID,$username,$email,$firstname,$lastname,$country,$phoneNo,$finalPassword,$salt,$dateCreated,$dateUpdated))
		{
			if (referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid,$dateCreatedReferral,$dateUpdatedReferral)) 
			{

				$_SESSION['messageType'] = 1;
				echo "success";
				header('location: ../adminPendingUser.php?type=1');

				// if (SignUpProduct($conn,$referrerUid,$referrerName,$uid,$referralName,$referralFullname,$product,$subtotal,$total,$phoneNo)) 
				// {
				// 	$_SESSION['messageType'] = 1;
				// 	echo "success";
				// 	header('location: ../adminPendingUser.php?type=1');
				// }
			}
		}
	}

}
?>