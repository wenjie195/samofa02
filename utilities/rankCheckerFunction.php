<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/SignUpProduct.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$userDetails = getUser($conn);

if ($userDetails) {
  for ($i=0; $i <count($userDetails) ; $i++) {
    $agentCnt = 0;
    $presidentCnt = 0;
    $partnerCnt = 0;
    $directorCnt = 0;
    $rankUpdated = null;
    $uid = $userDetails[$i]->getUid();
    $username = $userDetails[$i]->getUsername();
    // echo $username = $userDetails[$i]->getUsername()."<br>";
    // echo "-><br>";
    $allDownline = getWholeDownlineTree($conn,$uid,false);
    for ($j=0; $j <count($allDownline) ; $j++) {
      $downlineUid = $allDownline[$j]->getReferralId();
      // echo $downlineName = $allDownline[$j]->getReferralName()."<br>";
      $downlineDetails = getUser($conn, "WHERE uid =?",array("uid"),array($downlineUid),"s");
      $rank = $downlineDetails[0]->getUserRank();
      if ($rank == 'Agent') {
        $agentCnt+1;
      }elseif ($rank == 'President') {
        $presidentCnt+1;
      }elseif ($rank == 'Partner') {
        $partnerCnt+1;
      }elseif ($rank == 'Director') {
        $directorCnt+1;
      }

      if ($agentCnt >= 8) {
        $rankUpdated = 'President';
      }
      if ($presidentCnt >= 8) {
        $rankUpdated = 'Partner';
      }
      if ($partnerCnt >= 8) {
        $rankUpdated = 'Director';
      }

      if ($rankUpdated) {

        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($rankUpdated)
        {
             array_push($tableName,"user_rank");
             array_push($tableValue,$rankUpdated);
             $stringType .=  "s";
        }
        array_push($tableValue,$uid);
        $stringType .=  "s";
        $userUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($userUpdated){
          echo $username." : Rank Updated<br>";
        }

      }else {
        echo $username." : No Rank Updated<br>";
      }

    }
  }
}
 ?>
