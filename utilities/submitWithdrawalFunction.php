<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Withdrawal.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

function submitWithdrawal($conn,$uid,$withdrawalUid,$bankName,$bankAccHolder,$bankAccNo,$contact,$currentAmount,$amount,$finalAmount,$status)
{
     if(insertDynamicData($conn,"withdrawal",array("uid","withdrawal_uid","bank_name","bank_account_holder","bank_account_no","contact","current_amount","amount","final_amount","status"),
     array($uid,$withdrawalUid,$bankName,$bankAccHolder,$bankAccNo,$contact,$currentAmount,$amount,$finalAmount,$status),"ssssssssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $withdrawalUid = md5(uniqid());

     $bankName = rewrite($_POST["bank_name"]);
     $bankAccHolder = rewrite($_POST["bank_acc_holder"]);
     $bankAccNo = rewrite($_POST["bank_account_no"]);
     $amount = rewrite($_POST["withdrawal_amount"]);

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
     $contact = $userDetails[0]->getPhoneNo();
     $currentAmount = $userDetails[0]->getAmount();

     $finalAmount = $currentAmount - $amount;
     $status = "PENDING";

     $ePin  = $_POST['user_epin'];

     $user = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
     $userDetails = $user[0];
     $dbEPin =  $userDetails->getEpin();
     $dbEPinSalt =  $userDetails->getEpinSalt();

     $ePin_hashed = hash('sha256',$ePin);
     $ePin_hashed_salted = hash('sha256', $dbEPinSalt . $ePin_hashed);

     // //   FOR DEBUGGING
     // echo "<br>";
     // echo $uid."<br>";
     // echo $withdrawalUid."<br>";
     // echo $bankName."<br>";
     // echo $bankAccHolder."<br>";
     // echo $bankAccNo."<br>";
     // echo $amount."<br>";
     // echo $contact."<br>";
     // echo $currentAmount."<br>";
     // echo $finalAmount."<br>";
     
     if($ePin_hashed_salted == $dbEPin)
     {
          if($amount > $currentAmount)
          {
               echo "<script>alert('withdrawal amount is exceed than amount in wallet !!');window.location='../withdrawalTest.php'</script>";
          }
          else
          {
               if(submitWithdrawal($conn,$uid,$withdrawalUid,$bankName,$bankAccHolder,$bankAccNo,$contact,$currentAmount,$amount,$finalAmount,$status))
               {
                    if(isset($_POST['submit']))
                    {
                         $tableName = array();
                         $tableValue =  array();
                         $stringType =  "";
                         //echo "save to database";
                         if($finalAmount)
                         {
                              array_push($tableName,"amount");
                              array_push($tableValue,$finalAmount);
                              $stringType .=  "s";
                         }
                         if(!$finalAmount)
                         {    
                              $finalAmount = 0;
                              array_push($tableName,"amount");
                              array_push($tableValue,$finalAmount);
                              $stringType .=  "s";
                         }
                         array_push($tableValue,$uid);
                         $stringType .=  "s";
                         $orderUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                         if($orderUpdated)
                         {
                              // $_SESSION['messageType'] = 1;
                              // header('Location: ../adminShipping.php?type=11');
                              echo "<script>alert('Submit Withdrawal Request Successfully !');window.location='../myWallet.php'</script>";
                         }
                         else
                         {
                              echo "<script>alert('Unable to deduct credit in user side !');window.location='../myWallet.php'</script>";
                         }
                    }
                    else
                    {
                         echo "<script>alert('系统出现了问题 !');window.location='../myWallet.php'</script>";
                    }
               }
               else
               {
                    echo "<script>alert('Fail to Submit Withdrawal Request !');window.location='../myWallet.php'</script>";
               }
          }
     }
     else 
     {
          echo "<script>alert('E-Pin was wrong !!');window.location='../withdrawalTest.php'</script>";
     }  
}
else
{
     header('Location: ../index.php');
}
?>