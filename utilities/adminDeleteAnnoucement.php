<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Announcement.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $status  = "Delete";
    $type  = "3";

    $annouceUid = rewrite($_POST["announcement_uid"]);

    // // FOR DEBUGGING 
    // echo "<br>";
    // echo $username."<br>";
    // echo $firstname."<br>";

    $annouce = getAnnouncement($conn," uid = ? ",array("uid"),array($annouceUid),"s");    

    if(!$annouce)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        if($type)
        {
            array_push($tableName,"type");
            array_push($tableValue,$type);
            $stringType .=  "i";
        }
        array_push($tableValue,$annouceUid);
        $stringType .=  "s";
        $annouceUpdated = updateDynamicData($conn,"announcement"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($annouceUpdated)
        {
            echo "<script>alert('Annoucement Deleted !');window.location='../adminViewAnnouncement.php'</script>";
        }
        else
        {
            echo "<script>alert('Fail to delete annoucement!');window.location='../adminViewAnnouncement.php'</script>";
        }
    }
    else
    {
        echo "<script>alert('ERROR !!');window.location='../adminViewAnnouncement.php'</script>";
    }

}
else 
{
    header('Location: ../index.php');
}
?>
