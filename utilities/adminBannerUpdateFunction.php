<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../adminAccess.php';

require_once dirname(__FILE__) . '/../classes/Slider.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST['uid']);
    // $link = rewrite($_POST['link']);
    // $country = rewrite($_POST['country']);

    $oriImg = $_POST['ori_img'];

    $newImg = $timestamp.$_FILES['image_one']['name'];
    $target_dir = "../uploadsSlider/";
    $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
    // Select file type
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Valid file extensions
    $extensions_arr = array("jpg","jpeg","png","gif");
    if( in_array($imageFileType,$extensions_arr) )
    {
         move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$newImg);
    }

    // if($newImg != '')
    if(!$newImg)
    {
        // $imgName = $newImg;
        $imgName = $oriImg;
    }
    else
    {
        // $imgName = $oriImg;
        $imgName = $newImg;
    }

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $sliderDetails = getSlider($conn, "WHERE uid = ? ", array("uid"), array($uid), "s");

    // if(!$sliderDetails)
    if($sliderDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($imgName)
        {
            array_push($tableName,"img_name");
            array_push($tableValue,$imgName);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $profileUpdated = updateDynamicData($conn,"slider"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($profileUpdated)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../adminBannerAll.php?type=1');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../adminBannerAll.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../adminBannerAll.php?type=3');
    }

}
else 
{
    header('Location: ../index.php');
}
?>