<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Announcement.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/languageFunction.php';

$uid  = $_SESSION['uid'];

function addNewAnnoucement($conn,$uid,$title,$content,$status,$type)
{
     if(insertDynamicData($conn,"announcement",array("uid","title","content","status","type"),
          array($uid,$title,$content,$status,$type),"ssssi") === null)
     {
          // echo "gg";
     }
     else
     {    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     $title  = $_POST['title'];
     $content  = $_POST['content'];
     $status  = "Available";
     $type  = "1";

     if(addNewAnnoucement($conn,$uid,$title,$content,$status,$type))
     { 
          echo "<script>alert('Annoucement Added !!');window.location='../adminViewAnnouncement.php'</script>";  
     }
     else
     { 
          echo "<script>alert('fail to add annoucement !!');window.location='../adminAddAnnouncement.php'</script>";  
     }

}
else
{
     header('Location: ../index.php');
}
?>