<?php 
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/SignUpProduct.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/ExpiredUser.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$uid = $_POST['uid'];
$status = 'Inactive';
$expiredDetails = getUser($conn,"WHERE uid=?",array("uid"),array($uid), "s");

if (isset($_POST['submitBtn'])) {
	$userUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("status"),array($status,$uid),"ss");
	// $expiredUserUpdated = updateDynamicData($conn,"expired_user"," WHERE uid = ? ",array("status"),array($status,$uid),"ss");
	if ($userUpdated) {
		$_SESSION['messageType'] = 1;
		header('location: ../adminExpiredUser.php?type=1');
	}else{
		$_SESSION['messageType'] = 1;
		header('location: ../adminExpiredUser.php?type=2');
	}
}
 ?>