<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/SignUpProduct.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewUser($conn,$uid,$memberID,$username,$email,$firstname,$lastname,$country,$phoneNo,$finalPassword,$salt)
{
     if(insertDynamicData($conn,"user",array("uid","member_id","username","email","firstname","lastname","country","phone_no","password","salt"),
          array($uid,$memberID,$username,$email,$firstname,$lastname,$country,$phoneNo,$finalPassword,$salt),"ssssssssss") === null)
     {
          // echo "gg";
     }
     else
     {    }
     return true;
}

function referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid)
{
     if(insertDynamicData($conn,"referral_history",array("referrer_id","referral_id","referral_name","current_level","top_referrer_id"),
     array($referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid),"sssis") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$phoneNo)
{
     if(insertDynamicData($conn,"signup_product",array("referrer_id","referrer_name","referral_id","referral_name","referral_fullname","product","contact"),
     array($referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$phoneNo),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $memberID = uniqid();

     $country = rewrite($_POST['register_country']);
     $firstname = rewrite($_POST['register_firstname']);
     $lastname = rewrite($_POST['register_lastname']);
     $username = rewrite($_POST['register_username']);
     $email = rewrite($_POST['register_email']);
     $phoneNo = rewrite($_POST['register_mobileno']);

     $sponsorID = rewrite($_POST['sponsor_id']);
     $product = rewrite($_POST['register_product']);

     $userRanking = rewrite($_POST['user_ranking']);
     $downlineAmount = rewrite($_POST['downline_amount']);

     $register_password = "123321";
     $register_retype_password = "123321";
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     // $referralFullname = $lastname .'&nbsp'. $firstname;
     $referralFullname = $lastname . $firstname;


     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $memberID."<br>";
     // echo $country."<br>";
     // echo $firstname."<br>";
     // echo $lastname."<br>";
     // echo $username."<br>";
     // echo $email ."<br>";
     // echo $phoneNo."<br>";
     // echo $sponsorID."<br>";
     // // echo $userType."<br>";
     // echo $salt."<br>";
     // echo $finalPassword."<br>";
     // echo $referralFullname."<br>";

     if($sponsorID)
     {
          $referrerUserRows = getUser($conn," WHERE member_id = ? ",array("member_id"),array($sponsorID),"s");
          if($referrerUserRows)
          {
               $referrerUid = $referrerUserRows[0]->getUid();
               $referrerName = $referrerUserRows[0]->getUsername();

               $downlineAValue = $referrerUserRows[0]->getRankAValue();
               $downlineBValue = $referrerUserRows[0]->getRankBValue();
               $downlineCValue = $referrerUserRows[0]->getRankCValue();
               $downlineDValue = $referrerUserRows[0]->getRankDValue();

               $referralName = $username;
               $topReferrerUid = $referrerUid;//assign top referrer id to this guy 1st, if he is not the top, will be overwritten
               $currentLevel = 1;
               $getUplineCurrentLevel = 1;

               $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($referrerUid),"s");
               if($referralHistoryRows)
               {
                    $topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
                    $currentLevel = $referralHistoryRows[0]->getCurrentLevel() + 1;
               }
               $referralNewestRows = getReferralHistory($conn,"WHERE referral_name = ?", array("referral_name"),array($referrerName), "s");
               if($referralNewestRows)
               {
                    $getUplineCurrentLevel = $referralNewestRows[0]->getCurrentLevel() + 1;

                    $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($_POST['register_username']),"s");
                    $usernameDetails = $usernameRows[0];
               
                    $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email']),"s");
                    $userEmailDetails = $userEmailRows[0];
               
                    $userPhoneRows = getUser($conn," WHERE phone_no = ? ",array("phone_no"),array($_POST['register_mobileno']),"s");
                    $userPhoneDetails = $userPhoneRows[0];

                    if (!$usernameDetails && !$userEmailDetails && !$userPhoneDetails)
                    {

                         // if($userRanking == 'A')
                         if($userRanking == 'Member')
                         {
                              // if($downlineAmount < '10')
                              if($downlineAmount <= '9')
                              {
                                   $newDownline = $downlineAValue + 1;
                                   // $newRanking = 'A';

                                   if(registerNewUser($conn,$uid,$memberID,$username,$email,$firstname,$lastname,$country,$phoneNo,$finalPassword,$salt))
                                   {                    
                                        if(referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid))
                                        {
                                             $referralUid = $uid;
                                             if(SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$phoneNo))
                                             {
                                                  // echo "register successfully";
                                                  // echo "<script>alert('Register Successfully !');window.location='../register.php'</script>";    

                                                  if(isset($_POST['register']))
                                                  {
                                                       $tableName = array();
                                                       $tableValue =  array();
                                                       $stringType =  "";
                                                       //echo "save to database";
                                                       if($newDownline)
                                                       {
                                                       array_push($tableName,"rank_a_value");
                                                       array_push($tableValue,$newDownline);
                                                       $stringType .=  "s";
                                                       }
                                                       array_push($tableValue,$referrerUid);
                                                       $stringType .=  "s";
                                                       $newDownlineAmount = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                       if($newDownlineAmount)
                                                       {
                                                            // echo "update upline data successfully";
                                                            echo "<script>alert('Register Successfully !');window.location='../register.php'</script>";   
                                                       }
                                                       else 
                                                       {
                                                            echo "fail";
                                                       }
                                                  }
                                                  else 
                                                  {
                                                       echo "gg";
                                                  }

                                             }
                                             else
                                             { 
                                                  echo "fail to register sign up product";
                                             }
                                        }
                                        else
                                        { 
                                             echo "fail to register via upline";
                                        }
                                   }
                                   else
                                   { 
                                        echo "fail to register";
                                   }

                              }
                              elseif($downlineAmount > '9')
                              // elseif($downlineAmount > '10')
                              {
                                   $newDownline = $downlineBValue + 1;
                                   // $newRanking = 'B';
                                   $newRanking = 'Rising Star';

                                   if(registerNewUser($conn,$uid,$memberID,$username,$email,$firstname,$lastname,$country,$phoneNo,$finalPassword,$salt))
                                   {                    
                                        if(referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid))
                                        {
                                             $referralUid = $uid;
                                             if(SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$phoneNo))
                                             {
                                                  // echo "register successfully"; 

                                                  if(isset($_POST['register']))
                                                  {
                                                       $tableName = array();
                                                       $tableValue =  array();
                                                       $stringType =  "";
                                                       //echo "save to database";
                                                       if($newDownline)
                                                       {
                                                       // array_push($tableName,"rank_b_value");
                                                       array_push($tableName,"rank_b_value");
                                                       array_push($tableValue,$newDownline);
                                                       $stringType .=  "s";
                                                       }
                                                       if($newRanking)
                                                       {
                                                       array_push($tableName,"rank");
                                                       array_push($tableValue,$newRanking);
                                                       $stringType .=  "s";
                                                       }
                                                       array_push($tableValue,$referrerUid);
                                                       $stringType .=  "s";
                                                       $newDownlineAmount = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                       if($newDownlineAmount)
                                                       {
                                                            // echo "update upline data successfully";
                                                            echo "<script>alert('Register Successfully !');window.location='../register.php'</script>";   
                                                       }
                                                       else 
                                                       {
                                                            echo "fail";
                                                       }
                                                  }
                                                  else 
                                                  {
                                                       echo "gg";
                                                  }

                                             }
                                             else
                                             { 
                                                  echo "fail to register sign up product";
                                             }
                                        }
                                        else
                                        { 
                                             echo "fail to register via upline";
                                        }
                                   }
                                   else
                                   { 
                                        echo "fail to register";
                                   }

                              }
                         }
                         // elseif($userRanking == 'B')
                         elseif($userRanking == 'Rising Star')
                         {
                              if($downlineAmount <= '9')
                              {
                                   $newDownline = $downlineBValue + 1;
                                   // $newRanking = 'B';

                                   if(registerNewUser($conn,$uid,$memberID,$username,$email,$firstname,$lastname,$country,$phoneNo,$finalPassword,$salt))
                                   {                    
                                        if(referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid))
                                        {
                                             $referralUid = $uid;
                                             if(SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$phoneNo))
                                             {
                                                  // echo "register successfully";
                                                  // echo "<script>alert('Register Successfully !');window.location='../register.php'</script>";    

                                                  if(isset($_POST['register']))
                                                  {
                                                       $tableName = array();
                                                       $tableValue =  array();
                                                       $stringType =  "";
                                                       //echo "save to database";
                                                       if($newDownline)
                                                       {
                                                       array_push($tableName,"rank_b_value");
                                                       array_push($tableValue,$newDownline);
                                                       $stringType .=  "s";
                                                       }
                                                       array_push($tableValue,$referrerUid);
                                                       $stringType .=  "s";
                                                       $newDownlineAmount = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                       if($newDownlineAmount)
                                                       {
                                                            // echo "update upline data successfully";
                                                            // echo "<br>";
                                                            echo "<script>alert('Register Successfully !');window.location='../register.php'</script>";   
                                                       }
                                                       else 
                                                       {
                                                            echo "fail";
                                                       }
                                                  }
                                                  else 
                                                  {
                                                       echo "gg";
                                                  }

                                             }
                                             else
                                             { 
                                                  echo "fail to register sign up product";
                                             }
                                        }
                                        else
                                        { 
                                             echo "fail to register via upline";
                                        }
                                   }
                                   else
                                   { 
                                        echo "fail to register";
                                   }

                              }
                              // elseif($downlineAmount >= '10')
                              elseif($downlineAmount > '9')
                              {
                                   $newDownline = $downlineCValue + 1;
                                   // $newRanking = 'C';
                                   $newRanking = 'Group Star';

                                   if(registerNewUser($conn,$uid,$memberID,$username,$email,$firstname,$lastname,$country,$phoneNo,$finalPassword,$salt))
                                   {                    
                                        if(referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid))
                                        {
                                             $referralUid = $uid;
                                             if(SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$phoneNo))
                                             {
                                                  // echo "register successfully"; 

                                                  if(isset($_POST['register']))
                                                  {
                                                       $tableName = array();
                                                       $tableValue =  array();
                                                       $stringType =  "";
                                                       //echo "save to database";
                                                       if($newDownline)
                                                       {
                                                       // array_push($tableName,"rank_c_value");
                                                       array_push($tableName,"rank_b_value");
                                                       array_push($tableValue,$newDownline);
                                                       $stringType .=  "s";
                                                       }
                                                       if($newRanking)
                                                       {
                                                       array_push($tableName,"rank");
                                                       array_push($tableValue,$newRanking);
                                                       $stringType .=  "s";
                                                       }
                                                       array_push($tableValue,$referrerUid);
                                                       $stringType .=  "s";
                                                       $newDownlineAmount = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                       if($newDownlineAmount)
                                                       {
                                                            // echo "update upline data successfully";
                                                            // echo "<br>";
                                                            echo "<script>alert('Register Successfully !');window.location='../register.php'</script>";   
                                                       }
                                                       else 
                                                       {
                                                            echo "fail";
                                                       }
                                                  }
                                                  else 
                                                  {
                                                       echo "gg";
                                                  }

                                             }
                                             else
                                             { 
                                                  echo "fail to register sign up product";
                                             }
                                        }
                                        else
                                        { 
                                             echo "fail to register via upline";
                                        }
                                   }
                                   else
                                   { 
                                        echo "fail to register";
                                   }

                              }
                         }


                         elseif($userRanking == 'Group Star')
                         {
                              if($downlineAmount <= '9')
                              {
                                   $newDownline = $downlineBValue + 1;
                                   // $newRanking = 'B';

                                   if(registerNewUser($conn,$uid,$memberID,$username,$email,$firstname,$lastname,$country,$phoneNo,$finalPassword,$salt))
                                   {                    
                                        if(referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid))
                                        {
                                             $referralUid = $uid;
                                             if(SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$phoneNo))
                                             {
                                                  // echo "register successfully";
                                                  // echo "<script>alert('Register Successfully !');window.location='../register.php'</script>";    

                                                  if(isset($_POST['register']))
                                                  {
                                                       $tableName = array();
                                                       $tableValue =  array();
                                                       $stringType =  "";
                                                       //echo "save to database";
                                                       if($newDownline)
                                                       {
                                                       array_push($tableName,"rank_b_value");
                                                       array_push($tableValue,$newDownline);
                                                       $stringType .=  "s";
                                                       }
                                                       array_push($tableValue,$referrerUid);
                                                       $stringType .=  "s";
                                                       $newDownlineAmount = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                       if($newDownlineAmount)
                                                       {
                                                            // echo "update upline data successfully";
                                                            // echo "<br>";
                                                            echo "<script>alert('Register Successfully !');window.location='../register.php'</script>";   
                                                       }
                                                       else 
                                                       {
                                                            echo "fail";
                                                       }
                                                  }
                                                  else 
                                                  {
                                                       echo "gg";
                                                  }

                                             }
                                             else
                                             { 
                                                  echo "fail to register sign up product";
                                             }
                                        }
                                        else
                                        { 
                                             echo "fail to register via upline";
                                        }
                                   }
                                   else
                                   { 
                                        echo "fail to register";
                                   }

                              }
                              // elseif($downlineAmount >= '10')
                              elseif($downlineAmount > '9')
                              {
                                   $newDownline = $downlineCValue + 1;
                                   // $newRanking = 'C';
                                   $newRanking = 'Group Star';

                                   if(registerNewUser($conn,$uid,$memberID,$username,$email,$firstname,$lastname,$country,$phoneNo,$finalPassword,$salt))
                                   {                    
                                        if(referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid))
                                        {
                                             $referralUid = $uid;
                                             if(SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$phoneNo))
                                             {
                                                  // echo "register successfully"; 

                                                  if(isset($_POST['register']))
                                                  {
                                                       $tableName = array();
                                                       $tableValue =  array();
                                                       $stringType =  "";
                                                       //echo "save to database";
                                                       if($newDownline)
                                                       {
                                                       // array_push($tableName,"rank_c_value");
                                                       array_push($tableName,"rank_b_value");
                                                       array_push($tableValue,$newDownline);
                                                       $stringType .=  "s";
                                                       }
                                                       if($newRanking)
                                                       {
                                                       array_push($tableName,"rank");
                                                       array_push($tableValue,$newRanking);
                                                       $stringType .=  "s";
                                                       }
                                                       array_push($tableValue,$referrerUid);
                                                       $stringType .=  "s";
                                                       $newDownlineAmount = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                       if($newDownlineAmount)
                                                       {
                                                            // echo "update upline data successfully";
                                                            // echo "<br>";
                                                            echo "<script>alert('Register Successfully !');window.location='../register.php'</script>";   
                                                       }
                                                       else 
                                                       {
                                                            echo "fail";
                                                       }
                                                  }
                                                  else 
                                                  {
                                                       echo "gg";
                                                  }

                                             }
                                             else
                                             { 
                                                  echo "fail to register sign up product";
                                             }
                                        }
                                        else
                                        { 
                                             echo "fail to register via upline";
                                        }
                                   }
                                   else
                                   { 
                                        echo "fail to register";
                                   }

                              }
                         }


                         else
                         {
                              echo "to be find out";
                         }

                    }
                    else
                    { 
                         echo "register details has been used by others";
                    }

               }
               else
               {
                    echo "unable to find related data sponsor ID in Referral table !!";
               }
          }
          else
          {
               echo "unable to find related data sponsor ID!!";
          } 
     }  
     else
     {
          echo "invalid sponsor ID !!";
     } 

     // if($sponsorID)
     // {
     //      $referrerUserRows = getUser($conn," WHERE member_id = ? ",array("member_id"),array($sponsorID),"s");
     //      if($referrerUserRows)
     //      {
     //           $referrerUid = $referrerUserRows[0]->getUid();
     //           $referrerName = $referrerUserRows[0]->getUsername();
     //           // $referralUid = $referrerUserRows[0]->getUid();
     //           $referralName = $username;
     //           $topReferrerUid = $referrerUid;//assign top referrer id to this guy 1st, if he is not the top, will be overwritten
     //           $currentLevel = 1;
     //           $getUplineCurrentLevel = 1;

     //           $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($referrerUid),"s");
     //           if($referralHistoryRows)
     //           {
     //                $topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
     //                $currentLevel = $referralHistoryRows[0]->getCurrentLevel() + 1;
     //           }
     //           $referralNewestRows = getReferralHistory($conn,"WHERE referral_name = ?", array("referral_name"),array($referrerName), "s");
     //           if($referralNewestRows)
     //           {
     //                $getUplineCurrentLevel = $referralNewestRows[0]->getCurrentLevel() + 1;

     //                $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($_POST['register_username']),"s");
     //                $usernameDetails = $usernameRows[0];
               
     //                $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email']),"s");
     //                $userEmailDetails = $userEmailRows[0];
               
     //                $userPhoneRows = getUser($conn," WHERE phone_no = ? ",array("phone_no"),array($_POST['register_mobileno']),"s");
     //                $userPhoneDetails = $userPhoneRows[0];

     //                if (!$usernameDetails && !$userEmailDetails && !$userPhoneDetails)
     //                {

     //                     if(registerNewUser($conn,$uid,$memberID,$username,$email,$firstname,$lastname,$country,$phoneNo,$finalPassword,$salt))
     //                     {
     //                          // echo "done register";
     //                          // echo "<script>alert('Register Success !');window.location='../userDashboard.php'</script>";    
               
     //                          if(referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid))
     //                          {
     //                               // echo "done register";
     //                               // echo "<script>alert('Register Success !');window.location='../userDashboard.php'</script>";    

     //                               $referralUid = $uid;

     //                               if(SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$phoneNo))
     //                               {
     //                                    // echo "register successfully";
     //                                    echo "<script>alert('Register Successfully !');window.location='../register.php'</script>";    
     //                               }
     //                               else
     //                               { 
     //                                    // echo "fail to register sign up product";
     //                                    echo "<script>alert('Register, But Fail For Sign Up Product');window.location='../register.php'</script>";
     //                               }
               
     //                          }
     //                          else
     //                          { 
     //                               // echo "fail to register via upline";
     //                               echo "<script>alert('Register success without upline!');window.location='../register.php'</script>";
     //                          }
               
     //                     }
     //                     else
     //                     { 
     //                          // echo "fail to register";
     //                          echo "<script>alert('Fail to register !');window.location='../index.php'</script>";
     //                     }

     //                }
     //                else
     //                { 
     //                     echo "register details has been used by others";
     //                }

     //           }
     //           else
     //           {
     //                // echo "register error with referral ";
     //                echo "unable to find related data sponsor ID in Referral table !!";
     //           }
     //      }
     //      else
     //      {
     //           echo "unable to find related data sponsor ID!!";
     //      } 
     // }  
     // else
     // {
     //      echo "invalid sponsor ID !!";
     // } 
}
else 
{
     header('Location: ../index.php');
}

?>