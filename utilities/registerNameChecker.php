<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/UserPending.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$username = $_POST['username'];
// $username = 'samofaca';
$userDetails = getuser($conn, "WHERE username =?",array("username"),array($username), "s");
$userPendingDetails = getPendingUser($conn, "WHERE username =? AND user_status != 'REJECTED'",array("username"),array($username), "s");

if (empty($userDetails || $userPendingDetails)) {
  $alert = "Available Username.";
  $alertBin = 0;
}else {
  $alert = "Existing Username.";
  $alertBin = 1;
}
$result[] = array("result" => $alert, "alertBin" => $alertBin);
echo json_encode($result);
 ?>
