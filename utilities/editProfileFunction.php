<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    echo $username = rewrite($_POST["update_username"]);
    // $icno = rewrite($_POST["update_icno"]);
    // echo $firstname = rewrite($_POST["update_firstname"]);
    // echo $lastname = rewrite($_POST["update_lastname"]);
    $email = rewrite($_POST["update_email"]);
    $phone = str_replace("+6", "", $_POST["update_phone"]);
    $bankName = rewrite($_POST["update_bank_name"]);
    $bankAccountHolder = rewrite($_POST["update_bank_account_holder"]);
    $bankAccountNo = rewrite($_POST["update_bank_account_no"]);
    $address = rewrite($_POST["address"]);
    $addressTwo = rewrite($_POST["address_two"]);
    $zipcode = rewrite($_POST["zipcode"]);
    $country = rewrite($_POST["country"]);
    $state = rewrite($_POST["state"]);
    $city = rewrite($_POST["city"]);

    // // FOR DEBUGGING 
    // echo "<br>";
    // echo $username."<br>";
    // echo $firstname."<br>";

    $user = getUser($conn," uid = ? ",array("uid"),array($uid),"s");    

    if(!$user)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($username)
        {
            array_push($tableName,"username");
            array_push($tableValue,$username);
            $stringType .=  "s";
        }
        // if($icno)
        // {
        //     array_push($tableName,"icno");
        //     array_push($tableValue,$icno);
        //     $stringType .=  "s";
        // }
        // if($firstname)
        // {
        //     array_push($tableName,"firstname");
        //     array_push($tableValue,$firstname);
        //     $stringType .=  "s";
        // }
        // if($lastname)
        // {
        //     array_push($tableName,"lastname");
        //     array_push($tableValue,$lastname);
        //     $stringType .=  "s";
        // }
        if($email)
        {
            array_push($tableName,"email");
            array_push($tableValue,$email);
            $stringType .=  "s";
        }
        if($phone)
        {
            array_push($tableName,"phone_no");
            array_push($tableValue,$phone);
            $stringType .=  "s";
        }
        if($phone)
        {
            array_push($tableName,"phone_no");
            array_push($tableValue,$phone);
            $stringType .=  "s";
        }
        if($bankName)
        {
            array_push($tableName,"bank_name");
            array_push($tableValue,$bankName);
            $stringType .=  "s";
        }
        if($bankAccountHolder)
        {
            array_push($tableName,"bank_account_holder");
            array_push($tableValue,$bankAccountHolder);
            $stringType .=  "s";
        }
        if($bankAccountNo)
        {
            array_push($tableName,"bank_account_no");
            array_push($tableValue,$bankAccountNo);
            $stringType .=  "s";
        }
        if($address)
        {
            array_push($tableName,"address");
            array_push($tableValue,$address);
            $stringType .=  "s";
        }
        if($addressTwo)
        {
            array_push($tableName,"address_two");
            array_push($tableValue,$addressTwo);
            $stringType .=  "s";
        }
        if($zipcode)
        {
            array_push($tableName,"zipcode");
            array_push($tableValue,$zipcode);
            $stringType .=  "s";
        }
        if($country)
        {
            array_push($tableName,"nation");
            array_push($tableValue,$country);
            $stringType .=  "s";
        }
        if($state)
        {
            array_push($tableName,"state");
            array_push($tableValue,$state);
            $stringType .=  "s";
        }
        if($city)
        {
            array_push($tableName,"city");
            array_push($tableValue,$city);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            echo "<script>alert('Update Profile success !');window.location='../editProfile.php'</script>";
        }
        else
        {
            echo "<script>alert('Fail to update profile!');window.location='../editProfile.php'</script>";
        }
    }
    else
    {
        echo "<script>alert('This user is not available ');window.location='../editProfile.php'</script>";
    }

}
else 
{
    header('Location: ../index.php');
}
?>
