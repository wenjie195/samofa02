<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/MilesStones.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $milesStonesUid = md5(uniqid());

     $milesStonesUid = rewrite($_POST["uid"]);
     $bonus = rewrite($_POST["bonus"]);

     // $approvedStatus = rewrite($_POST["approved_status"]);
     // $rejectedStatus = rewrite($_POST["rejected_status"]);

     // //   FOR DEBUGGING
     // echo "<br>";
     // echo $milesStonesUid."<br>";
     // // echo $approvedStatus."<br>";
     // // echo $rejectedStatus."<br>";
     // echo $method."<br>";
     // echo $reference."<br>";
     // echo $rejectedReason."<br>";

     if(isset($_POST['approve']))
     {
       $status = 'APPROVED';

          if(isset($_POST['uid']))
          {
              $updateMilesStonesStatus = updateDynamicData($conn,'milestones', 'WHERE uid =? AND bonus =?',array('status'),array('APPROVED',$milesStonesUid,$bonus), 'sss');
               if($updateMilesStonesStatus)
               {
                    // echo "aprroved withdrawal success";
                    echo "<script>alert('MilesStones approved !!');window.location='../adminViewMilesStones.php'</script>";
               }
               else
               {
                    echo "fail";
               }
          }
          else
          {
               echo "error level 222";
          }
     }
     elseif(isset($_POST['reject']))
     {
          $updateRejectWithdrawal = updateDynamicData($conn,'milestones', 'WHERE uid =? AND bonus =?',array('status'),array('REJECTED',$milesStonesUid,$bonus), 'sss');;
               if($updateRejectWithdrawal)
               {
                 echo "<script>alert('MilesStones rejected !!');window.location='../adminViewMilesStones.php'</script>";
               }
               else
               {
                    echo "fail";
               }
     }
}
else
{
     header('Location: ../index.php');
}
?>
