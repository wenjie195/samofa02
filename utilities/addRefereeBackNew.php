<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/SignUpProduct.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/UserPending.php';
require_once dirname(__FILE__) . '/../classes/Product.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/memberIdGenerator.php';

function registerNewUser($conn,$uid,$memberID,$username,$email,$firstname,$lastname,$country,$phoneNo,$finalPassword,$salt)
{
     if(insertDynamicData($conn,"user_pending",array("uid","member_id","username","email","firstname","lastname","country","phone_no","password","salt"),
          array($uid,$memberID,$username,$email,$firstname,$lastname,$country,$phoneNo,$finalPassword,$salt),"ssssssssss") === null)
     {
          // echo "gg";
     }
     else
     {    }
     return true;
}

function referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid)
{
     if(insertDynamicData($conn,"referral_pending",array("referrer_id","referral_id","referral_name","current_level","top_referrer_id"),
     array($referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid),"sssis") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$productPrice,$totalProductPrice,$phoneNo)
{
     if(insertDynamicData($conn,"signup_product_pending",array("referrer_id","referrer_name","referral_id","referral_name","referral_fullname","product","price","total","contact"),
     array($referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$productPrice,$totalProductPrice,$phoneNo),"sssssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
function commission($conn,$referralName,$getUplineUid,$getUplineName,$getUplineCurrentLevel,$getUpline,$topReferrerUid,$referralUid,$uplineBonus,$referralName_referrer)
{
     if(insertDynamicData($conn,"bonus",array("referral_name","referrer_id","referrer_name","current_level","register_downline_no","top_referrer_id","referral_id","amount","signupby"),
     array($referralName,$getUplineUid,$getUplineName,$getUplineCurrentLevel,$getUpline,$topReferrerUid,$referralUid,$uplineBonus,$referralName_referrer),"sssiissis") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
function leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName)
{
     if(insertDynamicData($conn,"other_bonus",array("uid","receive_username","receive_uid","amount","bonus_type","new_user"),
     array($referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName),"sssiss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
function NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$register_username_referrer)
{
     if(insertDynamicData($conn,"bonus",array("referral_name","referrer_id","referrer_name","current_level","register_downline_no","top_referrer_id","referral_id","amount","signupby"),
     array($referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$register_username_referrer),"sssiissis") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
  $conn = connDB();

  $uid = $_SESSION['uid'];

  $registerUid = md5(uniqid());

/****************************member id generator******************************/
 $memberID = MemberIdGenerator($conn);
/****************************member id generator******************************/

  $country = rewrite($_POST['register_country']);
  $firstname = rewrite($_POST['register_firstname']);
  $lastname = rewrite($_POST['register_lastname']);
  $username = rewrite($_POST['register_username']);
  $email = rewrite($_POST['register_email']);
  $phoneNo = rewrite($_POST['register_mobileno']);

  $sponsorID = rewrite($_POST['sponsor_id']);
  $productId = rewrite($_POST['register_product']);
  $productDetails = getProduct($conn,"WHERE id=?",array("id"),array($productId), "s");
  $product = $productDetails[0]->getId();
  $productPrice = $productDetails[0]->getPrice();
  $totalProductPrice = $productPrice;

  $userRanking = rewrite($_POST['user_ranking']);
  $downlineAmount = rewrite($_POST['downline_amount']);

  // $register_password = "123321";
  // $register_retype_password = "123321";
  $register_password = rewrite($_POST['password']);
  $register_retype_password = rewrite($_POST['retype_password']);
  $password = hash('sha256',$register_password);
  $salt = substr(sha1(mt_rand()), 0, 100);
  $finalPassword = hash('sha256', $salt.$password);

  // $referralFullname = $lastname .'&nbsp'. $firstname;
  $referralFullname = $lastname . $firstname;

  if($sponsorID)
    {
         $referrerUserRows = getUser($conn," WHERE member_id = ? ",array("member_id"),array($sponsorID),"s");

         if($referrerUserRows)
         {
              $referrerUid = $referrerUserRows[0]->getUid();
              $referrerName = $referrerUserRows[0]->getUsername();
              $referralName = $username;
              $topReferrerUid = $referrerUid;//assign top referrer id to this guy 1st, if he is not the top, will be overwritten
              $currentLevel = 1;
              $getUplineCurrentLevel = 1;

              $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($referrerUid),"s");
              if($referralHistoryRows)
              {
                   $topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
                   $currentLevel = $referralHistoryRows[0]->getCurrentLevel() + 1;
              }
              $referralNewestRows = getReferralHistory($conn,"WHERE referral_name = ?", array("referral_name"),array($referrerName), "s");
              if($referralNewestRows)
              {
                   //$topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
                   $getUplineCurrentLevel = $referralNewestRows[0]->getCurrentLevel() + 1;
              }

              $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($_POST['register_username']),"s");

              if (!$usernameRows)
              {
                   // $userPointReferee = $userPointDetails->getUserPoint();
                   // $dckUid = $userPointDetails->getUid();

                   // if ($userPointReferee >= 0)
                   // {
                        if(registerNewUser($conn,$registerUid,$memberID,$username,$email,$firstname,$lastname,$country,$phoneNo,$finalPassword,$salt))
                        {
                          if(referralData($conn,$referrerUid,$registerUid,$referralName,$currentLevel,$topReferrerUid))
                          {
                             $userRows = getUser($conn," WHERE username = ? ",array("username"),array($referrerName),"s");
                             $userDetails = $userRows[0];

                             $referrerId = $userDetails -> getUid();//referrer_id

                             $userPointRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
                             $userPointDetails = $userPointRows[0];

                             $getUplineRows = getUser($conn, "WHERE uid = ? ", array("uid"), array($referrerId), "s");
                             $getUplineDetails = $getUplineRows[0];

                             $getUpline = $getUplineDetails -> getDownlineNo();
                             $getUplineName = $getUplineDetails -> getUsername();
                             $getUplineUid = $getUplineDetails -> getUid();

                             // $pointReferee = $userPointDetails->getUserPoint();//based on uid
                             $downlineNumber = $userDetails->getDownlineNo();
                             $cash = $userDetails->getSales();
                             $referralUid = $registerUid;
                             $getUid = $userDetails -> getUid();
                             $currentStatus = $userDetails->getUserRank();
                             $activeStatus = $userDetails->getStatus();

  //===============================================================Director Direct Bonus=============================================================================================

                             if ($currentStatus == 'Director' && $activeStatus != 'Inactive')
                             {
                                  $bonus = 120;
                                  $newBonus = 150; // partner Bonus
                                  $bonusType = 'Leadership';
                                  $currentStatus = 'Director';
                                  $uplineBonus = 0;
                                  $currentBonus = $userDetails -> getBonus();
                                  $totalBonus = $currentBonus + $bonus + $newBonus;
                                  $registerDownline = $userDetails->getDownlineNo() + 1;
                                  $receiveUid = $userDetails -> getUid();
                                  $receiveUsername = $userDetails -> getUsername();
                                  // $totalPoint = $pointReferee -300;
                                  $totalCash = $cash + $bonus;

                                  if (NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$referralName))
                                  {

                                       if(SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$productPrice,$totalProductPrice,$phoneNo))
                                       {}// echo "success";
                                       else
                                       {}// echo "fail";
                                  }
                                  else
                                  {
                                       // $_SESSION['messageType'] = 2;
                                       // header('Location: ../wallet.php?type=1');
                                  }
                                  if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
                                  {
                                    // $tableName = array();
                                    // $tableValue =  array();
                                    // $stringType =  "";
                                    // //echo "save to database";
                                    // if($registerDownline)
                                    // {
                                    //      array_push($tableName,"downline_no");
                                    //      array_push($tableValue,$registerDownline);
                                    //      $stringType .=  "i";
                                    // }

                                    // array_push($tableValue,$sponsorID);
                                    // $stringType .=  "s";
                                    // $passwordUpdated = updateDynamicData($conn,"user"," WHERE member_id = ? ",$tableName,$tableValue,$stringType);
                                    // if($passwordUpdated)
                                    // { }
                                    // else
                                    // { }
                                  }
                                  }

  //===============================================================End of Director Direct Bonus=============================================================================================
  //===============================================================Partner Direct Bonus=============================================================================================

                             if ($currentStatus == 'Partner' && $activeStatus != 'Inactive')
                             {
                                  $bonus = 120;
                                  $newBonus = 120; // president Bonus
                                  $bonusType = 'Leadership';
                                  $currentStatus = 'Partner';
                                  $uplineBonus = 0;
                                  $currentBonus = $userDetails -> getBonus();
                                  $totalBonus = $currentBonus + $bonus + $newBonus;
                                  $registerDownline = $userDetails->getDownlineNo() + 1;
                                  $receiveUid = $userDetails -> getUid();
                                  $receiveUsername = $userDetails -> getUsername();
                                  // $totalPoint = $pointReferee -300;
                                  $totalCash = $cash + $bonus;

                                  if (NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$referralName))
                                  {
                                       if(SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$productPrice,$totalProductPrice,$phoneNo))
                                       {}// echo "success";
                                       else
                                       {}// echo "fail";
                                  }
                                  else
                                  {
                                       // $_SESSION['messageType'] = 2;
                                       // header('Location: ../wallet.php?type=1');
                                  }
                                  if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
                                  {
                                    // $tableName = array();
                                    // $tableValue =  array();
                                    // $stringType =  "";
                                    // //echo "save to database";
                                    // if($registerDownline)
                                    // {
                                    //      array_push($tableName,"downline_no");
                                    //      array_push($tableValue,$registerDownline);
                                    //      $stringType .=  "i";
                                    // }

                                    // array_push($tableValue,$sponsorID);
                                    // $stringType .=  "s";
                                    // $passwordUpdated = updateDynamicData($conn,"user"," WHERE member_id = ? ",$tableName,$tableValue,$stringType);
                                    // if($passwordUpdated)
                                    // { }
                                    // else
                                    // { }
                                  }
                             }
  //===============================================================End of Partner Direct Bonus=============================================================================================
  //===============================================================President Direct Bonus=============================================================================================

                             if ($currentStatus == 'President' && $activeStatus != 'Inactive')
                             {
                                  $bonus = 120;
                                  $newBonus = 90; // president Bonus
                                  $bonusType = 'Leadership';
                                  $currentStatus = 'President';
                                  $uplineBonus = 0;
                                  $currentBonus = $userDetails -> getBonus();
                                  $totalBonus = $currentBonus + $bonus + $newBonus;
                                  $registerDownline = $userDetails->getDownlineNo() + 1;
                                  $receiveUid = $userDetails -> getUid();
                                  $receiveUsername = $userDetails -> getUsername();
                                  // $totalPoint = $pointReferee -300;
                                  $totalCash = $cash + $bonus;

                                  if (NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$referralName))
                                  {
                                       if(SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$productPrice,$totalProductPrice,$phoneNo))
                                       {}// echo "success";
                                       else
                                       {}// echo "fail";
                                  }
                                  else
                                  {
                                       // $_SESSION['messageType'] = 2;
                                       // header('Location: ../wallet.php?type=1');
                                  }
                                  if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
                                  {
                                    // $tableName = array();
                                    // $tableValue =  array();
                                    // $stringType =  "";
                                    // //echo "save to database";
                                    // if($registerDownline)
                                    // {
                                    //      array_push($tableName,"downline_no");
                                    //      array_push($tableValue,$registerDownline);
                                    //      $stringType .=  "i";
                                    // }

                                    // array_push($tableValue,$sponsorID);
                                    // $stringType .=  "s";
                                    // $passwordUpdated = updateDynamicData($conn,"user"," WHERE member_id = ? ",$tableName,$tableValue,$stringType);
                                    // if($passwordUpdated)
                                    // { }
                                    // else
                                    // { }
                                  }
                             }
  //===============================================================End of President Direct Bonus=============================================================================================
  //===============================================================Agent Direct Bonus=============================================================================================

                             if ($currentStatus == 'Agent' && $activeStatus != 'Inactive')
                             {
                                  $bonus = 120;
                                  $newBonus = 50; // agent Bonus
                                  $bonusType = 'Leadership';
                                  $currentStatus = 'President';
                                  $uplineBonus = 0;
                                  $currentBonus = $userDetails -> getBonus();
                                  $totalBonus = $currentBonus + $bonus + $newBonus;
                                  $registerDownline = $userDetails->getDownlineNo() + 1;
                                  $receiveUid = $userDetails -> getUid();
                                  $receiveUsername = $userDetails -> getUsername();
                                  // $totalPoint = $pointReferee -300;
                                  $totalCash = $cash + $bonus;

                                  if (NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$referralName))
                                  {
                                       if(SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$productPrice,$totalProductPrice,$phoneNo))
                                       {}// echo "success";
                                       else
                                       {}// echo "fail";
                                  }
                                  else
                                  {
                                       // $_SESSION['messageType'] = 2;
                                       // header('Location: ../wallet.php?type=1');
                                  }
                                  if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
                                  {
                                    // $tableName = array();
                                    // $tableValue =  array();
                                    // $stringType =  "";
                                    // //echo "save to database";
                                    // if($registerDownline)
                                    // {
                                    //      array_push($tableName,"downline_no");
                                    //      array_push($tableValue,$registerDownline);
                                    //      $stringType .=  "i";
                                    // }

                                    // array_push($tableValue,$sponsorID);
                                    // $stringType .=  "s";
                                    // $passwordUpdated = updateDynamicData($conn,"user"," WHERE member_id = ? ",$tableName,$tableValue,$stringType);
                                    // if($passwordUpdated)
                                    // { }
                                    // else
                                    // { }
                                  }
                             }
  //===============================================================End of President Direct Bonus=============================================================================================

                             if($downlineNumber < 7 && $currentStatus == 'Member' && $activeStatus != 'Inactive') //if your rank is member and downline below 7
                             {
                                  $bonus = 120;
                                  $newBonus = 0;
                                  $currentBonus = $userDetails -> getBonus();
                                  $totalBonus = $currentBonus + $bonus + $newBonus;
                                  $registerDownline = $userDetails->getDownlineNo() + 1; // direct downline
                                  // $totalPoint = $pointReferee -300;
                                  $totalCash = $cash + $bonus;

                                  if (NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$referralName))
                                  {
                                       if(SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$productPrice,$totalProductPrice,$phoneNo))
                                       {}// echo "success";
                                       else
                                       {}// echo "fail";
                                         // $tableName = array();
                                         // $tableValue =  array();
                                         // $stringType =  "";
                                         // //echo "save to database";
                                         // if($registerDownline)
                                         // {
                                         //      array_push($tableName,"downline_no");
                                         //      array_push($tableValue,$registerDownline);
                                         //      $stringType .=  "i";
                                         // }

                                         // array_push($tableValue,$sponsorID);
                                         // $stringType .=  "s";
                                         // $passwordUpdated = updateDynamicData($conn,"user"," WHERE member_id = ? ",$tableName,$tableValue,$stringType);
                                         // if($passwordUpdated)
                                         // { }
                                         // else
                                         // { }
                                  }
                                  else
                                  {
                                       // $_SESSION['messageType'] = 2;
                                       // header('Location: ../wallet.php?type=1');
                                  }

                             }
                             elseif($downlineNumber >= 7 && $currentStatus == 'Member' && $activeStatus != 'Inactive') // if your member 7 and above
                             {
                               $bonus = 120;
                               $newBonus = 0;
                               $bonusType = 'Leadership';
                               $yourStatusNow = $userDetails -> getUserRank();

                               if($yourStatusNow == 'Member') {
                                 $currentStatus = 'Agent';
                               }
                               $uplineBonus = 0;
                               $currentBonus = $userDetails -> getBonus();
                               $receiveUid = $userDetails -> getUid();
                               $receiveUsername = $userDetails -> getUsername();
                               $totalBonus = $currentBonus + $bonus + $newBonus;
                               $registerDownline = $userDetails->getDownlineNo() + 1;
                               // $totalPoint = $pointReferee -300;
                               $totalCash = $cash + $bonus;

                               if (NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$referralName))
                               {
                                    if(SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$productPrice,$totalProductPrice,$phoneNo))
                                    {}// echo "success";
                                    else
                                    {}// echo "fail";
                               }
                               else
                               {
                                    // $_SESSION['messageType'] = 2;
                                    // header('Location: ../wallet.php?type=1');
                               }
                               if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
                               {
                                 // $tableName = array();
                                 // $tableValue =  array();
                                 // $stringType =  "";
                                 // //echo "save to database";
                                 // if($registerDownline)
                                 // {
                                 //      array_push($tableName,"downline_no");
                                 //      array_push($tableValue,$registerDownline);
                                 //      $stringType .=  "i";
                                 // }

                                 // array_push($tableValue,$sponsorID);
                                 // $stringType .=  "s";
                                 // $passwordUpdated = updateDynamicData($conn,"user"," WHERE member_id = ? ",$tableName,$tableValue,$stringType);
                                 // if($passwordUpdated)
                                 // { }
                                 // else
                                 // { }
                               }
                             }

  //========================================================matching bonus start================================================================================================================================================================================
  $conn = connDB();
  $userRowsII = getUser($conn,"WHERE member_id = ?", array("member_id"), array($sponsorID), "s");
  $uid = $userRowsII[0]->getUid();

  $yourUid = getUser($conn, "WHERE uid = ? ", array("uid"), array($uid), "s");
  $yourOwnUid = $yourUid[0] -> getUid(); // get your uid
  $yourDownlineNo = $yourUid[0] -> getDownlineNo(); // get your downline
  $yourOwnCurrentStatus = $yourUid[0] -> getUserRank(); //get your current status
  $getUpline = getTop10ReferrerOfUser($conn,$uid); // get your upline

  $directBonus = 50; // bonus when add new referee
  $director = 2; //default
  $partner = 2; //default
  $president = 2; //default
  $agent = 2; //default
  $directorMatching = 2; //default
  $partnerMatching = 2; // default
  $presidentMatching = 2; // default
  $agentMatching = 2; // default
  $agentOverriding = 2; // default
  $districtOverriding = 2; // default
  $partnerOverriding = 2; // default

  $directorBonus = 150; // leadership
  $partnerBonus = 120; // leadership
  $presidentBonus = 90;
  $agentBonus = 50;

  if ($yourOwnCurrentStatus == 'Member') { // if your status is an member

   for ($cnt=0; $cnt <count($getUpline) ; $cnt++) { // loop all yourexisting upline
     // echo $getUpline[$cnt]."<br>";

    $getUplineUid = getUser($conn, "WHERE uid = ?", array("uid"), array($getUpline[$cnt]), "s");

    echo $getUplineUid[0]->getUserRank()."<br>"; // get your upline current status
    $receiveUid = $getUplineUid[0]->getUid(); // get uid
    $receiveUsername = $getUplineUid[0]->getusername(); // get username
    // $bonusType = 'Leadership';

    if ($getUplineUid[0]->getUserRank() == 'Director' && $getUplineUid[0]->getStatus() != 'Inactive') { // if your upline partner
      if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
      echo $newBonus = $directorBonus - $presidentBonus; // get 20 leadership bonus because president already take the leadership bonus
      $director = 1; // means we already loop into partner
      $directorMatching = 1;
      $bonusType = 'Leadership';
      if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
      {}

    }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $directorMatching == 2 ) { // means the data didnt enter president and agent loop
      echo $newBonus = $directorBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
      $director = 1; // means we already loop into partner
      $directorMatching = 1;
        $bonusType = 'Leadership';
        if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
        {}

    }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $directorMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
      echo $newBonus = $directorBonus - $agentBonus; // get leadership bonus 40 because the  Agent take the bonus 30
      $director = 1;
      $directorMatching = 1;
      $bonusType = 'Leadership';
      if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
      {}

    }elseif ($director == 2 && $partner == 2 && $president == 1 && $agent == 1 && $directorMatching == 2 ) { // means the data enter president & agent loop
      echo $newBonus = $directorBonus - $presidentBonus; // get leadership bonus 20 because the president take 50 bonus
      $director = 1;
      $bonusType = 'Leadership';
      $directorMatching = 1;
      if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
      {}
        // $partner = 2;
    }elseif ($director == 2 && $partner == 1 && $president == 1 && $agent == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
    echo $newBonus = $directorBonus - $partnerBonus; // get 20 leadership bonus because president already take the leadership bonus
    $director = 1; // means we already loop into partner
    $directorMatching = 1;
    $bonusType = 'Leadership';
    if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
    {}

  }elseif ($director == 2 && $partner == 1 && $president == 2 && $agent == 2 && $directorMatching == 2 ) { // means the data didnt enter president and agent loop
    echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
    $director = 1; // means we already loop into partner
    $directorMatching = 1;
      $bonusType = 'Leadership';
      if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
      {}

  }elseif ($director == 2 && $partner == 1 && $president == 2 && $agent == 1 && $directorMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
    echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 40 because the  Agent take the bonus 30
    $director = 1;
    $directorMatching = 1;
    $bonusType = 'Leadership';
    if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
    {}

  }elseif ($director == 2 && $partner == 1 && $president == 1 && $agent == 1 && $directorMatching == 2 ) { // means the data enter president & agent loop
    echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 20 because the president take 50 bonus
    $director = 1;
    $bonusType = 'Leadership';
    $directorMatching = 1;
    if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
    {}
      // $partner = 2;
  }
    elseif ($director == 1 && $directorMatching == 1 ) { // means the data already enter president loop because $president = 1
    echo $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
    $director = 1; // means we already loop into partner
    $directorMatching = 3;
    $bonusType = 'Matching';
    if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
    {}

  }}elseif ($getUplineUid[0]->getUserRank() == 'Partner' && $getUplineUid[0]->getStatus() != 'Inactive') { // if your upline partner
      if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $partnerMatching == 2 ) { // means the data already enter president loop because $president = 1
      echo $newBonus = $partnerBonus - $presidentBonus; // get 20 leadership bonus because president already take the leadership bonus
      $partner = 1; // means we already loop into partner
      $partnerMatching = 1;
      $bonusType = 'Leadership';
      if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
      {}

    }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $partnerMatching == 2 ) { // means the data didnt enter president and agent loop
      echo $newBonus = $partnerBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
      $partner = 1; // means we already loop into partner
      $partnerMatching = 1;
        $bonusType = 'Leadership';
        if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
        {}

    }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $partnerMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
      echo $newBonus = $partnerBonus - $agentBonus; // get leadership bonus 40 because the  Agent take the bonus 30
      
      $partner = 1;
      $partnerMatching = 1;
      $bonusType = 'Leadership';
      if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
      {}

    }elseif ($director == 2 && $partner == 2 && $president == 1 && $agent == 1 && $partnerMatching == 2 ) { // means the data enter president & agent loop
      echo $newBonus = $partnerBonus - $presidentBonus; // get leadership bonus 20 because the president take 50 bonus
      
      $partner = 1;
      $bonusType = 'Leadership';
      $partnerMatching = 1;
      if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
      {}
        // $partner = 2;
    }
    elseif ($partner == 1 && $partnerMatching == 1 ) { // means the data already enter president loop because $president = 1
    echo $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
    echo 
    $partner = 1; // means we already loop into partner
    $partnerMatching = 3;
    $bonusType = 'Matching';
    if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
    {}

  }}elseif ($getUplineUid[0]->getUserRank() == 'President' && $getUplineUid[0]->getStatus() != 'Inactive') { // if your upline president
      if ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $presidentMatching == 2) { // means the data didnt enter partner loop because $partner == 2
       echo $newBonus = $presidentBonus; // get leadership bonus 50
       
       $president = 1;
       $presidentMatching = 1;
       $bonusType = 'Leadership';
       if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
       {}

   }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $presidentMatching == 2) { // means the data  enter  agent loop because $agent == 1
     echo $newBonus = $presidentBonus - $agentBonus; // get leadership bonus 20 because the  Agent take the bonus 30
     
     $president = 1;
     $presidentMatching = 1;
     $bonusType = 'Leadership';
     if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
     {}

   }
  elseif ($president == 1 && $presidentMatching == 1) { // means the data didnt enter partner loop because $partner == 2
  echo $newBonus = 25; // get leadership bonus 50
  
  $president = 1;
  $presidentMatching = 3;
  $bonusType = 'Matching';
  if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
  {}

  }}elseif ($getUplineUid[0]->getUserRank() == 'Agent' && $getUplineUid[0]->getStatus() != 'Inactive') { // if your upline agent
    if ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $agentMatching == 2) { // means the data  enter  agent loop because $agent == 1
      echo $newBonus = $agentBonus; // get leadership bonus 20 because the  Agent take the bonus 30
      $agent = 1;
      $agentMatching = 1;
      $bonusType = 'Leadership';
      if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
      {}

    }
  elseif ($agent == 1 && $agentMatching == 1 ) { // means the data didnt enter partner loop because $partner == 2
   echo $newBonus = 25; // get leadership bonus 30
   
   $agent = 1;
   $agentMatching = 3;
   $bonusType = 'Matching';
   if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
   {}

  }}}}

     //================================================================================================================================

     if ($yourOwnCurrentStatus == 'Agent') { // if your status is an member
      // $agent = 1;
      // $agentMatching = 1;

      for ($cnt=0; $cnt <count($getUpline) ; $cnt++) { // loop all yourexisting upline
        // echo $getUpline[$cnt]."<br>";

       $getUplineUid = getUser($conn, "WHERE uid = ?", array("uid"), array($getUpline[$cnt]), "s");

       echo $getUplineUid[0]->getUserRank()."<br>"; // get your upline current status
       $receiveUid = $getUplineUid[0]->getUid(); // get uid
       $receiveUsername = $getUplineUid[0]->getusername(); // get username
       // $bonusType = 'Leadership';

       if ($getUplineUid[0]->getUserRank() == 'Director' && $getUplineUid[0]->getStatus() != 'Inactive') { // if your upline partner
         if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
         echo $newBonus = $directorBonus - $presidentBonus; // get 20 leadership bonus because president already take the leadership bonus
         $director = 1; // means we already loop into partner
         $directorMatching = 1;
         $bonusType = 'Leadership';
         if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         {}

       }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $directorMatching == 2 ) { // means the data didnt enter president and agent loop
         echo $newBonus = $directorBonus - $agentBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
         $director = 1; // means we already loop into partner
         $directorMatching = 1;
           $bonusType = 'Leadership';
           if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
           {}

       }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $directorMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
         echo $newBonus = $directorBonus - $agentBonus; // get leadership bonus 40 because the  Agent take the bonus 30
         $director = 1;
         $directorMatching = 1;
         $bonusType = 'Leadership';
         if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         {}

       }elseif ($director == 2 && $partner == 2 && $president == 1 && $agent == 1 && $directorMatching == 2 ) { // means the data enter president & agent loop
         echo $newBonus = $directorBonus - $presidentBonus; // get leadership bonus 20 because the president take 50 bonus
         $director = 1;
         $bonusType = 'Leadership';
         $directorMatching = 1;
         if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         {}
           // $partner = 2;
       }elseif ($director == 2 && $partner == 1 && $president == 1 && $agent == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
       echo $newBonus = $directorBonus - $partnerBonus; // get 20 leadership bonus because president already take the leadership bonus
       $director = 1; // means we already loop into partner
       $directorMatching = 1;
       $bonusType = 'Leadership';
       if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
       {}

     }elseif ($director == 2 && $partner == 1 && $president == 2 && $agent == 2 && $directorMatching == 2 ) { // means the data didnt enter president and agent loop
       echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
       $director = 1; // means we already loop into partner
       $directorMatching = 1;
         $bonusType = 'Leadership';
         if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         {}

     }elseif ($director == 2 && $partner == 1 && $president == 2 && $agent == 1 && $directorMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
       echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 40 because the  Agent take the bonus 30
       $director = 1;
       $directorMatching = 1;
       $bonusType = 'Leadership';
       if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
       {}

     }elseif ($director == 2 && $partner == 1 && $president == 1 && $agent == 1 && $directorMatching == 2 ) { // means the data enter president & agent loop
       echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 20 because the president take 50 bonus
       $director = 1;
       $bonusType = 'Leadership';
       $directorMatching = 1;
       if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
       {}
         // $partner = 2;
     }
       elseif ($director == 1 && $directorMatching == 1 ) { // means the data already enter president loop because $president = 1
       echo $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
       $director = 1; // means we already loop into partner
       $directorMatching = 3;
       $bonusType = 'Matching';
       if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
       {}

     }}elseif ($getUplineUid[0]->getUserRank() == 'Partner' && $getUplineUid[0]->getStatus() != 'Inactive') { // if your upline partner
         if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $partnerMatching == 2 ) { // means the data already enter president loop because $president = 1
         echo $newBonus = $partnerBonus - $presidentBonus; // get 20 leadership bonus because president already take the leadership bonus
         $partner = 1; // means we already loop into partner
         $partnerMatching = 1;
         $bonusType = 'Leadership';
         if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         {}

       }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $partnerMatching == 2 ) { // means the data didnt enter president and agent loop
         echo $newBonus = $partnerBonus - $agentBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
         $partner = 1; // means we already loop into partner
         $partnerMatching = 1;
           $bonusType = 'Leadership';
           if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
           {}

       }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $partnerMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
         echo $newBonus = $partnerBonus - $agentBonus; // get leadership bonus 40 because the  Agent take the bonus 30
         
         $partner = 1;
         $partnerMatching = 1;
         $bonusType = 'Leadership';
         if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         {}

       }elseif ($director == 2 && $partner == 2 && $president == 1 && $agent == 1 && $partnerMatching == 2 ) { // means the data enter president & agent loop
         echo $newBonus = $partnerBonus - $presidentBonus; // get leadership bonus 20 because the president take 50 bonus
         
         $partner = 1;
         $bonusType = 'Leadership';
         $partnerMatching = 1;
         if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         {}
           // $partner = 2;
       }
       elseif ($partner == 1 && $partnerMatching == 1 ) { // means the data already enter president loop because $president = 1
       echo $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
       echo 
       $partner = 1; // means we already loop into partner
       $partnerMatching = 3;
       $bonusType = 'Matching';
       if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
       {}

     }}elseif ($getUplineUid[0]->getUserRank() == 'President' && $getUplineUid[0]->getStatus() != 'Inactive') { // if your upline president
         if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $presidentMatching == 2) { // means the data already enter partner loop because $partner = 1
         // echo $newBonus = $partnerBonus - $presidentBonus - $agentBonus; // not get any leadership bonus because partner already take the leadership bonus
         $president = 1;
         $presidentMatching = 1;

       }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $presidentMatching == 2) { // means the data didnt enter partner loop because $partner == 2
          echo $newBonus = $presidentBonus - $agentBonus; // get leadership bonus 50
          
          $president = 1;
          $presidentMatching = 1;
          $bonusType = 'Leadership';
          if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
          {}

      }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $presidentMatching == 2) { // means the data  enter  agent loop because $agent == 1
        echo $newBonus = $presidentBonus - $agentBonus; // get leadership bonus 20 because the  Agent take the bonus 30
        
        $president = 1;
        $presidentMatching = 1;
        $bonusType = 'Leadership';
        if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
        {}

      }
     elseif ($president == 1 && $presidentMatching == 1) { // means the data didnt enter partner loop because $partner == 2
     echo $newBonus = 25; // get leadership bonus 50
     
     $president = 1;
     $presidentMatching = 3;
     $bonusType = 'Matching';
     if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
     {}

     }}elseif ($getUplineUid[0]->getUserRank() == 'Agent' && $getUplineUid[0]->getStatus() != 'Inactive') { // if your upline agent
       $agent = 1;
       $agentMatching = 1;
       if ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $agentMatching == 2) { // means the data  enter  agent loop because $agent == 1

       }
     elseif ($agentMatching == 1 ) { // means the data didnt enter partner loop because $partner == 2
      echo $newBonus = 25; // get leadership bonus 30
      
      $agent = 1;
      $agentMatching = 3;
      $bonusType = 'Matching';
      if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
      {}

     }}}}

     //=====================================================================================================

     if ($yourOwnCurrentStatus == 'President') { // if your status is an member
      // $president = 1;
      // $presidentMatching = 1;

      for ($cnt=0; $cnt <count($getUpline) ; $cnt++) { // loop all yourexisting upline
        // echo $getUpline[$cnt]."<br>";

       $getUplineUid = getUser($conn, "WHERE uid = ?", array("uid"), array($getUpline[$cnt]), "s");

       echo $getUplineUid[0]->getUserRank()."<br>"; // get your upline current status
       $receiveUid = $getUplineUid[0]->getUid(); // get uid
       $receiveUsername = $getUplineUid[0]->getusername(); // get username
       // $bonusType = 'Leadership';

       if ($getUplineUid[0]->getUserRank() == 'Director' && $getUplineUid[0]->getStatus() != 'Inactive') { // if your upline partner
         if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
         echo $newBonus = $directorBonus - $presidentBonus; // get 20 leadership bonus because president already take the leadership bonus
         $director = 1; // means we already loop into partner
         $directorMatching = 1;
         $bonusType = 'Leadership';
         if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         {}

       }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $directorMatching == 2 ) { // means the data didnt enter president and agent loop
         echo $newBonus = $directorBonus - $presidentBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
         $director = 1; // means we already loop into partner
         $directorMatching = 1;
           $bonusType = 'Leadership';
           if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
           {}

       }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $directorMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
         echo $newBonus = $directorBonus - $presidentBonus; // get leadership bonus 40 because the  Agent take the bonus 30
         $director = 1;
         $directorMatching = 1;
         $bonusType = 'Leadership';
         if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         {}

       }elseif ($director == 2 && $partner == 2 && $president == 1 && $agent == 1 && $directorMatching == 2 ) { // means the data enter president & agent loop
         echo $newBonus = $directorBonus - $presidentBonus; // get leadership bonus 20 because the president take 50 bonus
         $director = 1;
         $bonusType = 'Leadership';
         $directorMatching = 1;
         if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         {}
           // $partner = 2;
       }elseif ($director == 2 && $partner == 1 && $president == 1 && $agent == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
       echo $newBonus = $directorBonus - $partnerBonus; // get 20 leadership bonus because president already take the leadership bonus
       $director = 1; // means we already loop into partner
       $directorMatching = 1;
       $bonusType = 'Leadership';
       if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
       {}

     }elseif ($director == 2 && $partner == 1 && $president == 2 && $agent == 2 && $directorMatching == 2 ) { // means the data didnt enter president and agent loop
       echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
       $director = 1; // means we already loop into partner
       $directorMatching = 1;
         $bonusType = 'Leadership';
         if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         {}

     }elseif ($director == 2 && $partner == 1 && $president == 2 && $agent == 1 && $directorMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
       echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 40 because the  Agent take the bonus 30
       $director = 1;
       $directorMatching = 1;
       $bonusType = 'Leadership';
       if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
       {}

     }elseif ($director == 2 && $partner == 1 && $president == 1 && $agent == 1 && $directorMatching == 2 ) { // means the data enter president & agent loop
       echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 20 because the president take 50 bonus
       $director = 1;
       $bonusType = 'Leadership';
       $directorMatching = 1;
       if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
       {}
         // $partner = 2;
     }
       elseif ($director == 1 && $directorMatching == 1 ) { // means the data already enter president loop because $president = 1
       echo $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
       $director = 1; // means we already loop into partner
       $directorMatching = 3;
       $bonusType = 'Matching';
       if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
       {}

     }}elseif ($getUplineUid[0]->getUserRank() == 'Partner' && $getUplineUid[0]->getStatus() != 'Inactive') { // if your upline partner
         if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $partnerMatching == 2 ) { // means the data already enter president loop because $president = 1
         echo $newBonus = $partnerBonus - $presidentBonus; // get 20 leadership bonus because president already take the leadership bonus
         $partner = 1; // means we already loop into partner
         $partnerMatching = 1;
         $bonusType = 'Leadership';
         if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         {}

       }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $partnerMatching == 2 ) { // means the data didnt enter president and agent loop
         echo $newBonus = $partnerBonus - $presidentBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
         $partner = 1; // means we already loop into partner
         $partnerMatching = 1;
           $bonusType = 'Leadership';
           if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
           {}

       }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $partnerMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
         echo $newBonus = $partnerBonus - $presidentBonus; // get leadership bonus 40 because the  Agent take the bonus 30
         
         $partner = 1;
         $partnerMatching = 1;
         $bonusType = 'Leadership';
         if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         {}

       }elseif ($director == 2 && $partner == 2 && $president == 1 && $agent == 1 && $partnerMatching == 2 ) { // means the data enter president & agent loop
         echo $newBonus = $partnerBonus - $presidentBonus; // get leadership bonus 20 because the president take 50 bonus
         
         $partner = 1;
         $bonusType = 'Leadership';
         $partnerMatching = 1;
         if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         {}
           // $partner = 2;
       }
       elseif ($partner == 1 && $partnerMatching == 1 ) { // means the data already enter president loop because $president = 1
       echo $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
       echo 
       $partner = 1; // means we already loop into partner
       $partnerMatching = 3;
       $bonusType = 'Matching';
       if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
       {}

     }}elseif ($getUplineUid[0]->getUserRank() == 'President' && $getUplineUid[0]->getStatus() != 'Inactive') { // if your upline president
         if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $presidentMatching == 2) { // means the data already enter partner loop because $partner = 1
         // echo $newBonus = $partnerBonus - $presidentBonus - $agentBonus; // not get any leadership bonus because partner already take the leadership bonus
         $president = 1;
         $presidentMatching = 1;

       }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $presidentMatching == 2) { // means the data didnt enter partner loop because $partner == 2
         $president = 1;
         $presidentMatching = 1;

      }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $presidentMatching == 2) { // means the data  enter  agent loop because $agent == 1
        $president = 1;
        $presidentMatching = 1;
      }
     elseif ($president == 1 && $presidentMatching == 1) { // means the data didnt enter partner loop because $partner == 2
     echo $newBonus = 25; // get leadership bonus 50
     
     $president = 1;
     $presidentMatching = 3;
     $bonusType = 'Matching';
     if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
     {}

     }}elseif ($getUplineUid[0]->getUserRank() == 'Agent' && $getUplineUid[0]->getStatus() != 'Inactive') { // if your upline agent
       $agent = 1;
       $agentMatching = 1;
       if ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $agentMatching == 2) { // means the data  enter  agent loop because $agent == 1

       }
     elseif ($agentMatching == 1 ) { // means the data didnt enter partner loop because $partner == 2
      echo $newBonus = 25; // get leadership bonus 30
      
      $agent = 1;
      $agentMatching = 3;
      $bonusType = 'Matching';
      if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
      {}

     }}}}

     //========================================================================================================

     if ($yourOwnCurrentStatus == 'Partner') { // if your status is an member
      // $partner = 1;
      // $partnerMatching = 1;

      for ($cnt=0; $cnt <count($getUpline) ; $cnt++) { // loop all yourexisting upline
        // echo $getUpline[$cnt]."<br>";

       $getUplineUid = getUser($conn, "WHERE uid = ?", array("uid"), array($getUpline[$cnt]), "s");

       echo $getUplineUid[0]->getUserRank()."<br>"; // get your upline current status
       $receiveUid = $getUplineUid[0]->getUid(); // get uid
       $receiveUsername = $getUplineUid[0]->getusername(); // get username
       // $bonusType = 'Leadership';

       if ($getUplineUid[0]->getUserRank() == 'Director' && $getUplineUid[0]->getStatus() != 'Inactive') { // if your upline partner
         if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
         echo $newBonus = $directorBonus - $partnerBonus; // get 20 leadership bonus because president already take the leadership bonus
         $director = 1; // means we already loop into partner
         $directorMatching = 1;
         $bonusType = 'Leadership';
         if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         {}

       }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $directorMatching == 2 ) { // means the data didnt enter president and agent loop
         echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
         $director = 1; // means we already loop into partner
         $directorMatching = 1;
           $bonusType = 'Leadership';
           if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
           {}

       }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $directorMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
         echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 40 because the  Agent take the bonus 30
         $director = 1;
         $directorMatching = 1;
         $bonusType = 'Leadership';
         if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         {}

       }elseif ($director == 2 && $partner == 2 && $president == 1 && $agent == 1 && $directorMatching == 2 ) { // means the data enter president & agent loop
         echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 20 because the president take 50 bonus
         $director = 1;
         $bonusType = 'Leadership';
         $directorMatching = 1;
         if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         {}
           // $partner = 2;
       }elseif ($director == 2 && $partner == 1 && $president == 1 && $agent == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
       echo $newBonus = $directorBonus - $partnerBonus; // get 20 leadership bonus because president already take the leadership bonus
       $director = 1; // means we already loop into partner
       $directorMatching = 1;
       $bonusType = 'Leadership';
       if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
       {}

     }elseif ($director == 2 && $partner == 1 && $president == 2 && $agent == 2 && $directorMatching == 2 ) { // means the data didnt enter president and agent loop
       echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
       $director = 1; // means we already loop into partner
       $directorMatching = 1;
         $bonusType = 'Leadership';
         if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         {}

     }elseif ($director == 2 && $partner == 1 && $president == 2 && $agent == 1 && $directorMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
       echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 40 because the  Agent take the bonus 30
       $director = 1;
       $directorMatching = 1;
       $bonusType = 'Leadership';
       if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
       {}

     }elseif ($director == 2 && $partner == 1 && $president == 1 && $agent == 1 && $directorMatching == 2 ) { // means the data enter president & agent loop
       echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 20 because the president take 50 bonus
       $director = 1;
       $bonusType = 'Leadership';
       $directorMatching = 1;
       if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
       {}
         // $partner = 2;
     }
       elseif ($director == 1 && $directorMatching == 1 ) { // means the data already enter president loop because $president = 1
       echo $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
       $director = 1; // means we already loop into partner
       $directorMatching = 3;
       $bonusType = 'Matching';
       if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
       {}

     }}elseif ($getUplineUid[0]->getUserRank() == 'Partner' && $getUplineUid[0]->getStatus() != 'Inactive') { // if your upline partner
         if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $partnerMatching == 2 ) { // means the data already enter president loop because $president = 1
         // echo $newBonus = $partnerBonus - $presidentBonus; // get 20 leadership bonus because president already take the leadership bonus
         $partner = 1; // means we already loop into partner
         $partnerMatching = 1;
         // $bonusType = 'Leadership';
         // if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         // {
         //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
         //   if(!$user)
         //   {
         //       $getBonus = $getUplineUid[0]->getBonus(); // get bonus
         //       $getAmount = $getUplineUid[0]->getSales();
         //
         //       $latestBonus = $newBonus + $getBonus;
         //       $getFinalAmount = $newBonus + $getAmount;
         //
         //        $tableName = array();
         //        $tableValue =  array();
         //        $stringType =  "";
         //        if($latestBonus)
         //        {
         //             array_push($tableName,"bonus");
         //             array_push($tableValue,$latestBonus);
         //             $stringType .=  "i";
         //        }
         //        if($getFinalAmount)
         //        {
         //        array_push($tableName,"sales");
         //        array_push($tableValue,$getFinalAmount);
         //        $stringType .=  "i";
         //        }
         //        array_push($tableValue,$receiveUid);
         //        $stringType .=  "s";
         //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
         //        if($passwordUpdated)
         //        {
         //             // echo "success";
         //             // $_SESSION['messageType'] = 1;
         //             // header('Location: ../wallet.php?type=1');
         //             // header('Location: ../editprofile.php?type=3');
         //        }
         //        else
         //        {
         //             // echo "fail";
         //             // $_SESSION['messageType'] = 3;
         //             // header('Location: ../wallet.php?type=3');
         //        }
         //   }
         // }

       }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $partnerMatching == 2 ) { // means the data didnt enter president and agent loop
         // echo $newBonus = $partnerBonus - $agentBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
         $partner = 1; // means we already loop into partner
         $partnerMatching = 1;
         //   $bonusType = 'Leadership';
         //   if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         //   {
         //     $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
         //     if(!$user)
         //     {
         //         $getBonus = $getUplineUid[0]->getBonus(); // get bonus
         //         $getAmount = $getUplineUid[0]->getSales();
         //
         //         $latestBonus = $newBonus + $getBonus;
         //         $getFinalAmount = $newBonus + $getAmount;
         //
         //          $tableName = array();
         //          $tableValue =  array();
         //          $stringType =  "";
         //          if($latestBonus)
         //          {
         //               array_push($tableName,"bonus");
         //               array_push($tableValue,$latestBonus);
         //               $stringType .=  "i";
         //          }
         //          if($getFinalAmount)
         //          {
         //          array_push($tableName,"sales");
         //          array_push($tableValue,$getFinalAmount);
         //          $stringType .=  "i";
         //          }
         //          array_push($tableValue,$receiveUid);
         //          $stringType .=  "s";
         //          $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
         //          if($passwordUpdated)
         //          {
         //               // echo "success";
         //               // $_SESSION['messageType'] = 1;
         //               // header('Location: ../wallet.php?type=1');
         //               // header('Location: ../editprofile.php?type=3');
         //          }
         //          else
         //          {
         //               // echo "fail";
         //               // $_SESSION['messageType'] = 3;
         //               // header('Location: ../wallet.php?type=3');
         //          }
         //     }
         //   }

       }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $partnerMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
         // echo $newBonus = $partnerBonus - $agentBonus; // get leadership bonus 40 because the  Agent take the bonus 30
         // 
         $partner = 1;
         $partnerMatching = 1;
         // $bonusType = 'Leadership';
         // if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         // {
         //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
         //   if(!$user)
         //   {
         //       $getBonus = $getUplineUid[0]->getBonus(); // get bonus
         //       $getAmount = $getUplineUid[0]->getSales();
         //
         //       $latestBonus = $newBonus + $getBonus;
         //       $getFinalAmount = $newBonus + $getAmount;
         //
         //        $tableName = array();
         //        $tableValue =  array();
         //        $stringType =  "";
         //        if($latestBonus)
         //        {
         //             array_push($tableName,"bonus");
         //             array_push($tableValue,$latestBonus);
         //             $stringType .=  "i";
         //        }
         //        if($getFinalAmount)
         //        {
         //        array_push($tableName,"sales");
         //        array_push($tableValue,$getFinalAmount);
         //        $stringType .=  "i";
         //        }
         //        array_push($tableValue,$receiveUid);
         //        $stringType .=  "s";
         //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
         //        if($passwordUpdated)
         //        {
         //             // echo "success";
         //             // $_SESSION['messageType'] = 1;
         //             // header('Location: ../wallet.php?type=1');
         //             // header('Location: ../editprofile.php?type=3');
         //        }
         //        else
         //        {
         //             // echo "fail";
         //             // $_SESSION['messageType'] = 3;
         //             // header('Location: ../wallet.php?type=3');
         //        }
         //   }
         // }

       }elseif ($director == 2 && $partner == 2 && $president == 1 && $agent == 1 && $partnerMatching == 2 ) { // means the data enter president & agent loop
         // echo $newBonus = $partnerBonus - $presidentBonus; // get leadership bonus 20 because the president take 50 bonus
         // 
         $partner = 1;
         // $bonusType = 'Leadership';
         $partnerMatching = 1;
         // if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
         // {
         //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
         //   if(!$user)
         //   {
         //       $getBonus = $getUplineUid[0]->getBonus(); // get bonus
         //       $getAmount = $getUplineUid[0]->getSales();
         //
         //       $latestBonus = $newBonus + $getBonus;
         //       $getFinalAmount = $newBonus + $getAmount;
         //
         //        $tableName = array();
         //        $tableValue =  array();
         //        $stringType =  "";
         //        if($latestBonus)
         //        {
         //             array_push($tableName,"bonus");
         //             array_push($tableValue,$latestBonus);
         //             $stringType .=  "i";
         //        }
         //        if($getFinalAmount)
         //        {
         //        array_push($tableName,"sales");
         //        array_push($tableValue,$getFinalAmount);
         //        $stringType .=  "i";
         //        }
         //        array_push($tableValue,$receiveUid);
         //        $stringType .=  "s";
         //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
         //        if($passwordUpdated)
         //        {
         //             // echo "success";
         //             // $_SESSION['messageType'] = 1;
         //             // header('Location: ../wallet.php?type=1');
         //             // header('Location: ../editprofile.php?type=3');
         //        }
         //        else
         //        {
         //             // echo "fail";
         //             // $_SESSION['messageType'] = 3;
         //             // header('Location: ../wallet.php?type=3');
         //        }
         //   }
         // }
         //   // $partner = 2;
       }
       elseif ($partner == 1 && $partnerMatching == 1 ) { // means the data already enter president loop because $president = 1
       echo $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
       echo 
       $partner = 1; // means we already loop into partner
       $partnerMatching = 3;
       $bonusType = 'Matching';
       if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
       {}

     }}elseif ($getUplineUid[0]->getUserRank() == 'President' && $getUplineUid[0]->getStatus() != 'Inactive') { // if your upline president
         if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $presidentMatching == 2) { // means the data already enter partner loop because $partner = 1
         // echo $newBonus = $partnerBonus - $presidentBonus - $agentBonus; // not get any leadership bonus because partner already take the leadership bonus
         $president = 1;
         $presidentMatching = 1;

       }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $presidentMatching == 2) { // means the data didnt enter partner loop because $partner == 2
          // echo $newBonus = $presidentBonus - $agentBonus; // get leadership bonus 50
          // 
          $president = 1;
          $presidentMatching = 1;
          // $bonusType = 'Leadership';
          // if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
          // {
          //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
          //   if(!$user)
          //   {
          //       $getBonus = $getUplineUid[0]->getBonus(); // get bonus
          //       $getAmount = $getUplineUid[0]->getSales();
          //
          //       $latestBonus = $newBonus + $getBonus;
          //       $getFinalAmount = $newBonus + $getAmount;
          //
          //        $tableName = array();
          //        $tableValue =  array();
          //        $stringType =  "";
          //        if($latestBonus)
          //        {
          //             array_push($tableName,"bonus");
          //             array_push($tableValue,$latestBonus);
          //             $stringType .=  "i";
          //        }
          //        if($getFinalAmount)
          //        {
          //        array_push($tableName,"sales");
          //        array_push($tableValue,$getFinalAmount);
          //        $stringType .=  "i";
          //        }
          //        array_push($tableValue,$receiveUid);
          //        $stringType .=  "s";
          //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          //        if($passwordUpdated)
          //        {
          //             // echo "success";
          //             // $_SESSION['messageType'] = 1;
          //             // header('Location: ../wallet.php?type=1');
          //             // header('Location: ../editprofile.php?type=3');
          //        }
          //        else
          //        {
          //             // echo "fail";
          //             // $_SESSION['messageType'] = 3;
          //             // header('Location: ../wallet.php?type=3');
          //        }
          //   }
          // }

      }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $presidentMatching == 2) { // means the data  enter  agent loop because $agent == 1
        // echo $newBonus = $presidentBonus - $agentBonus; // get leadership bonus 20 because the  Agent take the bonus 30
        // 
        $president = 1;
        $presidentMatching = 1;
        // $bonusType = 'Leadership';
        // if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
        // {
        //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
        //   if(!$user)
        //   {
        //       $getBonus = $getUplineUid[0]->getBonus(); // get bonus
        //       $getAmount = $getUplineUid[0]->getSales();
        //
        //       $latestBonus = $newBonus + $getBonus;
        //       $getFinalAmount = $newBonus + $getAmount;
        //
        //        $tableName = array();
        //        $tableValue =  array();
        //        $stringType =  "";
        //        if($latestBonus)
        //        {
        //             array_push($tableName,"bonus");
        //             array_push($tableValue,$latestBonus);
        //             $stringType .=  "i";
        //        }
        //        if($getFinalAmount)
        //        {
        //        array_push($tableName,"sales");
        //        array_push($tableValue,$getFinalAmount);
        //        $stringType .=  "i";
        //        }
        //        array_push($tableValue,$receiveUid);
        //        $stringType .=  "s";
        //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        //        if($passwordUpdated)
        //        {
        //             // echo "success";
        //             // $_SESSION['messageType'] = 1;
        //             // header('Location: ../wallet.php?type=1');
        //             // header('Location: ../editprofile.php?type=3');
        //        }
        //        else
        //        {
        //             // echo "fail";
        //             // $_SESSION['messageType'] = 3;
        //             // header('Location: ../wallet.php?type=3');
        //        }
        //   }
        // }

      }
     elseif ($president == 1 && $presidentMatching == 1) { // means the data didnt enter partner loop because $partner == 2
     echo $newBonus = 25; // get leadership bonus 50
     
     $president = 1;
     $presidentMatching = 3;
     $bonusType = 'Matching';
     if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
     {}

     }}elseif ($getUplineUid[0]->getUserRank() == 'Agent' && $getUplineUid[0]->getStatus() != 'Inactive') { // if your upline agent
       $agent = 1;
       $agentMatching = 1;
       if ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $agentMatching == 2) { // means the data  enter  agent loop because $agent == 1

       }
     elseif ($agentMatching == 1 ) { // means the data didnt enter partner loop because $partner == 2
      echo $newBonus = 25; // get leadership bonus 30
      
      $agent = 1;
      $agentMatching = 3;
      $bonusType = 'Matching';
      if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
      {}

     }}}}
  //=========================================================================================================================================================================================================================================
  if ($yourOwnCurrentStatus == 'Director') { // if your status is an member
    // $director = 1;
    // $directorMatching = 1;

   for ($cnt=0; $cnt <count($getUpline) ; $cnt++) { // loop all yourexisting upline
     // echo $getUpline[$cnt]."<br>";

    $getUplineUid = getUser($conn, "WHERE uid = ?", array("uid"), array($getUpline[$cnt]), "s");

    echo $getUplineUid[0]->getUserRank()."<br>"; // get your upline current status
    $receiveUid = $getUplineUid[0]->getUid(); // get uid
    $receiveUsername = $getUplineUid[0]->getusername(); // get username
    // $bonusType = 'Leadership';

    if ($getUplineUid[0]->getUserRank() == 'Director' && $getUplineUid[0]->getStatus() != 'Inactive') { // if your upline partner
      if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
      // echo $newBonus = $directorBonus - $partnerBonus; // get 20 leadership bonus because president already take the leadership bonus
      $director = 1; // means we already loop into partner
      $directorMatching = 1;
      // $bonusType = 'Leadership';
      // if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
      // {
      //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
      //   if(!$user)
      //   {
      //       $getBonus = $getUplineUid[0]->getBonus(); // get bonus
      //       $getAmount = $getUplineUid[0]->getSales();
      //
      //       $latestBonus = $newBonus + $getBonus;
      //       $getFinalAmount = $newBonus + $getAmount;
      //
      //        $tableName = array();
      //        $tableValue =  array();
      //        $stringType =  "";
      //        if($latestBonus)
      //        {
      //             array_push($tableName,"bonus");
      //             array_push($tableValue,$latestBonus);
      //             $stringType .=  "i";
      //        }
      //        if($getFinalAmount)
      //        {
      //        array_push($tableName,"sales");
      //        array_push($tableValue,$getFinalAmount);
      //        $stringType .=  "i";
      //        }
      //        array_push($tableValue,$receiveUid);
      //        $stringType .=  "s";
      //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
      //        if($passwordUpdated)
      //        {
      //             // echo "success";
      //             // $_SESSION['messageType'] = 1;
      //             // header('Location: ../wallet.php?type=1');
      //             // header('Location: ../editprofile.php?type=3');
      //        }
      //        else
      //        {
      //             // echo "fail";
      //             // $_SESSION['messageType'] = 3;
      //             // header('Location: ../wallet.php?type=3');
      //        }
      //   }
      // }

    }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $directorMatching == 2 ) { // means the data didnt enter president and agent loop
      // echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
      $director = 1; // means we already loop into partner
      $directorMatching = 1;
      //   $bonusType = 'Leadership';
      //   if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
      //   {
      //     $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
      //     if(!$user)
      //     {
      //         $getBonus = $getUplineUid[0]->getBonus(); // get bonus
      //         $getAmount = $getUplineUid[0]->getSales();
      //
      //         $latestBonus = $newBonus + $getBonus;
      //         $getFinalAmount = $newBonus + $getAmount;
      //
      //          $tableName = array();
      //          $tableValue =  array();
      //          $stringType =  "";
      //          if($latestBonus)
      //          {
      //               array_push($tableName,"bonus");
      //               array_push($tableValue,$latestBonus);
      //               $stringType .=  "i";
      //          }
      //          if($getFinalAmount)
      //          {
      //          array_push($tableName,"sales");
      //          array_push($tableValue,$getFinalAmount);
      //          $stringType .=  "i";
      //          }
      //          array_push($tableValue,$receiveUid);
      //          $stringType .=  "s";
      //          $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
      //          if($passwordUpdated)
      //          {
      //               // echo "success";
      //               // $_SESSION['messageType'] = 1;
      //               // header('Location: ../wallet.php?type=1');
      //               // header('Location: ../editprofile.php?type=3');
      //          }
      //          else
      //          {
      //               // echo "fail";
      //               // $_SESSION['messageType'] = 3;
      //               // header('Location: ../wallet.php?type=3');
      //          }
      //     }
      //   }

    }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $directorMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
      // echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 40 because the  Agent take the bonus 30
      $director = 1;
      $directorMatching = 1;
      // $bonusType = 'Leadership';
      // if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
      // {
      //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
      //   if(!$user)
      //   {
      //       $getBonus = $getUplineUid[0]->getBonus(); // get bonus
      //       $getAmount = $getUplineUid[0]->getSales();
      //
      //       $latestBonus = $newBonus + $getBonus;
      //       $getFinalAmount = $newBonus + $getAmount;
      //
      //        $tableName = array();
      //        $tableValue =  array();
      //        $stringType =  "";
      //        if($latestBonus)
      //        {
      //             array_push($tableName,"bonus");
      //             array_push($tableValue,$latestBonus);
      //             $stringType .=  "i";
      //        }
      //        if($getFinalAmount)
      //        {
      //        array_push($tableName,"sales");
      //        array_push($tableValue,$getFinalAmount);
      //        $stringType .=  "i";
      //        }
      //        array_push($tableValue,$receiveUid);
      //        $stringType .=  "s";
      //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
      //        if($passwordUpdated)
      //        {
      //             // echo "success";
      //             // $_SESSION['messageType'] = 1;
      //             // header('Location: ../wallet.php?type=1');
      //             // header('Location: ../editprofile.php?type=3');
      //        }
      //        else
      //        {
      //             // echo "fail";
      //             // $_SESSION['messageType'] = 3;
      //             // header('Location: ../wallet.php?type=3');
      //        }
      //   }
      // }

    }elseif ($director == 2 && $partner == 2 && $president == 1 && $agent == 1 && $directorMatching == 2 ) { // means the data enter president & agent loop
      // echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 20 because the president take 50 bonus
      $director = 1;
      // $bonusType = 'Leadership';
      $directorMatching = 1;
      // if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
      // {
      //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
      //   if(!$user)
      //   {
      //       $getBonus = $getUplineUid[0]->getBonus(); // get bonus
      //       $getAmount = $getUplineUid[0]->getSales();
      //
      //       $latestBonus = $newBonus + $getBonus;
      //       $getFinalAmount = $newBonus + $getAmount;
      //
      //        $tableName = array();
      //        $tableValue =  array();
      //        $stringType =  "";
      //        if($latestBonus)
      //        {
      //             array_push($tableName,"bonus");
      //             array_push($tableValue,$latestBonus);
      //             $stringType .=  "i";
      //        }
      //        if($getFinalAmount)
      //        {
      //        array_push($tableName,"sales");
      //        array_push($tableValue,$getFinalAmount);
      //        $stringType .=  "i";
      //        }
      //        array_push($tableValue,$receiveUid);
      //        $stringType .=  "s";
      //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
      //        if($passwordUpdated)
      //        {
      //             // echo "success";
      //             // $_SESSION['messageType'] = 1;
      //             // header('Location: ../wallet.php?type=1');
      //             // header('Location: ../editprofile.php?type=3');
      //        }
      //        else
      //        {
      //             // echo "fail";
      //             // $_SESSION['messageType'] = 3;
      //             // header('Location: ../wallet.php?type=3');
      //        }
      //   }
      // }
        // $partner = 2;
    }elseif ($director == 2 && $partner == 1 && $president == 1 && $agent == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
    // echo $newBonus = $directorBonus - $partnerBonus; // get 20 leadership bonus because president already take the leadership bonus
    $director = 1; // means we already loop into partner
    $directorMatching = 1;
    // $bonusType = 'Leadership';
    // if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
    // {
    //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
    //   if(!$user)
    //   {
    //       $getBonus = $getUplineUid[0]->getBonus(); // get bonus
    //       $getAmount = $getUplineUid[0]->getSales();
    //
    //       $latestBonus = $newBonus + $getBonus;
    //       $getFinalAmount = $newBonus + $getAmount;
    //
    //        $tableName = array();
    //        $tableValue =  array();
    //        $stringType =  "";
    //        if($latestBonus)
    //        {
    //             array_push($tableName,"bonus");
    //             array_push($tableValue,$latestBonus);
    //             $stringType .=  "i";
    //        }
    //        if($getFinalAmount)
    //        {
    //        array_push($tableName,"sales");
    //        array_push($tableValue,$getFinalAmount);
    //        $stringType .=  "i";
    //        }
    //        array_push($tableValue,$receiveUid);
    //        $stringType .=  "s";
    //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //        if($passwordUpdated)
    //        {
    //             // echo "success";
    //             // $_SESSION['messageType'] = 1;
    //             // header('Location: ../wallet.php?type=1');
    //             // header('Location: ../editprofile.php?type=3');
    //        }
    //        else
    //        {
    //             // echo "fail";
    //             // $_SESSION['messageType'] = 3;
    //             // header('Location: ../wallet.php?type=3');
    //        }
    //   }
    // }

  }elseif ($director == 2 && $partner == 1 && $president == 2 && $agent == 2 && $directorMatching == 2 ) { // means the data didnt enter president and agent loop
    // echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
    $director = 1; // means we already loop into partner
    $directorMatching = 1;
    //   $bonusType = 'Leadership';
    //   if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
    //   {
    //     $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
    //     if(!$user)
    //     {
    //         $getBonus = $getUplineUid[0]->getBonus(); // get bonus
    //         $getAmount = $getUplineUid[0]->getSales();
    //
    //         $latestBonus = $newBonus + $getBonus;
    //         $getFinalAmount = $newBonus + $getAmount;
    //
    //          $tableName = array();
    //          $tableValue =  array();
    //          $stringType =  "";
    //          if($latestBonus)
    //          {
    //               array_push($tableName,"bonus");
    //               array_push($tableValue,$latestBonus);
    //               $stringType .=  "i";
    //          }
    //          if($getFinalAmount)
    //          {
    //          array_push($tableName,"sales");
    //          array_push($tableValue,$getFinalAmount);
    //          $stringType .=  "i";
    //          }
    //          array_push($tableValue,$receiveUid);
    //          $stringType .=  "s";
    //          $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //          if($passwordUpdated)
    //          {
    //               // echo "success";
    //               // $_SESSION['messageType'] = 1;
    //               // header('Location: ../wallet.php?type=1');
    //               // header('Location: ../editprofile.php?type=3');
    //          }
    //          else
    //          {
    //               // echo "fail";
    //               // $_SESSION['messageType'] = 3;
    //               // header('Location: ../wallet.php?type=3');
    //          }
    //     }
    //   }

  }elseif ($director == 2 && $partner == 1 && $president == 2 && $agent == 1 && $directorMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
    // echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 40 because the  Agent take the bonus 30
    $director = 1;
    $directorMatching = 1;
    // $bonusType = 'Leadership';
    // if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
    // {
    //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
    //   if(!$user)
    //   {
    //       $getBonus = $getUplineUid[0]->getBonus(); // get bonus
    //       $getAmount = $getUplineUid[0]->getSales();
    //
    //       $latestBonus = $newBonus + $getBonus;
    //       $getFinalAmount = $newBonus + $getAmount;
    //
    //        $tableName = array();
    //        $tableValue =  array();
    //        $stringType =  "";
    //        if($latestBonus)
    //        {
    //             array_push($tableName,"bonus");
    //             array_push($tableValue,$latestBonus);
    //             $stringType .=  "i";
    //        }
    //        if($getFinalAmount)
    //        {
    //        array_push($tableName,"sales");
    //        array_push($tableValue,$getFinalAmount);
    //        $stringType .=  "i";
    //        }
    //        array_push($tableValue,$receiveUid);
    //        $stringType .=  "s";
    //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //        if($passwordUpdated)
    //        {
    //             // echo "success";
    //             // $_SESSION['messageType'] = 1;
    //             // header('Location: ../wallet.php?type=1');
    //             // header('Location: ../editprofile.php?type=3');
    //        }
    //        else
    //        {
    //             // echo "fail";
    //             // $_SESSION['messageType'] = 3;
    //             // header('Location: ../wallet.php?type=3');
    //        }
    //   }
    // }

  }elseif ($director == 2 && $partner == 1 && $president == 1 && $agent == 1 && $directorMatching == 2 ) { // means the data enter president & agent loop
    // echo $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 20 because the president take 50 bonus
    $director = 1;
    // $bonusType = 'Leadership';
    $directorMatching = 1;
    // if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
    // {
    //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
    //   if(!$user)
    //   {
    //       $getBonus = $getUplineUid[0]->getBonus(); // get bonus
    //       $getAmount = $getUplineUid[0]->getSales();
    //
    //       $latestBonus = $newBonus + $getBonus;
    //       $getFinalAmount = $newBonus + $getAmount;
    //
    //        $tableName = array();
    //        $tableValue =  array();
    //        $stringType =  "";
    //        if($latestBonus)
    //        {
    //             array_push($tableName,"bonus");
    //             array_push($tableValue,$latestBonus);
    //             $stringType .=  "i";
    //        }
    //        if($getFinalAmount)
    //        {
    //        array_push($tableName,"sales");
    //        array_push($tableValue,$getFinalAmount);
    //        $stringType .=  "i";
    //        }
    //        array_push($tableValue,$receiveUid);
    //        $stringType .=  "s";
    //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //        if($passwordUpdated)
    //        {
    //             // echo "success";
    //             // $_SESSION['messageType'] = 1;
    //             // header('Location: ../wallet.php?type=1');
    //             // header('Location: ../editprofile.php?type=3');
    //        }
    //        else
    //        {
    //             // echo "fail";
    //             // $_SESSION['messageType'] = 3;
    //             // header('Location: ../wallet.php?type=3');
    //        }
    //   }
    // }
      // $partner = 2;
  }
    elseif ($director == 1 && $directorMatching == 1 ) { // means the data already enter president loop because $president = 1
    echo $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
    $director = 1; // means we already loop into partner
    $directorMatching = 3;
    $bonusType = 'Matching';
    if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
    {}

  }}elseif ($getUplineUid[0]->getUserRank() == 'Partner' && $getUplineUid[0]->getStatus() != 'Inactive') { // if your upline partner
      if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $partnerMatching == 2 ) { // means the data already enter president loop because $president = 1
      // echo $newBonus = $partnerBonus - $presidentBonus; // get 20 leadership bonus because president already take the leadership bonus
      $partner = 1; // means we already loop into partner
      $partnerMatching = 1;
      // $bonusType = 'Leadership';
      // if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
      // {
      //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
      //   if(!$user)
      //   {
      //       $getBonus = $getUplineUid[0]->getBonus(); // get bonus
      //       $getAmount = $getUplineUid[0]->getSales();
      //
      //       $latestBonus = $newBonus + $getBonus;
      //       $getFinalAmount = $newBonus + $getAmount;
      //
      //        $tableName = array();
      //        $tableValue =  array();
      //        $stringType =  "";
      //        if($latestBonus)
      //        {
      //             array_push($tableName,"bonus");
      //             array_push($tableValue,$latestBonus);
      //             $stringType .=  "i";
      //        }
      //        if($getFinalAmount)
      //        {
      //        array_push($tableName,"sales");
      //        array_push($tableValue,$getFinalAmount);
      //        $stringType .=  "i";
      //        }
      //        array_push($tableValue,$receiveUid);
      //        $stringType .=  "s";
      //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
      //        if($passwordUpdated)
      //        {
      //             // echo "success";
      //             // $_SESSION['messageType'] = 1;
      //             // header('Location: ../wallet.php?type=1');
      //             // header('Location: ../editprofile.php?type=3');
      //        }
      //        else
      //        {
      //             // echo "fail";
      //             // $_SESSION['messageType'] = 3;
      //             // header('Location: ../wallet.php?type=3');
      //        }
      //   }
      // }

    }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $partnerMatching == 2 ) { // means the data didnt enter president and agent loop
      // echo $newBonus = $partnerBonus - $agentBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
      $partner = 1; // means we already loop into partner
      $partnerMatching = 1;
      //   $bonusType = 'Leadership';
      //   if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
      //   {
      //     $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
      //     if(!$user)
      //     {
      //         $getBonus = $getUplineUid[0]->getBonus(); // get bonus
      //         $getAmount = $getUplineUid[0]->getSales();
      //
      //         $latestBonus = $newBonus + $getBonus;
      //         $getFinalAmount = $newBonus + $getAmount;
      //
      //          $tableName = array();
      //          $tableValue =  array();
      //          $stringType =  "";
      //          if($latestBonus)
      //          {
      //               array_push($tableName,"bonus");
      //               array_push($tableValue,$latestBonus);
      //               $stringType .=  "i";
      //          }
      //          if($getFinalAmount)
      //          {
      //          array_push($tableName,"sales");
      //          array_push($tableValue,$getFinalAmount);
      //          $stringType .=  "i";
      //          }
      //          array_push($tableValue,$receiveUid);
      //          $stringType .=  "s";
      //          $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
      //          if($passwordUpdated)
      //          {
      //               // echo "success";
      //               // $_SESSION['messageType'] = 1;
      //               // header('Location: ../wallet.php?type=1');
      //               // header('Location: ../editprofile.php?type=3');
      //          }
      //          else
      //          {
      //               // echo "fail";
      //               // $_SESSION['messageType'] = 3;
      //               // header('Location: ../wallet.php?type=3');
      //          }
      //     }
      //   }

    }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $partnerMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
      // echo $newBonus = $partnerBonus - $agentBonus; // get leadership bonus 40 because the  Agent take the bonus 30
      // 
      $partner = 1;
      $partnerMatching = 1;
      // $bonusType = 'Leadership';
      // if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
      // {
      //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
      //   if(!$user)
      //   {
      //       $getBonus = $getUplineUid[0]->getBonus(); // get bonus
      //       $getAmount = $getUplineUid[0]->getSales();
      //
      //       $latestBonus = $newBonus + $getBonus;
      //       $getFinalAmount = $newBonus + $getAmount;
      //
      //        $tableName = array();
      //        $tableValue =  array();
      //        $stringType =  "";
      //        if($latestBonus)
      //        {
      //             array_push($tableName,"bonus");
      //             array_push($tableValue,$latestBonus);
      //             $stringType .=  "i";
      //        }
      //        if($getFinalAmount)
      //        {
      //        array_push($tableName,"sales");
      //        array_push($tableValue,$getFinalAmount);
      //        $stringType .=  "i";
      //        }
      //        array_push($tableValue,$receiveUid);
      //        $stringType .=  "s";
      //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
      //        if($passwordUpdated)
      //        {
      //             // echo "success";
      //             // $_SESSION['messageType'] = 1;
      //             // header('Location: ../wallet.php?type=1');
      //             // header('Location: ../editprofile.php?type=3');
      //        }
      //        else
      //        {
      //             // echo "fail";
      //             // $_SESSION['messageType'] = 3;
      //             // header('Location: ../wallet.php?type=3');
      //        }
      //   }
      // }

    }elseif ($director == 2 && $partner == 2 && $president == 1 && $agent == 1 && $partnerMatching == 2 ) { // means the data enter president & agent loop
      // echo $newBonus = $partnerBonus - $presidentBonus; // get leadership bonus 20 because the president take 50 bonus
      // 
      $partner = 1;
      // $bonusType = 'Leadership';
      $partnerMatching = 1;
      // if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
      // {
      //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
      //   if(!$user)
      //   {
      //       $getBonus = $getUplineUid[0]->getBonus(); // get bonus
      //       $getAmount = $getUplineUid[0]->getSales();
      //
      //       $latestBonus = $newBonus + $getBonus;
      //       $getFinalAmount = $newBonus + $getAmount;
      //
      //        $tableName = array();
      //        $tableValue =  array();
      //        $stringType =  "";
      //        if($latestBonus)
      //        {
      //             array_push($tableName,"bonus");
      //             array_push($tableValue,$latestBonus);
      //             $stringType .=  "i";
      //        }
      //        if($getFinalAmount)
      //        {
      //        array_push($tableName,"sales");
      //        array_push($tableValue,$getFinalAmount);
      //        $stringType .=  "i";
      //        }
      //        array_push($tableValue,$receiveUid);
      //        $stringType .=  "s";
      //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
      //        if($passwordUpdated)
      //        {
      //             // echo "success";
      //             // $_SESSION['messageType'] = 1;
      //             // header('Location: ../wallet.php?type=1');
      //             // header('Location: ../editprofile.php?type=3');
      //        }
      //        else
      //        {
      //             // echo "fail";
      //             // $_SESSION['messageType'] = 3;
      //             // header('Location: ../wallet.php?type=3');
      //        }
      //   }
      // }
      //   // $partner = 2;
    }
    elseif ($partner == 1 && $partnerMatching == 1 ) { // means the data already enter president loop because $president = 1
    echo $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
    echo 
    $partner = 1; // means we already loop into partner
    $partnerMatching = 3;
    $bonusType = 'Matching';
    if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
    {}

  }}elseif ($getUplineUid[0]->getUserRank() == 'President' && $getUplineUid[0]->getStatus() != 'Inactive') { // if your upline president
      if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $presidentMatching == 2) { // means the data already enter partner loop because $partner = 1
      // echo $newBonus = $partnerBonus - $presidentBonus - $agentBonus; // not get any leadership bonus because partner already take the leadership bonus
      $president = 1;
      $presidentMatching = 1;

    }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $presidentMatching == 2) { // means the data didnt enter partner loop because $partner == 2
       // echo $newBonus = $presidentBonus - $agentBonus; // get leadership bonus 50
       // 
       $president = 1;
       $presidentMatching = 1;
       // $bonusType = 'Leadership';
       // if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
       // {
       //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
       //   if(!$user)
       //   {
       //       $getBonus = $getUplineUid[0]->getBonus(); // get bonus
       //       $getAmount = $getUplineUid[0]->getSales();
       //
       //       $latestBonus = $newBonus + $getBonus;
       //       $getFinalAmount = $newBonus + $getAmount;
       //
       //        $tableName = array();
       //        $tableValue =  array();
       //        $stringType =  "";
       //        if($latestBonus)
       //        {
       //             array_push($tableName,"bonus");
       //             array_push($tableValue,$latestBonus);
       //             $stringType .=  "i";
       //        }
       //        if($getFinalAmount)
       //        {
       //        array_push($tableName,"sales");
       //        array_push($tableValue,$getFinalAmount);
       //        $stringType .=  "i";
       //        }
       //        array_push($tableValue,$receiveUid);
       //        $stringType .=  "s";
       //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
       //        if($passwordUpdated)
       //        {
       //             // echo "success";
       //             // $_SESSION['messageType'] = 1;
       //             // header('Location: ../wallet.php?type=1');
       //             // header('Location: ../editprofile.php?type=3');
       //        }
       //        else
       //        {
       //             // echo "fail";
       //             // $_SESSION['messageType'] = 3;
       //             // header('Location: ../wallet.php?type=3');
       //        }
       //   }
       // }

   }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $presidentMatching == 2) { // means the data  enter  agent loop because $agent == 1
     // echo $newBonus = $presidentBonus - $agentBonus; // get leadership bonus 20 because the  Agent take the bonus 30
     // 
     $president = 1;
     $presidentMatching = 1;
     // $bonusType = 'Leadership';
     // if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
     // {
     //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
     //   if(!$user)
     //   {
     //       $getBonus = $getUplineUid[0]->getBonus(); // get bonus
     //       $getAmount = $getUplineUid[0]->getSales();
     //
     //       $latestBonus = $newBonus + $getBonus;
     //       $getFinalAmount = $newBonus + $getAmount;
     //
     //        $tableName = array();
     //        $tableValue =  array();
     //        $stringType =  "";
     //        if($latestBonus)
     //        {
     //             array_push($tableName,"bonus");
     //             array_push($tableValue,$latestBonus);
     //             $stringType .=  "i";
     //        }
     //        if($getFinalAmount)
     //        {
     //        array_push($tableName,"sales");
     //        array_push($tableValue,$getFinalAmount);
     //        $stringType .=  "i";
     //        }
     //        array_push($tableValue,$receiveUid);
     //        $stringType .=  "s";
     //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
     //        if($passwordUpdated)
     //        {
     //             // echo "success";
     //             // $_SESSION['messageType'] = 1;
     //             // header('Location: ../wallet.php?type=1');
     //             // header('Location: ../editprofile.php?type=3');
     //        }
     //        else
     //        {
     //             // echo "fail";
     //             // $_SESSION['messageType'] = 3;
     //             // header('Location: ../wallet.php?type=3');
     //        }
     //   }
     // }

   }
  elseif ($president == 1 && $presidentMatching == 1) { // means the data didnt enter partner loop because $partner == 2
  echo $newBonus = 25; // get leadership bonus 50
  
  $president = 1;
  $presidentMatching = 3;
  $bonusType = 'Matching';
  if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
  {}

  }}elseif ($getUplineUid[0]->getUserRank() == 'Agent' && $getUplineUid[0]->getStatus() != 'Inactive') { // if your upline agent
    if ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $agentMatching == 2) { // means the data  enter  agent loop because $agent == 1
      $agent = 1;
      $agentMatching = 1;
    }
  elseif ($agentMatching == 1 ) { // means the data didnt enter partner loop because $partner == 2
   echo $newBonus = 25; // get leadership bonus 30
   
   $agent = 1;
   $agentMatching = 3;
   $bonusType = 'Matching';
   if(leadership($conn,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName))
   {}

  }}}}
  //=========================================================================================================================================================================================================================================
  $_SESSION['messageType'] = 1;
  header('Location: ../register.php?type=1');
  // }
        }
        else
        {
             header('Location: ../register.php?promptError=1');
        }
      }
      else
      {
           header('Location: ../register.php?promptError=1');
      }
  }
  else
  {
      $_SESSION['messageType'] = 1;
      header('Location: ../register.php?type=2');
      //echo "// register error with referral ";
  }
    }
    else
    { }
}
}else {
  header('Location: ../index.php');
}
 ?>
