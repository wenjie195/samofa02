<?php 

function MemberIdGenerator($conn){
   do{
    // $memberID = uniqid();
  $randomAlphabetOrDigit1 = rand(0,1);
  $randomAlphabetOrDigit2 = rand(0,1);
  $randomAlphabetOrDigit3 = rand(0,1);
  $randomAlphabetOrDigit4 = rand(0,1);
  $randomAlphabetOrDigit5 = rand(0,1);
  if ($randomAlphabetOrDigit1 == 0) {
    $alphabet1 = range('A', 'Z');
    $random1 = rand(0,25);
  }else{
    $alphabet1 = range(1,9);
    $random1 = rand(0,8);
  }
  if ($randomAlphabetOrDigit2 == 0) {
    $alphabet2 = range('A', 'Z');
    $random2 = rand(0,25);
  }else{
    $alphabet2 = range(1,9);
    $random2 = rand(0,8);
  }
  if ($randomAlphabetOrDigit3 == 0) {
    $alphabet3 = range('A', 'Z');
    $random3 = rand(0,25);
  }else{
    $alphabet3 = range(1,9);
    $random3 = rand(0,8);
  }
  if ($randomAlphabetOrDigit4 == 0) {
    $alphabet4 = range('A', 'Z');
    $random4 = rand(0,25);
  }else{
    $alphabet4 = range(1,9);
    $random4 = rand(0,8);
  }
  if ($randomAlphabetOrDigit5 == 0) {
    $alphabet5 = range('A', 'Z');
    $random5 = rand(0,25);
  }else{
    $alphabet5 = range(1,9);
    $random5 = rand(0,8);
  }
  $memberID = $alphabet1[$random1].$alphabet2[$random2].$alphabet3[$random3].$alphabet4[$random4].$alphabet5[$random5];
  $pendingUserId = getPendingUser($conn, "WHERE member_id =?",array("member_id"),array($memberID),"s");
  $userId = getUser($conn, "WHERE member_id =?",array("member_id"),array($memberID),"s");

  }while($pendingUserId || $userId);

return $memberID;
 }
 ?>