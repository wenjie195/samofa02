<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/Bonus.php';
require_once dirname(__FILE__) . '/../classes/OtherBonus.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$bonusDetails = getBonusDetails($conn, "WHERE status = 'PENDING'");
$otherBonusDetails = getOtherBonus($conn, "WHERE status = 'PENDING'");
if ($bonusDetails) {
  for ($i=0; $i <count($bonusDetails) ; $i++) {
    $receiveUid = $bonusDetails[$i]->getReferrerId();
    $bonus = $bonusDetails[$i]->getAmount();
    $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($receiveUid), "s");
    $cash = $userDetails[0]->getSales();
    $currentBonus = $userDetails[0]->getBonus();
    $totalCash = $cash + $bonus;
    $totalBonus = $currentBonus + $bonus;
    $status = 'COMPLETED';

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    //echo "save to database";
    if($totalCash)
    {
         array_push($tableName,"sales");
         array_push($tableValue,$totalCash);
         $stringType .=  "i";
    }
    if($totalBonus)
    {
         array_push($tableName,"bonus");
         array_push($tableValue,$totalBonus);
         $stringType .=  "i";
    }
    array_push($tableValue,$receiveUid);
    $stringType .=  "s";
    $userUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    if($userUpdated)
    {
      $tableName = array();
      $tableValue =  array();
      $stringType =  "";
      //echo "save to database";
      if($status)
      {
           array_push($tableName,"status");
           array_push($tableValue,$status);
           $stringType .=  "s";
      }
      array_push($tableValue,$receiveUid);
      $stringType .=  "s";
      $bonusUpdated = updateDynamicData($conn,"bonus"," WHERE referrer_id = ? ",$tableName,$tableValue,$stringType);
      if($bonusUpdated){

      }
    }
    else
    { }
  }
}

if ($otherBonusDetails) {
  for ($j=0; $j <count($otherBonusDetails) ; $j++) {
    $receiveUid = $otherBonusDetails[$j]->getReceiveUid();
    $bonus = $otherBonusDetails[$j]->getBonus();
    $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($receiveUid), "s");
    $cash = $userDetails[0]->getSales();
    $currentBonus = $userDetails[0]->getBonus();
    $totalCash = $cash + $bonus;
    $totalBonus = $currentBonus + $bonus;
    $status = 'COMPLETED';

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    //echo "save to database";
    if($totalCash)
    {
         array_push($tableName,"sales");
         array_push($tableValue,$totalCash);
         $stringType .=  "i";
    }
    if($totalBonus)
    {
         array_push($tableName,"bonus");
         array_push($tableValue,$totalBonus);
         $stringType .=  "i";
    }
    array_push($tableValue,$receiveUid);
    $stringType .=  "s";
    $userUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    if($userUpdated)
    {
      $tableName = array();
      $tableValue =  array();
      $stringType =  "";
      //echo "save to database";
      if($status)
      {
           array_push($tableName,"status");
           array_push($tableValue,$status);
           $stringType .=  "s";
      }
      array_push($tableValue,$receiveUid);
      $stringType .=  "s";
      $bonusUpdated = updateDynamicData($conn,"other_bonus"," WHERE receive_uid = ? ",$tableName,$tableValue,$stringType);
      if($bonusUpdated){

      }
    }
    else
    { }
  }
}
 ?>
