<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $productId = rewrite($_POST["product_id"]);

    // $stringTwo = ($_POST['update_product_name']);
    // $productName = preg_replace('/[^\p{L}\p{N}\s][^(\x20-\x7F)]*/u', '', $stringTwo);

    $productName = ($_POST['product_name']);
    $description = ($_POST['product_description']);

    $productPrice = ($_POST['product_price']);
    $productOriPrice = ($_POST['product_member_price']);
    $rankAPrice = ($_POST['rank_a_price']);
    $rankBPrice = ($_POST['rank_b_price']);
    $rankCPrice = ($_POST['rank_c_price']);

    $oriFileOne = rewrite($_POST["ori_fileone"]);
    $newFileOne = $_FILES['image_one']['name'];
    if($newFileOne == '')
    {
        $file = $oriFileOne;
    }
    else
    {
        $file = $oriFileOne;
        $file = $timestamp.$_FILES['image_one']['name'];
        $target_dir = "../ProductImages/";
        $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
        // Select file type
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Valid file extensions
        $extensions_arr = array("jpg","jpeg","png","gif","pdf");
        if( in_array($imageFileType,$extensions_arr) )
        {
            move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$file);
        }
    }

    $product = getProduct($conn,"WHERE id = ? ",array("id"),array($productId),"s");     

    if($product)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($productName)
        {
            array_push($tableName,"name");
            array_push($tableValue,$productName);
            $stringType .=  "s";
        }
        if($description)
        {
            array_push($tableName,"description");
            array_push($tableValue,$description);
            $stringType .=  "s";
        }
        if($file)
        {
            array_push($tableName,"images");
            array_push($tableValue,$file);
            $stringType .=  "s";
        }

        if($productPrice)
        {
            array_push($tableName,"price");
            array_push($tableValue,$productPrice);
            $stringType .=  "d";
        }
        if($productOriPrice)
        {
            array_push($tableName,"rankOri");
            array_push($tableValue,$productOriPrice);
            $stringType .=  "d";
        }
        if($rankAPrice)
        {
            array_push($tableName,"rankA");
            array_push($tableValue,$rankAPrice);
            $stringType .=  "d";
        }
        if($rankBPrice)
        {
            array_push($tableName,"rankB");
            array_push($tableValue,$rankBPrice);
            $stringType .=  "d";
        }
        if($rankCPrice)
        {
            array_push($tableName,"rankC");
            array_push($tableValue,$rankCPrice);
            $stringType .=  "d";
        }
        

        array_push($tableValue,$productId);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"product"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            // $_SESSION['messageType'] = 1;
            // header('Location: ../adminProductAll.php?type=1');
            echo "<script>alert('Product Details Updated !!');window.location='../adminViewCurrentProduct.php'</script>";  
        }
        else
        {
            echo "<script>alert('FAIL TO EDIT PRODUCT DETAILS');window.location='../adminViewCurrentProduct.php'</script>";  
        }
    }
    else
    {
        echo "<script>alert('ERROR');window.location='../adminViewCurrentProduct.php'</script>";  
    }

}
else 
{
    header('Location: ../index.php');
}
?>