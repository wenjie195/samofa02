<?php 
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/SignUpProduct.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/Product.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$orderId = $_POST['orderId'];
$uid = $_POST['uid'];
$total = $_POST['total'];
// $orderId = 2;
// $uid = "caee57ad372f765a026404b3a9157f76";
// $total = 426;
$type = 'Wallet';
$status = 'APPROVED';
$NoProductToPack = 5;	// no of product need to become package
$rebate = "YES";
$rebateZero = 0;
$package = 799;
$totalProductBuy = 0;
$oldTotalProduct = 0;
$totalAmount = 0;
$totalAmountWithoutReb = 0;
$totalAmountWithoutRebate = 0;
$totalRebate = 0;
$totalSubTotal = 0;
$totalRebateFinal  = 0;
$reallyOldProductLeft = 0;
$reallyOldRebateTotal = 0;

$ordersDetails = getOrders($conn, "WHERE uid =? AND product_type = 'Product'",array("uid"),array($uid), "s");
for ($i=0; $i <count($ordersDetails) ; $i++) { 
	if ($ordersDetails[$i]->getRebate() == 'YES' && $ordersDetails[$i]->getProductExcludeRebate() != 0 && $ordersDetails[$i]->getRebateDone() != 0) { // if rebate = 'YES' and product_excl_rebate not equal 0.
		$totalProductBuy += $ordersDetails[$i]->getProductExcludeRebate();
		$reallyOldProductLeft = $ordersDetails[$i]->getProductExcludeRebate();
		$reallyOldRebateTotal = $ordersDetails[$i]->getRebateTotal();
		// $totalAmount = $ordersDetails[$i]->getSubTotal();
		// $totalAmountWithoutReb = $totalProductBuy * $totalAmount;
	}
	if ($ordersDetails[$i]->getRebate() == 'NO') {
		$totalProductBuy += $ordersDetails[$i]->getTotalProduct();
	}
}
	$totalProductBuy = $totalProductBuy + $reallyOldProductLeft; // total product plus with really old product left on rebate

if ($totalProductBuy == 5) {
	// echo "rebate";
	$rebateOrderDetails = getOrders($conn, "WHERE uid =? AND rebate ='NO' AND product_type = 'Product'",array("uid"),array($uid), "s");
	for ($n=0; $n <count($rebateOrderDetails) ; $n++) { 

		$totalAmountWithoutRebate += $rebateOrderDetails[$n]->getSubTotal(); // subtotal without rebate 

		$orderssUpdated = updateDynamicData($conn,"orders"," WHERE id = ? ",array("rebate"),array($rebate,$rebateOrderDetails[$n]->getId()),"ss");
		}
		$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid), "s");
		$currentwallet = $userDetails[0]->getSales();

		$totalRebate = $totalAmountWithoutRebate - $package;
		$totalRebatePlusWallet = $currentwallet + $totalRebate + $reallyOldRebateTotal;

		$totalRebateUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("sales"),array($totalRebatePlusWallet,$uid),"ss");
		// $ordersUpdated = updateDynamicData($conn,"orders"," WHERE id = ? ",array("rebate_done"),array($rebateZero,$ordersDetails[$n]->getId()),"ss");
}
elseif ($totalProductBuy > 5) {
	// echo "rebate";
	$ordersDetails = getOrders($conn, "WHERE uid =? AND product_type = 'Product' LIMIT 1",array("uid"),array($uid), "s");
	// for ($p=0; $p <count($ordersDetails) ; $p++) { 
		if ($ordersDetails[0]->getRebate() == 'YES' && $ordersDetails[0]->getProductExcludeRebate() != 0 && $ordersDetails[0]->getRebateDone() != 0){
			$ordersUpdated = updateDynamicData($conn,"orders"," WHERE id = ? ",array("rebate_done"),array($rebateZero,$ordersDetails[0]->getId()),"ss");
		}
	// }

	$oldOrdersDetails = getOrders($conn, "WHERE id !=? AND uid =? AND rebate ='NO' AND product_type = 'Product'",array("id","uid"),array($orderId,$uid), "ss");

	for ($j=0; $j <count($oldOrdersDetails) ; $j++) { 
		$oldTotalProduct += $oldOrdersDetails[$j]->getTotalProduct();
	}
	$oldTotalProduct += $reallyOldProductLeft;
	$productWillInc = $NoProductToPack - $oldTotalProduct;

	$newOrdersDetails = getOrders($conn, "WHERE id =? AND rebate ='NO' AND product_type = 'Product'",array("id"),array($orderId), "s");
	$newTotalProduct = $newOrdersDetails[0]->getTotalProduct(); // get new total product ex. 7
	$productWillExc = $newTotalProduct - $productWillInc; // no of product that will be excluded ex. 5
	$productQuantity = $newOrdersDetails[0]->getQuantity();
	$productId = $newOrdersDetails[0]->getProductId();
	$productQuantityExp = explode(",",$productQuantity);
	$productIdExp = explode(",",$productId);
	// if ($productWillExc == 1) {
		for ($k=0; $k <count($productQuantityExp) ; $k++) { 
			$productTotal = $productQuantityExp[$k] + $oldTotalProduct; // old product add with first quantity old total - 2, productquantityexp - 2, old total - 4, productquantityexp - 2 = 6
			if ($productQuantityExp[0] == 5) { // if total quantity >= 5
				// $addHowManyMore = $NoProductToPack - $oldTotalProduct; // how many more product we need, old total 4, 5-4 = 1;
				// if ($productQuantityExp[$k] > 1) {
				// 	$productExcludeNew = $productQuantityExp[$k] - $addHowManyMore; // new product - 2, addhowmanymore - 1 = 1;
				// }else{
				// 	$productExcludeNew = 1;
				// }
				$productExcludeNew = $newOrdersDetails[0]->getRebateDone(); // exclude product
				$productDetails = getProduct($conn,"WHERE id =?",array("id"),array($productIdExp[$k]), "s");
				$productPrice = $productDetails[0]->getPrice(); // get product price 188
				$totalPriceInclude = $productPrice * $productQuantityExp[0];
				$totalPriceExclude = $newOrdersDetails[0]->getSubTotal() - $totalPriceInclude;
				$ordersUpdated = updateDynamicData($conn,"orders"," WHERE id = ? ",array("product_excl_rebate","rebate_done","rebate_total"),array($productExcludeNew,$productExcludeNew,$totalPriceExclude,$orderId),"ssss");
			}if ($productQuantityExp[0] >= 5) { // if total quantity >= 5
				// $addHowManyMore = $NoProductToPack - $oldTotalProduct; // how many more product we need, old total 4, 5-4 = 1;
				// if ($productQuantityExp[$k] > 1) {
				// 	$productExcludeNew = $productQuantityExp[$k] - $addHowManyMore; // new product - 2, addhowmanymore - 1 = 1;
				// }else{
				// 	$productExcludeNew = 1;
				// }
				$productExcludeNew = $newOrdersDetails[0]->getRebateDone(); // exclude product
				$productDetails = getProduct($conn,"WHERE id =?",array("id"),array($productIdExp[$k]), "s");
				$productPrice = $productDetails[0]->getPrice(); // get product price 188
				$totalPriceInclude = $productPrice * $productTotal;
				$ordersUpdated = updateDynamicData($conn,"orders"," WHERE id = ? ",array("product_excl_rebate","rebate_done","rebate_total"),array($productExcludeNew,$productExcludeNew,$totalPriceExclude,$orderId),"ssss");
			}if ($productTotal >= 5) { // if total quantity >= 5
				// $addHowManyMore = $NoProductToPack - $oldTotalProduct; // how many more product we need, old total 4, 5-4 = 1;
				// if ($productQuantityExp[$k] > 1) {
				// 	$productExcludeNew = $productQuantityExp[$k] - $addHowManyMore; // new product - 2, addhowmanymore - 1 = 1;
				// }else{
				// 	$productExcludeNew = 1;
				// }
				$productExcludeNew = $newOrdersDetails[0]->getRebateDone(); // exclude product
				$productDetails = getProduct($conn,"WHERE id =?",array("id"),array($productIdExp[$k]), "s");
				$productPrice = $productDetails[0]->getPrice(); // get product price 188
				$totalPriceExclude = $productPrice * $productExcludeNew;
				$ordersUpdated = updateDynamicData($conn,"orders"," WHERE id = ? ",array("product_excl_rebate","rebate_done","rebate_total"),array($productExcludeNew,$productExcludeNew,$totalPriceExclude,$orderId),"ssss");
			}else{ // if quantity not >= 5 we will add first quantityto old total product variable
				$oldTotalProduct += $productQuantityExp[$k]; // old - 2 + productquantityexp - 2 = 4;
				// $totalProductSelected += $productQuantityExp[$k];

			}
		}
	// }
	$totalAmountWithoutRebate = $newOrdersDetails[0]->getSubTotal();
	$totalRebate = $totalAmountWithoutRebate - $productPrice;

	// $ordersUpdated = updateDynamicData($conn,"orders"," WHERE id = ? ",array("product_excl_rebate"),array($productWillExc,$orderId),"ss");
	if ($ordersUpdated) {
		$rebateOrdersDetails = getOrders($conn, "WHERE uid =? AND rebate ='NO' AND product_type = 'Product'",array("uid"),array($uid), "s");
		for ($m=0; $m <count($rebateOrdersDetails) ; $m++) { 
			$totalSubTotal += $rebateOrdersDetails[$m]->getSubTotal();
			$totalRebateFinal += $rebateOrdersDetails[$m]->getRebateTotal();
			$orderssUpdated = updateDynamicData($conn,"orders"," WHERE id = ? ",array("rebate"),array($rebate,$rebateOrdersDetails[$m]->getId()),"ss");
		}
		$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid), "s");
		$currentwallet = $userDetails[0]->getSales();

		$totalSubTotalNoMinusWithPackage = $totalSubTotal - $totalRebateFinal;
		$rebateToUser = $totalSubTotalNoMinusWithPackage - $package;
		// $newWallet = $currentwallet + $rebateToUser;
		$totalRebatePlusWallet = $currentwallet + $rebateToUser + $reallyOldRebateTotal;
		$totalRebateUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("sales"),array($totalRebatePlusWallet,$uid),"ss");
		// for ($o=0; $o <count($ordersDetails) ; $o++) { 
		// 	$ordersUpdated = updateDynamicData($conn,"orders"," WHERE id = ? ",array("rebate_done"),array($rebateZero,$ordersDetails[$o]->getId()),"ss");
		// }
	}
}

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid), "s");
$currentwallet = $userDetails[0]->getSales();

$newWallet = $currentwallet - $total;
// $totalRebatePlusWallet = $totalRebate + $newWallet;

$userUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("sales"),array($newWallet,$uid),"ss");
$orderUpdated = updateDynamicData($conn,"orders"," WHERE id = ? ",array("payment_method","payment_amount","status"),array($type,$total,$status,$orderId),"ssss");
// $totalRebateUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("sales"),array($totalRebatePlusWallet,$uid),"ss");
// if ($userUpdated && $orderUpdated) {
	$store[] = array("uid" => $uid);
	echo json_encode($store);
// }
 ?>