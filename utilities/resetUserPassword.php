<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid  = $_POST['uid'];
     $verify_Pass  = 123321;
     $status = "APPROVED";

     $user = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

                    $password = hash('sha256',$verify_Pass);
                    $salt = substr(sha1(mt_rand()), 0, 100);
                    $finalPassword = hash('sha256', $salt.$password);

                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("password","salt"),array($finalPassword,$salt,$user[0]->getUid()),"sss");
                    $forgotPasswordUpdated = updateDynamicData($conn,"forgot"," WHERE uid = ? ",array("status"),array($status,$uid),"ss");
                    if($passwordUpdated && $forgotPasswordUpdated)
                    {
                         // echo "Successfullly changed password";
                         echo "<script>alert('successfullly changed password');window.location='../adminViewForgot.php'</script>"; 
                    }
                    else 
                    {
                         // echo "unable to change password";
                         echo "<script>alert('unable to change password !');window.location='../adminViewForgot.php'</script>"; 
                    }
}
else 
{
     header('Location: ../index.php');
}
?>