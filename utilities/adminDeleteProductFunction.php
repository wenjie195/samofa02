<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $display  = "0";
    $type  = "0";

    $productId = rewrite($_POST["product_id"]);

    // // FOR DEBUGGING 
    // echo "<br>";
    // echo $username."<br>";
    // echo $firstname."<br>";

    $product = getProduct($conn, "WHERE id = ? ",array("id"),array($productId),"s");    

    if($product)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if(!$display || $display)
        {
            array_push($tableName,"display");
            array_push($tableValue,$display);
            $stringType .=  "s";
        }
        if(!$type || $type)
        {
            array_push($tableName,"type");
            array_push($tableValue,$type);
            $stringType .=  "i";
        }
        array_push($tableValue,$productId);
        $stringType .=  "s";
        $annouceUpdated = updateDynamicData($conn,"product"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        if($annouceUpdated)
        {
            echo "<script>alert('Product Deleted !');window.location='../adminViewCurrentProduct.php'</script>";
        }
        else
        {
            echo "<script>alert('Fail to delete product!');window.location='../adminViewCurrentProduct.php'</script>";
        }
    }
    else
    {
        echo "<script>alert('ERROR !!');window.location='../adminViewCurrentProduct.php'</script>";
    }

}
else 
{
    header('Location: ../index.php');
}
?>
