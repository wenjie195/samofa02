<?php 
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/SignUpProduct.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Orders.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$id = $_POST['id'];
$pickupType = $_POST['pickup_type'];
$courierType = $_POST['courier_type'];
$courierName = $_POST['courier_other'];
$trackingNo = $_POST['tracking_no'];

if ($courierType == 'Other') {
	$courierType = $courierName;
}

if (isset($_POST["submit"])) {
	$status = "APPROVED";
	$orderUpdated = updateDynamicData($conn,"orders"," WHERE id = ? ",array("pickup_type","pickup_name","tracking_no"),array($pickupType,$courierType,$trackingNo,$id),"ssss");
	$_SESSION['messageType'] = 1;
	header('location: ../adminViewOrder.php?type=1');
}
 ?>