<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$countryList = getCountries($conn);

$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userData = $userDetails[0];

$productDetails = getProduct($conn, "WHERE product_type='Package'");
// $userRanking = $userData->getRank();
// $downlineNumber = $userData->getRankDValue();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dxforextrade88.com/" />-->
    <meta property="og:title" content="Register | Samofa 莎魔髪" />
    <title>Register | Samofa 莎魔髪</title>
    <!--<link rel="canonical" href="https://dxforextrade88.com/" />-->
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 same-padding menu-distance75">

   <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _HEADERBEFORELOGIN_REGISTER ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

    <!-- <form> -->
 	<form action="utilities/addReferee.php" method="POST">
 		<div class="dual-input">
        	<p class="input-top-text"><?php echo _JS_COUNTRY ?></p>
            <!-- <input class="clean de-input" type="text" placeholder="<?php //echo _JS_COUNTRY ?>" id="register_country" name="register_country" required> -->
            <select class="clean de-input" id="register_country" name="register_country" required>
                <option><?php echo _JS_COUNTRY ?></option>
                <?php
                for ($cntPro=0; $cntPro <count($countryList) ; $cntPro++)
                {
                ?>
                    <option value="<?php echo $countryList[$cntPro]->getEnName(); ?>">
                        <?php echo $countryList[$cntPro]->getEnName();?>
                    </option>
                <?php
                }
                ?>
            </select>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _JS_USERNAME ?></p>
            <input class="clean de-input" type="text" placeholder="<?php echo _JS_USERNAME ?>" id="register_username" name="register_username" required>
        	<!-- <p class="input-top-text"><?php echo _JS_FIRSTNAME ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _JS_FIRSTNAME ?>" id="register_firstname" name="register_firstname" required> -->
        </div>
        <div class="clear"></div>
        <div class="dual-input">
            <p class="input-top-text"><?php echo _JS_FIRSTNAME ?></p>
            <input class="clean de-input" type="text" placeholder="<?php echo _JS_FIRSTNAME ?>" id="register_firstname" name="register_firstname" required>
        	<!-- <p class="input-top-text"><?php echo _JS_LASTNAME ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _JS_LASTNAME ?>" id="register_lastname" name="register_lastname" required> -->
        </div>
        <div class="dual-input second-dual-input">
        	<!-- <p class="input-top-text"><?php echo _JS_USERNAME ?></p>
            <input class="clean de-input" type="text" placeholder="<?php echo _JS_USERNAME ?>" id="register_username" name="register_username" required> -->
            <p class="input-top-text"><?php echo _JS_LASTNAME ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _JS_LASTNAME ?>" id="register_lastname" name="register_lastname" required>
        </div>
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-text"><?php echo _JS_EMAIL ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _JS_EMAIL ?>" id="register_email" name="register_email" required>
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-text"><?php echo _JS_PHONE ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _JS_PHONE ?>" id="register_mobileno" name="register_mobileno" required>
        </div>
        <div class="clear"></div>

        <?php //$userRanking;
            // if($userRanking == 'Member')
            // {
            //     $downlineAmount = $userData->getRankAValue();
            // }
            // elseif($userRanking == 'Rising Star')
            // {
            //     $downlineAmount = $userData->getRankBValue();
            // }
            // elseif($userRanking == 'Group Star')
            // {
            //     $downlineAmount = $userData->getRankCValue();
            // }
            // elseif($userRanking == 'Group Boss')
            // {
            //     $downlineAmount = $userData->getRankDValue();
            // }
            // else
            // {
            //     $downlineAmount = "0";
            // }
        ?>

        <div class="dual-input">
        	<p class="input-top-text"><?php echo _JS_ENROLLMENT_PRODUCT ?></p>
            <select class="clean de-input" type="text" id="register_product" name="register_product" required>
                <option value="" name=" "><?php echo _JS_SELECT_PRODUCT ?></option>
                <?php if ($productDetails) {
                    for ($i=0; $i <count($productDetails) ; $i++) { 
                        ?>
                        <option value="<?php echo $productDetails[$i]->getId() ?>"><?php echo $productDetails[$i]->getName() ?></option>
                        <?php
                    }
                } ?>
                <!-- <option value="Magic BloCA" name="Magic BloCA">Magic BloCA 脂糖宝</option> -->
                <!-- <option value="Hair Shampoo Serum" name="Product 2"><?php //echo _INDEX_HAIR_SERUM_SHORT ?></option> -->
            </select>
        </div>

        <!-- <div class="dual-input second-dual-input">
        	<p class="input-top-text"><?php echo _JS_SPONSOR_ID ?></p>
            <input class="clean de-input no-input" type="text" value="<?php echo $userData->getMemberID();?>" id="sponsor_id" name="sponsor_id" readonly>
		</div>

        <div class="clear"></div>

        <div class="dual-input">
        	<p class="input-top-text"><?php echo _USERDASHBOARD_RANK ?></p>
            <input class="clean de-input no-input" type="text" value="<?php echo $userRanking ?>" id="user_ranking" name="user_ranking" readonly>
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-text"><?php echo _USERDASHBOARD_DOWNLINE ?></p>
            <input class="clean de-input no-input" type="text" value="<?php echo $downlineNumber ?>" id="downline_amount" name="downline_amount" readonly>
		</div> -->

        <input class="clean de-input no-input" type="hidden" value="<?php echo $userData->getMemberID();?>" id="sponsor_id" name="sponsor_id" readonly>
        <input class="clean de-input no-input" type="hidden" value="<?php echo $userRanking ?>" id="user_ranking" name="user_ranking" readonly>
        <input class="clean de-input no-input" type="hidden" value="<?php echo $downlineNumber ?>" id="downline_amount" name="downline_amount" readonly>

        <div class="clear"></div>

        <div class="width100 text-center top-bottom-distance">
        	<button class="clean button-width transparent-button dark-pink-button" name="register"><?php echo _JS_SUBMIT ?></button>
        </div>
    </form>

</div>

	<div class="clear"></div>
    <img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">
    <div class="clear"></div>

<?php include 'js.php'; ?>
<script type="text/javascript">
    var x = location.search.replace('?', '').split('=');
    if (x[1] == 1) {
        alert("Success");
    }else if(x[1] == 2){
        alert("Error");
    }
</script>
</body>
</html>
