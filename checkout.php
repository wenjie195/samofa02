<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Checkout.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductImage.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$uid = $_SESSION['uid'];

// $id = getOrders($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1 ",array("uid"),array($uid),"s");
// $asd = $id[0]->getId();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];
// $rank= $userDetails->getStatus();
$rank= $userDetails->getRank();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $name = rewrite($_POST["insert_name"]);
    $contactNo = rewrite($_POST["insert_contactNo"]);
    $address_1 = rewrite($_POST["insert_address_1"]);
    $address_2 = rewrite($_POST["insert_address_2"]);
    $city = rewrite($_POST["insert_city"]);
    $zipcode = rewrite($_POST["insert_zipcode"]);
    $state = rewrite($_POST["insert_state"]);
    $country = rewrite($_POST["insert_country"]);
}

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,$rank,2);
}
// else
// {}

//shopping cart
$latestOrder = getOrders($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1 ",array("uid"),array($uid),"s");
$latestOrderUid = $latestOrder[0]->getId();
$ordersId = getProductOrders($conn," WHERE order_id = ? ",array("order_id"),array($latestOrderUid),"i");


$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/checkout.php" />

    <meta property="og:title" content="Check Out | Samofa 莎魔髪" />
    <title>Check Out | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/checkout.php" />
    <?php include 'css.php'; ?> 
    <style>
    td {
        text-align: center; 
        vertical-align: middle;
    }
    </style>
</head>

<body class="body">

<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-menu-distance75 same-padding">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _CHECKOUT_CHECKOUT ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    
    <form method="POST"  action="shipping.php" id="myFormId">
        <p class="info-title dark-pink-text"><b><?php echo _CHECKOUT_CONTACT ?></b></p>
        <?php
        if($userDetails)
        {
            $conn = connDB();
            //$orders = getUser($conn,"WHERE id = ? ", array("id") ,array($_POST['uid']),"s");
            $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
        ?> 
            <div class="dual-input">
                <p class="input-top-text"><?php echo _USERDASHBOARD_NAME ?></p>
                <input class="clean de-input" type="text" id="insert_name" name="insert_name" placeholder="<?php echo _JS_FULLNAME ?>" value="<?php echo $userRows[0]->getUsername()." ".$userRows[0]->getRecipientLastname();?>" required>
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text"><?php echo _JS_PHONE ?>  (<?php echo _CHECKOUT_VALIDPHONE ?>)</p>
                <input class="clean de-input" type="text" id="insert_contactNo" name="insert_contactNo"  placeholder="<?php echo _JS_PHONE ?>" value="<?php echo $userRows[0]->getPhoneNo();?>" required>
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-text"><?php echo _USERDASHBOARD_EMAIL ?></p>
                <input class="clean de-input" type="text" id="insert_email" name="insert_email" placeholder="<?php echo _USERDASHBOARD_EMAIL ?>" value="<?php echo $userRows[0]->getEmail();?>" required>
            </div>

            <div class="clear"></div>

            <p class="info-title dark-pink-text"><b><?php echo _CHECKOUT_SHIPPING_ADDRESS ?></b></p>

            <div class="dual-input">
                <p class="input-top-text"><?php echo _CHECKOUT_ADDRESS1 ?></p>
                <input class="clean de-input" type="text" type="text" id="insert_address_1" name="insert_address_1" placeholder="<?php echo _CHECKOUT_ADDRESS1 ?>" value="<?php echo $userRows[0]->getAddress();?>" required>
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text"><?php echo _CHECKOUT_ADDRESS2 ?></p>
                <input class="clean de-input" type="text" id="insert_address_2" name="insert_address_2"  placeholder="<?php echo _CHECKOUT_ADDRESS2 ?>" value="<?php echo $userRows[0]->getAddressB();?>" required>
            </div>

            <div class="clear"></div>    

            <div class="dual-input">
                <p class="input-top-text"><?php echo _CHECKOUT_CITY ?></p>
                <input class="clean de-input" type="text" type="text" id="insert_city" name="insert_city" placeholder="<?php echo _CHECKOUT_CITY ?>" value="<?php echo $userRows[0]->getCity();?>" required>
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text"><?php echo _CHECKOUT_ZIP_CODE ?></p>
                <input class="clean de-input" type="number" id="insert_zipcode" name="insert_zipcode"   placeholder="<?php echo _CHECKOUT_ZIP_CODE ?>" value="<?php echo $userRows[0]->getZipcode();?>" required>
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-text"><?php echo _CHECKOUT_STATE ?></p>
                <input class="clean de-input" type="text" type="text" id="insert_state" name="insert_state" placeholder="<?php echo _CHECKOUT_STATE ?>" value="<?php echo $userRows[0]->getState();?>" required>
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text"><?php echo _JS_COUNTRY ?></p>
                <input class="clean de-input" type="text" id="insert_country" name="insert_country"   placeholder="<?php echo _JS_COUNTRY ?>" value="<?php echo $userRows[0]->getCountry();?>" required>
            </div>

            <div class="dual-input">
                <input class="clean de-input" type="hidden" id="insert_bank" name="insert_bank" value="<?php echo $userRows[0]->getBankName();?>">
                <input class="clean de-input" type="hidden" id="insert_bank_accNo" name="insert_bank_accNo" value="<?php echo $userRows[0]->getBankAccNo();?>">
                <input class="clean de-input" type="hidden" id="insert_bank_accHolder" name="insert_bank_accHolder" value="<?php echo $userRows[0]->getBankAccHolder();?>">
            </div>

            <div class="clear"></div> 

            <div class="width100 text-center">
                <div class="width100 text-center top-bottom-spacing">
                    <input class="clean button-width transparent-button dark-pink-button checkout-btn" name="reset" type="reset" onclick="resetForm('myFormId'); return false;" />
                </div>
            </div>

            <div class="width100 top-bottom-spacing">
                <table class="table-css">
                    <thead>
                        <th style="text-align:center;" width="20%"><?php echo _HEADERBEFORELOGIN_PRODUCT ?></th>
                        <th style="text-align:center;" width="20%"><?php echo _VIEW_CART_QUANTITY ?></th>
                        <th style="text-align:center;" width="40%"><?php echo _PRODUCTDETAILS_NAME ?></th>
                        <th style="text-align:center;" width="20%"><?php echo _PRODUCTDETAILS_PRICE ?></th>
                    </thead>

                    <tbody>
                        <?php //echo $latestOrderUid;?>
                        <?php //echo $productListHtml;?>
                            <?php
                            $conn = connDB();
                            if($ordersId)
                            {   
                                for($cnt = 0;$cnt < count($ordersId) ;$cnt++)
                                {
                                ?>
                                <tr>
                                    <?php $proId = $ordersId[$cnt]->getProductId();?>
                                    <td width="20%">
                                        <?php echo $proId = $ordersId[$cnt]->getProductId();?>
                                        <?php
                                            $conn = connDB();
                                            $productRows = getProductImage($conn," WHERE id = ? ",array("id"),array($proId),"i");
                                            $proImage = $productRows[0]->getImages();
                                            // echo $proImage = $productRows[0]->getImages();
                                        ?>
                                        <img src="ProductImages/<?php echo $proImage;?>" class="product-img" alt="<?php echo $productRows[0]->getName();?>" title="<?php echo $productRows[0]->getName();?>" >
                                    </td>
                                    <td width="20%"><?php echo $quantity = $ordersId[$cnt]->getQuantity();?></td>
                                    <!-- <td width="40%"><?php //echo $ordersId[$cnt]->getProductId();?></td> -->
                                    <td width="40%"><?php echo $productRows[0]->getName();?></td>
                                    <?php 
                                        $price = $ordersId[$cnt]->getFinalPrice();
                                        $totalPrice = ($quantity * $price);
                                    ?>
                                    <td width="20%"><?php echo $totalPrice;?></td>
                                </tr>

                                <?php
                                }
                                ?>

                                <!-- <div class="width100 text-center margin-top40">
                                    <p class="pink-text">Shipping: Calculated at next step</p>
                                </div>
                                
                                <div class="width100 text-center">
                                    <p class="pink-text">Total: RM' .$subtotal.'.00</p>
                                </div>	 -->

                            <?php
                            }
                            // $conn->close();
                            ?>

                    </tbody>

                </table>

                <?php echo $productListHtml; ?>
            </div>

            <div class="clear"></div>

            <div class="width100 text-center">
                <div class="width100 text-center top-bottom-spacing">
                    <button id="shippingSubmit" class="clean button-width transparent-button dark-pink-button checkout-btn" name="shippingSubmit"><?php echo _CHECKOUT_NEXT ?></button>
                </div>

                <div class="clear"></div>

                <div class="width100 text-center margin-bottom30">
                    <p class="continue-shopping pointer continue2"><a href="viewCart.php" class="pink-text opacity-hover back-text"><img src="img/back.png" class="checkout-back-btn" alt="<?php echo _VIEW_CART_BACK ?>" title="<?php echo _VIEW_CART_BACK ?>" > <?php echo _VIEW_CART_BACK ?></a></p>
                </div>                    
            </div>
        <?php
        }
        ?> 

    </form>

</div>

<?php include 'js.php'; ?>

<?php 
    if(isset($_GET['type']))
    {
        $messageType = null;

        if($_SESSION['messageType'] == 1)
        {
            if($_GET['type'] == 1)
            {
                $messageType = "Please Fill Up The Required Details !";
            }
            if($_GET['type'] == 2)
            {
                $messageType = "Fail To Make Order !";
            }

            echo '
            <script>
                putNoticeJavascript("Notice !! ","'.$messageType.'");
            </script>
            ';   
            $_SESSION['messageType'] = 0;
        }
    }
?>
<script type="text/javascript">
   function resetForm(myFormId)
   {
       var myForm = document.getElementById(myFormId);

       //for (var i = 0; i < myForm.elements.length; i++)
       for (var i = 0; i < 9; i++)
       {
           if ('submit' != myForm.elements[i].type && 'reset' != myForm.elements[i].type)
           {
               myForm.elements[i].checked = false;
               myForm.elements[i].value = '';
               myForm.elements[i].selectedIndex = 0;
           }
       }
   }
</script>

</body>
</html>