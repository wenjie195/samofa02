<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/OtherBonus.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn);
// $conn->close();
$no = 0;

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/otherCommissionReport.php" />
    <meta property="og:title" content="Commission Report | Samofa 莎魔髪" />
    <title>Commission Report | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/otherCommissionReport.php" />
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
	<?php include 'css.php'; ?>

</head>

<body class="body">

<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h2 class="dark-pink-text hi-title text-center modal-h1 big-header-color"><a href="commissionReport.php" class="link-a"><?php echo "Direct Sponsor Bonus" ?></a> | <?php echo "Leadership Bonus" ?> | <a href="matchingReport.php" class="link-a"><?php echo "Matching Bonus" ?></a></h2>

    <div class="width100 container-div1">

        <div class="overflow-scroll-div same-padding">
            <table class="table-css">
                <thead>
                    <tr>
                        <th><?php echo _PRODUCTDETAILS_NO ?></th>
                        <th><?php echo _JS_USERNAME ?></th>
                        <th><?php echo _ADMIN_AMOUNT ?></th>
                        <th>Bonus Type</th>
                        <th><?php echo _ADMINHEADER_REMARKS ?></th>
                        <th><?php echo _USERDASHBOARD_STATUS ?></th>
                        <th><?php echo _PURCHASE_DATE ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                      $bonusDetails = getOtherBonus($conn, " WHERE receive_uid =? AND bonus_type = 'Leadership'", array("receive_uid"), array($uid), "s");
                        if($bonusDetails)
                        {
                            for($cnt = 0;$cnt < count($bonusDetails) ;$cnt++)
                            {
                              $no += 1;
                              $referralDetails = getUser($conn, "WHERE uid =?",array("uid"),array($bonusDetails[$cnt]->getUid()), "s");
                            ?>
                                <tr>
                                    <td><?php echo $no?></td>
                                    <td><?php echo $bonusDetails[$cnt]->getReceiveUsername();?></td>
                                    <td><?php echo $bonusDetails[$cnt]->getBonus();?></td>
                                    <td><?php echo $bonusDetails[$cnt]->getBonusType();?></td>
                                    <?php
                                    if ($bonusDetails[$cnt]->getNewUser() == 'Buy Product') {
                                      ?><td><?php echo $bonusDetails[$cnt]->getNewUser()." (#".$bonusDetails[$cnt]->getInvoiceNo().")";?></td><?php
                                    }else{
                                      ?><td><?php echo $bonusDetails[$cnt]->getNewUser();?></td><?php
                                    }
                                     ?>
                                    <td><?php echo $bonusDetails[$cnt]->getStatus();?></td>
                                    <td><?php echo date('d/m/Y',strtotime($bonusDetails[$cnt]->getDateCreated()));?></td>
                                </tr>
                            <?php
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>

    </div>

    <div class="clear"></div>

</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>
