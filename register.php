<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$countryList = getCountries($conn);

$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userData = $userDetails[0];

$productDetails = getProduct($conn, "WHERE product_type='Package'");
// $userRanking = $userData->getRank();
// $downlineNumber = $userData->getRankDValue();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/register.php" />
    <meta property="og:title" content="Register | Samofa 莎魔髪" />
    <title>Register | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/register.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 same-padding menu-distance75">

   <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _HEADERBEFORELOGIN_REGISTER ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

    <!-- <form> -->
 	<form action="utilities/addReferee.php" method="POST">
 		<div class="dual-input">
        	<p class="input-top-text"><?php echo _JS_COUNTRY ?></p>
            <!-- <input class="clean de-input" type="text" placeholder="<?php //echo _JS_COUNTRY ?>" id="register_country" name="register_country" required> -->
            <select class="clean de-input" id="register_country" name="register_country" required>
                <option><?php echo _JS_COUNTRY ?></option>
                <?php
                for ($cntPro=0; $cntPro <count($countryList) ; $cntPro++)
                {
                ?>
                    <option value="<?php echo $countryList[$cntPro]->getEnName(); ?>">
                        <?php echo $countryList[$cntPro]->getEnName();?>
                    </option>
                <?php
                }
                ?>
            </select>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _JS_USERNAME ?><a id="usernameAlert"></a></p>
            <input class="clean de-input" type="text" placeholder="<?php echo _JS_USERNAME ?>" id="registerUsername" name="register_username" required>

        	<!-- <p class="input-top-text"><?php echo _JS_FIRSTNAME ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _JS_FIRSTNAME ?>" id="register_firstname" name="register_firstname" required> -->
        </div>
        <div class="clear"></div>
        <div class="dual-input">
            <p class="input-top-text"><?php echo _JS_FIRSTNAME ?></p>
            <input class="clean de-input" type="text" placeholder="<?php echo _JS_FIRSTNAME ?>" id="register_firstname" name="register_firstname" required>
        	<!-- <p class="input-top-text"><?php echo _JS_LASTNAME ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _JS_LASTNAME ?>" id="register_lastname" name="register_lastname" required> -->
        </div>
        <div class="dual-input second-dual-input">
        	<!-- <p class="input-top-text"><?php echo _JS_USERNAME ?></p>
            <input class="clean de-input" type="text" placeholder="<?php echo _JS_USERNAME ?>" id="register_username" name="register_username" required> -->
            <p class="input-top-text"><?php echo _JS_LASTNAME ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _JS_LASTNAME ?>" id="register_lastname" name="register_lastname" required>
        </div>
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-text"><?php echo _JS_EMAIL ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _JS_EMAIL ?>" id="register_email" name="register_email" required>
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-text"><?php echo _JS_PHONE ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _JS_PHONE ?>" id="register_mobileno" name="register_mobileno" required>
        </div>
        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-text"><?php echo _JS_PASSWORD ?></p>
            <input id="password" class="clean de-input" type="password" placeholder="<?php echo _JS_PASSWORD ?>" name="password" required>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _JS_RETYPE_PASSWORD ?><a id="passwordAlert"></a></p>
            <input id="rePassword" class="clean de-input" type="password" placeholder="<?php echo _JS_RETYPE_PASSWORD ?>" name="re_password" required>
        </div>
        <div class="clear"></div>

        <div class="dual-input">
        	<p class="input-top-text"><?php echo _JS_ENROLLMENT_PRODUCT ?></p>
            <select class="clean de-input" type="text" id="register_product" name="register_product" required>
                <option value="" name=" "><?php echo _JS_SELECT_PRODUCT ?></option>
                <?php if ($productDetails) {
                    for ($i=0; $i <count($productDetails) ; $i++) {
                        ?>
                        <option value="<?php echo $productDetails[$i]->getId() ?>"><?php echo $productDetails[$i]->getName() ?></option>
                        <?php
                    }
                } ?>
                <!-- <option value="Magic BloCA" name="Magic BloCA">Magic BloCA 脂糖宝</option> -->
                <!-- <option value="Hair Shampoo Serum" name="Product 2"><?php //echo _INDEX_HAIR_SERUM_SHORT ?></option> -->
            </select>
        </div>

        <!-- <div class="dual-input second-dual-input">
        	<p class="input-top-text"><?php echo _JS_SPONSOR_ID ?></p>
            <input class="clean de-input no-input" type="text" value="<?php echo $userData->getMemberID();?>" id="sponsor_id" name="sponsor_id" readonly>
		</div>

        <div class="clear"></div>

        <div class="dual-input">
        	<p class="input-top-text"><?php echo _USERDASHBOARD_RANK ?></p>
            <input class="clean de-input no-input" type="text" value="<?php echo $userRanking ?>" id="user_ranking" name="user_ranking" readonly>
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-text"><?php echo _USERDASHBOARD_DOWNLINE ?></p>
            <input class="clean de-input no-input" type="text" value="<?php echo $downlineNumber ?>" id="downline_amount" name="downline_amount" readonly>
		</div> -->

        <input class="clean de-input no-input" type="hidden" value="<?php echo $userData->getMemberID();?>" id="sponsor_id" name="sponsor_id" readonly>
        <input class="clean de-input no-input" type="hidden" value="<?php echo $userRanking ?>" id="user_ranking" name="user_ranking" readonly>
        <input class="clean de-input no-input" type="hidden" value="<?php echo $downlineNumber ?>" id="downline_amount" name="downline_amount" readonly>

        <div class="clear"></div>

        <div class="width100 text-center top-bottom-distance">
        	<button id="submitBtn" class="clean button-width transparent-button dark-pink-button" name="register"><?php echo _JS_SUBMIT ?></button>
        </div>
    </form>

</div>

	<div class="clear"></div>
    <img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">
    <div class="clear"></div>

<?php include 'js.php'; ?>
<script type="text/javascript">
    var x = location.search.replace('?', '').split('=');
    if (x[1] == 1) {
        alert("Success");
    }else if(x[1] == 2){
        alert("Error");
    }
    $("#rePassword").on("keyup",function(){
        // alert("SS");
        var password = $("#password").val();
        var rePassword = $(this).val();

        if (password == rePassword) {
            $("#passwordAlert").text("Password Match");
            $("#passwordAlert").css({
                "color":"green",
                "font-size":"12px",
            });
            $("#submitBtn").prop("disabled",false);
        }else{
            $("#passwordAlert").text("Password Mismatch");
            $("#passwordAlert").css({
              "color":"red",
              "font-size":"12px",
              "font-weight": "bold",
              "float": "right",
            });
            $("#submitBtn").prop("disabled",true);
        }
    });
    $("#registerUsername").on("keyup",function(){
      var username = $(this).val();
      // alert(registerUsername);
      $.ajax({
        url: 'utilities/registerNameChecker.php',
        data: {username:username},
        type: 'post',
        dataType: 'json',
        success:function(response){
          var resultAlert = response[0]['result'];
          var alertBin = response[0]['alertBin'];
          if (alertBin == 0 && username) {
          $("#usernameAlert").text(resultAlert);
            $("#usernameAlert").css({
                "color":"green",
                "font-size":"12px",
                "font-weight": "bold",
                "float": "right",
            });
            $("#submitBtn").prop("disabled",false);
          }else if(alertBin == 1 && username){
          $("#usernameAlert").text(resultAlert);
            $("#usernameAlert").css({
                "color":"red",
                "font-size":"12px",
                "font-weight": "bold",
                "float": "right",
            });
            $("#submitBtn").prop("disabled",true);
          }else{
            $("#usernameAlert").empty();
          }
        },
      });
    });
</script>
</body>
</html>
