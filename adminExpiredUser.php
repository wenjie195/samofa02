<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();
$mantainanceMonth = 3;
$todayDate = date('Y-m-d H:i');
$status = 'Active';

$userDetails = getUser($conn,"WHERE user_rank != 'Member' AND status = 'Inactive'");

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/adminExpiredUser.php" />
    <meta property="og:title" content="<?php echo _ADMINHEADER_ALL_EXPIRED_MEMBER ?> | Samofa 莎魔髪" />
    <title><?php echo _ADMINHEADER_ALL_EXPIRED_MEMBER ?> | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/adminExpiredUser.php" />
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
	<?php include 'css.php'; ?>
    
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMINHEADER_ALL_EXPIRED_MEMBER ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    <div class="width100 overflow same-padding margin-bottom30">
    	<div class="search-input-div">
    		<img src="img/search.png" class="search-png" alt="<?php echo _ADMIN_SEARCH ?>" title="<?php echo _ADMIN_SEARCH ?>">
            <input class="clean fake-line-input" type="text" placeholder="<?php echo _ADMIN_SEARCH ?>" id="myInput" onkeyup="myFunction()">
        </div>
    </div>
    <div class="width100 same-padding container-div1">

    <div class="overflow-scroll-div">
        <table class="table-css" id="myTable">
            <thead>
                <tr>
                    <th><?php echo _PRODUCTDETAILS_NO ?></th>
                    <th><?php echo _JS_SPONSOR_ID ?></th>
                    <th><?php echo _JS_USERNAME ?></th>
                    <th><?php echo _USERDASHBOARD_RANK ?></th>
                    <th><?php echo _ADMIN_EXPIRED_DATE ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($userDetails)
                {
                    for($cnt = 0;$cnt < count($userDetails) ;$cnt++)
                    {
                        $ordersDetails = getOrders($conn, "WHERE uid=? AND status = 'APPROVED' AND product_type='Package' ORDER BY date_created DESC LIMIT 1",array("uid"),array($userDetails[$cnt]->getUid()), "s");
                        $referralHistory = getReferralHistory($conn, "WHERE referrer_id=? ORDER BY date_created DESC LIMIT 1",array("referrer_id"),array($userDetails[$cnt]->getUid()), "s");
                        if ($userDetails[$cnt]->getUserRank() == 'Member') {
                            $oldRank = "Agent";
                        }else
                        if ($userDetails[$cnt]->getUserRank() == 'Agent') {
                            $oldRank = "President";
                        }else
                        if ($userDetails[$cnt]->getUserRank() == 'President') {
                            $oldRank = "Partner";
                        }else
                        if ($userDetails[$cnt]->getUserRank() == 'Partner') {
                            $oldRank = "Director";
                        }
                        if ($oldRank == 'Agent') {
                            $mantainanceMonth = 12;
                        }else {
                            $mantainanceMonth = 3;
                        }
                        if ($ordersDetails) {
                            $dateCreated = $ordersDetails[0]->getDateUpdated();
                            $dateExpiredOrders = date('Y-m-d H:i',strtotime($dateCreated. "+".$mantainanceMonth." Month"));
                            $dateExpiredReferral = '2000-01-01';
                            $dateExpiredUser = '2000-01-01';
                        }elseif ($referralHistory) {
                            $dateCreated = $referralHistory[0]->getDateCreated();
                            $dateExpiredReferral = date('Y-m-d H:i',strtotime($dateCreated. "+".$mantainanceMonth." Month"));
                            $dateExpiredOrders = '2000-01-01';
                            $dateExpiredUser = '2000-01-01';
                        }else {
                            $dateCreated = $userDetails[$cnt]->getDateCreated();
                            $dateExpiredUser = date('Y-m-d H:i',strtotime($dateCreated. "+".$mantainanceMonth." Month"));
                            $dateExpiredOrders = '2000-01-01';
                            $dateExpiredReferral = '2000-01-01';
                        }
                        if ($dateExpiredOrders > $dateExpiredReferral) {
                            $dateExpired = $dateExpiredOrders;
                        }elseif ($dateExpiredOrders < $dateExpiredReferral) {
                            $dateExpired = $dateExpiredReferral;
                        }else{
                            $dateExpired = $dateExpiredUser;
                        }
                        $status = $userDetails[$cnt]->getStatus();
                        ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $userDetails[$cnt]->getMemberId() ?></td>
                            <td><?php echo $userDetails[$cnt]->getUsername();?></td>
                            <td><?php echo $oldRank;?></td>
                            <td><?php echo date('d/m/Y',strtotime($userDetails[$cnt]->getDtRank()));?></td>
                            <!-- <td><?php echo date('d/m/Y',strtotime($dateExpired));?></td> -->
                        </tr>
                    <?php
                    }
                }
				$conn->close();
                ?> 
            </tbody>
        </table>
    </div>

    </div>
</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>