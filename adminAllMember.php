<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Orders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/rankConvert.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$allUser = getUser($conn, " WHERE user_type = ? ", array("user_type"), array(1), "i");

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/adminAllMember.php" />
    <meta property="og:title" content="All Member | Samofa 莎魔髪" />
    <title>All Member | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/adminAllMember.php" />
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
	<?php include 'css.php'; ?>

</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMINHEADER_ALLMEMBER ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    <div class="width100 overflow same-padding margin-bottom30">
    	<div class="search-input-div">
    		<img src="img/search.png" class="search-png" alt="<?php echo _ADMIN_SEARCH ?>" title="<?php echo _ADMIN_SEARCH ?>">
            <input class="clean fake-line-input" type="text" placeholder="<?php echo _ADMIN_SEARCH ?> <?php echo _JS_USERNAME ?>" id="myInput" onkeyup="myFunction()">
        </div>
    </div>
    <div class="width100 same-padding container-div1">

    <div class="overflow-scroll-div">
        <table class="table-css" id="myTable">
            <thead>
                <tr>
                    <th><?php echo _PRODUCTDETAILS_NO ?></th>
                    <th><?php echo _JS_USERNAME ?></th>
                    <th><?php echo _USERDASHBOARD_SPONSOR ?></th>
                    <th><?php echo _USERDASHBOARD_RANK ?></th>
                    <th><?php echo _PRODUCTDETAILS_STATUS ?></th>
                    <th><?php echo _ADMIN_PERSONAL_SALES ?>(RM)</th>
                    <th><?php echo _ADMIN_GROUP_SALES ?>(RM)</th>
                    <th><?php echo _ADMIN_LAST_ORDER ?></th>
                    <th>All Bonus</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($allUser)
                {
                    for($cnt = 0;$cnt < count($allUser) ;$cnt++)
                    {
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>

                            <td><?php echo $allUser[$cnt]->getUsername();?></td>
                            <td>
                                <?php
                                    $personalSales = 0;
                                    $userUid = $allUser[$cnt]->getUid();
                                    $getWho = getWholeDownlineTree($conn, $userUid, false);
                                    $groupSales = 0;
                                    $ordersDetails = getOrders($conn, "WHERE uid =? ORDER BY date_created DESC LIMIT 1",array("uid"),array($userUid), "s");
                                    $personalSalesDetails = getOrders($conn, "WHERE uid =? AND status = 'APPROVED'",array("uid"),array($userUid), "s");
                                    if ($ordersDetails) {
                                      $latestOrders = date('d/m/Y',strtotime($ordersDetails[0]->getDateCreated()));
                                    }else {
                                      $latestOrders = "-";
                                    }
                                    if ($personalSalesDetails) {
                                      for ($j=0; $j <count($personalSalesDetails) ; $j++) {
                                        $personalSales += $personalSalesDetails[$j]->getPaymentAmount();
                                      }
                                    }
                                    for ($i=0; $i <count($getWho) ; $i++) {
                                      $downlineUserDetails = getOrders($conn, "WHERE uid =? AND status = 'APPROVED'",array("uid"),array($getWho[$i]->getReferralId()), "s");
                                      if ($downlineUserDetails) {
                                        for ($k=0; $k <count($downlineUserDetails) ; $k++) {
                                          $groupSales += $downlineUserDetails[$k]->getPaymentAmount();
                                        }
                                      }
                                    }

                                    $conn = connDB();
                                    $referralDetails = getReferralHistory($conn, " WHERE referral_id = ? ", array("referral_id"), array($userUid), "s");
                                    $uplineUid = $referralDetails[0]->getReferrerId();

                                    $uplineDetails = getUser($conn, " WHERE uid = ? ", array("uid"), array($uplineUid), "s");
                                    echo $uplineUsername = $uplineDetails[0]->getUsername();
                                ?>
                            </td>
                            <td><?php echo rank($allUser[$cnt]->getUserRank());?></td>
                            <td><?php echo $allUser[$cnt]->getStatus() ?></td>
                            <td><?php echo number_format($personalSales);?></td>
                            <td><?php echo number_format($groupSales) ?></td>
                            <td><?php echo $latestOrders ?></td>

                            <td>
                              <form action="adminViewMemberBonus.php" method="POST" target="_blank">
                                  <button class="clean hover1 img-btn transparent-button pink-hover-text" type="submit" name="user_uid" value="<?php echo $allUser[$cnt]->getUid();?>">
                                      <img src="img/details1.png" class="edit-icon1 hover1a" alt="Bonus Flow Details" title="Bonus Flow Details">
                                      <img src="img/details2.png" class="edit-icon1 hover1b" alt="Bonus Flow Details" title="Bonus Flow Details">
                                  </button>
                              </form>
                            </td>

                        </tr>
                    <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>

    </div>
</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

</body>
</html>
