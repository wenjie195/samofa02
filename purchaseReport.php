<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/phoneFormat.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$allUser = getUser($conn);

$productDetails = getProduct($conn);

$productsOrders =  getOrders($conn," WHERE uid = ? AND status='APPROVED'",array("uid"),array($_SESSION['uid']),"s");

$adminProductView =  getOrders($conn, "WHERE status='APPROVED'");

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/purchaseReport.php" />
    
    <meta property="og:title" content="Customer Purchase History | Samofa 莎魔髪" />
    <title>Customer Purchase History | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/purchaseReport.php" />
	<?php include 'css.php'; ?> 
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 menu-distance75 min-height-with-flower">
	<h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMINHEADER_CUSTOMER_HISTORY ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

    <div class="overflow-scroll-div same-padding">
        
        <table class="table-css">
            <thead>
                <tr>
                <?php
                for($i= 0;$i < count($userRows) ;$i++)
                {
                    if($userRows[$i]->getUserType()==1)
                    {
                    ?>
                        <th><?php echo _PRODUCTDETAILS_NO ?></th>
                        <th><?php echo _PRODUCTDETAILS_NAME ?></th>
                        <?php for ($i=0; $i <count($productDetails) ; $i++) { 
                            ?>
                            <th><?php echo $productDetails[$i]->getName() ?></th>
                            <?php
                        } ?>
                </tr>
            </thead>
            <tbody>
                <?php
                        for ($n=0; $n <count($userRows) ; $n++) {
                            ?>
                            <tr>
                                <td><?php echo $n + 1 ?></td>
                            <td><?php echo $userRows[$n]->getUsername() ?></td>
                            <?php 
                            for ($j=0; $j <count($productDetails) ; $j++) { 
                            $productsOrdersDetails = getProductOrders($conn, "WHERE product_id =?",array("product_id"),array($productDetails[$j]->getId()), "s"); // get product order with product subsequence
                                if ($productsOrdersDetails) {
                                    $totalProduct = 0;
                                    ?>
                                        <?php 
                                        for ($i=0; $i <count($productsOrdersDetails) ; $i++) {
                                        // echo $productsOrdersDetails[$i]->getOrderId();
                                        $ordersDetails = getOrders($conn, "WHERE id=? AND uid=? AND status ='APPROVED'",array("id","uid"),array($productsOrdersDetails[$i]->getOrderId(),$uid), "ss");
                                        if ($ordersDetails) {
                                            $totalProduct += $productsOrdersDetails[$i]->getQuantity();
                                        }
                                        }
                                        ?>
                                        <td><?php echo $totalProduct; ?></td>
                                        <?php
                                         ?>
                                    <?php
                                    }else{
                                        ?>
                                        <td><?php echo 0; ?></td>
                                        <?php
                                    }
                                
                        } ?>
                            </tr>
                         <?php
                     }
                        ?>
                </tbody>
                    <?php
                    }
                    else if($userRows[$i]->getUserType()==0)
                    {
                        ?>
                            <thead>
                                <tr>
                                    <th><?php echo _PRODUCTDETAILS_NO ?></th>
                                    <th><?php echo _PRODUCTDETAILS_NAME ?></th>
                                    <?php for ($i=0; $i <count($productDetails) ; $i++) { 
                                        ?>
                                        <th><?php echo $productDetails[$i]->getName() ?></th>
                                        <?php
                                    } ?>
                                </tr>
                            </thead>
                            <tbody>  
                                <?php 
                                for ($n=0; $n <count($allUser) ; $n++) {
                                ?>  
                                    <tr>
                                        <td><?php echo $n + 1 ?></td>
                                    <td><?php echo $allUser[$n]->getUsername() ?></td>
                                <?php 
                                    for ($j=0; $j <count($productDetails) ; $j++) { 
                            $productsOrdersDetails = getProductOrders($conn, "WHERE product_id =?",array("product_id"),array($productDetails[$j]->getId()), "s"); // get product order with product id subsequence
                                        if ($productsOrdersDetails) {
                                            $totalProduct = 0;
                                            ?>
                                                <?php 
                                                for ($i=0; $i <count($productsOrdersDetails) ; $i++) {
                                                // echo $productsOrdersDetails[$i]->getOrderId();
                                                $ordersDetails = getOrders($conn, "WHERE id=? AND uid=? AND status ='APPROVED'",array("id","uid"),array($productsOrdersDetails[$i]->getOrderId(),$allUser[$n]->getUid()), "ss");
                                                if ($ordersDetails) {
                                                    $totalProduct += $productsOrdersDetails[$i]->getQuantity();
                                                }
                                                }
                                                ?>
                                                <td><?php echo $totalProduct; ?></td>
                                                <?php
                                                 ?>
                                            <?php
                                            }else{
                                                ?>
                                                <td><?php echo 0; ?></td>
                                                <?php
                                            }
                                
                                    } ?>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <?php
                    }
                }
                ?>   
            </tbody>
        </table>

    </div>
</div>    

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>