<?php
/*
 * This file shows how to Create A Bill using Billplz class
 * The commented line is Optional parameter
 * 
 */

require 'vendor/autoload.php';

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

$amount_line = $_GET['amount'];
$amountExp = explode(",",$amount_line);
$amount = $amountExp[0];
$name = $amountExp[1];
$email = $amountExp[2];

require_once dirname(__FILE__) . '/classes/BillPlz.php';

//$api_key = 'f3cf5434-8b1e-48f7-8612-a879816f15b5';
$api_key ='1fd3fc86-2f10-4963-8224-8e674c75bbb2';
$a = new Billplz;
$a->setName($name);
$a->setAmount($amount);
$a->setEmail($email);
$a->setDescription("SASA SUCCESS RESOURCES");
$a->setPassbackURL('http://callback-url.com', 'http://redirect-url.com');
//$a->setCollection('collect_id');
//$a->setPassbackURL('http://callback-url.com');
// $a->setReference_1_Label('Item To Buy');
// $a->setReference_1('Item Name');

$a->setMobile('999');
//$a->setDeliver('0'); //No Notification
//$a->setDeliver('1'); //Email Notification
//$a->setDeliver('2'); //SMS Notification
//$a->setDeliver('3'); //Email & SMS Notification
//$a->create_bill($api_key, true);
$a->create_bill($api_key);
//echo $a->getURL();
header('Location: ' .$a->getURL());