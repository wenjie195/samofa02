<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/adminEditPassword.php" />
    <meta property="og:title" content="<?php echo _HEADERBEFORELOGIN_EDIT_PASSWORD ?> | Samofa 莎魔髪" />
    <title><?php echo _HEADERBEFORELOGIN_EDIT_PASSWORD ?> | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/adminEditPassword.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 same-padding menu-distance75 min-height-with-flower">

   <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _HEADERBEFORELOGIN_EDIT_PASSWORD ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

    <!-- <form> -->
 	<form action="utilities/adminEditPasswordFunction.php" method="POST">

        <div class="dual-input">
            <p class="input-top-text"><?php echo _JS_NEW_PASSWORD ?></p>
            <div class="fake-input-bg">
                <input class="clean de-input password-input" type="password" placeholder="<?php echo _JS_NEW_PASSWORD ?>"  id="new_password" name="new_password">
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="<?php echo _JS_VIEW_PASSWORD ?>" title="<?php echo _JS_VIEW_PASSWORD ?>">
            </div>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _JS_RETYPE_NEW_PASSWORD ?></p>
            <div class="fake-input-bg">
                <input class="clean de-input password-input" type="password" placeholder="<?php echo _JS_RETYPE_NEW_PASSWORD ?>" id="retype_new_password" name="retype_new_password">
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionC()" alt="<?php echo _JS_VIEW_PASSWORD ?>" title="<?php echo _JS_VIEW_PASSWORD ?>">
            </div>
        </div>

        <div class="clear"></div>

        <div class="width100 text-center top-bottom-distance">
        	<button id="submitBtn" class="clean button-width transparent-button dark-pink-button" name="submit"><?php echo _JS_SUBMIT ?></button>
        </div>

    </form>

</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
function myFunctionB()
{
    var x = document.getElementById("new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionC()
{
    var x = document.getElementById("retype_new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

</body>
</html>