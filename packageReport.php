<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();
$totalSalesArray = [];

$productDetails = getProduct($conn);
for ($i=0; $i <count($productDetails) ; $i++) { 
    $totalSales = 0;
    $productOrders = getProductOrders($conn, "WHERE product_id =?",array("product_id"),array($productDetails[$i]->getId()), "s");
    if ($productOrders) {
        for ($j=0; $j <count($productOrders) ; $j++) { 
        $ordersDetails = getOrders($conn, "WHERE id=? AND status = 'APPROVED'",array("id"),array($productOrders[$j]->getOrderId()), "s");
        if ($ordersDetails) {
            $totalSales += $productOrders[$j]->getQuantity();
        }
        }
    }
    $totalSalesArray[] = $totalSales;
    $productIdArray[] = $productDetails[$i]->getId();
}
$productIdImp = implode(",",$productIdArray);
$totalSalesImp = implode(",",$totalSalesArray);
$productIdExp = explode(",",$productIdImp);
$totalSalesExp = explode(",",$totalSalesImp);

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/packageReport.php" />
    <meta property="og:title" content="Package Report | Samofa 莎魔髪" />
    <title>Package Report | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/packageReport.php" />
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
	<?php include 'css.php'; ?>
    
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMINHEADER_PACKAGE_REPORT ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    <div class="width100 overflow same-padding margin-bottom30">
    	<!-- <div class="search-input-div">
    		<img src="img/search.png" class="search-png" alt="<?php echo _ADMIN_SEARCH ?>" title="<?php echo _ADMIN_SEARCH ?>">
            <input class="clean fake-line-input" type="text" placeholder="<?php echo _ADMIN_SEARCH ?> <?php echo _JS_USERNAME ?>" id="myInput" onkeyup="myFunction()">
        </div> -->
    </div>
    <div class="width100 same-padding container-div1">

    <div class="overflow-scroll-div">
        <table class="table-css" id="myTable">
            <thead>
                <tr>
                    <th><?php echo _PRODUCTDETAILS_NO ?></th>
                    <th><?php echo _ADMIN_PACKAGE_PRODUCT ?></th>
                    <th><?php echo _ADMIN_UNIT_SOLD ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($totalSalesExp)
                {
                    for($cnt = 0;$cnt < count($totalSalesExp) ;$cnt++)
                    {
                        $productDetails = getProduct($conn, "WHERE id =?",array("id"),array($productIdExp[$cnt]), "s");
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>

                            <td><?php echo $productDetails[0]->getName();?></td>
                            <td><?php echo $totalSalesExp[$cnt] ?></td>
                        </tr>
                    <?php
                    }
                }               
                $conn->close();
                ?> 
            </tbody>
        </table>
    </div>

    </div>
</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>