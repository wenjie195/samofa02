

<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <a href="index.php"><img src="img/samofa.png" class="logo-img" alt="Logo" title="Logo"></a>
            </div>



            <div class="right-menu-div float-right logged-in-menu">
            
                <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">
                    
                    <?php echo _ADMINHEADER_NETWORK ?>
                            	<img src="img/dropdown.png" class="dropdown-img hover1a" >
                                <img src="img/dropdown2.png" class="dropdown-img hover1b" >

                	
                	<div class="dropdown-content yellow-dropdown-content">

                        <p class="dropdown-p"><a href="viewAllMember.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_ALLMEMBER ?></a></p>
                        <p class="dropdown-p"><a href="register.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMIN_REGISTER_NEW_MEMBER ?></a></p>
						<p class="dropdown-p"><a href="adminViewAllMembers.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_MEMBER_PROFILE ?></a></p>   
						<p class="dropdown-p"><a href="adminViewWithdrawal.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMIN_WITHDRAWAL_REQUEST ?></a></p>                                             
                	</div>
                </div>     
                <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">
                    
                    <?php echo _ADMINHEADER_PRODUCTS ?>
                            	<img src="img/dropdown.png" class="dropdown-img hover1a" >
                                <img src="img/dropdown2.png" class="dropdown-img hover1b" >

                	<div class="dropdown-content yellow-dropdown-content">


                        <p class="dropdown-p"><a href="addNewPackage.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_ADD_NEW_PACKAGE ?></a></p>
                        <p class="dropdown-p"><a href="addNewProduct.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_ADD_NEW_PRODUCT ?></a></p>
                        <p class="dropdown-p"><a href="currentProduct.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_CURRENT_PRODUCT ?></a></p>                     
                	</div>
                </div>  
                
                <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">
                    
                    <?php echo _HEADERBEFORELOGIN_REPORT ?>
                            	<img src="img/dropdown.png" class="dropdown-img hover1a" >
                                <img src="img/dropdown2.png" class="dropdown-img hover1b" >
                                
                	<div class="dropdown-content yellow-dropdown-content">


                        <p class="dropdown-p"><a href="salesReport.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_SALES_REPORT ?></a></p>
                        <p class="dropdown-p"><a href="payoutReport.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_PAYOUT_REPORT ?></a></p>   
						<p class="dropdown-p"><a href="packageReport.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_PACKAGE_REPORT ?></a></p>  
						<p class="dropdown-p"><a href="customerSalesReport.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINHEADER_CUSTOMER_REPORT ?></a></p>                                                                    
                        <p class="dropdown-p"><a href="purchaseReport.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _HEADERBEFORELOGIN_PURCHASE_REPORT ?></a></p>                             
                        <p class="dropdown-p"><a href="withdrawalReport.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _HEADERBEFORELOGIN_WITHDRAWAL_REPORT ?></a></p>
                   
                	</div>
                </div>                                         
                <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">
                    
                    Language / 语言
                            	<img src="img/dropdown.png" class="dropdown-img hover1a" alt="Language / 语言" title="Language / 语言">
                                <img src="img/dropdown2.png" class="dropdown-img hover1b" alt="Language / 语言" title="Language / 语言">
                                <!-- <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->
                	
                	<div class="dropdown-content yellow-dropdown-content">
                        <!-- <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                        <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                        <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">English</a></p>
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">中文</a></p>
                	</div>
                </div>  
            	<a href="logout.php" class="pink-hover-text menu-item">
                	<?php echo _MAINJS_ALL_LOGOUT ?>
                </a>
                           	<div id="dl-menu" class="dl-menuwrapper logged-in-dl">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
                                  <li><a href="viewAllMember.php"><?php echo _ADMINHEADER_ALLMEMBER ?></a></li>
                                  <li><a href="register.php"><?php echo _ADMIN_REGISTER_NEW_MEMBER ?></a></li>
                                  <li><a href="adminViewAllMembers.php"><?php echo _ADMINHEADER_MEMBER_PROFILE ?></a></li>
                                  <li><a href="adminViewWithdrawal.php"><?php echo _ADMIN_WITHDRAWAL_REQUEST ?></a></li>  
                                  <li><a href="addNewPackage.php"><?php echo _ADMINHEADER_ADD_NEW_PACKAGE ?></a></li>
                                  <li><a href="addNewProduct.php"><?php echo _ADMINHEADER_ADD_NEW_PRODUCT ?></a></li>
                                  <li><a href="currentProduct.php"><?php echo _ADMINHEADER_CURRENT_PRODUCT ?></a></li> 
                                  <li><a href="salesReport.php"><?php echo _ADMINHEADER_SALES_REPORT ?></a></li>  
                                  <li><a href="payoutReport.php"><?php echo _ADMINHEADER_PAYOUT_REPORT ?></a></li>    
                                  <li><a href="packageReport.php"><?php echo _ADMINHEADER_PACKAGE_REPORT ?></a></li> 
                                  <li><a href="customerSalesReport.php"><?php echo _ADMINHEADER_CUSTOMER_REPORT ?></a></li>  
                                  <li><a href="purchaseReport.php"><?php echo _HEADERBEFORELOGIN_PURCHASE_REPORT ?></a></li>                                  <li><a href="withdrawalReport.php"><?php echo _HEADERBEFORELOGIN_WITHDRAWAL_REPORT ?></a></li>                              
                                  <li><a href="<?php $link ?>?lang=en">Switch to English</a></li>
                                  <li><a href="<?php $link ?>?lang=ch">转换成中文</a></li>
                                  <li><a href="logout.php" ><?php echo _MAINJS_ALL_LOGOUT ?></a></li>

                                </ul>
							</div><!-- /dl-menuwrapper -->                                         	
            </div>
        </div>

</header>
