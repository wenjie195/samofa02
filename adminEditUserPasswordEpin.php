<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $allUser = getUser($conn, " WHERE user_type = ? ", array("user_type"), array(1), "s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/adminEditUserPasswordEpin.php" />
    <meta property="og:title" content="Admin Edit User Password or E-Pin | Samofa 莎魔髪" />
    <title>Admin Edit User Password or E-Pin | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/adminEditUserPasswordEpin.php" />
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
	<?php include 'css.php'; ?>
    
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center big-header-color"><?php echo _ADMIN_EDIT_USER_PASSWORD ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    <div class="width100 same-padding">

        <?php
            if(isset($_POST['user_uid']))
            {
                $conn = connDB();
                $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");
            ?>

                <form method="POST" action="utilities/adminEditUserPasswordFunction.php">
                
                    <p class="box-p"><b><?php echo _ADMIN_EDIT_PASSWORD ?></b></p>
                    <div class="width100">
                        <p class="input-top-text"><?php echo _JS_NEW_PASSWORD ?></p>
                        <div class="fake-input-bg">
                        <input class="clean de-input password-input" type="text" placeholder="<?php echo _JS_NEW_PASSWORD ?>" id="update_password" name="update_password">
                        <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="<?php echo _JS_VIEW_PASSWORD ?>" title="<?php echo _JS_VIEW_PASSWORD ?>">
                        </div>
                    </div>

                    <input class="clean de-input" type="hidden" value="<?php echo $userDetails[0]->getUid();?>" id="user_uid" name="user_uid" readonly>

                    <div class="clear"></div>

                    <div class="width100 text-center top-bottom-distance">
                        <button class="clean button-width transparent-button dark-pink-button" name="submit"><?php echo _JS_SUBMIT ?></button>
                    </div>

                </form>
    <h1 class="dark-pink-text hi-title contact-title text-center big-header-color"><?php echo _ADMIN_EDIT_USER_EPIN ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
                <form method="POST" action="utilities/adminEditUserEpinFunction.php">

                    <p class="box-p"><b><?php echo _ADMIN_EDIT_EPIN ?></b></p>
                    <div class="width100">
                        <p class="input-top-text"><?php echo _EPIN_NEW_EPIN ?></p>
                        <div class="fake-input-bg">
                        <input class="clean de-input password-input" type="text" placeholder="<?php echo _EPIN_NEW_EPIN ?>" id="update_epin" name="update_epin">
                        <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="<?php echo _ADMIN_VIEW_EPIN ?>" title="<?php echo _ADMIN_VIEW_EPIN ?>">
                        </div>                        
                    </div>

                    <input class="clean de-input" type="hidden" value="<?php echo $userDetails[0]->getUid();?>" id="user_uid" name="user_uid" readonly>

                    <div class="clear"></div>

                    <div class="width100 text-center top-bottom-distance">
                        <button class="clean button-width transparent-button dark-pink-button" name="submit"><?php echo _JS_SUBMIT ?></button>
                    </div>

                </form>

            <?php
        }
        ?>

    </div>
</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
function myFunctionA()
{
    var x = document.getElementById("update_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("update_epin");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>
</body>
</html>
