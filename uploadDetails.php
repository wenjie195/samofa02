<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/uploadDetails.php" />
    <meta property="og:title" content="Samofa 莎魔髪" />
    <title>Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/uploadDetails.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">

<div class="width100 same-padding menu-distance bg1">
    <h1 class="h1-title white-text text-center">IC Front</h1>
    <form  action="utilities/uploadIcFrontFunction.php" method="POST" enctype="multipart/form-data" class="upload-img-form">
        <input type="file" id="file" name="file" class="clean upload-file-input" required>
        <div class="clear"></div>
        <button class="clean hover1 blue-button smaller-font upload-photo ow-green-bg" type="submit" name="submit">
            Upload
        </button>
    </form>

    <h1 class="h1-title white-text text-center">IC Back</h1>
    <form  action="utilities/uploadIcBackFunction.php" method="POST" enctype="multipart/form-data" class="upload-img-form">
        <input type="file" id="file" name="file" class="clean upload-file-input" required>
        <div class="clear"></div>
        <button class="clean hover1 blue-button smaller-font upload-photo ow-green-bg" type="submit" name="submit">
            Upload
        </button>
    </form>

    <h1 class="h1-title white-text text-center">Signature</h1>
    <form  action="utilities/uploadSignatureFunction.php" method="POST" enctype="multipart/form-data" class="upload-img-form">
        <input type="file" id="file" name="file" class="clean upload-file-input" required>
        <div class="clear"></div>
        <button class="clean hover1 blue-button smaller-font upload-photo ow-green-bg" type="submit" name="submit">
            Upload
        </button>
    </form>

    <h1 class="h1-title white-text text-center">Driving License</h1>
    <form  action="utilities/uploadLicenseFunction.php" method="POST" enctype="multipart/form-data" class="upload-img-form">
        <input type="file" id="file" name="file" class="clean upload-file-input" required>
        <div class="clear"></div>
        <button class="clean hover1 blue-button smaller-font upload-photo ow-green-bg" type="submit" name="submit">
            Upload
        </button>
    </form>

</div>

</body>
</html>