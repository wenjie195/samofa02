<?php
/**
 * Instruction:
 *
 * 1. Replace the APIKEY with your API Key.
 * 2. OPTIONAL: Replace the COLLECTION with your Collection ID.
 * 3. Replace the X_SIGNATURE with your X Signature Key
 * 4. Replace the http://www.google.com/ with your FULL PATH TO YOUR WEBSITE. It must be end with trailing slash "/".
 * 5. Replace the http://www.google.com/success.html with your FULL PATH TO YOUR SUCCESS PAGE. *The URL can be overridden later
 * 6. OPTIONAL: Set $amount value.
 * 7. OPTIONAL: Set $fallbackurl if the user are failed to be redirected to the Billplz Payment Page.
 *
 */

// // real data begin
// $api_key = '1fd3fc86-2f10-4963-8224-8e674c75bbb2';
// $collection_id = 'COLLECTION';
// $x_signature = 'S-O0VcNFq08l7WsKTRMZUAag'; // x-signature sandbox

// $path = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
// $fullPath = dirname($path);

// // $websiteurl = 'http://localhost/samofa02';
// $websiteurl = "http://".$fullPath;

// // $successpath = 'http://localhost/samofa02/successPayment.php';
// $successpath = $websiteurl.'/successPayment.php';
// $amount = ''; //Example (RM13.50): $amount = '1350';
// $fallbackurl = ''; //Example: $fallbackurl = 'http://www.google.com/pay.php';
// $description = 'PAYMENT DESCRIPTION';
// $reference_1_label = 'Product';
// $reference_2_label = 'Order ID';


// sandbox data begin
$api_key = 'dc8b2c92-96ac-45e7-bd2b-df7628729562';
$collection_id = 'COLLECTION';
$x_signature = 'S-v1Q2xGC5QXikPIw_VguOHg';

$path = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$fullPath = dirname($path);

$websiteurl = 'http://localhost/samofa';
// $websiteurl = "http://".$fullPath;

$successpath = 'http://localhost/samofa/successPayment.php';
// $successpath = $websiteurl.'/successPayment.php';
$amount = ''; //Example (RM13.50): $amount = '1350';
$fallbackurl = ''; //Example: $fallbackurl = 'http://www.google.com/pay.php';
$description = 'PAYMENT DESCRIPTION';
$reference_1_label = 'Product';
$reference_2_label = 'Order ID';
// sandbox data end