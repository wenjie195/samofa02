<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/adminPerformMaintenance.php" />
    <meta property="og:title" content="<?php echo _ADMIN_CHECK_MAINTENANCE ?> | Samofa 莎魔髪" />
    <title><?php echo _ADMIN_CHECK_MAINTENANCE ?> | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/adminPerformMaintenance.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 same-padding menu-distance75 min-height-with-flower">

   <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMIN_CHECK_MAINTENANCE ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
	<div class="clear">
    <div class="width103 ow-css1">
    <div class="three-div-css dashboard-box">
       
        <form action="cronjob/RankDowngradeAgent.php" method="POST" target="_blank">
            
                <button id="submitBtn" class="clean button-width transparent-button dark-pink-button" name="submit"><?php echo _ADMIN_AGENT ?> (<?php echo _ADMIN_DOWNGRADE ?>)</button><p class="note-p3"><?php echo _ADMIN_ANNUALLY ?></p>
                
           
        </form>

    </div>
	<div class="three-div-css dashboard-box">
        <form action="cronjob/RankDowngradePresident.php" method="POST" target="_blank">
           
                <button id="submitBtn" class="clean button-width transparent-button dark-pink-button" name="submit"><?php echo _ADMIN_PRESIDENT ?> (<?php echo _ADMIN_DOWNGRADE ?>)</button><p class="note-p3"><?php echo _ADMIN_QUARTERLY ?></p>
            
        </form>

    </div>
	<div class="three-div-css dashboard-box">
   
        <form action="cronjob/RankDowngradePartner.php" method="POST" target="_blank">
            
                <button id="submitBtn" class="clean button-width transparent-button dark-pink-button" name="submit"><?php echo _ADMIN_PARTNER ?> (<?php echo _ADMIN_DOWNGRADE ?>)</button><p class="note-p3"><?php echo _ADMIN_QUARTERLY ?></p>
            
        </form>

    </div>
	<div class="three-div-css dashboard-box">
        <form action="cronjob/RankDowngradeDirector.php" method="POST" target="_blank">
           
                <button id="submitBtn" class="clean button-width transparent-button dark-pink-button" name="submit"><?php echo _ADMIN_DIRECTOR ?> (<?php echo _ADMIN_DOWNGRADE ?>)</button><p class="note-p3"><?php echo _ADMIN_QUARTERLY ?></p>
           
        </form>

    </div>
	<div class="three-div-css dashboard-box">
        <form action="cronjob/RankActive_renew.php" method="POST" target="_blank">
           
                <button id="submitBtn" class="clean button-width transparent-button dark-pink-button" name="submit"><?php echo _ADMIN_RENEW_RANKING ?></button><p class="note-p3">&nbsp;</p>
            
        </form>
    </div>
 </div>
</div>
</div>
<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
function myFunctionB()
{
    var x = document.getElementById("new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionC()
{
    var x = document.getElementById("retype_new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

</body>
</html>