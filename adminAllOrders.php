<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/phoneFormat.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $adminProductView =  getOrders($conn, "WHERE status='APPROVED'");
$adminProductView =  getOrders($conn, "WHERE status='APPROVED' ORDER BY date_created DESC ");

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/adminAllOrders.php" />
    
    <meta property="og:title" content="All Orders | Samofa 莎魔髪" />
    <title>All Orders | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/adminAllOrders.php" />
    <?php include 'css.php'; ?> 
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMINHEADER_ALL_ORDERS ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

    <div class="overflow-scroll-div same-padding">
        
        <table class="table-css">
            <thead>
                <tr>
                        <th><?php echo _PRODUCTDETAILS_NO ?></th>
                        <th><?php echo _ADMIN_INVOICE_NO ?></th>
                        <th><?php echo _PURCHASE_DATE ?></th>
                        <th><?php echo _JS_USERNAME ?></th>
                        <!-- <th><?php //echo _INDEX_CONTACT_NO ?></th> -->
                        <th><?php echo _ADMIN_FULFILLMENT_STATUS ?></th>
                        <th><?php echo _SHIPPING_AMOUNT ?> (RM)</th>
                        <th>Bonus Flow</th>
                </tr>
            </thead>
            <tbody>
                <?php
                        for($cnt = 0;$cnt < count($adminProductView) ;$cnt++)
                        {
                            $userPurchaseDetails = getUser($conn, "WHERE uid =?",array("uid"),array($adminProductView[$cnt]->getUid()), "s");
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo "#".$adminProductView[$cnt]->getId()?></td>
                                <td>
                                    <?php echo $date = date("d/m/Y",strtotime($adminProductView[$cnt]->getDateCreated()));?>
                                </td>
                                <td><?php echo $userPurchaseDetails[0]->getUsername();?></td>
                                <td><?php 
                                if ($adminProductView[$cnt]->getPickupType()) {
                                    if (isset($_SESSION['lang']) && $_SESSION['lang'] == 'en') {
                                        echo "Fulfilled";
                                    }else{
                                        echo "已出货";
                                    }
                                }else{
                                    if (isset($_SESSION['lang']) && $_SESSION['lang'] == 'en') {
                                        echo "Unfulfill";
                                    }else{
                                        echo "未出货";
                                    }
                                }
                                 ?></td>
                                <td><?php echo $adminProductView[$cnt]->getTotal();?></td>

                                <td>
                                    <form action="adminViewBonusFlow.php" method="POST" target="_blank">
                                        <button class="clean hover1 img-btn transparent-button pink-hover-text" type="submit" name="order_id" value="<?php echo $adminProductView[$cnt]->getId();?>">
                                            <img src="img/details1.png" class="edit-icon1 hover1a" alt="Bonus Flow Details" title="Bonus Flow Details">
                                            <img src="img/details2.png" class="edit-icon1 hover1b" alt="Bonus Flow Details" title="Bonus Flow Details">
                                        </button>
                                    </form>
                                </td>

                            </tr>
                        <?php
                        }
                        $conn->close();
                        ?>
            </tbody>
        </table>

    </div>
</div>    

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>