<?php
if (session_id() == ""){
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/serverMaintenance.php" />
    <meta property="og:title" content="Server Maintenance | Samofa 莎魔髪" />
    <title>Server Maintenance | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/serverMaintenance.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">

<!-- <?php //include 'headerBeforeLogin.php'; ?> -->
<?php //include 'headerAfterLogin.php'; ?>


<div class="width100 same-padding padding-top75 overflow server-bg min-height-only-footer">
	<div class="main-notice-div">
		<div class="main-notice-content-div width100">

        <div class="clear"></div>
        <h1 class="dark-pink-text contact-title  hi-title text-center modal-h1 ow-mbtm10"><?php echo _MAINJS_INDEX_LOGIN ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
        <div class="clear"></div>
        <form action="utilities/loginFunction.php" method="POST">
            <p class="input-top-text"><?php echo _MAINJS_INDEX_USERNAME ?></p>
            <input class="clean de-input" type="text" placeholder="<?php echo _MAINJS_INDEX_USERNAME ?>" id="username" name="username" required>
            <div class="clear"></div>
            <p class="input-top-text"><?php echo _MAINJS_INDEX_PASSWORD ?></p>
            <input class="clean de-input" type="password" placeholder="<?php echo _MAINJS_INDEX_PASSWORD ?>" id="password" name="password" required>
            <div class="clear"></div>
            <div class="width100 text-center margin-top30">
            	<button class="clean width100 transparent-button dark-pink-button" name="loginButton"><?php echo _MAINJS_INDEX_LOGIN ?></button>
            </div>
            <div class="clear"></div>
            <div class="width100 text-center overflow margin-top20">
            	<a class="open-forgot pink-hover-text"><?php echo _JS_FORGOT_PASSWORD  ?></a>
            </div>
            <div class="clear"></div>
    	</form>

		</div>
	</div>
</div>

<?php include 'js.php'; ?>
</body>
</html>