<?php 
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/ForgotUser.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$username = $_POST['username'];
 ?>
<table class="table-css" id="myTable">
            <thead>
                <tr>
                    <th><?php echo _PRODUCTDETAILS_NO ?></th>
                    <th><?php echo _JS_USERNAME ?></th>
                    <th><?php echo _USERDASHBOARD_RANK ?></th>
                    <th><?php echo _PRODUCTDETAILS_STATUS ?></th>
                    <th><?php echo _ADMIN_REQUESTED_DATE ?></th>
                    <th><?php echo _ADMIN_ACTION ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $forgotUserDetails = getUser($conn, "WHERE username =?",array("username"),array($username), "s");

                if ($forgotUserDetails) {
                    for ($i=0; $i <count($forgotUserDetails) ; $i++) { 
                        $allUser = getUser($conn, " WHERE uid = ? ", array("uid"), array($forgotUserDetails[$i]->getUid()), "s");
                            ?>
                                <tr>
                                    <td><?php echo ($i+1)?></td>

                                    <td><?php echo $allUser[0]->getUsername();?></td>
                                    <td><?php echo $allUser[0]->getUserRank();?></td>
                                    <td><?php echo $allUser[0]->getStatus();?></td>
                                    <td><?php echo date('d/m/Y',strtotime($forgotUserDetails[$i]->getDateCreated()));?></td>
                                    <td>
                                    <form action="utilities/resetUserPassword.php" method="post">
                                    <input type="hidden" name="uid" value="<?php echo $allUser[0]->getUid() ?>">
                                    <button class="clean inactive-btn transparent-button dark-pink-button" type="submit" name="submitBtn"><?php echo str_replace(" Password", "", _MAINJS_INDEX_RESET_PASSWORD) ?></button>
                                    </form>
                                    </td>
                                </tr>
                            <?php
                        }
                    }else{
                        ?>
                        <td class="no-data-td" colspan="6">No User Found.</td>
                        <?php
                    }
                ?> 
            </tbody>
        </table>