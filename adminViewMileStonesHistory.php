<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/MilesStones.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$milesStonesDetails = getMilesStones($conn, " WHERE status != 'PENDING' ");

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/adminViewMileStonesHistory.php" />
    <meta property="og:title" content="MilesStones Bonus | Samofa 莎魔髪" />
    <title>MilesStones Bonus | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/adminViewMileStonesHistory.php" />
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
	<?php include 'css.php'; ?>

</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 style="color: grey" class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"> <a class="dark-pink-text" href="adminViewMilesStones.php"><?php echo _ADMIN_MILESTONES_BONUS ?></a> | <?php echo _ADMIN_MILESTONES_HISTORY ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    <div class="width100 same-padding container-div1">

    <div class="overflow-scroll-div">
        <table class="table-css">
            <thead>
                <tr>
                    <th><?php echo _PRODUCTDETAILS_NO ?></th>
                    <th><?php echo _MAINJS_INDEX_USERNAME ?></th>
                    <th><?php echo _ADMIN_BONUS_TYPE ?></th>
                    <th><?php echo _PURCHASE_DATE ?></th>
                    <th><?php echo _ADMIN_ACTION ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($milesStonesDetails)
                {
                    for($cnt = 0;$cnt < count($milesStonesDetails) ;$cnt++)
                    {
                        $userDetails = getUser($conn, "WHERE uid=?",array("uid"),array($milesStonesDetails[$cnt]->getUid()), "s");
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $userDetails[0]->getUsername();?></td>
                            <td><?php echo $milesStonesDetails[$cnt]->getBonus();?></td>
                            <td><?php echo date('d/m/Y',strtotime($milesStonesDetails[$cnt]->getDateCreated()));?></td>

                            <td>
                              <?php echo $milesStonesDetails[$cnt]->getStatus(); ?>
                            </td>
                        </tr>
                    <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>

    </div>
</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>
<!-- <script>
  $("#confirmAlert").on('click',function(){
    var buttonPressed = confirm("Press a button!");
  if (buttonPressed == true) {
    var uid =
    $.ajax({
      url : 'utilities/adminVerifyMilesStonesFunction.php';
      data: ''
    });
  } else {
    txt = "You pressed Cancel!";
  }
  });
</script> -->

</body>
</html>
