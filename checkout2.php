<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Checkout.php';
require_once dirname(__FILE__) . '/classes/Orders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$uid = $_SESSION['uid'];

// $id = getOrders($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1 ",array("uid"),array($uid),"s");
// $asd = $id[0]->getId();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];
// $rank= $userDetails->getStatus();
$rank= $userDetails->getRank();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    echo $name = rewrite($_POST["insert_name"]);
    echo $contactNo = rewrite($_POST["insert_contactNo"]);
    echo $address_1 = rewrite($_POST["insert_address_1"]);
    echo $address_2 = rewrite($_POST["insert_address_2"]);
    echo $city = rewrite($_POST["insert_city"]);
    echo $zipcode = rewrite($_POST["insert_zipcode"]);
    echo $state = rewrite($_POST["insert_state"]);
    echo $country = rewrite($_POST["insert_country"]);
}

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,$rank,2);
}
// else
// {}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/checkout2.php" />
   
    <meta property="og:title" content="Check Out | Samofa 莎魔髪" />
    <title>Check Out | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/checkout2.php" />
	<?php include 'css.php'; ?> 
</head>

<body class="body">

<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-menu-distance75 same-padding">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _CHECKOUT_CHECKOUT ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    
    <form method="POST"  action="shipping.php">
        <p class="info-title dark-pink-text"><b><?php echo _CHECKOUT_CONTACT ?></b></p>
        <div class="dual-input">
            <p class="input-top-text"><?php echo _USERDASHBOARD_NAME ?></p>
            <input class="clean de-input" type="text" id="insert_name" name="insert_name" placeholder="<?php echo _JS_FULLNAME ?>" required>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _JS_PHONE ?></p>
            <input class="clean de-input" type="text" id="insert_contactNo" name="insert_contactNo"  placeholder="<?php echo _JS_PHONE ?>" required>
        </div>

        <div class="clear"></div>

        <p class="info-title dark-pink-text"><b><?php echo _CHECKOUT_SHIPPING_ADDRESS ?></b></p>

        <div class="dual-input">
            <p class="input-top-text"><?php echo _CHECKOUT_ADDRESS1 ?></p>
            <input class="clean de-input" type="text" type="text" id="insert_address_1" name="insert_address_1" placeholder="<?php echo _CHECKOUT_ADDRESS1 ?>" required>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _CHECKOUT_ADDRESS2 ?></p>
            <input class="clean de-input" type="text" id="insert_address_2" name="insert_address_2"  placeholder="<?php echo _CHECKOUT_ADDRESS2 ?>" required>
        </div>

        <div class="clear"></div>    

        <div class="dual-input">
            <p class="input-top-text"><?php echo _CHECKOUT_CITY ?></p>
            <input class="clean de-input" type="text" type="text" id="insert_city" name="insert_city" placeholder="<?php echo _CHECKOUT_CITY ?>" required>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _CHECKOUT_ZIP_CODE ?></p>
            <input class="clean de-input" type="number" id="insert_zipcode" name="insert_zipcode"   placeholder="<?php echo _CHECKOUT_ZIP_CODE ?>" required>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-text"><?php echo _CHECKOUT_STATE ?></p>
            <input class="clean de-input" type="text" type="text" id="insert_state" name="insert_state" placeholder="<?php echo _CHECKOUT_STATE ?>" required>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _JS_COUNTRY ?></p>
            <input class="clean de-input" type="text" id="insert_country" name="insert_country"   placeholder="<?php echo _JS_COUNTRY ?>" required>
        </div>

        <div class="clear"></div> 

        <div class="width100 top-bottom-spacing">
            <table class="table-css">
                <thead>
                    <th><?php echo _HEADERBEFORELOGIN_PRODUCT ?></th>
                    <th><?php echo _VIEW_CART_QUANTITY ?></th>
                    <th><?php echo _PRODUCTDETAILS_NAME ?></th>
                    <th><?php echo _PRODUCTDETAILS_PRICE ?></th>
                </thead>

                    <?php echo $productListHtml; ?>
               
        </div>

        <div class="clear"></div>

        <div class="width100 text-center">
            <div class="width100 text-center top-bottom-spacing">
                <button class="clean button-width transparent-button dark-pink-button checkout-btn" name="register"><?php echo _CHECKOUT_NEXT ?></button>
            </div>

            <div class="clear"></div>

            <div class="width100 text-center margin-bottom30">
                <p class="continue-shopping pointer continue2"><a href="viewCart.php" class="pink-text opacity-hover back-text"><img src="img/back.png" class="checkout-back-btn" alt="<?php echo _VIEW_CART_BACK ?>" title="<?php echo _VIEW_CART_BACK ?>" > <?php echo _VIEW_CART_BACK ?></a></p>
            </div>                    
        </div>
    </form>

</div>

<?php include 'js.php'; ?>

<?php 
    if(isset($_GET['type']))
    {
        $messageType = null;

        if($_SESSION['messageType'] == 1)
        {
            if($_GET['type'] == 1)
            {
                $messageType = "Please Fill Up The Required Details !";
            }
            if($_GET['type'] == 2)
            {
                $messageType = "Fail To Make Order !";
            }

            echo '
            <script>
                putNoticeJavascript("Notice !! ","'.$messageType.'");
            </script>
            ';   
            $_SESSION['messageType'] = 0;
        }
    }
?>

</body>
</html>