<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ProductOrders.php';
require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/Orders.php';

require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/languageFunction.php';

require_once dirname(__FILE__) . '/../lib/API.php';
require_once dirname(__FILE__) . '/../lib/Connect.php';
require_once dirname(__FILE__) . '/../configuration.php';


use Billplz\Minisite\API;
use Billplz\Minisite\Connect;

$data = Connect::getXSignature($x_signature);
$connnect = (new Connect($api_key))->detectMode();
$billplz = new API($connnect);


$approved = 'APPROVED';
$pending = 'PENDING';
$status = 'APPROVED';
$reject = 'REJECTED';
$referralName = "Buy Product";
$active = 'Active';
$dtRank = NULL;

function registerNewUser($conn,$uid,$memberID,$username,$email,$firstname,$lastname,$country,$phoneNo,$finalPassword,$salt)
{
     if(insertDynamicData($conn,"user_pending",array("uid","member_id","username","email","firstname","lastname","country","phone_no","password","salt"),
          array($uid,$memberID,$username,$email,$firstname,$lastname,$country,$phoneNo,$finalPassword,$salt),"ssssssssss") === null)
     {
          //  "gg";
     }
     else
     {    }
     return true;
}

function referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid)
{
     if(insertDynamicData($conn,"referral_pending",array("referrer_id","referral_id","referral_name","current_level","top_referrer_id"),
     array($referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid),"sssis") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$productPrice,$totalProductPrice,$phoneNo)
{
     if(insertDynamicData($conn,"signup_product_pending",array("referrer_id","referrer_name","referral_id","referral_name","referral_fullname","product","price","total","contact"),
     array($referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$productPrice,$totalProductPrice,$phoneNo),"sssssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
function commission($conn,$orderId,$referralName,$getUplineUid,$getUplineName,$getUplineCurrentLevel,$getUpline,$topReferrerUid,$referralUid,$uplineBonus,$referralName_referrer,$status)
{
     if(insertDynamicData($conn,"bonus",array("invoice_no","referral_name","referrer_id","referrer_name","current_level","register_downline_no","top_referrer_id","referral_id","amount","signupby","status"),
     array($orderId,$referralName,$getUplineUid,$getUplineName,$getUplineCurrentLevel,$getUpline,$topReferrerUid,$referralUid,$uplineBonus,$referralName_referrer,$status),"ssssiississ") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
function leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status)
{
     if(insertDynamicData($conn,"other_bonus",array("invoice_no","uid","receive_username","receive_uid","amount","bonus_type","new_user","status"),
     array($orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status),"ssssisss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
function NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$register_username_referrer)
{
     if(insertDynamicData($conn,"bonus",array("referral_name","referrer_id","referrer_name","current_level","register_downline_no","top_referrer_id","referral_id","amount","signupby"),
     array($referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$register_username_referrer),"sssiissis") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

$conn = connDB();

    $successUpdateOrders = 0;
    $paidAmount = $_POST['paid_amount'];
    $totalPackageBuy = 0;
    $uid = $_POST['uid'];
    $orderId = $_POST['order_id'];
    $billId = $_POST['bill_id'];
    $paymentMethod = 'Online Banking';

      if (isset($billId) && isset($_POST['check'])) {
        $successPaid = updateDynamicData($conn, 'orders', 'WHERE payment_references=?', array('status','payment_amount','payment_method'), array($approved,$paidAmount,$paymentMethod,$billId), "ssss");
        $successUpdateOrders = 1;
      }else {
        $pendingPaid = updateDynamicData($conn, 'orders', 'WHERE payment_references=?', array('status'), array($reject,$billId), "ss");
      }

      if (isset($successUpdateOrders) && $successUpdateOrders == 1) {
        $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid), "s");
        $orderDetails = getOrders($conn, "WHERE id=?", array("id"), array($orderId), "s");
        $productId = $orderDetails[0]->getProductId();
        $productQuantity = $orderDetails[0]->getQuantity();
        $productIdExplode = explode(",",$productId);
        $productQuantityExplode = explode(",",$productQuantity);
        for ($i=0; $i <count($productIdExplode) ; $i++) { 
          $productDetails = getProduct($conn, "WHERE id=?", array("id"), array($productIdExplode[$i]), "s");
          if ($productDetails[0]->getProductType() == 'Package') {
            $totalPackageBuy += $productQuantityExplode[$i];
          }
        }
        $currentwallet = $userDetails[0]->getSales();
        $downlineNumber = $userDetails[0]->getDownlineNo();
         $cash = $userDetails[0]->getSales();
         $referrerName = $userDetails[0]->getUsername();
         $referralUid = $uid;
         $getUid = $userDetails[0] -> getUid();
         $currentStatus = $userDetails[0]->getUserRank();
         $activeStatus = $userDetails[0]->getStatus();
         $referrerUid = $uid;

            // $newWallet = $currentwallet - $total;

            // $orderUpdated = updateDynamicData($conn,"orders"," WHERE id = ? ",array("payment_method","payment_amount","status"),array($type,$total,$status,$orderId),"ssss");

            // $userUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("sales","status"),array($newWallet,$statusActive,$uid),"sss");

          $currentLevel = 1;
          $getUplineCurrentLevel = 1;

          $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($referrerUid),"s");
          if($referralHistoryRows)
          {
               $topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
               $currentLevel = $referralHistoryRows[0]->getCurrentLevel() + 1;
               $uplineUid = $referralHistoryRows[0]->getReferrerId();

               $uplineDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uplineUid), "s");
               $uplineUsername = $uplineDetails[0]->getUsername();
          }
          // $referralNewestRows = getReferralHistory($conn,"WHERE referral_name = ?", array("referral_name"),array($referrerName), "s");
          // if($referralNewestRows)
          // {
          //      //$topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
          //      $getUplineCurrentLevel = $referralNewestRows[0]->getCurrentLevel() + 1;
          // }

          for ($j=0; $j <$totalPackageBuy ; $j++) { 
            //===============================================================Director Direct Bonus=============================================================================================

            if ($currentStatus == 'Director')
            {
                 $bonus = 120;
                 $newBonus = 150; // partner Bonus
                 $bonusType = 'Leadership';
                 $currentStatus = 'Director';
                 $uplineBonus = 0;
                 $currentBonus = $userDetails[0] -> getBonus();
                 $totalBonus = $currentBonus + $bonus + $newBonus;
                 $registerDownline = $userDetails[0]->getDownlineNo() + 1;
                 $receiveUid = $userDetails[0] -> getUid();
                 $receiveUsername = $userDetails[0] -> getUsername();
                 // $totalPoint = $pointReferee -300;
                 $totalCash = $cash + $bonus;

                 $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($uplineUid), "s");
                 $referrerWallet = $referrerDetails[0]->getSales();
                 $referrerBonus = $referrerDetails[0]->getBonus();
                $newReferrerWallet = $referrerWallet + $bonus;
                $newReferrerBonus = $referrerBonus + $bonus;

                 $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$uplineUid),"sss");
                 $userUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("status","dt_rank"),array($active,$dtRank,$uid),"sss");

                 if ($userWalletUpdated) {
                 if (commission($conn,$orderId,$referralName,$uplineUid,$uplineUsername,$currentLevel,$registerDownline,$topReferrerUid,$referrerUid,$bonus,$referralName,$status))
                 {}
                 else
                 {
                      // $_SESSION['messageType'] = 2;
                      // header('Location: ../wallet.php?type=1');
                 }
                 // if(leadership($conn,$orderId,$referralUid,$uplineUsername,$uplineUid,$newBonus,$bonusType,$referralName,$status))
                 // {
                 // }
                 }
                 }

//===============================================================End of Director Direct Bonus=============================================================================================
//===============================================================Partner Direct Bonus=============================================================================================

            if ($currentStatus == 'Partner')
            {
                 $bonus = 120;
                 $newBonus = 120; // president Bonus
                 $bonusType = 'Leadership';
                 $currentStatus = 'Partner';
                 $uplineBonus = 0;
                 $currentBonus = $userDetails[0] -> getBonus();
                 $totalBonus = $currentBonus + $bonus + $newBonus;
                 $registerDownline = $userDetails[0] ->getDownlineNo() + 1;
                 $receiveUid = $userDetails[0] -> getUid();
                 $receiveUsername = $userDetails[0] -> getUsername();
                 // $totalPoint = $pointReferee -300;
                 $totalCash = $cash + $bonus;

                 $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($uplineUid), "s");
                 $referrerWallet = $referrerDetails[0]->getSales();
                 $referrerBonus = $referrerDetails[0]->getBonus();
                //  $newReferrerWallet = $referrerWallet + $bonus + $newBonus;
                //  $newReferrerBonus = $referrerBonus + $bonus + $newBonus;
                $newReferrerWallet = $referrerWallet + $bonus;
                $newReferrerBonus = $referrerBonus + $bonus;

                 $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$uplineUid),"sss");
                 $userUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("status","dt_rank"),array($active,$dtRank,$uid),"sss");

                 if ($userWalletUpdated) {
                 if (commission($conn,$orderId,$referralName,$uplineUid,$uplineUsername,$currentLevel,$registerDownline,$topReferrerUid,$referrerUid,$bonus,$referralName,$status))
                 {}
                 else
                 {
                      // $_SESSION['messageType'] = 2;
                      // header('Location: ../wallet.php?type=1');
                 }
                 // if(leadership($conn,$orderId,$referralUid,$uplineUsername,$uplineUid,$newBonus,$bonusType,$referralName,$status))
                 // {
                 // }
                 }
            }
//===============================================================End of Partner Direct Bonus=============================================================================================
//===============================================================President Direct Bonus=============================================================================================

            if ($currentStatus == 'President')
            {
                 $bonus = 120;
                 $newBonus = 90; // president Bonus
                 $bonusType = 'Leadership';
                 $currentStatus = 'President';
                 $uplineBonus = 0;
                 $currentBonus = $userDetails[0] -> getBonus();
                 $totalBonus = $currentBonus + $bonus + $newBonus;
                 $registerDownline = $userDetails[0] ->getDownlineNo() + 1;
                 $receiveUid = $userDetails[0] -> getUid();
                 $receiveUsername = $userDetails[0] -> getUsername();
                 // $totalPoint = $pointReferee -300;
                 $totalCash = $cash + $bonus;

                 $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($uplineUid), "s");
                 $referrerWallet = $referrerDetails[0]->getSales();
                 $referrerBonus = $referrerDetails[0]->getBonus();
                //  $newReferrerWallet = $referrerWallet + $bonus + $newBonus;
                //  $newReferrerBonus = $referrerBonus + $bonus + $newBonus;
                $newReferrerWallet = $referrerWallet + $bonus;
                $newReferrerBonus = $referrerBonus + $bonus;

                 $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$uplineUid),"sss");
                 $userUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("status","dt_rank"),array($active,$dtRank,$uid),"sss");

                 if ($userWalletUpdated) {
                 if (commission($conn,$orderId,$referralName,$uplineUid,$uplineUsername,$currentLevel,$registerDownline,$topReferrerUid,$referrerUid,$bonus,$referralName,$status))
                 {}
                 else
                 {
                      // $_SESSION['messageType'] = 2;
                      // header('Location: ../wallet.php?type=1');
                 }
                 // if(leadership($conn,$orderId,$referralUid,$uplineUsername,$uplineUid,$newBonus,$bonusType,$referralName,$status))
                 // {
                 // }
                 }
            }
//===============================================================End of President Direct Bonus=============================================================================================
//===============================================================Agent Direct Bonus=============================================================================================

            if ($currentStatus == 'Agent')
            {
                 $bonus = 120;
                 $newBonus = 50; // agent Bonus
                 $bonusType = 'Leadership';
                 $currentStatus = 'President';
                 $uplineBonus = 0;
                 $currentBonus = $userDetails[0] -> getBonus();
                 $totalBonus = $currentBonus + $bonus + $newBonus;
                 $registerDownline = $userDetails[0] ->getDownlineNo() + 1;
                 $receiveUid = $userDetails[0] -> getUid();
                 $receiveUsername = $userDetails[0] -> getUsername();
                 // $totalPoint = $pointReferee -300;
                 $totalCash = $cash + $bonus;

                 $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($uplineUid), "s");
                 $referrerWallet = $referrerDetails[0]->getSales();
                 $referrerBonus = $referrerDetails[0]->getBonus();
                //  $newReferrerWallet = $referrerWallet + $bonus + $newBonus;
                //  $newReferrerBonus = $referrerBonus + $bonus + $newBonus;
                $newReferrerWallet = $referrerWallet + $bonus;
                $newReferrerBonus = $referrerBonus + $bonus;

                 $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$uplineUid),"sss");
                 $userUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("status","dt_rank"),array($active,$dtRank,$uid),"sss");

                 if ($userWalletUpdated) {
                 if (commission($conn,$orderId,$referralName,$uplineUid,$uplineUsername,$currentLevel,$registerDownline,$topReferrerUid,$referrerUid,$bonus,$referralName,$status))
                 {}
                 else
                 {
                      // $_SESSION['messageType'] = 2;
                      // header('Location: ../wallet.php?type=1');
                 }
                 // if(leadership($conn,$orderId,$referralUid,$uplineUsername,$uplineUid,$newBonus,$bonusType,$referralName,$status))
                 // {
                 // }
                 }
            }
//===============================================================End of President Direct Bonus=============================================================================================

            if($downlineNumber < 7 && $currentStatus == 'Member') //if your rank is member and downline below 7
            {
                 $bonus = 120;
                 $newBonus = 0;
                 $currentBonus = $userDetails[0] -> getBonus();
                 $totalBonus = $currentBonus + $bonus + $newBonus;
                 $registerDownline = $userDetails[0] ->getDownlineNo() + 1; // direct downline
                 // $totalPoint = $pointReferee -300;
                 $totalCash = $cash + $bonus;

                 $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($uplineUid), "s");
                 $referrerWallet = $referrerDetails[0]->getSales();
                 $referrerBonus = $referrerDetails[0]->getBonus();
                //  $newReferrerWallet = $referrerWallet + $bonus + $newBonus;
                //  $newReferrerBonus = $referrerBonus + $bonus + $newBonus;
                $newReferrerWallet = $referrerWallet + $bonus;
                $newReferrerBonus = $referrerBonus + $bonus;

                 $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$uplineUid),"sss");
                 $userUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("status","dt_rank"),array($active,$dtRank,$uid),"sss");

                 if ($userWalletUpdated) {
                 if (commission($conn,$orderId,$referralName,$uplineUid,$uplineUsername,$currentLevel,$registerDownline,$topReferrerUid,$referrerUid,$bonus,$referralName,$status))
                 {}
                 else
                 {
                      // $_SESSION['messageType'] = 2;
                      // header('Location: ../wallet.php?type=1');
                 }
                 }

            }
            elseif($downlineNumber >= 7 && $currentStatus == 'Member') // if your member 7 and above
            {
              $bonus = 120;
              $newBonus = 0;
              $bonusType = 'Leadership';
              $yourStatusNow = $userDetails[0] -> getUserRank();

              if($yourStatusNow == 'Member') {
                $currentStatus = 'Agent';
              }
              $uplineBonus = 0;
              $currentBonus = $userDetails[0] -> getBonus();
              $receiveUid = $userDetails[0] -> getUid();
              $receiveUsername = $userDetails[0] -> getUsername();
              $totalBonus = $currentBonus + $bonus + $newBonus;
              $registerDownline = $userDetails[0] ->getDownlineNo() + 1;
              // $totalPoint = $pointReferee -300;
              $totalCash = $cash + $bonus;

              $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($uplineUid), "s");
                 $referrerWallet = $referrerDetails[0]->getSales();
                 $referrerBonus = $referrerDetails[0]->getBonus();
                //  $newReferrerWallet = $referrerWallet + $bonus + $newBonus;
                //  $newReferrerBonus = $referrerBonus + $bonus + $newBonus;
                $newReferrerWallet = $referrerWallet + $bonus;
                $newReferrerBonus = $referrerBonus + $bonus;

                 $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$uplineUid),"sss");
                 $userUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("status","dt_rank"),array($active,$dtRank,$uid),"sss");

                 if ($userWalletUpdated) {
                 if (commission($conn,$orderId,$referralName,$uplineUid,$uplineUsername,$currentLevel,$registerDownline,$topReferrerUid,$referrerUid,$bonus,$referralName,$status))
                 {}
                 else
                 {
                      // $_SESSION['messageType'] = 2;
                      // header('Location: ../wallet.php?type=1');
                 }
                 // if(leadership($conn,$orderId,$referralUid,$uplineUsername,$uplineUid,$newBonus,$bonusType,$referralName,$status))
                 // {
                 // }
                 }
            }

//========================================================matching bonus start================================================================================================================================================================================
          }
          for ($k=0; $k <$totalPackageBuy ; $k++) { 
            //========================================================matching bonus start====================================
            // $userRowsII = getUser($conn,"WHERE member_id = ?", array("member_id"), array($sponsorID), "s");
            // $uid = $userRowsII[0]->getUid();

            $yourUid = getUser($conn, "WHERE uid = ? ", array("uid"), array($uid), "s");
            $yourOwnUid = $yourUid[0] -> getUid(); // get your uid
            $yourDownlineNo = $yourUid[0] -> getDownlineNo(); // get your downline
            $yourOwnCurrentStatus = $yourUid[0] -> getUserRank(); //get your current status
            $getUpline = getTop10ReferrerOfUser($conn,$uid); // get your upline
            $referralUid = $uid;
          	// $referralUid = "none";
          	$referralName = "Buy Product";
          	$status = "APPROVED";
            $directBonus = 50; // bonus when add new referee
            $director = 2; //default
            $partner = 2; //default
            $president = 2; //default
            $agent = 2; //default
            $directorMatching = 2; //default
            $partnerMatching = 2; // default
            $presidentMatching = 2; // default
            $agentMatching = 2; // default
            $agentOverriding = 2; // default
            $districtOverriding = 2; // default
            $partnerOverriding = 2; // default

            $directorBonus = 150; // leadership
            $partnerBonus = 120; // leadership
            $presidentBonus = 90;
            $agentBonus = 50;

            if ($yourOwnCurrentStatus == 'Member') { // if your status is an member

             for ($cnt=0; $cnt <count($getUpline) ; $cnt++) { // loop all yourexisting upline
               //  $getUpline[$cnt]."<br>";

              // $getUplineUid = getUser($conn, "WHERE uid = ? AND status = 'Active'", array("uid"), array($getUpline[$cnt]), "s");
              $getUplineUid = getUser($conn, "WHERE uid = ? ", array("uid"), array($getUpline[$cnt]), "s");

              $getUplineUid[0]->getUserRank()."<br>"; // get your upline current status
              $receiveUid = $getUplineUid[0]->getUid(); // get uid
              $receiveUsername = $getUplineUid[0]->getusername(); // get username
              // $bonusType = 'Leadership';

              if ($getUplineUid[0]->getUserRank() == 'Director') { // if your upline partner
                if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                $newBonus = $directorBonus - $presidentBonus; // get 20 leadership bonus because president already take the leadership bonus
                $director = 1; // means we already loop into partner
                // $directorMatching = 1;
                $bonusType = 'Leadership';
                if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

              }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $directorMatching == 2 ) { // means the data didnt enter president and agent loop
                $newBonus = $directorBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
                $director = 1; // means we already loop into partner
                // $directorMatching = 1;
                  $bonusType = 'Leadership';
                  if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

              }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $directorMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
                $newBonus = $directorBonus - $agentBonus; // get leadership bonus 40 because the  Agent take the bonus 30
                $director = 1;
                // $directorMatching = 1;
                $bonusType = 'Leadership';
                if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

              }elseif ($director == 2 && $partner == 2 && $president == 1 && $agent == 1 && $directorMatching == 2 ) { // means the data enter president & agent loop
                $newBonus = $directorBonus - $presidentBonus; // get leadership bonus 20 because the president take 50 bonus
                $director = 1;
                $bonusType = 'Leadership';
                // $directorMatching = 1;
                if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }
                  // $partner = 2;
              }elseif ($director == 2 && $partner == 1 && $president == 1 && $agent == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
              $newBonus = $directorBonus - $partnerBonus; // get 20 leadership bonus because president already take the leadership bonus
              $director = 1; // means we already loop into partner
              // $directorMatching = 1;
              $bonusType = 'Leadership';
              if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

            }elseif ($director == 2 && $partner == 1 && $president == 2 && $agent == 2 && $directorMatching == 2 ) { // means the data didnt enter president and agent loop
              $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
              $director = 1; // means we already loop into partner
              // $directorMatching = 1;
                $bonusType = 'Leadership';
                if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

            }elseif ($director == 2 && $partner == 1 && $president == 2 && $agent == 1 && $directorMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
              $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 40 because the  Agent take the bonus 30
              $director = 1;
              // $directorMatching = 1;
              $bonusType = 'Leadership';
              if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

            }elseif ($director == 2 && $partner == 1 && $president == 1 && $agent == 1 && $directorMatching == 2 ) { // means the data enter president & agent loop
              $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 20 because the president take 50 bonus
              $director = 1;
              // $bonusType = 'Leadership';
              $directorMatching = 1;
              if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }
                // $partner = 2;
            }
              elseif ($director == 1 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
               $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
              $director = 1; // means we already loop into partner
              $directorMatching = 1;
              $bonusType = 'Matching';
              if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

            }}elseif ($getUplineUid[0]->getUserRank() == 'Partner') { // if your upline partner
                if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $partnerMatching == 2 ) { // means the data already enter president loop because $president = 1
                 $newBonus = $partnerBonus - $presidentBonus; // get 20 leadership bonus because president already take the leadership bonus
                $partner = 1; // means we already loop into partner
                // $partnerMatching = 1;
                $bonusType = 'Leadership';
                if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

              }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $partnerMatching == 2 ) { // means the data didnt enter president and agent loop
                 $newBonus = $partnerBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
                $partner = 1; // means we already loop into partner
                // $partnerMatching = 1;
                  $bonusType = 'Leadership';
                  if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

              }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $partnerMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
                 $newBonus = $partnerBonus - $agentBonus; // get leadership bonus 40 because the  Agent take the bonus 30

                $partner = 1;
                // $partnerMatching = 1;
                $bonusType = 'Leadership';
                if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

              }elseif ($director == 2 && $partner == 2 && $president == 1 && $agent == 1 && $partnerMatching == 2 ) { // means the data enter president & agent loop
                 $newBonus = $partnerBonus - $presidentBonus; // get leadership bonus 20 because the president take 50 bonus

                $partner = 1;
                $bonusType = 'Leadership';
                // $partnerMatching = 1;
                if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }
                  // $partner = 2;
              }
              elseif ($partner == 1 && $partnerMatching == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                if ($directorMatching == 2) {
                  $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                  $partner = 1; // means we already loop into partner
                  $partnerMatching = 1;
                  $bonusType = 'Matching';
                  if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }
                }

            }}elseif ($getUplineUid[0]->getUserRank() == 'President') { // if your upline president
                if ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $presidentMatching == 2) { // means the data didnt enter partner loop because $partner == 2
                  $newBonus = $presidentBonus; // get leadership bonus 50

                 $president = 1;
                 // $presidentMatching = 1;
                 $bonusType = 'Leadership';
                 if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

             }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $presidentMatching == 2) { // means the data  enter  agent loop because $agent == 1
                $newBonus = $presidentBonus - $agentBonus; // get leadership bonus 20 because the  Agent take the bonus 30

               $president = 1;
               // $presidentMatching = 1;
               $bonusType = 'Leadership';
               if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

             }
            elseif ($president == 1 && $presidentMatching == 2 && $directorMatching == 2) { // means the data didnt enter partner loop because $partner == 2
                $newBonus = 25; // get leadership bonus 50

                $president = 1;
                $presidentMatching = 1;
                $bonusType = 'Matching';
                if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

            }}elseif ($getUplineUid[0]->getUserRank() == 'Agent') { // if your upline agent
              if ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $agentMatching == 2) { // means the data  enter  agent loop because $agent == 1
                 $newBonus = $agentBonus; // get leadership bonus 20 because the  Agent take the bonus 30
                $agent = 1;
                // $agentMatching = 1;
                $bonusType = 'Leadership';
                if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

              }
            elseif ($agent == 1 && $agentMatching == 2 && $presidentMatching == 2 && $partnerMatching == 2 && $directorMatching == 2 ) { // means the data didnt enter partner loop because $partner == 2
                  $newBonus = 25; // get leadership bonus 30
                 $agent = 1;
                 $agentMatching = 1;
                 $bonusType = 'Matching';
                 if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

            }}}}

               //================================================================================================================================

               if ($yourOwnCurrentStatus == 'Agent') { // if your status is an member
                // $agent = 1;
                // $agentMatching = 1;

                for ($cnt=0; $cnt <count($getUpline) ; $cnt++) { // loop all yourexisting upline
                  //  $getUpline[$cnt]."<br>";

                //  $getUplineUid = getUser($conn, "WHERE uid = ? AND status = 'Active'", array("uid"), array($getUpline[$cnt]), "s");
                $getUplineUid = getUser($conn, "WHERE uid = ? ", array("uid"), array($getUpline[$cnt]), "s");

                  $getUplineUid[0]->getUserRank()."<br>"; // get your upline current status
                 $receiveUid = $getUplineUid[0]->getUid(); // get uid
                 $receiveUsername = $getUplineUid[0]->getusername(); // get username
                 // $bonusType = 'Leadership';

                 if ($getUplineUid[0]->getUserRank() == 'Director') { // if your upline partner
                   if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                    $newBonus = $directorBonus - $presidentBonus; // get 20 leadership bonus because president already take the leadership bonus
                   $director = 1; // means we already loop into partner
                   // $directorMatching = 1;
                   $bonusType = 'Leadership';
                   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

                 }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $directorMatching == 2 ) { // means the data didnt enter president and agent loop
                    $newBonus = $directorBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
                   $director = 1; // means we already loop into partner
                   // $directorMatching = 1;
                     $bonusType = 'Leadership';
                     if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

                 }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $directorMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
                    $newBonus = $directorBonus - $agentBonus; // get leadership bonus 40 because the  Agent take the bonus 30
                   $director = 1;
                   // $directorMatching = 1;
                   $bonusType = 'Leadership';
                   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

                 }elseif ($director == 2 && $partner == 2 && $president == 1 && $agent == 1 && $directorMatching == 2 ) { // means the data enter president & agent loop
                    $newBonus = $directorBonus - $presidentBonus; // get leadership bonus 20 because the president take 50 bonus
                   $director = 1;
                   $bonusType = 'Leadership';
                   // $directorMatching = 1;
                   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }
                     // $partner = 2;
                 }elseif ($director == 2 && $partner == 1 && $president == 1 && $agent == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                  $newBonus = $directorBonus - $partnerBonus; // get 20 leadership bonus because president already take the leadership bonus
                 $director = 1; // means we already loop into partner
                 // $directorMatching = 1;
                 $bonusType = 'Leadership';
                 if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

               }elseif ($director == 2 && $partner == 1 && $president == 2 && $agent == 2 && $directorMatching == 2 ) { // means the data didnt enter president and agent loop
                  $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
                 $director = 1; // means we already loop into partner
                 // $directorMatching = 1;
                   $bonusType = 'Leadership';
                   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

               }elseif ($director == 2 && $partner == 1 && $president == 2 && $agent == 1 && $directorMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
                  $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 40 because the  Agent take the bonus 30
                 $director = 1;
                 // $directorMatching = 1;
                 $bonusType = 'Leadership';
                 if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

               }elseif ($director == 2 && $partner == 1 && $president == 1 && $agent == 1 && $directorMatching == 2 ) { // means the data enter president & agent loop
                  $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 20 because the president take 50 bonus
                 $director = 1;
                 $bonusType = 'Leadership';
                 // $directorMatching = 1;
                 if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }
                   // $partner = 2;
               }
                 elseif ($director == 1 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                  $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                 $director = 1; // means we already loop into partner
                 $directorMatching = 1;
                 $bonusType = 'Matching';
                 if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

               }}elseif ($getUplineUid[0]->getUserRank() == 'Partner') { // if your upline partner
                   if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $partnerMatching == 2 ) { // means the data already enter president loop because $president = 1
                    $newBonus = $partnerBonus - $presidentBonus; // get 20 leadership bonus because president already take the leadership bonus
                   $partner = 1; // means we already loop into partner
                   // $partnerMatching = 1;
                   $bonusType = 'Leadership';
                   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

                 }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $partnerMatching == 2 ) { // means the data didnt enter president and agent loop
                    $newBonus = $partnerBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
                   $partner = 1; // means we already loop into partner
                   // $partnerMatching = 1;
                     $bonusType = 'Leadership';
                     if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

                 }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $partnerMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
                    $newBonus = $partnerBonus - $agentBonus; // get leadership bonus 40 because the  Agent take the bonus 30

                   $partner = 1;
                   // $partnerMatching = 1;
                   $bonusType = 'Leadership';
                   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

                 }elseif ($director == 2 && $partner == 2 && $president == 1 && $agent == 1 && $partnerMatching == 2 ) { // means the data enter president & agent loop
                    $newBonus = $partnerBonus - $presidentBonus; // get leadership bonus 20 because the president take 50 bonus

                   $partner = 1;
                   $bonusType = 'Leadership';
                   // $partnerMatching = 1;
                   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }
                     // $partner = 2;
                 }
                 elseif ($partner == 1 && $partnerMatching == 2 && $directorMatching == 2) { // means the data already enter president loop because $president = 1
                    $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus

                   $partner = 1; // means we already loop into partner
                   $partnerMatching = 1;
                   $bonusType = 'Matching';
                   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

               }}elseif ($getUplineUid[0]->getUserRank() == 'President') { // if your upline president
                   if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $presidentMatching == 2) { // means the data already enter partner loop because $partner = 1
                   //  $newBonus = $partnerBonus - $presidentBonus - $agentBonus; // not get any leadership bonus because partner already take the leadership bonus
                   $president = 1;
                   // $presidentMatching = 1;

                 }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $presidentMatching == 2) { // means the data didnt enter partner loop because $partner == 2
                     $newBonus = $presidentBonus; // get leadership bonus 50
                    $president = 1;
                    // $presidentMatching = 1;
                    $bonusType = 'Leadership';
                    if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

                }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $presidentMatching == 2) { // means the data  enter  agent loop because $agent == 1
                   $newBonus = $presidentBonus - $agentBonus; // get leadership bonus 20 because the  Agent take the bonus 30
                  $president = 1;
                  // $presidentMatching = 1;
                  $bonusType = 'Leadership';
                  if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

                }
               elseif ($president == 1 && $presidentMatching == 2 && $partnerMatching == 2 && $directorMatching == 2) { // means the data didnt enter partner loop because $partner == 2
                $newBonus = 25; // get leadership bonus 50
               $president = 1;
               $presidentMatching = 1;
               $bonusType = 'Matching';
               if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

               }}elseif ($getUplineUid[0]->getUserRank() == 'Agent') { // if your upline agent
                 if ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $agentMatching == 2) { // means the data  enter  agent loop because $agent == 1
                 $agent = 1;
                 $newBonus = 25; // get leadership bonus 30
                 $agentMatching = 1;
                 $bonusType = 'Matching';
                 $newBonusII = $agentBonus;
                 $bonusTypeII = 'Leadership';
                 if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
               {
                 $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                           $referrerWallet = $referrerDetails[0]->getSales();
                                           $referrerBonus = $referrerDetails[0]->getBonus();
                                           $newReferrerWallet = $referrerWallet + $newBonus;
                                           $newReferrerBonus = $referrerBonus + $newBonus;

                                           $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
               }
               if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonusII,$bonusTypeII,$referralName,$status))
             {
               $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                         $referrerWallet = $referrerDetails[0]->getSales();
                                         $referrerBonus = $referrerDetails[0]->getBonus();
                                         $newReferrerWallet = $referrerWallet + $newBonusII;
                                         $newReferrerBonus = $referrerBonus + $newBonusII;

                                         $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
             }
                 // $agentMatching = 1;
                 if ($agent == 1 && $agentMatching == 2 && $presidentMatching == 2 && $partnerMatching == 2 && $directorMatching == 2) { // means the data didnt enter partner loop because $partner == 2
                //   $newBonus = 25; // get leadership bonus 30
                //   $agent = 1;
                //   $agentMatching = 1;
                //   $bonusType = 'Matching';
                //   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                // {
                //   $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                //                             $referrerWallet = $referrerDetails[0]->getSales();
                //                             $referrerBonus = $referrerDetails[0]->getBonus();
                //                             $newReferrerWallet = $referrerWallet + $newBonus;
                //                             $newReferrerBonus = $referrerBonus + $newBonus;
                //
                //                             $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                // }

               }

                 }
               elseif ($agent == 1 && $agentMatching == 2 && $presidentMatching == 2 && $partnerMatching == 2 && $directorMatching == 2) { // means the data didnt enter partner loop because $partner == 2
                //   $newBonus = 25; // get leadership bonus 30
                //   $agent = 1;
                //   $agentMatching = 1;
                //   $bonusType = 'Matching';
                //   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                // {
                //   $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                //                             $referrerWallet = $referrerDetails[0]->getSales();
                //                             $referrerBonus = $referrerDetails[0]->getBonus();
                //                             $newReferrerWallet = $referrerWallet + $newBonus;
                //                             $newReferrerBonus = $referrerBonus + $newBonus;
                //
                //                             $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                // }

               }}}}

               //=====================================================================================================

               if ($yourOwnCurrentStatus == 'President') { // if your status is an member
                // $president = 1;
                // $presidentMatching = 1;

                for ($cnt=0; $cnt <count($getUpline) ; $cnt++) { // loop all yourexisting upline
                  //  $getUpline[$cnt]."<br>";

                //  $getUplineUid = getUser($conn, "WHERE uid = ? AND status = 'Active'", array("uid"), array($getUpline[$cnt]), "s");
                $getUplineUid = getUser($conn, "WHERE uid = ? ", array("uid"), array($getUpline[$cnt]), "s");

                  $getUplineUid[0]->getUserRank()."<br>"; // get your upline current status
                 $receiveUid = $getUplineUid[0]->getUid(); // get uid
                 $receiveUsername = $getUplineUid[0]->getusername(); // get username
                 // $bonusType = 'Leadership';

                 if ($getUplineUid[0]->getUserRank() == 'Director') { // if your upline partner
                   if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                    $newBonus = $directorBonus - $presidentBonus; // get 20 leadership bonus because president already take the leadership bonus
                   $director = 1; // means we already loop into partner
                   // $directorMatching = 1;
                   $bonusType = 'Leadership';
                   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

                 }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $directorMatching == 2 ) { // means the data didnt enter president and agent loop
                    $newBonus = $directorBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
                   $director = 1; // means we already loop into partner
                   // $directorMatching = 1;
                     $bonusType = 'Leadership';
                     if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

                 }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $directorMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
                    $newBonus = $directorBonus; // get leadership bonus 40 because the  Agent take the bonus 30
                   $director = 1;
                   // $directorMatching = 1;
                   $bonusType = 'Leadership';
                   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

                 }elseif ($director == 2 && $partner == 2 && $president == 1 && $agent == 1 && $directorMatching == 2 ) { // means the data enter president & agent loop
                    $newBonus = $directorBonus - $presidentBonus; // get leadership bonus 20 because the president take 50 bonus
                   $director = 1;
                   $bonusType = 'Leadership';
                   // $directorMatching = 1;
                   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }
                     // $partner = 2;
                 }elseif ($director == 2 && $partner == 1 && $president == 1 && $agent == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                  $newBonus = $directorBonus - $partnerBonus; // get 20 leadership bonus because president already take the leadership bonus
                 $director = 1; // means we already loop into partner
                 // $directorMatching = 1;
                 $bonusType = 'Leadership';
                 if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

               }elseif ($director == 2 && $partner == 1 && $president == 2 && $agent == 2 && $directorMatching == 2 ) { // means the data didnt enter president and agent loop
                  $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
                 $director = 1; // means we already loop into partner
                 // $directorMatching = 1;
                   $bonusType = 'Leadership';
                   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

               }elseif ($director == 2 && $partner == 1 && $president == 2 && $agent == 1 && $directorMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
                  $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 40 because the  Agent take the bonus 30
                 $director = 1;
                 // $directorMatching = 1;
                 $bonusType = 'Leadership';
                 if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

               }elseif ($director == 2 && $partner == 1 && $president == 1 && $agent == 1 && $directorMatching == 2 ) { // means the data enter president & agent loop
                  $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 20 because the president take 50 bonus
                 $director = 1;
                 $bonusType = 'Leadership';
                 // $directorMatching = 1;
                 if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }
                   // $partner = 2;
               }
                 elseif ($director == 1 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                  $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                 $director = 1; // means we already loop into partner
                 $directorMatching = 1;
                 $bonusType = 'Matching';
                 if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

               }}elseif ($getUplineUid[0]->getUserRank() == 'Partner') { // if your upline partner
                   if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $partnerMatching == 2 ) { // means the data already enter president loop because $president = 1
                    $newBonus = $partnerBonus - $presidentBonus; // get 20 leadership bonus because president already take the leadership bonus
                   $partner = 1; // means we already loop into partner
                   // $partnerMatching = 1;
                   $bonusType = 'Leadership';
                   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

                 }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $partnerMatching == 2 ) { // means the data didnt enter president and agent loop
                    $newBonus = $partnerBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
                   $partner = 1; // means we already loop into partner
                   // $partnerMatching = 1;
                     $bonusType = 'Leadership';
                     if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

                 }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $partnerMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
                    $newBonus = $partnerBonus; // get leadership bonus 40 because the  Agent take the bonus 30

                   $partner = 1;
                   // $partnerMatching = 1;
                   $bonusType = 'Leadership';
                   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

                 }elseif ($director == 2 && $partner == 2 && $president == 1 && $agent == 1 && $partnerMatching == 2 ) { // means the data enter president & agent loop
                    $newBonus = $partnerBonus - $presidentBonus; // get leadership bonus 20 because the president take 50 bonus

                   $partner = 1;
                   $bonusType = 'Leadership';
                   // $partnerMatching = 1;
                   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }
                     // $partner = 2;
                 }
                 elseif ($partner == 1 && $partnerMatching == 2 && $directorMatching == 2) { // means the data already enter president loop because $president = 1
                    $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus

                    $partner = 1; // means we already loop into partner
                    $partnerMatching = 1;
                    $bonusType = 'Matching';
                     if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

               }}elseif ($getUplineUid[0]->getUserRank() == 'President') { // if your upline president
                   if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $presidentMatching == 2) { // means the data already enter partner loop because $partner = 1
                   //  $newBonus = $partnerBonus - $presidentBonus - $agentBonus; // not get any leadership bonus because partner already take the leadership bonus
                //    $president = 1;
                //    // $presidentMatching = 1;
                //    if ($president == 1 && $presidentMatching == 2 && $partnerMatching == 2 && $directorMatching == 2) { // means the data didnt enter partner loop because $partner == 2
                //     $newBonus = 25; // get leadership bonus 50
                //
                //      $president = 1;
                //      $presidentMatching = 1;
                //      $bonusType = 'Matching';
                //      if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                // {
                //   $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                //                             $referrerWallet = $referrerDetails[0]->getSales();
                //                             $referrerBonus = $referrerDetails[0]->getBonus();
                //                             $newReferrerWallet = $referrerWallet + $newBonus;
                //                             $newReferrerBonus = $referrerBonus + $newBonus;
                //
                //                             $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                // }

                    // }

                 }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $presidentMatching == 2) { // means the data didnt enter partner loop because $partner == 2
                   $president = 1;
                   $newBonus = 25; // get leadership bonus 50
                    $bonusType = 'Matching';
                    $newBonusII = $presidentBonus;
                    $bonusTypeII = 'Leadership';
                    if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
               {
                 $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                           $referrerWallet = $referrerDetails[0]->getSales();
                                           $referrerBonus = $referrerDetails[0]->getBonus();
                                           $newReferrerWallet = $referrerWallet + $newBonus;
                                           $newReferrerBonus = $referrerBonus + $newBonus;

                                           $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
               }
               if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonusII,$bonusTypeII,$referralName,$status))
          {
            $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                      $referrerWallet = $referrerDetails[0]->getSales();
                                      $referrerBonus = $referrerDetails[0]->getBonus();
                                      $newReferrerWallet = $referrerWallet + $newBonusII;
                                      $newReferrerBonus = $referrerBonus + $newBonusII;

                                      $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
          }
                   // $presidentMatching = 1;
                //    if ($president == 1 && $presidentMatching == 2 && $partnerMatching == 2 && $directorMatching == 2) { // means the data didnt enter partner loop because $partner == 2
                //     $newBonus = 25; // get leadership bonus 50
                //
                //      $president = 1;
                //      $presidentMatching = 1;
                //      $bonusType = 'Matching';
                //      if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                // {
                //   $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                //                             $referrerWallet = $referrerDetails[0]->getSales();
                //                             $referrerBonus = $referrerDetails[0]->getBonus();
                //                             $newReferrerWallet = $referrerWallet + $newBonus;
                //                             $newReferrerBonus = $referrerBonus + $newBonus;
                //
                //                             $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                // }
                //
                //     }
                //     if ($president == 1 && $presidentMatching == 2 && $partnerMatching == 2 && $directorMatching == 2) { // means the data didnt enter partner loop because $partner == 2
                //     $newBonus = 25; // get leadership bonus 50
                //
                //      $president = 1;
                //      $presidentMatching = 1;
                //      $bonusType = 'Matching';
                //      if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                // {
                //   $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                //                             $referrerWallet = $referrerDetails[0]->getSales();
                //                             $referrerBonus = $referrerDetails[0]->getBonus();
                //                             $newReferrerWallet = $referrerWallet + $newBonus;
                //                             $newReferrerBonus = $referrerBonus + $newBonus;
                //
                //                             $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                // }
                //
                //     }

                }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $presidentMatching == 2) { // means the data  enter  agent loop because $agent == 1
                  $newBonus = $partnerBonus; // get leadership bonus 40 because the  Agent take the bonus 30

                 $partner = 1;
                 // $partnerMatching = 1;
                 $bonusType = 'Leadership';
                 if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
              {
                $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                          $referrerWallet = $referrerDetails[0]->getSales();
                                          $referrerBonus = $referrerDetails[0]->getBonus();
                                          $newReferrerWallet = $referrerWallet + $newBonus;
                                          $newReferrerBonus = $referrerBonus + $newBonus;

                                          $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
              }
                  // $presidentMatching = 1;
                //   if ($president == 1 && $presidentMatching == 2 && $partnerMatching == 2 && $directorMatching == 2) { // means the data didnt enter partner loop because $partner == 2
                //     $newBonus = 25; // get leadership bonus 50
                //
                //      $president = 1;
                //      $presidentMatching = 1;
                //      $bonusType = 'Matching';
                //      if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                // {
                //   $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                //                             $referrerWallet = $referrerDetails[0]->getSales();
                //                             $referrerBonus = $referrerDetails[0]->getBonus();
                //                             $newReferrerWallet = $referrerWallet + $newBonus;
                //                             $newReferrerBonus = $referrerBonus + $newBonus;
                //
                //                             $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                // }
                //
                    // }
                }
               elseif ($president == 1 && $presidentMatching == 2 && $partnerMatching == 2 && $directorMatching == 2) { // means the data didnt enter partner loop because $partner == 2
                //   $newBonus = 25; // get leadership bonus 50
                //
                //  $president = 1;
                //  $presidentMatching = 1;
                //  $bonusType = 'Matching';
                //  if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                // {
                //   $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                //                             $referrerWallet = $referrerDetails[0]->getSales();
                //                             $referrerBonus = $referrerDetails[0]->getBonus();
                //                             $newReferrerWallet = $referrerWallet + $newBonus;
                //                             $newReferrerBonus = $referrerBonus + $newBonus;
                //
                //                             $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                // }

               }}elseif ($getUplineUid[0]->getUserRank() == 'Agent') { // if your upline agent
                 if ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $agentMatching == 2) { // means the data  enter  agent loop because $agent == 1
                 $agent = 1;
                 // $agentMatching = 1;

                 }
               elseif ($agent = 1 && $agentMatching == 2 && $presidentMatching == 2 && $partnerMatching == 2 && $directorMatching == 2) { // means the data didnt enter partner loop because $partner == 2
                 $newBonus = 25; // get leadership bonus 30

                $agent = 1;
                $agentMatching = 1;
                $bonusType = 'Matching';
                if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

               }}}}
          // till here
               //========================================================================================================

               if ($yourOwnCurrentStatus == 'Partner') { // if your status is an member
                // $partner = 1;
                // $partnerMatching = 1;

                for ($cnt=0; $cnt <count($getUpline) ; $cnt++) { // loop all yourexisting upline
                  //  $getUpline[$cnt]."<br>";

                //  $getUplineUid = getUser($conn, "WHERE uid = ? AND status = 'Active'", array("uid"), array($getUpline[$cnt]), "s");
                $getUplineUid = getUser($conn, "WHERE uid = ? ", array("uid"), array($getUpline[$cnt]), "s");

                  $getUplineUid[0]->getUserRank()."<br>"; // get your upline current status
                 $receiveUid = $getUplineUid[0]->getUid(); // get uid
                 $receiveUsername = $getUplineUid[0]->getusername(); // get username
                 // $bonusType = 'Leadership';

                 if ($getUplineUid[0]->getUserRank() == 'Director') { // if your upline partner
                   if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                    $newBonus = $directorBonus; // get 20 leadership bonus because president already take the leadership bonus
                   $director = 1; // means we already loop into partner
                   // $directorMatching = 1;
                   $bonusType = 'Leadership';
                   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

                 }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $directorMatching == 2 ) { // means the data didnt enter president and agent loop
                    $newBonus = $directorBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
                   $director = 1; // means we already loop into partner
                   // $directorMatching = 1;
                     $bonusType = 'Leadership';
                     if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

                 }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $directorMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
                    $newBonus = $directorBonus; // get leadership bonus 40 because the  Agent take the bonus 30
                   $director = 1;
                   // $directorMatching = 1;
                   $bonusType = 'Leadership';
                   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

                 }elseif ($director == 2 && $partner == 2 && $president == 1 && $agent == 1 && $directorMatching == 2 ) { // means the data enter president & agent loop
                    $newBonus = $directorBonus; // get leadership bonus 20 because the president take 50 bonus
                   $director = 1;
                   $bonusType = 'Leadership';
                   // $directorMatching = 1;
                   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }
                     // $partner = 2;
                 }elseif ($director == 2 && $partner == 1 && $president == 1 && $agent == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                  $newBonus = $directorBonus - $partnerBonus; // get 20 leadership bonus because president already take the leadership bonus
                 $director = 1; // means we already loop into partner
                 // $directorMatching = 1;
                 $bonusType = 'Leadership';
                 if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

               }elseif ($director == 2 && $partner == 1 && $president == 2 && $agent == 2 && $directorMatching == 2 ) { // means the data didnt enter president and agent loop
                  $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 70 because the partner and agent didnt not take the bonus yet
                 $director = 1; // means we already loop into partner
                 // $directorMatching = 1;
                   $bonusType = 'Leadership';
                   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

               }elseif ($director == 2 && $partner == 1 && $president == 2 && $agent == 1 && $directorMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
                  $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 40 because the  Agent take the bonus 30
                 $director = 1;
                 // $directorMatching = 1;
                 $bonusType = 'Leadership';
                 if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

               }elseif ($director == 2 && $partner == 1 && $president == 1 && $agent == 1 && $directorMatching == 2 ) { // means the data enter president & agent loop
                  $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 20 because the president take 50 bonus
                 $director = 1;
                 $bonusType = 'Leadership';
                 // $directorMatching = 1;
                 if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }
                   // $partner = 2;
               }
                 elseif ($director == 1 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                  $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                 $director = 1; // means we already loop into partner
                 $directorMatching = 1;
                 $bonusType = 'Matching';
                 if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

               }}elseif ($getUplineUid[0]->getUserRank() == 'Partner') { // if your upline partner
                   if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $partnerMatching == 2 ) { // means the data already enter president loop because $president = 1
                     $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                    $partner = 1; // means we already loop into partner
                    $partnerMatching = 1;
                    $bonusType = 'Matching';
                    $newBonusII = $partnerBonus;
                    $bonusTypeII = 'Leadership';
                    if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                 {
                   $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                             $referrerWallet = $referrerDetails[0]->getSales();
                                             $referrerBonus = $referrerDetails[0]->getBonus();
                                             $newReferrerWallet = $referrerWallet + $newBonus;
                                             $newReferrerBonus = $referrerBonus + $newBonus;

                                             $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                 }
                 if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonusII,$bonusTypeII,$referralName,$status))
              {
                $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                          $referrerWallet = $referrerDetails[0]->getSales();
                                          $referrerBonus = $referrerDetails[0]->getBonus();
                                          $newReferrerWallet = $referrerWallet + $newBonusII;
                                          $newReferrerBonus = $referrerBonus + $newBonusII;

                                          $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
              }
                   // $partnerMatching = 1;
                //    if ($partner == 1 && $partnerMatching == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                //     $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                //
                //    $partner = 1; // means we already loop into partner
                //    $partnerMatching = 1;
                //    $bonusType = 'Matching';
                //    if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                // {
                //   $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                //                             $referrerWallet = $referrerDetails[0]->getSales();
                //                             $referrerBonus = $referrerDetails[0]->getBonus();
                //                             $newReferrerWallet = $referrerWallet + $newBonus;
                //                             $newReferrerBonus = $referrerBonus + $newBonus;
                //
                //                             $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                // }
                //
                //   }
                 }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $partnerMatching == 2 ) { // means the data didnt enter president and agent loop
                   $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                  $partner = 1; // means we already loop into partner
                  $partnerMatching = 1;
                  $bonusType = 'Matching';
                  $newBonusII = $partnerBonus;
                  $bonusTypeII = 'Leadership';
                  if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
               {
                 $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                           $referrerWallet = $referrerDetails[0]->getSales();
                                           $referrerBonus = $referrerDetails[0]->getBonus();
                                           $newReferrerWallet = $referrerWallet + $newBonus;
                                           $newReferrerBonus = $referrerBonus + $newBonus;

                                           $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
               }
               if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonusII,$bonusTypeII,$referralName,$status))
            {
              $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                        $referrerWallet = $referrerDetails[0]->getSales();
                                        $referrerBonus = $referrerDetails[0]->getBonus();
                                        $newReferrerWallet = $referrerWallet + $newBonusII;
                                        $newReferrerBonus = $referrerBonus + $newBonusII;

                                        $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
            }
                   // $partnerMatching = 1;
                //   if ($partner == 1 && $partnerMatching == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                //     $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                //
                //    $partner = 1; // means we already loop into partner
                //    $partnerMatching = 1;
                //    $bonusType = 'Matching';
                //    if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                // {
                //   $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                //                             $referrerWallet = $referrerDetails[0]->getSales();
                //                             $referrerBonus = $referrerDetails[0]->getBonus();
                //                             $newReferrerWallet = $referrerWallet + $newBonus;
                //                             $newReferrerBonus = $referrerBonus + $newBonus;
                //
                //                             $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                // }
                //
                //   }
                 }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $partnerMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
                   $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                  $partner = 1; // means we already loop into partner
                  $partnerMatching = 1;
                  $bonusType = 'Matching';
                  $newBonusII = $partnerBonus;
                  $bonusTypeII = 'Leadership';
                  if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
               {
                 $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                           $referrerWallet = $referrerDetails[0]->getSales();
                                           $referrerBonus = $referrerDetails[0]->getBonus();
                                           $newReferrerWallet = $referrerWallet + $newBonus;
                                           $newReferrerBonus = $referrerBonus + $newBonus;

                                           $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
               }
               if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonusII,$bonusTypeII,$referralName,$status))
            {
              $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                        $referrerWallet = $referrerDetails[0]->getSales();
                                        $referrerBonus = $referrerDetails[0]->getBonus();
                                        $newReferrerWallet = $referrerWallet + $newBonusII;
                                        $newReferrerBonus = $referrerBonus + $newBonusII;

                                        $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
            }
                   // $partnerMatching = 1;
                //    if ($partner == 1 && $partnerMatching == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                //     $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                //
                //    $partner = 1; // means we already loop into partner
                //    $partnerMatching = 1;
                //    $bonusType = 'Matching';
                //    if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                // {
                //   $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                //                             $referrerWallet = $referrerDetails[0]->getSales();
                //                             $referrerBonus = $referrerDetails[0]->getBonus();
                //                             $newReferrerWallet = $referrerWallet + $newBonus;
                //                             $newReferrerBonus = $referrerBonus + $newBonus;
                //
                //                             $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                // }
                //
                //   }
                 }elseif ($director == 2 && $partner == 2 && $president == 1 && $agent == 1 && $partnerMatching == 2 ) { // means the data enter president & agent loop
                   $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                  $partner = 1; // means we already loop into partner
                  $partnerMatching = 1;
                  $bonusType = 'Matching';
                  $newBonusII = $partnerBonus;
                  $bonusTypeII = 'Leadership';
                  if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
               {
                 $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                           $referrerWallet = $referrerDetails[0]->getSales();
                                           $referrerBonus = $referrerDetails[0]->getBonus();
                                           $newReferrerWallet = $referrerWallet + $newBonus;
                                           $newReferrerBonus = $referrerBonus + $newBonus;

                                           $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
               }
               if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonusII,$bonusTypeII,$referralName,$status))
            {
              $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                        $referrerWallet = $referrerDetails[0]->getSales();
                                        $referrerBonus = $referrerDetails[0]->getBonus();
                                        $newReferrerWallet = $referrerWallet + $newBonusII;
                                        $newReferrerBonus = $referrerBonus + $newBonusII;

                                        $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
            }
                   // $bonusType = 'Leadership';
                   // $partnerMatching = 1;
                //    if ($partner == 1 && $partnerMatching == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                //     $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                //
                //    $partner = 1; // means we already loop into partner
                //    $partnerMatching = 1;
                //    $bonusType = 'Matching';
                //    if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                // {
                //   $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                //                             $referrerWallet = $referrerDetails[0]->getSales();
                //                             $referrerBonus = $referrerDetails[0]->getBonus();
                //                             $newReferrerWallet = $referrerWallet + $newBonus;
                //                             $newReferrerBonus = $referrerBonus + $newBonus;
                //
                //                             $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                // }
                //
                //   }
                 }
                 elseif ($partner == 1 && $partnerMatching == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                //     $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                //
                //    $partner = 1; // means we already loop into partner
                //    $partnerMatching = 1;
                //    $bonusType = 'Matching';
                //    if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                // {
                //   $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                //                             $referrerWallet = $referrerDetails[0]->getSales();
                //                             $referrerBonus = $referrerDetails[0]->getBonus();
                //                             $newReferrerWallet = $referrerWallet + $newBonus;
                //                             $newReferrerBonus = $referrerBonus + $newBonus;
                //
                //                             $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                // }

               }}elseif ($getUplineUid[0]->getUserRank() == 'President') { // if your upline president
                   if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $presidentMatching == 2) { // means the data already enter partner loop because $partner = 1
                   //  $newBonus = $partnerBonus - $presidentBonus - $agentBonus; // not get any leadership bonus because partner already take the leadership bonus
                   $president = 1;
                   // $presidentMatching = 1;

                 }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $presidentMatching == 2) { // means the data didnt enter partner loop because $partner == 2
                    $president = 1;
                    // $presidentMatching = 1;
                }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $presidentMatching == 2) { // means the data  enter  agent loop because $agent == 1
                  $president = 1;
                  // $presidentMatching = 1;
                }
               elseif ($president == 1 && $presidentMatching == 2 && $partnerMatching == 2 && $directorMatching == 2) { // means the data didnt enter partner loop because $partner == 2
                  $newBonus = 25; // get leadership bonus 50

                 $president = 1;
                 $presidentMatching = 1;
                 $bonusType = 'Matching';
                 if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

               }}elseif ($getUplineUid[0]->getUserRank() == 'Agent') { // if your upline agent
                 if ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $agentMatching == 2) { // means the data  enter  agent loop because $agent == 1
                 $agent = 1;
                 // $agentMatching = 1;

                 }
               elseif ($agent == 1 && $agentMatching == 2 && $presidentMatching == 2 && $partnerMatching == 2 & $directorMatching == 2) { // means the data didnt enter partner loop because $partner == 2
                  $newBonus = 25; // get leadership bonus 30

                  $agent = 1;
                  $agentMatching = 1;
                  $bonusType = 'Matching';
                  if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }


               }}}}
            //=========================================================================================================================================================================================================================================
            if ($yourOwnCurrentStatus == 'Director') { // if your status is an member
              // $director = 1;
              // $directorMatching = 1;

             for ($cnt=0; $cnt <count($getUpline) ; $cnt++) { // loop all yourexisting upline
               //  $getUpline[$cnt]."<br>";

              // $getUplineUid = getUser($conn, "WHERE uid = ? AND status = 'Active'", array("uid"), array($getUpline[$cnt]), "s");
              $getUplineUid = getUser($conn, "WHERE uid = ? ", array("uid"), array($getUpline[$cnt]), "s");

               $getUplineUid[0]->getUserRank()."<br>"; // get your upline current status
              $receiveUid = $getUplineUid[0]->getUid(); // get uid
              $receiveUsername = $getUplineUid[0]->getusername(); // get username
              // $bonusType = 'Leadership';

              if ($getUplineUid[0]->getUserRank() == 'Director') { // if your upline partner
                if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                  $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                  $director = 1; // means we already loop into partner
                  $directorMatching = 1;
                  $bonusType = 'Matching';
                  $newBonusII = $directorBonus;
                  $bonusTypeII = 'leadership';
                  if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                  {
                    $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                              $referrerWallet = $referrerDetails[0]->getSales();
                                              $referrerBonus = $referrerDetails[0]->getBonus();
                                              $newReferrerWallet = $referrerWallet + $newBonus;
                                              $newReferrerBonus = $referrerBonus + $newBonus;

                                              $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                  }
                  if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonusII,$bonusTypeII,$referralName,$status))
                  {
                    $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                              $referrerWallet = $referrerDetails[0]->getSales();
                                              $referrerBonus = $referrerDetails[0]->getBonus();
                                              $newReferrerWallet = $referrerWallet + $newBonusII;
                                              $newReferrerBonus = $referrerBonus + $newBonusII;

                                              $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                  }
                  }
                if ($director == 1 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                // $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                // $director = 1; // means we already loop into partner
                // $directorMatching = 1;
                // $bonusType = 'Matching';
                // if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                // {
                //   $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                //                             $referrerWallet = $referrerDetails[0]->getSales();
                //                             $referrerBonus = $referrerDetails[0]->getBonus();
                //                             $newReferrerWallet = $referrerWallet + $newBonus;
                //                             $newReferrerBonus = $referrerBonus + $newBonus;
                //
                //                             $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                // }
                // }
                // $directorMatching = 1;
              }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $directorMatching == 2 ) { // means the data didnt enter president and agent loop
                $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
        $director = 1; // means we already loop into partner
        $directorMatching = 1;
        $bonusType = 'Matching';
        $newBonusII = $directorBonus;
        $bonusTypeII = 'leadership';
        if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
        {
          $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                    $referrerWallet = $referrerDetails[0]->getSales();
                                    $referrerBonus = $referrerDetails[0]->getBonus();
                                    $newReferrerWallet = $referrerWallet + $newBonus;
                                    $newReferrerBonus = $referrerBonus + $newBonus;

                                    $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
        }
        if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonusII,$bonusTypeII,$referralName,$status))
        {
          $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                    $referrerWallet = $referrerDetails[0]->getSales();
                                    $referrerBonus = $referrerDetails[0]->getBonus();
                                    $newReferrerWallet = $referrerWallet + $newBonusII;
                                    $newReferrerBonus = $referrerBonus + $newBonusII;

                                    $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
        }
                if ($director == 1 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                // $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                // $director = 1; // means we already loop into partner
                // $directorMatching = 1;
                // $bonusType = 'Matching';
                // if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                // {
                //   $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                //                             $referrerWallet = $referrerDetails[0]->getSales();
                //                             $referrerBonus = $referrerDetails[0]->getBonus();
                //                             $newReferrerWallet = $referrerWallet + $newBonus;
                //                             $newReferrerBonus = $referrerBonus + $newBonus;

                //                             $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                // }
                }
                // $directorMatching = 1;
              }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $directorMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
                $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                $director = 1; // means we already loop into partner
                $directorMatching = 1;
                $bonusType = 'Matching';
                $newBonusII = $directorBonus;
                $bonusTypeII = 'leadership';
                if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }
                if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonusII,$bonusTypeII,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonusII;
                                            $newReferrerBonus = $referrerBonus + $newBonusII;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }
                // $directorMatching = 1;
                // if ($director == 1 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                // $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                // $director = 1; // means we already loop into partner
                // $directorMatching = 1;
                // $bonusType = 'Matching';
                // if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                // {
                //   $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                //                             $referrerWallet = $referrerDetails[0]->getSales();
                //                             $referrerBonus = $referrerDetails[0]->getBonus();
                //                             $newReferrerWallet = $referrerWallet + $newBonus;
                //                             $newReferrerBonus = $referrerBonus + $newBonus;
                //
                //                             $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                // }
                // }
              }elseif ($director == 2 && $partner == 2 && $president == 1 && $agent == 1 && $directorMatching == 2 ) { // means the data enter president & agent loop
                $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                $director = 1; // means we already loop into partner
                $directorMatching = 1;
                $bonusType = 'Matching';
                $newBonusII = $directorBonus;
                $bonusTypeII = 'leadership';
                if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }
                if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonusII,$bonusTypeII,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonusII;
                                            $newReferrerBonus = $referrerBonus + $newBonusII;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }
                // $bonusType = 'Leadership';
                // $directorMatching = 1;
                // if ($director == 1 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                // $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                // $director = 1; // means we already loop into partner
                // $directorMatching = 1;
                // $bonusType = 'Matching';
                // if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                // {
                //   $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                //                             $referrerWallet = $referrerDetails[0]->getSales();
                //                             $referrerBonus = $referrerDetails[0]->getBonus();
                //                             $newReferrerWallet = $referrerWallet + $newBonus;
                //                             $newReferrerBonus = $referrerBonus + $newBonus;
                //
                //                             $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                // }
                // }
              }elseif ($director == 2 && $partner == 1 && $president == 1 && $agent == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
                $director = 1; // means we already loop into partner
                $directorMatching = 1;
                $bonusType = 'Matching';
                $newBonusII = $directorBonus;
                $bonusTypeII = 'leadership';
                if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }
                if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonusII,$bonusTypeII,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonusII;
                                            $newReferrerBonus = $referrerBonus + $newBonusII;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }
              // $directorMatching = 1;
              // if ($director == 1 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
              //   $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
              //   $director = 1; // means we already loop into partner
              //   $directorMatching = 1;
              //   $bonusType = 'Matching';
              //   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
              //   {
              //     $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
              //                               $referrerWallet = $referrerDetails[0]->getSales();
              //                               $referrerBonus = $referrerDetails[0]->getBonus();
              //                               $newReferrerWallet = $referrerWallet + $newBonus;
              //                               $newReferrerBonus = $referrerBonus + $newBonus;
              //
              //                               $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
              //   }
              //   }
            }elseif ($director == 2 && $partner == 1 && $president == 2 && $agent == 2 && $directorMatching == 2 ) { // means the data didnt enter president and agent loop
              $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
              $director = 1; // means we already loop into partner
              $directorMatching = 1;
              $bonusType = 'Matching';
              $newBonusII = $directorBonus;
              $bonusTypeII = 'leadership';
              if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
              {
                $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                          $referrerWallet = $referrerDetails[0]->getSales();
                                          $referrerBonus = $referrerDetails[0]->getBonus();
                                          $newReferrerWallet = $referrerWallet + $newBonus;
                                          $newReferrerBonus = $referrerBonus + $newBonus;

                                          $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
              }
              if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonusII,$bonusTypeII,$referralName,$status))
              {
                $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                          $referrerWallet = $referrerDetails[0]->getSales();
                                          $referrerBonus = $referrerDetails[0]->getBonus();
                                          $newReferrerWallet = $referrerWallet + $newBonusII;
                                          $newReferrerBonus = $referrerBonus + $newBonusII;

                                          $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
              }
              // $directorMatching = 1;
              // if ($director == 1 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
              //   $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
              //   $director = 1; // means we already loop into partner
              //   $directorMatching = 1;
              //   $bonusType = 'Matching';
              //   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
              //   {
              //     $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
              //                               $referrerWallet = $referrerDetails[0]->getSales();
              //                               $referrerBonus = $referrerDetails[0]->getBonus();
              //                               $newReferrerWallet = $referrerWallet + $newBonus;
              //                               $newReferrerBonus = $referrerBonus + $newBonus;
              //
              //                               $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
              //   }
              //   }
            }elseif ($director == 2 && $partner == 1 && $president == 2 && $agent == 1 && $directorMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
              //  $newBonus = $directorBonus - $partnerBonus; // get leadership bonus 40 because the  Agent take the bonus 30
              $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
              $director = 1; // means we already loop into partner
              $directorMatching = 1;
              $bonusType = 'Matching';
              $newBonusII = $directorBonus;
              $bonusTypeII = 'leadership';
              if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
              {
                $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                          $referrerWallet = $referrerDetails[0]->getSales();
                                          $referrerBonus = $referrerDetails[0]->getBonus();
                                          $newReferrerWallet = $referrerWallet + $newBonus;
                                          $newReferrerBonus = $referrerBonus + $newBonus;

                                          $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
              }
              if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonusII,$bonusTypeII,$referralName,$status))
              {
                $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                          $referrerWallet = $referrerDetails[0]->getSales();
                                          $referrerBonus = $referrerDetails[0]->getBonus();
                                          $newReferrerWallet = $referrerWallet + $newBonusII;
                                          $newReferrerBonus = $referrerBonus + $newBonusII;

                                          $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
              }
              // $directorMatching = 1;
              // if ($director == 1 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
              //   $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
              //   $director = 1; // means we already loop into partner
              //   $directorMatching = 1;
              //   $bonusType = 'Matching';
              //   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
              //   {
              //     $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
              //                               $referrerWallet = $referrerDetails[0]->getSales();
              //                               $referrerBonus = $referrerDetails[0]->getBonus();
              //                               $newReferrerWallet = $referrerWallet + $newBonus;
              //                               $newReferrerBonus = $referrerBonus + $newBonus;
              //
              //                               $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
              //   }
              //   }
            }elseif ($director == 2 && $partner == 1 && $president == 1 && $agent == 1 && $directorMatching == 2 ) { // means the data enter president & agent loop
              $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
              $director = 1; // means we already loop into partner
              $directorMatching = 1;
              $bonusType = 'Matching';
              $newBonusII = $directorBonus;
              $bonusTypeII = 'leadership';
              if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
              {
                $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                          $referrerWallet = $referrerDetails[0]->getSales();
                                          $referrerBonus = $referrerDetails[0]->getBonus();
                                          $newReferrerWallet = $referrerWallet + $newBonus;
                                          $newReferrerBonus = $referrerBonus + $newBonus;

                                          $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
              }
              if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonusII,$bonusTypeII,$referralName,$status))
              {
                $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                          $referrerWallet = $referrerDetails[0]->getSales();
                                          $referrerBonus = $referrerDetails[0]->getBonus();
                                          $newReferrerWallet = $referrerWallet + $newBonusII;
                                          $newReferrerBonus = $referrerBonus + $newBonusII;

                                          $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
              }
              // $bonusType = 'Leadership';
              // $directorMatching = 1;
              // if ($director == 1 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
              //   $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
              //   $director = 1; // means we already loop into partner
              //   $directorMatching = 1;
              //   $bonusType = 'Matching';
              //   if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
              //   {
              //     $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
              //                               $referrerWallet = $referrerDetails[0]->getSales();
              //                               $referrerBonus = $referrerDetails[0]->getBonus();
              //                               $newReferrerWallet = $referrerWallet + $newBonus;
              //                               $newReferrerBonus = $referrerBonus + $newBonus;
              //
              //                               $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
              //   }
              //   }
            }
              elseif ($director == 1 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
              //  $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus
              // $director = 1; // means we already loop into partner
              // $directorMatching = 1;
              // $bonusType = 'Matching';
              // if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
              //   {
              //     $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
              //                               $referrerWallet = $referrerDetails[0]->getSales();
              //                               $referrerBonus = $referrerDetails[0]->getBonus();
              //                               $newReferrerWallet = $referrerWallet + $newBonus;
              //                               $newReferrerBonus = $referrerBonus + $newBonus;
              //
              //                               $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
              //   }

            }}elseif ($getUplineUid[0]->getUserRank() == 'Partner') { // if your upline partner
                if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $partnerMatching == 2 ) { // means the data already enter president loop because $president = 1
                $partner = 1; // means we already loop into partner
                // $partnerMatching = 1;
              }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $partnerMatching == 2 ) { // means the data didnt enter president and agent loop
                $partner = 1; // means we already loop into partner
                // $partnerMatching = 1;
              }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $partnerMatching == 2 ) { // means the data  enter  agent loop because $agent == 1
                $partner = 1;
                // $partnerMatching = 1;
              }elseif ($director == 2 && $partner == 2 && $president == 1 && $agent == 1 && $partnerMatching == 2 ) { // means the data enter president & agent loop
                //  $newBonus = $partnerBonus - $presidentBonus; // get leadership bonus 20 because the president take 50 bonus
                //
                $partner = 1;
                // $bonusType = 'Leadership';
                // $partnerMatching = 1;
              }
              elseif ($partner == 1 && $partnerMatching == 2 && $directorMatching == 2 ) { // means the data already enter president loop because $president = 1
                  $newBonus = 25; // get 20 leadership bonus because president already take the leadership bonus

                  $partner = 1; // means we already loop into partner
                  $partnerMatching = 1;
                  $bonusType = 'Matching';
                  if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

            }}elseif ($getUplineUid[0]->getUserRank() == 'President') { // if your upline president
                if ($director == 2 && $partner == 2 && $president == 1 && $agent == 2 && $presidentMatching == 2) { // means the data already enter partner loop because $partner = 1
                $president = 1;
                // $presidentMatching = 1;

              }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $presidentMatching == 2) { // means the data didnt enter partner loop because $partner == 2
                $president = 1;
               // $presidentMatching = 1;
             }elseif ($director == 2 && $partner == 2 && $president == 2 && $agent == 1 && $presidentMatching == 2) { // means the data  enter  agent loop because $agent == 1
               $president = 1;
               // $presidentMatching = 1;

             }
            elseif ($president == 1 && $presidentMatching == 2 && $partnerMatching == 2 && $directorMatching == 2) { // means the data didnt enter partner loop because $partner == 2
                 $newBonus = 25; // get leadership bonus 50

                $president = 1;
                $presidentMatching = 1;
                $bonusType = 'Matching';
                if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

            }}elseif ($getUplineUid[0]->getUserRank() == 'Agent') { // if your upline agent
              if ($director == 2 && $partner == 2 && $president == 2 && $agent == 2 && $agentMatching == 2) { // means the data  enter  agent loop because $agent == 1
                $agent = 1;
                // $agentMatching = 1;
              }
            elseif ($agent == 1 && $agentMatching == 2 && $presidentMatching == 2 && $partnerMatching == 2 && $directorMatching == 2 ) { // means the data didnt enter partner loop because $partner == 2
               $newBonus = 25; // get leadership bonus 30

               $agent = 1;
               $agentMatching = 1;
               $bonusType = 'Matching';
               if(leadership($conn,$orderId,$referralUid,$receiveUsername,$receiveUid,$newBonus,$bonusType,$referralName,$status))
                {
                  $referrerDetails = getUser($conn,"WHERE uid =?",array("uid"),array($receiveUid), "s");
                                            $referrerWallet = $referrerDetails[0]->getSales();
                                            $referrerBonus = $referrerDetails[0]->getBonus();
                                            $newReferrerWallet = $referrerWallet + $newBonus;
                                            $newReferrerBonus = $referrerBonus + $newBonus;

                                            $userWalletUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("bonus","sales"),array($newReferrerBonus,$newReferrerWallet,$receiveUid),"sss");
                }

            }}}}
          }
}
header('location: ../adminViewBill.php?type=1');
