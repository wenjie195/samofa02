<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Withdrawal.php';
require_once dirname(__FILE__) . '/../classes/Bonus.php';
require_once dirname(__FILE__) . '/../classes/OtherBonus.php';

require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/memberIdGenerator.php';

$conn = connDB();

$dateStart = '2020-09-01';
$dateEnd = '2020-10-01';
$total = 0;



?>

<table>
    <?php 
        $bonusDetails = getBonusDetails($conn, "WHERE referrer_id = '3e0892cc30dcf93f4e64ed64c20694c9' and date_created >? AND date_created < ?",array("date_created","date_created"),array($dateStart,$dateEnd), "ss");
        if ($bonusDetails) {
            for ($i=0; $i <count($bonusDetails) ; $i++) { 
                $total += $bonusDetails[$i]->getAmount();
                ?>
                <tr>
                    <td><?php echo $bonusDetails[$i]->getInvoiceNo() ?></td>
                    <td><?php echo $bonusDetails[$i]->getReferrerName() ?></td>
                    <td><?php echo $bonusDetails[$i]->getAmount() ?></td>
                    <td><?php echo date('d/m/Y',strtotime($bonusDetails[$i]->getDateCreated())) ?></td>
                </tr>
                <?php
            }
        }
    ?>
</table>
<hr>
<table>
    <?php 
        $bonusDetails = getOtherBonus($conn, "WHERE receive_uid = '3e0892cc30dcf93f4e64ed64c20694c9' and date_created >? AND date_created < ?",array("date_created","date_created"),array($dateStart,$dateEnd), "ss");
        if ($bonusDetails) {
            for ($i=0; $i <count($bonusDetails) ; $i++) { 
                $total += $bonusDetails[$i]->getBonus();
                ?>
                <tr>
                    <td><?php echo $bonusDetails[$i]->getInvoiceNo() ?></td>
                    <td><?php echo $bonusDetails[$i]->getReceiveUsername() ?></td>
                    <td><?php echo $bonusDetails[$i]->getBonus() ?></td>
                    <td><?php echo date('d/m/Y',strtotime($bonusDetails[$i]->getDateCreated())) ?></td>
                </tr>
                <?php
            }
        }
    ?>
</table>
<hr>
<?php echo $total;?>