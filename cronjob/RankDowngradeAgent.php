<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';

$conn = connDB();

echo $todayDate = date('Y-m-d');
echo "<br>";
// echo $todayDate = date('m');
echo $currentMonth = date('m');
echo "<br>";
echo $currentYear = date('Y');
echo "<br>";
echo "<br>";

$M1 = '01';
$M2 = '02';
$M3 = '03';
$M4 = '04';
$M5 = '05';
$M6 = '06';
$M7 = '07';
$M8 = '08';
$M9 = '09';
$M10 = '10';
$M11 = '11';
$M12 = '12';

// $status = "Inactive";

$allUsers = getUser($conn,"WHERE user_rank = 'Agent' ");

if($allUsers) 
{
    for($cnt = 0;$cnt < count($allUsers) ;$cnt++)
    {

        $userRank = $allUsers[$cnt]->getUserRank();
        // $ordersDetails = getOrders($conn, "WHERE uid= ? AND status = 'APPROVED' ORDER BY date_created DESC LIMIT 1",array("uid"),array($allUsers[$cnt]->getUid()), "s");
        $ordersDetails = getOrders($conn, "WHERE uid= ? AND status = 'APPROVED' AND product_type != 'SalesAids' ORDER BY date_created DESC LIMIT 1",array("uid"),array($allUsers[$cnt]->getUid()), "s");

        if($ordersDetails) 
        {
            for($cntA = 0;$cntA < count($ordersDetails) ;$cntA++)
            {
                // echo "no order ppl";
                echo "got order ppl";
                echo "<br>";
                echo $userUidnoOrder = $allUsers[$cnt]->getUid();
                echo "<br>";
                echo $userUsernamenoOrder = $allUsers[$cnt]->getUsername();
                echo "<br>";
                echo "Rank : ";
                echo $userRanknoOrder = $allUsers[$cnt]->getUserRank();
                echo "<br>";
                echo "Date of Join : ";
                echo $userJoinDatenoOrder = $allUsers[$cnt]->getDateCreated();
                echo "<br>";
                // echo "Inactive Status !!";
                // echo "<br>";
                // echo $orderYear = date('Y',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateCreated()));
                // echo "<br>";    
                // echo $orderMonth = date('m',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateCreated()));
                // echo "<br>";

                //renew agent maintenance
                echo "Today Date : ";
                echo $todayDate = date('Y-m-d');
                // echo $todayDate = '2021-12-25';
                echo "<br>";
                echo "Last Order Date : ";
                echo $orderDate = date('Y-m-d',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateCreated()));
                echo "<br>";

                $Date1 = $orderDate;
                $date = new DateTime($Date1);
                $duration = "365 days";
                $additional = $duration;
                // echo $additional = "10 days";
                // $date->modify('+1 day');
                $date->modify($additional);

                echo "Expired Date : ";
                echo $expiredDate = $date->format('Y-m-d');
                echo "<br>";
                echo "<br>";
                // echo "<br>";

                if($todayDate > $expiredDate) 
                {
                    echo "Inactive Status !!";
                    echo "<br>";
                    echo "<br>";
                    echo "<br>";

                    if($userRanknoOrder == 'Agent')
                    {
                        $downgradeRank = 'Member';
                    }
        
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
            
                    // $rank = "Member";
                    $status = "Inactive";
        
                    if($downgradeRank)
                    {
                        array_push($tableName,"user_rank");
                        array_push($tableValue,$downgradeRank);
                        $stringType .=  "s";
                    }
                    if($status)
                    {
                        array_push($tableName,"status");
                        array_push($tableValue,$status);
                        $stringType .=  "s";
                    }         
                    if($status)
                    {
                        array_push($tableName,"recipient_phone");
                        array_push($tableValue,$status);
                        $stringType .=  "s";
                    }                       
                    array_push($tableValue,$userUidnoOrder);
                    $stringType .=  "s";
                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($passwordUpdated)
                    {
                        echo "Inactive Status !!";
                        echo "<br>";
                        echo "Downgrading !!";
                        echo "<br>";
                    }
                    else
                    {
                        echo "ERROR 2020 ++ : FAIL To Downgrade Status and Ranking !!";
                        echo "<br>";
                    }

                }
                else
                {
                    echo "Active Status !!";
                    echo "<br>";
                    echo "<br>";
                    echo "<br>";
                }
            }
        }
        
        // if(!$ordersDetails) 
        elseif(!$ordersDetails) 
        {
            echo "no order ppl";
            echo "<br>";
            echo $userUidnoOrder = $allUsers[$cnt]->getUid();
            echo "<br>";
            echo $userUsernamenoOrder = $allUsers[$cnt]->getUsername();
            echo "<br>";
            echo "Rank";
            echo "<br>";
            echo $userRanknoOrder = $allUsers[$cnt]->getUserRank();
            echo "<br>";
            echo "Date of Join";
            echo "<br>";
            echo $userJoinDatenoOrder = $allUsers[$cnt]->getDateCreated();
            echo "<br>";
            echo "Inactive Status !!";
            echo "<br>";
            echo "<br>";
            echo "<br>";

            if($userRanknoOrder == 'Agent')
            {
                $downgradeRank = 'Member';
            }

            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
    
            // $rank = "Member";
            $status = "Inactive";

            if($downgradeRank)
            {
                array_push($tableName,"user_rank");
                array_push($tableValue,$downgradeRank);
                $stringType .=  "s";
            }
            if($status)
            {
                array_push($tableName,"status");
                array_push($tableValue,$status);
                $stringType .=  "s";
            }         
            if($status)
            {
                array_push($tableName,"recipient_phone");
                array_push($tableValue,$status);
                $stringType .=  "s";
            }                       
            array_push($tableValue,$userUidnoOrder);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                echo "Inactive Status !!";
                echo "<br>";
                echo "Downgrading !!";
                echo "<br>";
            }
            else
            {
                echo "ERROR 2020 ++ : FAIL To Downgrade Status and Ranking !!";
                echo "<br>";
            }

        }
    }
}
?>