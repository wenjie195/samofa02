<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';

$conn = connDB();

// echo $todayDate = date('Y-m-d H:i');
// echo $todayDate = date('m');
echo $currentMonth = date('m');
echo $currentYear = date('Y');
echo "<br>";
echo "<br>";

$M1 = 1;
$M2 = 2;
$M3 = 3;
$M4 = 4;
$M5 = 5;
$M6 = 6;
$M7 = 7;
$M8 = 8;
$M9 = 9;
$M10 = 10;
$M11 = 11;
$M12 = 12;

$allUsers = getUser($conn);
// $allUsers = getUser($conn,"WHERE user_rank != 'Member'");
// $allUsers = getUser($conn,"WHERE user_rank == 'Director' ");
if($allUsers) 
{
    for($cnt = 0;$cnt < count($allUsers) ;$cnt++)
    {

        $userRank = $allUsers[$cnt]->getUserRank();
        if($userRank == 'Member') 
        {
            $ordersDetails = getOrders($conn, "WHERE uid= ? AND status = 'APPROVED'  ORDER BY date_created DESC LIMIT 1",array("uid"),array($allUsers[$cnt]->getUid()), "s");
            if($ordersDetails) 
            {
                for($cntA = 0;$cntA < count($ordersDetails) ;$cntA++)
                {
                    echo $userRank = $allUsers[$cnt]->getUserRank();
                    echo "<br>";
                    echo $orderUsername = $ordersDetails[$cntA]->getUsername();
                    echo "<br>";
                    echo $orderDateApproved = $ordersDetails[$cntA]->getDateUpdated();
                    echo "<br>";
                    // echo $orderMonth = date('m',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateUpdated()));
                    echo $orderYear = date('Y',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateUpdated()));
                    echo "<br>";    

                    if($orderYear == $currentYear) 
                    {
                        echo "Maintenance Whole Year Done !!";
                        echo "<br>";
                        echo "<br>";
                    }
                    else
                    {
                        echo "Inactive Status !!";
                        echo "<br>";
                        echo "<br>";
                    }

                }
            }
        }
        elseif($userRank == 'Agent') 
        {
            $ordersDetails = getOrders($conn, "WHERE uid= ? AND status = 'APPROVED' ORDER BY date_created DESC LIMIT 1",array("uid"),array($allUsers[$cnt]->getUid()), "s");
            if($ordersDetails) 
            {
                for($cntA = 0;$cntA < count($ordersDetails) ;$cntA++)
                {
                    echo $userRank = $allUsers[$cnt]->getUserRank();
                    echo "<br>";
                    echo $orderUsername = $ordersDetails[$cntA]->getUsername();
                    echo "<br>";
                    echo $orderDateApproved = $ordersDetails[$cntA]->getDateUpdated();
                    echo "<br>";
                    // echo $orderMonth = date('m',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateUpdated()));
                    echo $orderYear = date('m',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateUpdated()));
                    echo "<br>";
    
                    if($orderYear == $currentYear) 
                    {
                        echo "Maintenance Whole Year Done !!";
                        echo "<br>";
                        echo "<br>";
                    }
                    else
                    {
                        echo "Inactive Status !!";
                        echo "<br>";
                        echo "<br>";
                    }
    
                }
            }
        }
        elseif($userRank == 'President') 
        {
            $ordersDetails = getOrders($conn, "WHERE uid= ? AND status = 'APPROVED'  ORDER BY date_created DESC LIMIT 1",array("uid"),array($allUsers[$cnt]->getUid()), "s");
            if($ordersDetails) 
            {
                for($cntA = 0;$cntA < count($ordersDetails) ;$cntA++)
                {
                    echo $userRank = $allUsers[$cnt]->getUserRank();
                    echo "<br>";
                    echo $orderUsername = $ordersDetails[$cntA]->getUsername();
                    echo "<br>";
                    echo $orderDateApproved = $ordersDetails[$cntA]->getDateUpdated();
                    echo "<br>";
                    echo $orderMonth = date('m',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateUpdated()));
                    echo "<br>";
    
                    if($orderMonth == $M1 || $orderMonth == $M2 || $orderMonth == $M3) 
                    {
                        echo "Quarter 1";
                        echo "<br>";
    
                        if($orderMonth == $currentMonth) 
                        {
                            echo "Maintenance Q1 Done !!";
                            echo "<br>";
                            echo "<br>";
                        }
                        else
                        {
                            echo "Inactive Status !!";
                            echo "<br>";
                            echo "<br>";
                        }
    
                    }
                    elseif($orderMonth == $M4 || $orderMonth == $M5 || $orderMonth == $M6) 
                    {
                        echo "Quarter 2";
                        echo "<br>";
    
                        if($orderMonth == $currentMonth) 
                        {
                            echo "Maintenance Q2 Done !!";
                            echo "<br>";
                            echo "<br>";
                        }
                        else
                        {
                            echo "Inactive Status !!";
                            echo "<br>";
                            echo "<br>";
                        }
    
                    }
                    elseif($orderMonth == $M7 || $orderMonth == $M8 || $orderMonth == $M9) 
                    {
                        echo "Quarter 3";
                        echo "<br>";
    
                        if($orderMonth == $currentMonth) 
                        {
                            echo "Maintenance Q3 Done !!";
                            echo "<br>";
                            echo "<br>";
                        }
                        else
                        {
                            echo "Inactive Status !!";
                            echo "<br>";
                            echo "<br>";
                        }
    
                    }
                    elseif($orderMonth == $M10 || $orderMonth == $M11 || $orderMonth == $M12) 
                    {
                        echo "Quarter 4";
                        echo "<br>";
    
                        if($orderMonth == $currentMonth) 
                        {
                            echo "Maintenance Q4 Done !!";
                            echo "<br>";
                            echo "<br>";
                        }
                        else
                        {
                            echo "Inactive Status !!";
                            echo "<br>";
                            echo "<br>";
                        }
    
                    }
                    else
                    {
                        echo "Undefined Quarter !!";
                        echo "<br>";
                    }
    
                }
            }
        }
        elseif($userRank == 'Partner') 
        {
            $ordersDetails = getOrders($conn, "WHERE uid= ? AND status = 'APPROVED' AND product_type = 'Package' ORDER BY date_created DESC LIMIT 1",array("uid"),array($allUsers[$cnt]->getUid()), "s");
            if($ordersDetails) 
            {
                for($cntA = 0;$cntA < count($ordersDetails) ;$cntA++)
                {
                    echo $userRank = $allUsers[$cnt]->getUserRank();
                    echo "<br>";
                    echo $orderUsername = $ordersDetails[$cntA]->getUsername();
                    echo "<br>";
                    echo $orderDateApproved = $ordersDetails[$cntA]->getDateUpdated();
                    echo "<br>";
                    echo $orderMonth = date('m',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateUpdated()));
                    echo "<br>";
    
                    if($orderMonth == $M1 || $orderMonth == $M2 || $orderMonth == $M3) 
                    {
                        echo "Quarter 1";
                        echo "<br>";
    
                        if($orderMonth == $currentMonth) 
                        {
                            echo "Maintenance Q1 Done !!";
                            echo "<br>";
                            echo "<br>";
                        }
                        else
                        {
                            echo "Inactive Status !!";
                            echo "<br>";
                            echo "<br>";
                        }
    
                    }
                    elseif($orderMonth == $M4 || $orderMonth == $M5 || $orderMonth == $M6) 
                    {
                        echo "Quarter 2";
                        echo "<br>";
    
                        if($orderMonth == $currentMonth) 
                        {
                            echo "Maintenance Q2 Done !!";
                            echo "<br>";
                            echo "<br>";
                        }
                        else
                        {
                            echo "Inactive Status !!";
                            echo "<br>";
                            echo "<br>";
                        }
    
                    }
                    elseif($orderMonth == $M7 || $orderMonth == $M8 || $orderMonth == $M9) 
                    {
                        echo "Quarter 3";
                        echo "<br>";
    
                        if($orderMonth == $currentMonth) 
                        {
                            echo "Maintenance Q3 Done !!";
                            echo "<br>";
                            echo "<br>";
                        }
                        else
                        {
                            echo "Inactive Status !!";
                            echo "<br>";
                            echo "<br>";
                        }
    
                    }
                    elseif($orderMonth == $M10 || $orderMonth == $M11 || $orderMonth == $M12) 
                    {
                        echo "Quarter 4";
                        echo "<br>";
    
                        if($orderMonth == $currentMonth) 
                        {
                            echo "Maintenance Q4 Done !!";
                            echo "<br>";
                            echo "<br>";
                        }
                        else
                        {
                            echo "Inactive Status !!";
                            echo "<br>";
                            echo "<br>";
                        }
    
                    }
                    else
                    {
                        echo "Undefined Quarter !!";
                        echo "<br>";
                    }
    
                }
            }
        }
        elseif($userRank == 'Director') 
        {
            $ordersDetails = getOrders($conn, "WHERE uid= ? AND status = 'APPROVED' AND product_type = 'Package' ORDER BY date_created DESC LIMIT 1",array("uid"),array($allUsers[$cnt]->getUid()), "s");
            if($ordersDetails) 
            {
                for($cntA = 0;$cntA < count($ordersDetails) ;$cntA++)
                {
                    echo $userRank = $allUsers[$cnt]->getUserRank();
                    echo "<br>";
                    echo $orderUsername = $ordersDetails[$cntA]->getUsername();
                    echo "<br>";
                    echo $orderDateApproved = $ordersDetails[$cntA]->getDateUpdated();
                    echo "<br>";
                    echo $orderMonth = date('m',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateUpdated()));
                    echo "<br>";
    
                    if($orderMonth == $M1 || $orderMonth == $M2 || $orderMonth == $M3) 
                    {
                        echo "Quarter 1";
                        echo "<br>";
    
                        if($orderMonth == $currentMonth) 
                        {
                            echo "Maintenance Q1 Done !!";
                            echo "<br>";
                            echo "<br>";
                        }
                        else
                        {
                            echo "Inactive Status !!";
                            echo "<br>";
                            echo "<br>";
                        }
    
                    }
                    elseif($orderMonth == $M4 || $orderMonth == $M5 || $orderMonth == $M6) 
                    {
                        echo "Quarter 2";
                        echo "<br>";
    
                        if($orderMonth == $currentMonth) 
                        {
                            echo "Maintenance Q2 Done !!";
                            echo "<br>";
                            echo "<br>";
                        }
                        else
                        {
                            echo "Inactive Status !!";
                            echo "<br>";
                            echo "<br>";
                        }
    
                    }
                    elseif($orderMonth == $M7 || $orderMonth == $M8 || $orderMonth == $M9) 
                    {
                        echo "Quarter 3";
                        echo "<br>";
    
                        if($orderMonth == $currentMonth) 
                        {
                            echo "Maintenance Q3 Done !!";
                            echo "<br>";
                            echo "<br>";
                        }
                        else
                        {
                            echo "Inactive Status !!";
                            echo "<br>";
                            echo "<br>";
                        }
    
                    }
                    elseif($orderMonth == $M10 || $orderMonth == $M11 || $orderMonth == $M12) 
                    {
                        echo "Quarter 4";
                        echo "<br>";
    
                        if($orderMonth == $currentMonth) 
                        {
                            echo "Maintenance Q4 Done !!";
                            echo "<br>";
                            echo "<br>";
                        }
                        else
                        {
                            echo "Inactive Status !!";
                            echo "<br>";
                            echo "<br>";
                        }
    
                    }
                    else
                    {
                        echo "Undefined Quarter !!";
                        echo "<br>";
                    }
    
                }
            }
        }


        // $ordersDetails = getOrders($conn, "WHERE uid= ? AND status = 'APPROVED' AND product_type = 'Package' ORDER BY date_created DESC LIMIT 1",array("uid"),array($allUsers[$cnt]->getUid()), "s");
        // if($ordersDetails) 
        // {
        //     for($cntA = 0;$cntA < count($ordersDetails) ;$cntA++)
        //     {
        //         echo $userRank = $allUsers[$cnt]->getUserRank();
        //         echo "<br>";
        //         echo $orderUsername = $ordersDetails[$cntA]->getUsername();
        //         echo "<br>";
        //         echo $orderDateApproved = $ordersDetails[$cntA]->getDateUpdated();
        //         echo "<br>";
        //         echo $orderMonth = date('m',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateUpdated()));
        //         echo "<br>";

        //         if($orderMonth == $M1 || $orderMonth == $M2 || $orderMonth == $M3) 
        //         {
        //             echo "Quarter 1";
        //             echo "<br>";

        //             if($orderMonth == $currentMonth) 
        //             {
        //                 echo "Maintenance Q1 Done !!";
        //                 echo "<br>";
        //                 echo "<br>";
        //             }
        //             else
        //             {
        //                 echo "Inactive Status !!";
        //                 echo "<br>";
        //                 echo "<br>";
        //             }

        //         }
        //         elseif($orderMonth == $M4 || $orderMonth == $M5 || $orderMonth == $M6) 
        //         {
        //             echo "Quarter 2";
        //             echo "<br>";

        //             if($orderMonth == $currentMonth) 
        //             {
        //                 echo "Maintenance Q2 Done !!";
        //                 echo "<br>";
        //                 echo "<br>";
        //             }
        //             else
        //             {
        //                 echo "Inactive Status !!";
        //                 echo "<br>";
        //                 echo "<br>";
        //             }

        //         }
        //         elseif($orderMonth == $M7 || $orderMonth == $M8 || $orderMonth == $M9) 
        //         {
        //             echo "Quarter 3";
        //             echo "<br>";

        //             if($orderMonth == $currentMonth) 
        //             {
        //                 echo "Maintenance Q3 Done !!";
        //                 echo "<br>";
        //                 echo "<br>";
        //             }
        //             else
        //             {
        //                 echo "Inactive Status !!";
        //                 echo "<br>";
        //                 echo "<br>";
        //             }

        //         }
        //         elseif($orderMonth == $M10 || $orderMonth == $M11 || $orderMonth == $M12) 
        //         {
        //             echo "Quarter 4";
        //             echo "<br>";

        //             if($orderMonth == $currentMonth) 
        //             {
        //                 echo "Maintenance Q4 Done !!";
        //                 echo "<br>";
        //                 echo "<br>";
        //             }
        //             else
        //             {
        //                 echo "Inactive Status !!";
        //                 echo "<br>";
        //                 echo "<br>";
        //             }

        //         }
        //         else
        //         {
        //             echo "Undefined Quarter !!";
        //             echo "<br>";
        //         }

        //     }
        // }
    }

}
?>