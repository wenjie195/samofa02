<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/languageFunction.php';


$conn = connDB();
$mantainanceMonth = 3;
$todayDate = date('YmdHi');
$dtRankNow = date('Y-m-d');
$status = 'Inactive';

$userDetails = getUser($conn,"WHERE user_rank != 'Member'");

                if($userDetails)
                {
                    for($cnt = 0;$cnt < count($userDetails) ;$cnt++)
                    {

                        $dateExpiredOrders = null;
                        $dateExpiredReferral = null;

                        $ordersDetails = getOrders($conn, "WHERE uid=? AND status = 'APPROVED' AND product_type='Package' ORDER BY date_created DESC LIMIT 1",array("uid"),array($userDetails[$cnt]->getUid()), "s");
                        $referralHistory = getReferralHistory($conn, "WHERE referrer_id=? ORDER BY date_created DESC LIMIT 1",array("referrer_id"),array($userDetails[$cnt]->getUid()), "s");
                        if ($userDetails[$cnt]->getUserRank() == 'Agent') {
                            $mantainanceMonth = 12;
                        }else {
                            $mantainanceMonth = 3;
                        }
                        if ($userDetails[$cnt]->getDtRank()) {
                            $dtRank = date('YmdHi',strtotime($userDetails[$cnt]->getDtRank(). "+".$mantainanceMonth." Month"));
                        }else {
                            $dtRank = '';
                        }
                        if ($ordersDetails) {
                            $dateCreatedOrder = $ordersDetails[0]->getDateUpdated();
                            $dateExpiredOrders = date('YmdHi',strtotime($dateCreatedOrder. "+".$mantainanceMonth." months"));
                        }
                        if ($referralHistory) {
                            $dateCreatedReferral = $referralHistory[0]->getDateCreated();
                            $dateExpiredReferral = date('YmdHi',strtotime($dateCreatedReferral. "+".$mantainanceMonth." months"));
                        }
                        if(!$referralHistory && !$ordersDetails) {
                            $dateCreated = $userDetails[$cnt]->getDateCreated();
                            $dateExpiredUser = date('YmdHi',strtotime($dateCreated. "+".$mantainanceMonth." months"));
                        }
                        if ($dateExpiredOrders > $dateExpiredReferral && $dateExpiredOrders > $dtRank) {
                            $dateExpired = $dateExpiredOrders;
                        }elseif ($dateExpiredOrders < $dateExpiredReferral && $dateExpiredReferral > $dtRank) {
                            $dateExpired = $dateExpiredReferral;
                        }else{
                            $dateExpired = $dateExpiredUser;
                        }
                        echo '<br>Today Date : '. $todayDate.' ';
                        echo 'Date Expired : '. $dateExpired.' ';
                    if ($todayDate > $dateExpired && $todayDate > $dtRank) {
                        $uid = $userDetails[$cnt]->getUid();
                        $currentRank = $userDetails[$cnt]->getUserRank();
                        $expiredDetails = getUser($conn,"WHERE uid=?",array("uid"),array($uid), "s");

                        if ($currentRank == 'Agent') {
                            $newRank = 'Member';
                        }else
                        if ($currentRank == 'President') {
                            $newRank = 'Agent';
                        }else
                        if ($currentRank == 'Partner') {
                            $newRank = 'President';
                        }else
                        if ($currentRank == 'Director') {
                            $newRank = 'Partner';
                        }

                        $userUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("status","user_rank","dt_rank"),array($status,$newRank,$dtRankNow,$uid),"ssss");
                            // $expiredUserUpdated = updateDynamicData($conn,"expired_user"," WHERE uid = ? ",array("status"),array($status,$uid),"ss");
                        $success = $userUpdated ? $userDetails[$cnt]->getUsername().' Success Update' : 'No Update';
                        echo $success;
                            
                    }
                    }
                }
				$conn->close();
                