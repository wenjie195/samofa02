<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Withdrawal.php';

require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/memberIdGenerator.php';

$conn = connDB();

$dateNow = date('Y-m-d'); // current date and year
$dateLast = date('Y-m-t',strtotime($dateNow)); // get Last Date for this month
echo $dateNow."<br>".$dateLast."<br>";
// $dateNow == $dateLast
if ($dateNow == $dateLast) { // if today date equal to last date
  /* It Will run auto witdrawal to all user*/
  $userDetails = getUser($conn, "WHERE user_type = 1");
  if ($userDetails) {
    for ($i=0; $i <count($userDetails) ; $i++) {
      $uid = $userDetails[$i]->getUid();
      $withdrawUid = md5(uniqid());
      $wallet = $userDetails[$i]->getSales();
      $bankName = $userDetails[$i]->getBankName();
      $bankAccHolder = $userDetails[$i]->getBankAccHolder();
      $bankAccNo = $userDetails[$i]->getBankAccNo();
      $empty = 0;
      $status = 'PENDING';
      if ($wallet > 0 && $bankAccHolder && $bankAccNo) {
        $autoWithdrawal = insertDynamicData($conn, 'withdrawal',array("uid","withdrawal_uid","amount","status","bank_name","bank_account_holder","bank_account_no"),array($uid,$withdrawUid,$wallet,$status,$bankName,$bankAccHolder,$bankAccNo), "sssssss");
        if ($autoWithdrawal) {
          // echo "success";
          $userWalletUpdated = updateDynamicData($conn,"user", "WHERE uid =?",array("sales"),array($empty,$uid), "ss");
        }else {
          // echo "fail";
        }
      }
    }
  }
}
// if (1) { // if today date equal to last date
//   /* It Will run auto witdrawal to all user*/
//   $userDetails = getUser($conn, "WHERE user_type = 1");
//   if ($userDetails) {
//     for ($i=0; $i <count($userDetails) ; $i++) {
//       $uid = $userDetails[$i]->getUid();
//       $withdrawUid = md5(uniqid());
//       $wallet = $userDetails[$i]->getBonus();
//       $empty = 0;
//       if ($wallet > 0) {
//         // $autoWithdrawal = insertDynamicData($conn, 'withdrawal',array("uid","withdrawal_uid","amount"),array($uid,$withdrawUid,$wallet), "sss");
//         // if ($autoWithdrawal) {
//           // echo "success";
//           $userWalletUpdated = updateDynamicData($conn,"user", "WHERE uid =?",array("sales"),array($wallet,$uid), "ss");
//         // }else {
//         //   echo "fail";
//         // }
//       }
//     }
//   }
// }

 ?>
