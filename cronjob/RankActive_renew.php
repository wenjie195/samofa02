<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';

$conn = connDB();

echo $todayDate = date('Y-m-d');
echo "<br>";
// echo $todayDate = date('m');
echo $currentMonth = date('m');
echo "<br>";
echo $currentYear = date('Y');
echo "<br>";
echo "<br>";

$M1 = '01';
$M2 = '02';
$M3 = '03';
$M4 = '04';
$M5 = '05';
$M6 = '06';
$M7 = '07';
$M8 = '08';
$M9 = '09';
$M10 = '10';
$M11 = '11';
$M12 = '12';

$renewStatus = NULL ;

$allUsers = getUser($conn);

if($allUsers) 
{
    for($cnt = 0;$cnt < count($allUsers) ;$cnt++)
    {
        $userRank = $allUsers[$cnt]->getUserRank();
        if($userRank == 'Member') 
        {
            $ordersDetails = getOrders($conn, "WHERE uid= ? AND status = 'APPROVED' AND product_type != 'SalesAids' ORDER BY date_created DESC LIMIT 1",array("uid"),array($allUsers[$cnt]->getUid()), "s");
            if($ordersDetails) 
            {
                for($cntA = 0;$cntA < count($ordersDetails) ;$cntA++)
                {
                    echo $memberUserUid = $allUsers[$cnt]->getUid();
                    echo "<br>";
                    echo $userRank = $allUsers[$cnt]->getUserRank();
                    echo "<br>";
                    echo $orderUsername = $ordersDetails[$cntA]->getUsername();
                    echo "<br>";
                    echo $orderDateApproved = $ordersDetails[$cntA]->getDateCreated();
                    echo "<br>";

                    echo $orderYear = date('Y',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateCreated()));
                    echo "<br>";    
                    echo "Agent Order Date";
                    echo "<br>";
                    echo $orderDate = date('Y-m-d',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateCreated()));
                    echo "<br>";

                    echo $userRankEditMember = $allUsers[$cnt]->getUserRankEdit();
                    echo "<br>";
                    
                    // if($userRankEditMember == 'Agent') 
                    if($userRankEditMember == 'Agent' || $userRankEditMember == 'President' || $userRankEditMember == 'Partner' || $userRankEditMember == 'Director' ) 
                    {
                        $tableName = array();
                        $tableValue =  array();
                        $stringType =  "";
                        //echo "save to database";
                
                        // $rank = "Agent";
                        $rank = $userRankEditMember;
                        $status = "Active";

                        if($rank)
                        {
                            array_push($tableName,"user_rank");
                            array_push($tableValue,$rank);
                            $stringType .=  "s";
                        }
                        if($status)
                        {
                            array_push($tableName,"status");
                            array_push($tableValue,$status);
                            $stringType .=  "s";
                        }    
                        if(!$renewStatus || $renewStatus)
                        {
                            array_push($tableName,"recipient_phone");
                            array_push($tableValue,$renewStatus);
                            $stringType .=  "s";
                        }                              
                        array_push($tableValue,$memberUserUid);
                        $stringType .=  "s";
                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                        if($passwordUpdated)
                        {
                            echo "Maintenance Done !!";
                            echo "<br>";
                            echo "Status Active !!";
                            echo "<br>";
                        }
                        else
                        {
                            echo "ERROR Member A1 : FAIL To Update Status and Ranking !!";
                            echo "<br>";
                        }
                    }
                    else
                    {
                        $tableName = array();
                        $tableValue =  array();
                        $stringType =  "";
                        //echo "save to database";
                
                        $status = "Active";

                        if($status)
                        {
                            array_push($tableName,"status");
                            array_push($tableValue,$status);
                            $stringType .=  "s";
                        } 
                        if(!$renewStatus || $renewStatus)
                        {
                            array_push($tableName,"recipient_phone");
                            array_push($tableValue,$renewStatus);
                            $stringType .=  "s";
                        }                               
                        array_push($tableValue,$memberUserUid);
                        $stringType .=  "s";
                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                        if($passwordUpdated)
                        {
                            echo "Maintenance Done !!";
                            echo "<br>";
                            echo "Status Active !!";
                            echo "<br>";
                        }
                        else
                        {
                            echo "ERROR Member A2 : FAIL To Update Status and Ranking !!";
                            echo "<br>";
                        }
                    }
                }
            }
        }
        elseif($userRank == 'Agent') 
        {
            $ordersDetails = getOrders($conn, "WHERE uid= ? AND status = 'APPROVED' AND product_type != 'SalesAids' ORDER BY date_created DESC LIMIT 1",array("uid"),array($allUsers[$cnt]->getUid()), "s");
            if($ordersDetails) 
            {
                for($cntA = 0;$cntA < count($ordersDetails) ;$cntA++)
                {
                    echo $agentUserUid = $allUsers[$cnt]->getUid();
                    echo "<br>";
                    echo $userRank = $allUsers[$cnt]->getUserRank();
                    echo "<br>";
                    echo $orderUsername = $ordersDetails[$cntA]->getUsername();
                    echo "<br>";
                    echo $orderDateApproved = $ordersDetails[$cntA]->getDateCreated();
                    echo "<br>";
                    echo $orderYear = date('Y',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateCreated()));
                    echo "<br>";    
                    echo $orderMonth = date('m',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateCreated()));
                    echo "<br>";    
                    echo "Agent Last Order Date :";
                    echo $orderDate = date('Y-m-d',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateCreated()));
                    echo "<br>";

                    $Date1 = $orderDate;
                    $date = new DateTime($Date1);
                    $duration = "365 days";
                    $additional = $duration;
                    // echo $additional = "10 days";
                    // $date->modify('+1 day');
                    $date->modify($additional);
                    echo $expiredDate = $date->format('Y-m-d');
                    echo "<br>";
                    // if($expiredDate > $todayDate) 
                    if($todayDate > $expiredDate) 
                    {
                        echo "Inactive Status !!";
                        echo "<br>";
                        echo "<br>";
                        echo "<br>";
                    }
                    else
                    {
                        echo "Maintenance Done !!";
                        echo "<br>";
                        echo "Status Active !!";
                        echo "<br>";
                        echo "<br>";
                        echo $userRankEditAgent = $allUsers[$cnt]->getUserRankEdit();
                        echo "<br>";
                        
                        // if($userRankEditAgent == 'Agent' || $userRankEditAgent == 'President' || $userRankEditAgent == 'Partner' || $userRankEditAgent == 'Director' ) 
                        if($userRankEditAgent == 'Agent') 
                        {
                            $tableName = array();
                            $tableValue =  array();
                            $stringType =  "";
                            //echo "save to database";
                    
                            $rank = $userRankEditAgent;
                            $status = "Active";

                            if($rank)
                            {
                                array_push($tableName,"user_rank");
                                array_push($tableValue,$rank);
                                $stringType .=  "s";
                            }
                            if($status)
                            {
                                array_push($tableName,"status");
                                array_push($tableValue,$status);
                                $stringType .=  "s";
                            }    
                            if(!$renewStatus || $renewStatus)
                            {
                                array_push($tableName,"recipient_phone");
                                array_push($tableValue,$renewStatus);
                                $stringType .=  "s";
                            }                              
                            array_push($tableValue,$agentUserUid);
                            $stringType .=  "s";
                            $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                            if($passwordUpdated)
                            {
                                echo "Agent Yearly Maintenance Done !!";
                                echo "<br>";
                                echo "Status Active !!";
                                echo "<br>";
                            }
                            else
                            {
                                echo "ERROR 2020 Agent A : FAIL To Update Status and Ranking !!";
                                echo "<br>";
                            }
                        }
                        elseif($userRankEditAgent == 'President' || $userRankEditAgent == 'Partner' || $userRankEditAgent == 'Director' ) 
                        {
                            if($orderMonth == $M1 || $orderMonth == $M2 || $orderMonth == $M3) 
                            {
                                if($orderYear == $currentYear) 
                                {
                                    if($currentMonth == $M1 || $currentMonth == $M2 || $currentMonth == $M3 || $currentMonth == $M4 || $currentMonth == $M5 || $currentMonth == $M6) 
                                    {
                                        $tableName = array();
                                        $tableValue =  array();
                                        $stringType =  "";
                                        //echo "save to database";
    
                                        $rank = $userRankEditAgent;
                                        $status = "Active";
    
                                        if($rank)
                                        {
                                            array_push($tableName,"user_rank");
                                            array_push($tableValue,$rank);
                                            $stringType .=  "s";
                                        }
                                        if($status)
                                        {
                                            array_push($tableName,"status");
                                            array_push($tableValue,$status);
                                            $stringType .=  "s";
                                        }    
                                        if(!$renewStatus || $renewStatus)
                                        {
                                            array_push($tableName,"recipient_phone");
                                            array_push($tableValue,$renewStatus);
                                            $stringType .=  "s";
                                        }                            
                                        array_push($tableValue,$agentUserUid);
                                        $stringType .=  "s";
                                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                        if($passwordUpdated)
                                        {
                                            echo "Maintenance Q1 Done !!";
                                            echo "<br>";
                                            echo "Status Active !!";
                                            echo "<br>";
                                        }
                                        else
                                        {
                                            echo "ERROR Agent Above 1 : FAIL To Update Status and Ranking !!";
                                            echo "<br>";
                                        }
                                    }
                                    else
                                    {
                                        echo "Maintenance Q1 FAIL !!";
                                        echo "<br>";
                                    }
                                }
                                else
                                {
                                    echo "Maintenance Q1 FAIL !!";
                                    echo "<br>";
                                }
                            }
                            elseif($orderMonth == $M4 || $orderMonth == $M5 || $orderMonth == $M6) 
                            {
                                if($orderYear == $currentYear) 
                                { 
                                    if($currentMonth == $M4 || $currentMonth == $M5 || $currentMonth == $M6 || $currentMonth == $M7 || $currentMonth == $M8 || $currentMonth == $M9) 
                                    {
                                        $tableName = array();
                                        $tableValue =  array();
                                        $stringType =  "";
                                        //echo "save to database";
    
                                        $rank = $userRankEditAgent;
                                        $status = "Active";
    
                                        if($rank)
                                        {
                                            array_push($tableName,"user_rank");
                                            array_push($tableValue,$rank);
                                            $stringType .=  "s";
                                        }
                                        if($status)
                                        {
                                            array_push($tableName,"status");
                                            array_push($tableValue,$status);
                                            $stringType .=  "s";
                                        }    
                                        if(!$renewStatus || $renewStatus)
                                        {
                                            array_push($tableName,"recipient_phone");
                                            array_push($tableValue,$renewStatus);
                                            $stringType .=  "s";
                                        }                            
                                        array_push($tableValue,$agentUserUid);
                                        $stringType .=  "s";
                                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                        if($passwordUpdated)
                                        {
                                            echo "Maintenance Q2 Done !!";
                                            echo "<br>";
                                            echo "Status Active !!";
                                            echo "<br>";
                                        }
                                        else
                                        {
                                            echo "ERROR Agent Above 1 : FAIL To Update Status and Ranking !!";
                                            echo "<br>";
                                        }
                                    }
                                    else
                                    {
                                        echo "Maintenance Q2 FAIL !!";
                                        echo "<br>";
                                    }
                                }
                                else
                                {
                                    echo "Maintenance Q2 FAIL !!";
                                    echo "<br>";
                                }
                            }
                            elseif($orderMonth == $M7 || $orderMonth == $M8 || $orderMonth == $M9) 
                            {
                                if($orderYear == $currentYear) 
                                {
                                    if($currentMonth == $M7 || $currentMonth == $M8 || $currentMonth == $M9 || $currentMonth == $M10 || $currentMonth == $M11 || $currentMonth == $M12)
                                    {
                                        $tableName = array();
                                        $tableValue =  array();
                                        $stringType =  "";
                                        //echo "save to database";
    
                                        $rank = $userRankEditAgent;
                                        $status = "Active";
    
                                        if($rank)
                                        {
                                            array_push($tableName,"user_rank");
                                            array_push($tableValue,$rank);
                                            $stringType .=  "s";
                                        }
                                        if($status)
                                        {
                                            array_push($tableName,"status");
                                            array_push($tableValue,$status);
                                            $stringType .=  "s";
                                        }    
                                        if(!$renewStatus || $renewStatus)
                                        {
                                            array_push($tableName,"recipient_phone");
                                            array_push($tableValue,$renewStatus);
                                            $stringType .=  "s";
                                        }                            
                                        array_push($tableValue,$agentUserUid);
                                        $stringType .=  "s";
                                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                        if($passwordUpdated)
                                        {
                                            echo "Maintenance Q3 Done !!";
                                            echo "<br>";
                                            echo "Status Active !!";
                                            echo "<br>";
                                        }
                                        else
                                        {
                                            echo "ERROR Agent Above 1 : FAIL To Update Status and Ranking !!";
                                            echo "<br>";
                                        }
                                    }
                                    else
                                    {
                                        echo "Maintenance Q3 FAIL !!";
                                        echo "<br>";
                                    }
                                }
                                else
                                {
                                    echo "Maintenance Q3 FAIL !!";
                                    echo "<br>";
                                }
                            }
                            elseif($orderMonth == $M10 || $orderMonth == $M11 || $orderMonth == $M12) 
                            {
                                $nextYear = $orderYear + 1;

                                if($orderYear == $currentYear) 
                                {
                                    if($currentMonth == $M10 || $currentMonth == $M11 || $currentMonth == $M12)
                                    {
                                        $tableName = array();
                                        $tableValue =  array();
                                        $stringType =  "";
                                        //echo "save to database";
    
                                        $rank = $userRankEditAgent;
                                        $status = "Active";
    
                                        if($rank)
                                        {
                                            array_push($tableName,"user_rank");
                                            array_push($tableValue,$rank);
                                            $stringType .=  "s";
                                        }
                                        if($status)
                                        {
                                            array_push($tableName,"status");
                                            array_push($tableValue,$status);
                                            $stringType .=  "s";
                                        }    
                                        if(!$renewStatus || $renewStatus)
                                        {
                                            array_push($tableName,"recipient_phone");
                                            array_push($tableValue,$renewStatus);
                                            $stringType .=  "s";
                                        }                            
                                        array_push($tableValue,$agentUserUid);
                                        $stringType .=  "s";
                                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                        if($passwordUpdated)
                                        {
                                            echo "Maintenance Q4 Done !!";
                                            echo "<br>";
                                            echo "Status Active !!";
                                            echo "<br>";
                                        }
                                        else
                                        {
                                            echo "ERROR Agent Above 1 : FAIL To Update Status and Ranking !!";
                                            echo "<br>";
                                        }
                                    }
                                    else
                                    {
                                        echo "Maintenance Q4 FAIL !!";
                                        echo "<br>";
                                    }
                                }
                                elseif($currentYear == $nextYear) 
                                {
                                    if($currentMonth == $M1 || $currentMonth == $M2 || $currentMonth == $M3 ) 
                                    {
                                        $tableName = array();
                                        $tableValue =  array();
                                        $stringType =  "";
                                        //echo "save to database";
    
                                        $rank = $userRankEditAgent;
                                        $status = "Active";
    
                                        if($rank)
                                        {
                                            array_push($tableName,"user_rank");
                                            array_push($tableValue,$rank);
                                            $stringType .=  "s";
                                        }
                                        if($status)
                                        {
                                            array_push($tableName,"status");
                                            array_push($tableValue,$status);
                                            $stringType .=  "s";
                                        }    
                                        if(!$renewStatus || $renewStatus)
                                        {
                                            array_push($tableName,"recipient_phone");
                                            array_push($tableValue,$renewStatus);
                                            $stringType .=  "s";
                                        }                            
                                        array_push($tableValue,$agentUserUid);
                                        $stringType .=  "s";
                                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                        if($passwordUpdated)
                                        {
                                            echo "Maintenance Q4 Done !!";
                                            echo "<br>";
                                            echo "Status Active !!";
                                            echo "<br>";
                                        }
                                        else
                                        {
                                            echo "ERROR Agent Above 1 : FAIL To Update Status and Ranking !!";
                                            echo "<br>";
                                        }
                                    }
                                    else
                                    {   
                                        echo "Maintenance Q4 FAIL !!";
                                        echo "<br>";
                                    }
                                }
                                else
                                {
                                    echo "Unknown Timeline (Agent Level)";
                                    echo "<br>";
                                }
                            }
                        }
                        else
                        {
                            echo "Unknown Ranking (Agent Level)";
                            echo "<br>";
                        }
                    }
                }
            }
        }
        elseif($userRank == 'President') 
        {
            $ordersDetails = getOrders($conn, "WHERE uid= ? AND status = 'APPROVED' AND product_type != 'SalesAids' ORDER BY date_created DESC LIMIT 1",array("uid"),array($allUsers[$cnt]->getUid()), "s");
            if($ordersDetails) 
            {
                for($cntA = 0;$cntA < count($ordersDetails) ;$cntA++)
                {
                    echo $presidentUserUid = $allUsers[$cnt]->getUid();
                    echo "<br>";
                    echo $userRank = $allUsers[$cnt]->getUserRank();
                    echo "<br>";
                    echo $orderUsername = $ordersDetails[$cntA]->getUsername();
                    echo "<br>";
                    echo $orderDateApproved = $ordersDetails[$cntA]->getDateCreated();
                    echo "<br>";
                    echo $orderMonth = date('m',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateCreated()));
                    echo "<br>";
                    echo $orderYear = date('Y',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateCreated()));
                    echo "<br>";    
                    echo $userRankEditPresident = $allUsers[$cnt]->getUserRankEdit();
                    echo "<br>";

                    if($orderMonth == $M1 || $orderMonth == $M2 || $orderMonth == $M3) 
                    {
                        if($orderYear == $currentYear) 
                        {
                            if($currentMonth == $M1 || $currentMonth == $M2 || $currentMonth == $M3 || $currentMonth == $M4 || $currentMonth == $M5 || $currentMonth == $M6) 
                            {
                                // if($userRankEditPresident == 'Agent' || $userRankEditPresident == 'President' || $userRankEditPresident == 'Partner' || $userRankEditPresident == 'Director' ) 
                                if($userRankEditPresident == 'President' || $userRankEditPresident == 'Partner' || $userRankEditPresident == 'Director' ) 
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    $rank = $userRankEditPresident;
                                    $status = "Active";
                                    if($rank)
                                    {
                                        array_push($tableName,"user_rank");
                                        array_push($tableValue,$rank);
                                        $stringType .=  "s";
                                    }
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }    
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                            
                                    array_push($tableValue,$presidentUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Q1 Done !!";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR President Above: FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                                else
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    $status = "Active";
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }             
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                   
                                    array_push($tableValue,$presidentUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Q1 Done !!";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR President Below : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                            }
                            else
                            {
                                echo "Maintenance Q1 FAIL !!";
                                echo "<br>";
                            }
                        }
                        else
                        {
                            echo "Maintenance Q1 FAIL !!";
                            echo "<br>";
                        }
                    }
                    elseif($orderMonth == $M4 || $orderMonth == $M5 || $orderMonth == $M6) 
                    {
                        if($orderYear == $currentYear) 
                        { 
                            if($currentMonth == $M4 || $currentMonth == $M5 || $currentMonth == $M6 || $currentMonth == $M7 || $currentMonth == $M8 || $currentMonth == $M9) 
                            {
                                // if($userRankEditPresident == 'Agent' || $userRankEditPresident == 'President' || $userRankEditPresident == 'Partner' || $userRankEditPresident == 'Director' ) 
                                if($userRankEditPresident == 'President' || $userRankEditPresident == 'Partner' || $userRankEditPresident == 'Director' ) 
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    $rank = $userRankEditPresident;
                                    $status = "Active";
                                    if($rank)
                                    {
                                        array_push($tableName,"user_rank");
                                        array_push($tableValue,$rank);
                                        $stringType .=  "s";
                                    }
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }    
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                            
                                    array_push($tableValue,$presidentUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Q2 Done !!";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR President Above: FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                                else
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    $status = "Active";
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }             
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                   
                                    array_push($tableValue,$presidentUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Q2 Done !!";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR President Below : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                            }
                            else
                            {
                                echo "Maintenance Q2 FAIL !!";
                                echo "<br>";
                            }
                        }
                        else
                        {
                            echo "Maintenance Q2 FAIL !!";
                            echo "<br>";
                        }
                    }
                    elseif($orderMonth == $M7 || $orderMonth == $M8 || $orderMonth == $M9) 
                    {
                        if($orderYear == $currentYear) 
                        {
                            if($currentMonth == $M7 || $currentMonth == $M8 || $currentMonth == $M9 || $currentMonth == $M10 || $currentMonth == $M11 || $currentMonth == $M12)
                            {
                                // if($userRankEditPresident == 'Agent' || $userRankEditPresident == 'President' || $userRankEditPresident == 'Partner' || $userRankEditPresident == 'Director' ) 
                                if($userRankEditPresident == 'President' || $userRankEditPresident == 'Partner' || $userRankEditPresident == 'Director' ) 
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    $rank = $userRankEditPresident;
                                    $status = "Active";
                                    if($rank)
                                    {
                                        array_push($tableName,"user_rank");
                                        array_push($tableValue,$rank);
                                        $stringType .=  "s";
                                    }
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }    
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                            
                                    array_push($tableValue,$presidentUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Q3 Done !!";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR President Above: FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                                else
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    $status = "Active";
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }             
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                   
                                    array_push($tableValue,$presidentUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Q3 Done !!";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR President Below : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                            }
                            else
                            {
                                echo "Maintenance Q3 FAIL !!";
                                echo "<br>";
                            }
                        }
                        else
                        {
                            echo "Maintenance Q3 FAIL !!";
                            echo "<br>";
                        }
                    }
                    elseif($orderMonth == $M10 || $orderMonth == $M11 || $orderMonth == $M12) 
                    {
                        $nextYear = $orderYear + 1;

                        if($orderYear == $currentYear) 
                        {
                            if($currentMonth == $M10 || $currentMonth == $M11 || $currentMonth == $M12)
                            {
                                // if($userRankEditPresident == 'Agent' || $userRankEditPresident == 'President' || $userRankEditPresident == 'Partner' || $userRankEditPresident == 'Director' ) 
                                if($userRankEditPresident == 'President' || $userRankEditPresident == 'Partner' || $userRankEditPresident == 'Director' ) 
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    $rank = $userRankEditPresident;
                                    $status = "Active";
                                    if($rank)
                                    {
                                        array_push($tableName,"user_rank");
                                        array_push($tableValue,$rank);
                                        $stringType .=  "s";
                                    }
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }    
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                            
                                    array_push($tableValue,$presidentUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Q4 Done !!";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR President Above: FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                                else
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    $status = "Active";
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }             
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                   
                                    array_push($tableValue,$presidentUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Q4 Done !!";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR President Below : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                            }
                            else
                            {
                                echo "Maintenance Q4 FAIL !!";
                                echo "<br>";
                            }
                        }
                        elseif($currentYear == $nextYear) 
                        {
                            if($currentMonth == $M1 || $currentMonth == $M2 || $currentMonth == $M3 ) 
                            {
                                // if($userRankEditPresident == 'Agent' || $userRankEditPresident == 'President' || $userRankEditPresident == 'Partner' || $userRankEditPresident == 'Director' ) 
                                if($userRankEditPresident == 'President' || $userRankEditPresident == 'Partner' || $userRankEditPresident == 'Director' ) 
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    $rank = $userRankEditPresident;
                                    $status = "Active";
                                    if($rank)
                                    {
                                        array_push($tableName,"user_rank");
                                        array_push($tableValue,$rank);
                                        $stringType .=  "s";
                                    }
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }    
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                            
                                    array_push($tableValue,$presidentUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Q4 Done !!";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR President Above: FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                                else
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    $status = "Active";
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }             
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                   
                                    array_push($tableValue,$presidentUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Q4 Done !!";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR President Below : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                            }
                            else
                            {   
                                echo "Maintenance Q4 FAIL !!";
                                echo "<br>";
                            }
                        }
                        else
                        {
                            echo "Maintenance Q4 FAIL !!";
                            echo "<br>";
                        }
                    }
                }
            }
        }
        elseif($userRank == 'Partner') 
        {
            $ordersDetails = getOrders($conn, "WHERE uid= ? AND status = 'APPROVED' AND product_type = 'Package' ORDER BY date_created DESC LIMIT 1",array("uid"),array($allUsers[$cnt]->getUid()), "s");
            if($ordersDetails) 
            {
                for($cntA = 0;$cntA < count($ordersDetails) ;$cntA++)
                {
                    echo $partnerUserUid = $allUsers[$cnt]->getUid();
                    echo "<br>";
                    echo $userRank = $allUsers[$cnt]->getUserRank();
                    echo "<br>";
                    echo $orderUsername = $ordersDetails[$cntA]->getUsername();
                    echo "<br>";
                    echo $orderDateApproved = $ordersDetails[$cntA]->getDateCreated();
                    echo "<br>";
                    echo $orderMonth = date('m',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateCreated()));
                    echo "<br>";
                    echo $orderYear = date('Y',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateCreated()));
                    echo "<br>"; 
                    echo $userRankEditPartner = $allUsers[$cnt]->getUserRankEdit();
                    echo "<br>";

                    if($orderMonth == $M1 || $orderMonth == $M2 || $orderMonth == $M3) 
                    {
                        if($orderYear == $currentYear) 
                        {
                            if($currentMonth == $M1 || $currentMonth == $M2 || $currentMonth == $M3 || $currentMonth == $M4 || $currentMonth == $M5 || $currentMonth == $M6) 
                            {
                                // if($userRankEditPartner == 'Partner') 
                                // if($userRankEditPartner == 'Agent' || $userRankEditPartner == 'President' || $userRankEditPartner == 'Partner' || $userRankEditPartner == 'Director' ) 
                                if($userRankEditPartner == 'Partner' || $userRankEditPartner == 'Director' ) 
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    // $rank = "Partner";
                                    $rank = $userRankEditPartner;
                                    $status = "Active";

                                    if($rank)
                                    {
                                        array_push($tableName,"user_rank");
                                        array_push($tableValue,$rank);
                                        $stringType .=  "s";
                                    }
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }         
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                       
                                    array_push($tableValue,$partnerUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Q1 Done !!";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR 2020 PARTNER A : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                                else
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    $status = "Active";
                                    
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }       
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                         
                                    array_push($tableValue,$partnerUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Done (Partner)";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR 2020 PARTNER B : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                            }
                            else
                            {
                                echo "Maintenance Q1 FAIL !!";
                                echo "<br>";
                            }
                        }
                        else
                        {
                            echo "Maintenance Q1 FAIL !!";
                            echo "<br>";
                        }
                    }
                    elseif($orderMonth == $M4 || $orderMonth == $M5 || $orderMonth == $M6) 
                    {
                        if($orderYear == $currentYear) 
                        { 
                            if($currentMonth == $M4 || $currentMonth == $M5 || $currentMonth == $M6 || $currentMonth == $M7 || $currentMonth == $M8 || $currentMonth == $M9) 
                            {
                                // if($userRankEditPartner == 'Partner') 
                                // if($userRankEditPartner == 'Agent' || $userRankEditPartner == 'President' || $userRankEditPartner == 'Partner' || $userRankEditPartner == 'Director' ) 
                                if($userRankEditPartner == 'Partner' || $userRankEditPartner == 'Director' ) 
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    // $rank = "Partner";
                                    $rank = $userRankEditPartner;
                                    $status = "Active";

                                    if($rank)
                                    {
                                        array_push($tableName,"user_rank");
                                        array_push($tableValue,$rank);
                                        $stringType .=  "s";
                                    }
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }         
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                       
                                    array_push($tableValue,$partnerUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Q1 Done !!";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR 2020 PARTNER A : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                                else
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    $status = "Active";
                                    
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }       
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                         
                                    array_push($tableValue,$partnerUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Done (Partner)";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR 2020 PARTNER B : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                            }
                            else
                            {
                                echo "Maintenance Q2 FAIL !!";
                                echo "<br>";
                            }
                        }
                        else
                        {
                            echo "Maintenance Q2 FAIL !!";
                            echo "<br>";
                        }
                    }
                    elseif($orderMonth == $M7 || $orderMonth == $M8 || $orderMonth == $M9) 
                    {
                        if($orderYear == $currentYear) 
                        {
                            if($currentMonth == $M7 || $currentMonth == $M8 || $currentMonth == $M9 || $currentMonth == $M10 || $currentMonth == $M11 || $currentMonth == $M12)
                            {
                                // if($userRankEditPartner == 'Partner') 
                                // if($userRankEditPartner == 'Agent' || $userRankEditPartner == 'President' || $userRankEditPartner == 'Partner' || $userRankEditPartner == 'Director' ) 
                                if($userRankEditPartner == 'Partner' || $userRankEditPartner == 'Director' ) 
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    // $rank = "Partner";
                                    $rank = $userRankEditPartner;
                                    $status = "Active";

                                    if($rank)
                                    {
                                        array_push($tableName,"user_rank");
                                        array_push($tableValue,$rank);
                                        $stringType .=  "s";
                                    }
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }         
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                       
                                    array_push($tableValue,$partnerUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Q1 Done !!";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR 2020 PARTNER A : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                                else
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    $status = "Active";
                                    
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }       
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                         
                                    array_push($tableValue,$partnerUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Done (Partner)";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR 2020 PARTNER B : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                            }
                            else
                            {
                                echo "Maintenance Q3 FAIL !!";
                                echo "<br>";
                            }
                        }
                        else
                        {
                            echo "Maintenance Q3 FAIL !!";
                            echo "<br>";
                        }
                    }
                    elseif($orderMonth == $M10 || $orderMonth == $M11 || $orderMonth == $M12) 
                    {
                        $nextYear = $orderYear + 1;

                        if($orderYear == $currentYear) 
                        {
                            if($currentMonth == $M10 || $currentMonth == $M11 || $currentMonth == $M12)
                            {
                                // if($userRankEditPartner == 'Partner') 
                                // if($userRankEditPartner == 'Agent' || $userRankEditPartner == 'President' || $userRankEditPartner == 'Partner' || $userRankEditPartner == 'Director' ) 
                                if($userRankEditPartner == 'Partner' || $userRankEditPartner == 'Director' ) 
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    // $rank = "Partner";
                                    $rank = $userRankEditPartner;
                                    $status = "Active";

                                    if($rank)
                                    {
                                        array_push($tableName,"user_rank");
                                        array_push($tableValue,$rank);
                                        $stringType .=  "s";
                                    }
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }         
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                       
                                    array_push($tableValue,$partnerUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Q1 Done !!";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR 2020 PARTNER A : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                                else
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    $status = "Active";
                                    
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }       
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                         
                                    array_push($tableValue,$partnerUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Done (Partner)";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR 2020 PARTNER B : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                            }
                            else
                            {
                                echo "Maintenance Q4 FAIL !!";
                                echo "<br>";
                            }
                        }
                        elseif($currentYear == $nextYear) 
                        {
                            if($currentMonth == $M1 || $currentMonth == $M2 || $currentMonth == $M3 ) 
                            {
                                // if($userRankEditPartner == 'Partner') 
                                // if($userRankEditPartner == 'Agent' || $userRankEditPartner == 'President' || $userRankEditPartner == 'Partner' || $userRankEditPartner == 'Director' ) 
                                if($userRankEditPartner == 'Partner' || $userRankEditPartner == 'Director' ) 
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    // $rank = "Partner";
                                    $rank = $userRankEditPartner;
                                    $status = "Active";

                                    if($rank)
                                    {
                                        array_push($tableName,"user_rank");
                                        array_push($tableValue,$rank);
                                        $stringType .=  "s";
                                    }
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }         
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                       
                                    array_push($tableValue,$partnerUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Q1 Done !!";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR 2020 PARTNER A : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                                else
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    $status = "Active";
                                    
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }       
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                         
                                    array_push($tableValue,$partnerUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Done (Partner)";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR 2020 PARTNER B : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                            }
                            else
                            {   
                                echo "Maintenance Q4 FAIL !!";
                                echo "<br>";
                            }
                        }
                        else
                        {
                            echo "Unknown Timeline (Partner Level)";
                            echo "<br>";
                        }
                    }
                }
            }  
        }
        elseif($userRank == 'Director') 
        {
            $ordersDetails = getOrders($conn, "WHERE uid= ? AND status = 'APPROVED' AND product_type = 'Package' ORDER BY date_created DESC LIMIT 1",array("uid"),array($allUsers[$cnt]->getUid()), "s");
            if($ordersDetails) 
            {
                for($cntA = 0;$cntA < count($ordersDetails) ;$cntA++)
                {
                    // echo $presidentUserUid = $allUsers[$cnt]->getUid();
                    echo $directorUserUid = $allUsers[$cnt]->getUid();
                    echo "<br>";
                    echo $userRank = $allUsers[$cnt]->getUserRank();
                    echo "<br>";
                    echo $orderUsername = $ordersDetails[$cntA]->getUsername();
                    echo "<br>";
                    echo $orderDateApproved = $ordersDetails[$cntA]->getDateCreated();
                    echo "<br>";
                    echo $orderMonth = date('m',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateCreated()));
                    echo "<br>";
                    echo $orderYear = date('Y',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateCreated()));
                    echo "<br>";  
                    echo $userRankEditDirector = $allUsers[$cnt]->getUserRankEdit();
                    echo "<br>";

                    if($orderMonth == $M1 || $orderMonth == $M2 || $orderMonth == $M3) 
                    {
                        if($orderYear == $currentYear) 
                        {
                            if($currentMonth == $M1 || $currentMonth == $M2 || $currentMonth == $M3 || $currentMonth == $M4 || $currentMonth == $M5 || $currentMonth == $M6) 
                            {
                                // if($userRankEditDirector == 'Agent' || $userRankEditDirector == 'President' || $userRankEditDirector == 'Partner' || $userRankEditDirector == 'Director' ) 
                                if($userRankEditDirector == 'Director' ) 
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    $rank = $userRankEditDirector;
                                    $status = "Active";

                                    if($rank)
                                    {
                                        array_push($tableName,"user_rank");
                                        array_push($tableValue,$rank);
                                        $stringType .=  "s";
                                    }
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }                   
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }             
                                    // array_push($tableValue,$presidentUserUid);
                                    array_push($tableValue,$directorUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Q1 Done !!";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR 2021 DIRECTOR : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                                else
                                {
                                    // echo "Ranking Issue (Director Level)";
                                    // echo "<br>";

                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";

                                    $status = "Active";
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }         
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                       
                                    array_push($tableValue,$presidentUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Done (Director)";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR 2020 DIRECTOR : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                            }
                            else
                            {
                                echo "Maintenance Q1 FAIL !!";
                                echo "<br>";
                            }
                        }
                        else
                        {
                            echo "Maintenance Q1 FAIL !!";
                            echo "<br>";
                        }
                    }
                    elseif($orderMonth == $M4 || $orderMonth == $M5 || $orderMonth == $M6) 
                    {
                        if($orderYear == $currentYear) 
                        { 
                            if($currentMonth == $M4 || $currentMonth == $M5 || $currentMonth == $M6 || $currentMonth == $M7 || $currentMonth == $M8 || $currentMonth == $M9) 
                            {
                                // if($userRankEditDirector == 'Agent' || $userRankEditDirector == 'President' || $userRankEditDirector == 'Partner' || $userRankEditDirector == 'Director' ) 
                                if($userRankEditDirector == 'Director' ) 
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    $rank = $userRankEditDirector;
                                    $status = "Active";

                                    if($rank)
                                    {
                                        array_push($tableName,"user_rank");
                                        array_push($tableValue,$rank);
                                        $stringType .=  "s";
                                    }
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }                   
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }             
                                    // array_push($tableValue,$presidentUserUid);
                                    array_push($tableValue,$directorUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Q2 Done !!";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR 2021 DIRECTOR : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                                else
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";

                                    $status = "Active";
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }         
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                       
                                    array_push($tableValue,$presidentUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Done (Director)";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR 2020 DIRECTOR : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                            }
                            else
                            {
                                echo "Maintenance Q2 FAIL !!";
                                echo "<br>";
                            }
                        }
                        else
                        {
                            echo "Maintenance Q2 FAIL !!";
                            echo "<br>";
                        }
                    }
                    elseif($orderMonth == $M7 || $orderMonth == $M8 || $orderMonth == $M9) 
                    {
                        if($orderYear == $currentYear) 
                        {
                            if($currentMonth == $M7 || $currentMonth == $M8 || $currentMonth == $M9 || $currentMonth == $M10 || $currentMonth == $M11 || $currentMonth == $M12)
                            {
                                // if($userRankEditDirector == 'Agent' || $userRankEditDirector == 'President' || $userRankEditDirector == 'Partner' || $userRankEditDirector == 'Director' ) 
                                if($userRankEditDirector == 'Director' ) 
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    $rank = $userRankEditDirector;
                                    $status = "Active";

                                    if($rank)
                                    {
                                        array_push($tableName,"user_rank");
                                        array_push($tableValue,$rank);
                                        $stringType .=  "s";
                                    }
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }                   
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }             
                                    // array_push($tableValue,$presidentUserUid);
                                    array_push($tableValue,$directorUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Q3 Done !!";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR 2021 DIRECTOR : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                                else
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";

                                    $status = "Active";
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }         
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                       
                                    array_push($tableValue,$presidentUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Done (Director)";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR 2020 DIRECTOR : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                            }
                            else
                            {
                                echo "Maintenance Q3 FAIL !!";
                                echo "<br>";
                            }
                        }
                        else
                        {
                            echo "Maintenance Q3 FAIL !!";
                            echo "<br>";
                        }
                    }
                    elseif($orderMonth == $M10 || $orderMonth == $M11 || $orderMonth == $M12) 
                    {
                        $nextYear = $orderYear + 1;

                        if($orderYear == $currentYear) 
                        {
                            if($currentMonth == $M10 || $currentMonth == $M11 || $currentMonth == $M12)
                            {
                                // if($userRankEditDirector == 'Agent' || $userRankEditDirector == 'President' || $userRankEditDirector == 'Partner' || $userRankEditDirector == 'Director' ) 
                                if($userRankEditDirector == 'Director' ) 
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    $rank = $userRankEditDirector;
                                    $status = "Active";

                                    if($rank)
                                    {
                                        array_push($tableName,"user_rank");
                                        array_push($tableValue,$rank);
                                        $stringType .=  "s";
                                    }
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }                   
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }             
                                    // array_push($tableValue,$presidentUserUid);
                                    array_push($tableValue,$directorUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Q4 Done !!";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR 2021 DIRECTOR : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                                else
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";

                                    $status = "Active";
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }         
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                       
                                    array_push($tableValue,$presidentUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Done (Director)";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR 2020 DIRECTOR : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                            }
                            else
                            {
                                echo "Maintenance Q4 FAIL !!";
                                echo "<br>";
                            }
                        }
                        elseif($currentYear == $nextYear) 
                        {
                            if($currentMonth == $M1 || $currentMonth == $M2 || $currentMonth == $M3 ) 
                            {
                                // if($userRankEditDirector == 'Agent' || $userRankEditDirector == 'President' || $userRankEditDirector == 'Partner' || $userRankEditDirector == 'Director' ) 
                                if($userRankEditDirector == 'Director' ) 
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";
                            
                                    $rank = $userRankEditDirector;
                                    $status = "Active";

                                    if($rank)
                                    {
                                        array_push($tableName,"user_rank");
                                        array_push($tableValue,$rank);
                                        $stringType .=  "s";
                                    }
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }                   
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }             
                                    // array_push($tableValue,$presidentUserUid);
                                    array_push($tableValue,$directorUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Q4 Done !!";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR 2021 DIRECTOR : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                                else
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";
                                    //echo "save to database";

                                    $status = "Active";
                                    if($status)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$status);
                                        $stringType .=  "s";
                                    }         
                                    if(!$renewStatus || $renewStatus)
                                    {
                                        array_push($tableName,"recipient_phone");
                                        array_push($tableValue,$renewStatus);
                                        $stringType .=  "s";
                                    }                       
                                    array_push($tableValue,$presidentUserUid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {
                                        echo "Maintenance Done (Director)";
                                        echo "<br>";
                                        echo "Status Active !!";
                                        echo "<br>";
                                    }
                                    else
                                    {
                                        echo "ERROR 2020 DIRECTOR : FAIL To Update Status and Ranking !!";
                                        echo "<br>";
                                    }
                                }
                            }
                            else
                            {   
                                echo "Maintenance Q4 FAIL !!";
                                echo "<br>";
                            }
                        }
                        else
                        {
                            echo "Unknown Timeline (Director Level)";
                            echo "<br>";
                        }
                    }
                }
            }
        }
    }
}
?>