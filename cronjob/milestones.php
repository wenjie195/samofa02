<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ProductOrders.php';
require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/MilesStones.php';

require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/languageFunction.php';

$conn = connDB();

$userDetails = getUser($conn, " WHERE user_rank_edit = '' ");
// $userDetails = getUser($conn);

for ($l=0; $l <count($userDetails) ; $l++) 
// for ($cnt=0; $cnt < count($allUser) ; $cnt++)
{
  // echo $userDetails[$l]->getUsername().' '.$userDetails[$l]->getUserRank().'<br>';
  // echo $uid = $userDetails[$cnt]->getUid();

  //initital
  $addMember = 0;
  $groupSalesPackage = 0;

  // ------------------------------ new milestone ------------------------------
  $getAllDirectDownline = getReferralHistory($conn, "WHERE referrer_id =?",array("referrer_id"), array($userDetails[$l]->getUid()), "s");
  // $sponsorUid = $getAllDirectDownline[0]->getReferrerId();
  // $allDownlineUid = $getAllDirectDownline[0]->getReferralId();
  if ($getAllDirectDownline)
  // if ($allDownlineUid)
  {
    echo $userDetails[$l]->getUid();
    echo "<br>"; 
    $allUser = getUser($conn, "WHERE uid = ? ", array("uid"), array($userDetails[$l]->getUid()), "s");
    echo $username = $allUser[0]->getUsername();
    echo "<br>"; 
    echo $totalDownline = count($getAllDirectDownline);
    echo "<br>"; 
    echo "end of count total downline"; 
    echo "<br>"; 

    for ($cnt=0; $cnt < count($getAllDirectDownline) ; $cnt++)
    // for ($cnt=0; $cnt < count($allDownlineUid) ; $cnt++)
    {

      // $downlineUserOrders = getOrders($conn, "WHERE uid =? AND status = 'APPROVED' AND product_type = 'package' ",array("uid"),array($userDetails[$l]->getUid()), "s");
      $downlineUserOrders = getOrders($conn, "WHERE uid =? AND status = 'APPROVED' AND product_type = 'package' ",array("uid"),array($getAllDirectDownline[$cnt]->getReferralId()), "s");
      if ($downlineUserOrders)
      {
        for ($j=0; $j <count($downlineUserOrders) ; $j++)
        {
          $downlineUserProductOrders = getProductOrders($conn, "WHERE order_id =?",array("order_id"),array($downlineUserOrders[$j]->getId()), "s");
          if ($downlineUserProductOrders)
          {
            for ($k=0; $k <count($downlineUserProductOrders) ; $k++)
            {
              echo "start"; 
              echo "<br>"; 
              echo $groupSalesPackage += $downlineUserProductOrders[$k]->getQuantity();
              echo "<br>"; 
              echo "end"; 
              echo "<br>"; 
            }
          }
        }
      }

      $selfOrder = getOrders($conn, "WHERE uid =? AND status = 'APPROVED' AND product_type = 'package' ",array("uid"),array($userDetails[$l]->getUid()), "s");
      if ($selfOrder)
      {
        for ($aa=0; $aa <count($selfOrder) ; $aa++)
        {
          $selfProductOrders = getProductOrders($conn, "WHERE order_id =?",array("order_id"),array($selfOrder[$aa]->getId()), "s");
          if ($selfProductOrders)
          {
            echo $totalSelfOrder = count($selfProductOrders);
            echo "<br>"; 
          }
        }
      }

    }

  }


  if ($totalDownline >= 3 && $groupSalesPackage >= 6 && $totalSelfOrder >= 1)
  // if ($totalDownline >= 3 && $groupSalesPackage >= 6 )
  {
    // $selfOrder = getOrders($conn, "WHERE uid =? AND status = 'APPROVED' AND product_type = 'package'",array("uid"),array($userDetails[$l]->getUid()), "s");
    // if($selfOrder)
    // {
    //   // $bonusDetails = getMilesStones($conn, "WHERE uid =? AND bonus =?",array('uid','bonus'),array($userDetails[$l]->getUid(),1), "ss");
    //   $bonusDetails = getMilesStones($conn, "WHERE uid =? AND bonus =?",array('uid','bonus'),array($userDetails[$l]->getUid(),'A1'), "ss");
    //   if (!$bonusDetails)
    //   {
    //     insertDynamicData($conn, "milestones",array('uid','bonus'),array($userDetails[$l]->getUid(),1), "ss");
    //   }
    // }
    // else
    // {}
    $bonusDetails = getMilesStones($conn, "WHERE uid =? AND bonus =?",array('uid','bonus'),array($userDetails[$l]->getUid(),1), "ss");
    if (!$bonusDetails)
    {
      insertDynamicData($conn, "milestones",array('uid','bonus'),array($userDetails[$l]->getUid(),1), "ss");
    }
  }

  if (($userDetails[$l]->getUserRank() == 'Agent') && $groupSalesPackage >= 20)
  {
    $bonusDetails = getMilesStones($conn, "WHERE uid =? AND bonus =?",array('uid','bonus'),array($userDetails[$l]->getUid(),2), "ss");
    if (!$bonusDetails && $groupSalesPackage >= 20)
    {
      insertDynamicData($conn, "milestones",array('uid','bonus'),array($userDetails[$l]->getUid(),2), "ss");
    }
  }


  // if($userDetails[$l]->getUserRank() == 'Agent')
  // {
  //   if($groupSalesPackage >= 20)
  //   {
  //     $bonusDetails = getMilesStones($conn, "WHERE uid =? AND bonus =?",array('uid','bonus'),array($userDetails[$l]->getUid(),2), "ss");
  //     if (!$bonusDetails && $groupSalesPackage >= 20)
  //     {
  //       insertDynamicData($conn, "milestones",array('uid','bonus'),array($userDetails[$l]->getUid(),2), "ss");
  //     }
  //   }
  // }


  if (($userDetails[$l]->getUserRank() == 'President') && $groupSalesPackage >= 106)
  {
    $bonusDetails = getMilesStones($conn, "WHERE uid =? AND bonus =?",array('uid','bonus'),array($userDetails[$l]->getUid(),3), "ss");
    if (!$bonusDetails && $groupSalesPackage >= 106)
    {
      insertDynamicData($conn, "milestones",array('uid','bonus'),array($userDetails[$l]->getUid(),3), "ss");
    }
  }

  if ($groupSalesPackage >= 442)
  {
    $bonusDetails = getMilesStones($conn, "WHERE uid =? AND bonus =?",array('uid','bonus'),array($userDetails[$l]->getUid(),4), "ss");
    if (!$bonusDetails)
    {
      insertDynamicData($conn, "milestones",array('uid','bonus'),array($userDetails[$l]->getUid(),4), "ss");
    }
  }

  if ($groupSalesPackage >= 736)
  {
    $bonusDetails = getMilesStones($conn, "WHERE uid =? AND bonus =?",array('uid','bonus'),array($userDetails[$l]->getUid(),5), "ss");
    if (!$bonusDetails)
    {
      insertDynamicData($conn, "milestones",array('uid','bonus'),array($userDetails[$l]->getUid(),5), "ss");
    }
  }

  if (($userDetails[$l]->getUserRank() == 'Partner' || $userDetails[$l]->getUserRank() == 'Director') && $groupSalesPackage >= 1320)
  {
    $bonusDetails = getMilesStones($conn, "WHERE uid =? AND bonus =?",array('uid','bonus'),array($userDetails[$l]->getUid(),6), "ss");
    if (!$bonusDetails && $groupSalesPackage >= 1320)
    {
      insertDynamicData($conn, "milestones",array('uid','bonus'),array($userDetails[$l]->getUid(),6), "ss");
    }
  }

  if ($groupSalesPackage >= 2170)
  {
    $bonusDetails = getMilesStones($conn, "WHERE uid =? AND bonus =?",array('uid','bonus'),array($userDetails[$l]->getUid(),7), "ss");
    if (!$bonusDetails)
    {
      insertDynamicData($conn, "milestones",array('uid','bonus'),array($userDetails[$l]->getUid(),7), "ss");
    }
  }

  if ($groupSalesPackage >= 2170)
  {
    $bonusDetails = getMilesStones($conn, "WHERE uid =? AND bonus =?",array('uid','bonus'),array($userDetails[$l]->getUid(),8), "ss");
    if (!$bonusDetails)
    {
      insertDynamicData($conn, "milestones",array('uid','bonus'),array($userDetails[$l]->getUid(),8), "ss");
    }
  }

}
?>