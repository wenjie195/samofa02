<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Announcement.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/languageFunction.php';

$conn = connDB();

$userDetails = getUser($conn, "WHERE user_rank_edit = '' AND status = 'Active' AND dt_rank is NULL");

for ($i=0; $i <count($userDetails) ; $i++) {
  $rankUpdated = 'Member'; // default rank
  $agentCount = 0;
  $presidentCount = 0;
  $partnerCount = 0;
  $directorCount = 0;
  $total_direct_downline = 0;
  $referralHistory = getReferralHistory($conn, "WHERE referral_id =?",array("referral_id"),array($userDetails[$i]->getUid()), "s");
  $currentLevel = $referralHistory[0]->getCurrentLevel();
  $directDownlineLevel = $currentLevel + 1;
  $getWho = getWholeDownlineTree($conn, $userDetails[$i]->getUid(), false);
  if ($getWho) {
    for ($j=0; $j <count($getWho) ; $j++) {
      if ($getWho[$j]->getCurrentLevel() == $directDownlineLevel) {
        $downlineDetails = getUser($conn, "WHERE uid=?",array("uid"),array($getWho[$j]->getReferralId()), "s");

        $total_direct_downline++;

        if ($downlineDetails[0]->getUserRank() == 'Agent') {
          $agentCount += 1;
        }
        if ($downlineDetails[0]->getUserRank() == 'President') {
          $presidentCount += 1;
        }
        if ($downlineDetails[0]->getUserRank() == 'Partner') {
          $partnerCount += 1;
        }
        if ($downlineDetails[0]->getUserRank() == 'Director') {
          $directorCount += 1;
        }
      }
    }
  }
  if ($total_direct_downline >= 8) {
    // echo "Name : ".$userDetails[$i]->getUsername()." President<br>";
    $rankUpdated = 'Agent';
  }
  if ($agentCount >= 8) {
    // echo "Name : ".$userDetails[$i]->getUsername()." President<br>";
    $rankUpdated = 'President';
  }
  if ($presidentCount >= 8) {
    // echo "Name : ".$userDetails[$i]->getUsername()." Partner<br>";
    $rankUpdated = 'Partner';
  }
  if ($partnerCount >= 8) {
    // echo "Name : ".$userDetails[$i]->getUsername()." Director<br>";
    $rankUpdated = 'Director';
  }

  if ($userDetails[$i]->getUserRankEdit() == 'Director') {
    if ($rankUpdated == 'Member' ||$rankUpdated == 'Agent' || $rankUpdated == 'President' || $rankUpdated == 'Partner') {
      $rankUpdated = 'Director';
    }
  }
  if ($userDetails[$i]->getUserRankEdit() == 'Partner') {
    if ($rankUpdated == 'Member' ||$rankUpdated == 'Agent' || $rankUpdated == 'President') {
      $rankUpdated = 'Partner';
    }
  }
  if ($userDetails[$i]->getUserRankEdit() == 'President') {
    if ($rankUpdated == 'Member' ||$rankUpdated == 'Agent') {
      $rankUpdated = 'President';
    }
  }
  if ($userDetails[$i]->getUserRankEdit() == 'Agent') {
    if ($rankUpdated == 'Member') {
      $rankUpdated = 'Agent';
    }
  }
  echo $userDetails[$i]->getUsername() . 'New Rank : ' . $rankUpdated;
  echo ' Direct Downline : '. $total_direct_downline . 'Downline Agent :' . $agentCount . 'Downline President :' . $presidentCount . 'Downline Partner :' . $partnerCount . 'Downline Director :' . $directorCount . '<br>';

  $userUpdateRank = updateDynamicData($conn,'user','WHERE uid =?',array('user_rank'),array($rankUpdated,$userDetails[$i]->getUid()), "ss");
}
 ?>
