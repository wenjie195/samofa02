<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';

$conn = connDB();

echo $todayDate = date('Y-m-d');
echo "<br>";
// echo $todayDate = date('m');
echo $currentMonth = date('m');
echo "<br>";
echo $currentYear = date('Y');
echo "<br>";
echo "<br>";

$M1 = '01';
$M2 = '02';
$M3 = '03';
$M4 = '04';
$M5 = '05';
$M6 = '06';
$M7 = '07';
$M8 = '08';
$M9 = '09';
$M10 = '10';
$M11 = '11';
$M12 = '12';

$status = "Inactive";

$allUsers = getUser($conn,"WHERE user_rank = 'Partner' ");

if($allUsers) 
{
    for($cnt = 0;$cnt < count($allUsers) ;$cnt++)
    {
        $userRank = $allUsers[$cnt]->getUserRank();
        $ordersDetails = getOrders($conn, "WHERE uid= ? AND status = 'APPROVED' AND product_type = 'Package' ORDER BY date_created DESC LIMIT 1",array("uid"),array($allUsers[$cnt]->getUid()), "s");
        
        if($ordersDetails) 
        {
            for($cntA = 0;$cntA < count($ordersDetails) ;$cntA++)
            {
                // echo "no order ppl";
                // echo "<br>";
                echo $userUidnoOrder = $allUsers[$cnt]->getUid();
                echo "<br>";
                echo $userUsernamenoOrder = $allUsers[$cnt]->getUsername();
                echo "<br>";
                echo "Rank : ";
                echo $userRanknoOrder = $allUsers[$cnt]->getUserRank();
                echo "<br>";
                echo "Date of Join : ";
                echo $userJoinDatenoOrder = $allUsers[$cnt]->getDateCreated();
                echo "<br>";

                echo "Last Order Date : ";
                echo $ordersDetails[$cntA]->getDateCreated();
                // echo "Inactive Status !!";
                // echo "<br>";
                
                echo $orderYear = date('Y',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateCreated()));
                echo "<br>";    
                echo $orderMonth = date('m',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateCreated()));
                echo "<br>";

                if($orderMonth == $M1 || $orderMonth == $M2 || $orderMonth == $M3) 
                {
                    if($orderYear == $currentYear) 
                    {
                        if($currentMonth == $M1 || $currentMonth == $M2 || $currentMonth == $M3 || $currentMonth == $M4 || $currentMonth == $M5 || $currentMonth == $M6) 
                        {   }
                        else
                        {
                            if($userRanknoOrder == 'Partner')
                            {
                                $downgradeRank = 'President';
                            }
    
                            $tableName = array();
                            $tableValue =  array();
                            $stringType =  "";
                            //echo "save to database";
    
                            if($downgradeRank)
                            {
                                array_push($tableName,"user_rank");
                                array_push($tableValue,$downgradeRank);
                                $stringType .=  "s";
                            }
                            if($status)
                            {
                                array_push($tableName,"status");
                                array_push($tableValue,$status);
                                $stringType .=  "s";
                            }           
                            if($status)
                            {
                                array_push($tableName,"recipient_phone");
                                array_push($tableValue,$status);
                                $stringType .=  "s";
                            }                       
                            array_push($tableValue,$userUidnoOrder);
                            $stringType .=  "s";
                            $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                            if($passwordUpdated)
                            {
                                echo "Inactive Status , ";
                                echo "Downgrading !!";
                                echo "<br>";
                            }
                            else
                            {
                                echo "ERROR 2020 ++ : FAIL To Downgrade Status and Ranking !!";
                                echo "<br>";
                            }
                        }
                    }
                    else
                    {
                        if($userRanknoOrder == 'Partner')
                        {
                            $downgradeRank = 'President';
                        }

                        $tableName = array();
                        $tableValue =  array();
                        $stringType =  "";
                        //echo "save to database";

                        if($downgradeRank)
                        {
                            array_push($tableName,"user_rank");
                            array_push($tableValue,$downgradeRank);
                            $stringType .=  "s";
                        }
                        if($status)
                        {
                            array_push($tableName,"status");
                            array_push($tableValue,$status);
                            $stringType .=  "s";
                        }           
                        if($status)
                        {
                            array_push($tableName,"recipient_phone");
                            array_push($tableValue,$status);
                            $stringType .=  "s";
                        }                       
                        array_push($tableValue,$userUidnoOrder);
                        $stringType .=  "s";
                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                        if($passwordUpdated)
                        {
                            echo "Inactive Status , ";
                            echo "Downgrading !!";
                            echo "<br>";
                        }
                        else
                        {
                            echo "ERROR 2020 ++ : FAIL To Downgrade Status and Ranking !!";
                            echo "<br>";
                        }
                    }
                }
                elseif($orderMonth == $M4 || $orderMonth == $M5 || $orderMonth == $M6) 
                {
                    if($orderYear == $currentYear) 
                    {
                        if($currentMonth == $M4 || $currentMonth == $M5 || $currentMonth == $M6 || $currentMonth == $M7 || $currentMonth == $M8 || $currentMonth == $M9) 
                        {   }
                        else
                        {
                            if($userRanknoOrder == 'Partner')
                            {
                                $downgradeRank = 'President';
                            }
    
                            $tableName = array();
                            $tableValue =  array();
                            $stringType =  "";
                            //echo "save to database";
    
                            if($downgradeRank)
                            {
                                array_push($tableName,"user_rank");
                                array_push($tableValue,$downgradeRank);
                                $stringType .=  "s";
                            }
                            if($status)
                            {
                                array_push($tableName,"status");
                                array_push($tableValue,$status);
                                $stringType .=  "s";
                            }           
                            if($status)
                            {
                                array_push($tableName,"recipient_phone");
                                array_push($tableValue,$status);
                                $stringType .=  "s";
                            }                       
                            array_push($tableValue,$userUidnoOrder);
                            $stringType .=  "s";
                            $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                            if($passwordUpdated)
                            {
                                echo "Inactive Status , ";
                                echo "Downgrading !!";
                                echo "<br>";
                            }
                            else
                            {
                                echo "ERROR 2020 ++ : FAIL To Downgrade Status and Ranking !!";
                                echo "<br>";
                            }
                        }
                    }
                    else
                    {
                        if($userRanknoOrder == 'Partner')
                        {
                            $downgradeRank = 'President';
                        }

                        $tableName = array();
                        $tableValue =  array();
                        $stringType =  "";
                        //echo "save to database";

                        if($downgradeRank)
                        {
                            array_push($tableName,"user_rank");
                            array_push($tableValue,$downgradeRank);
                            $stringType .=  "s";
                        }
                        if($status)
                        {
                            array_push($tableName,"status");
                            array_push($tableValue,$status);
                            $stringType .=  "s";
                        }           
                        if($status)
                        {
                            array_push($tableName,"recipient_phone");
                            array_push($tableValue,$status);
                            $stringType .=  "s";
                        }                       
                        array_push($tableValue,$userUidnoOrder);
                        $stringType .=  "s";
                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                        if($passwordUpdated)
                        {
                            echo "Inactive Status , ";
                            echo "Downgrading !!";
                            echo "<br>";
                        }
                        else
                        {
                            echo "ERROR 2020 ++ : FAIL To Downgrade Status and Ranking !!";
                            echo "<br>";
                        }
                    }
                }
                elseif($orderMonth == $M7 || $orderMonth == $M8 || $orderMonth == $M9) 
                {
                    if($orderYear == $currentYear) 
                    {
                        if($currentMonth == $M7 || $currentMonth == $M8 || $currentMonth == $M9 || $currentMonth == $M10 || $currentMonth == $M11 || $currentMonth == $M12)
                        {   }
                        else
                        {
                            if($userRanknoOrder == 'Partner')
                            {
                                $downgradeRank = 'President';
                            }
    
                            $tableName = array();
                            $tableValue =  array();
                            $stringType =  "";
                            //echo "save to database";
    
                            if($downgradeRank)
                            {
                                array_push($tableName,"user_rank");
                                array_push($tableValue,$downgradeRank);
                                $stringType .=  "s";
                            }
                            if($status)
                            {
                                array_push($tableName,"status");
                                array_push($tableValue,$status);
                                $stringType .=  "s";
                            }           
                            if($status)
                            {
                                array_push($tableName,"recipient_phone");
                                array_push($tableValue,$status);
                                $stringType .=  "s";
                            }                       
                            array_push($tableValue,$userUidnoOrder);
                            $stringType .=  "s";
                            $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                            if($passwordUpdated)
                            {
                                echo "Inactive Status , ";
                                echo "Downgrading !!";
                                echo "<br>";
                            }
                            else
                            {
                                echo "ERROR 2020 ++ : FAIL To Downgrade Status and Ranking !!";
                                echo "<br>";
                            }
                        }
                    }
                    else
                    {
                        if($userRanknoOrder == 'Partner')
                        {
                            $downgradeRank = 'President';
                        }

                        $tableName = array();
                        $tableValue =  array();
                        $stringType =  "";
                        //echo "save to database";

                        if($downgradeRank)
                        {
                            array_push($tableName,"user_rank");
                            array_push($tableValue,$downgradeRank);
                            $stringType .=  "s";
                        }
                        if($status)
                        {
                            array_push($tableName,"status");
                            array_push($tableValue,$status);
                            $stringType .=  "s";
                        }           
                        if($status)
                        {
                            array_push($tableName,"recipient_phone");
                            array_push($tableValue,$status);
                            $stringType .=  "s";
                        }                       
                        array_push($tableValue,$userUidnoOrder);
                        $stringType .=  "s";
                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                        if($passwordUpdated)
                        {
                            echo "Inactive Status , ";
                            echo "Downgrading !!";
                            echo "<br>";
                        }
                        else
                        {
                            echo "ERROR 2020 ++ : FAIL To Downgrade Status and Ranking !!";
                            echo "<br>";
                        }
                    }
                }
                elseif($orderMonth == $M10 || $orderMonth == $M11 || $orderMonth == $M12) 
                { 
                    $nextYear = $orderYear + 1;

                    if($orderYear == $currentYear) 
                    {
                        // if($currentMonth == $M10 || $currentMonth == $M11 || $currentMonth == $M12 || $currentMonth == $M1 || $currentMonth == $M2 || $currentMonth == $M3 ) 
                        if($currentMonth == $M10 || $currentMonth == $M11 || $currentMonth == $M12)
                        {   }
                        else
                        {
                            if($userRanknoOrder == 'Partner')
                            {
                                $downgradeRank = 'President';
                            }
    
                            $tableName = array();
                            $tableValue =  array();
                            $stringType =  "";
                            //echo "save to database";
    
                            if($downgradeRank)
                            {
                                array_push($tableName,"user_rank");
                                array_push($tableValue,$downgradeRank);
                                $stringType .=  "s";
                            }
                            if($status)
                            {
                                array_push($tableName,"status");
                                array_push($tableValue,$status);
                                $stringType .=  "s";
                            }           
                            if($status)
                            {
                                array_push($tableName,"recipient_phone");
                                array_push($tableValue,$status);
                                $stringType .=  "s";
                            }                       
                            array_push($tableValue,$userUidnoOrder);
                            $stringType .=  "s";
                            $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                            if($passwordUpdated)
                            {
                                echo "Inactive Status , ";
                                echo "Downgrading !!";
                                echo "<br>";
                            }
                            else
                            {
                                echo "ERROR 2020 ++ : FAIL To Downgrade Status and Ranking !!";
                                echo "<br>";
                            }
                        }
                    }
                    elseif($currentYear == $nextYear) 
                    {
                        if($currentMonth == $M1 || $currentMonth == $M2 || $currentMonth == $M3 ) 
                        {   }
                        else
                        {
                            if($userRanknoOrder == 'Partner')
                            {
                                $downgradeRank = 'President';
                            }
    
                            $tableName = array();
                            $tableValue =  array();
                            $stringType =  "";
                            //echo "save to database";
    
                            if($downgradeRank)
                            {
                                array_push($tableName,"user_rank");
                                array_push($tableValue,$downgradeRank);
                                $stringType .=  "s";
                            }
                            if($status)
                            {
                                array_push($tableName,"status");
                                array_push($tableValue,$status);
                                $stringType .=  "s";
                            }           
                            if($status)
                            {
                                array_push($tableName,"recipient_phone");
                                array_push($tableValue,$status);
                                $stringType .=  "s";
                            }                       
                            array_push($tableValue,$userUidnoOrder);
                            $stringType .=  "s";
                            $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                            if($passwordUpdated)
                            {
                                echo "Inactive Status , ";
                                echo "Downgrading !!";
                                echo "<br>";
                            }
                            else
                            {
                                echo "ERROR 2020 ++ : FAIL To Downgrade Status and Ranking !!";
                                echo "<br>";
                            }
                        }
                    }
                    else
                    {
                        if($userRanknoOrder == 'Partner')
                        {
                            $downgradeRank = 'President';
                        }

                        $tableName = array();
                        $tableValue =  array();
                        $stringType =  "";
                        //echo "save to database";

                        if($downgradeRank)
                        {
                            array_push($tableName,"user_rank");
                            array_push($tableValue,$downgradeRank);
                            $stringType .=  "s";
                        }
                        if($status)
                        {
                            array_push($tableName,"status");
                            array_push($tableValue,$status);
                            $stringType .=  "s";
                        }           
                        if($status)
                        {
                            array_push($tableName,"recipient_phone");
                            array_push($tableValue,$status);
                            $stringType .=  "s";
                        }                       
                        array_push($tableValue,$userUidnoOrder);
                        $stringType .=  "s";
                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                        if($passwordUpdated)
                        {
                            echo "Inactive Status , ";
                            echo "Downgrading !!";
                            echo "<br>";
                        }
                        else
                        {
                            echo "ERROR 2020 ++ : FAIL To Downgrade Status and Ranking !!";
                            echo "<br>";
                        }
                    }
                }
            }
        }
        
        // if(!$ordersDetails) 
        elseif(!$ordersDetails) 
        {
            echo "no order ppl";
            echo "<br>";
            echo $userUidnoOrder = $allUsers[$cnt]->getUid();
            echo "<br>";
            echo $userUsernamenoOrder = $allUsers[$cnt]->getUsername();
            echo "<br>";
            echo "Rank";
            echo "<br>";
            echo $userRanknoOrder = $allUsers[$cnt]->getUserRank();
            echo "<br>";
            echo "Date of Join";
            echo "<br>";
            echo $userJoinDatenoOrder = $allUsers[$cnt]->getDateCreated();
            echo "<br>";
            echo "Inactive Status !!";
            echo "<br>";
            echo "<br>";
            echo "<br>";

            if($userRanknoOrder == 'Partner')
            {
                $downgradeRank = 'President';
            }

            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
    
            // $rank = "Member";
            $downgradeStatus = "Inactive";

            if($downgradeRank)
            {
                array_push($tableName,"user_rank");
                array_push($tableValue,$downgradeRank);
                $stringType .=  "s";
            }
            if($downgradeStatus)
            {
                array_push($tableName,"status");
                array_push($tableValue,$downgradeStatus);
                $stringType .=  "s";
            }           
            if($downgradeStatus)
            {
                array_push($tableName,"recipient_phone");
                array_push($tableValue,$downgradeStatus);
                $stringType .=  "s";
            }                       
            array_push($tableValue,$userUidnoOrder);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                echo "Inactive Status !!";
                echo "<br>";
                echo "Downgrading !!";
                echo "<br>";
            }
            else
            {
                echo "ERROR 2020 ++ : FAIL To Downgrade Status and Ranking !!";
                echo "<br>";
            }

        }
    }
}
?>