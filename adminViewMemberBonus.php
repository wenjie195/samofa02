<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';

require_once dirname(__FILE__) . '/classes/Bonus.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/OtherBonus.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];
// $conn = connDB();
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/adminViewBonusFlow.php" />
    <meta property="og:title" content="Bonus Details | Samofa 莎魔髪" />
    <title>Bonus Details | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/adminViewBonusFlow.php" />
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
	<?php include 'css.php'; ?>
</head>

<body class="body">
<!-- <?php //include 'headerAfterLogin.php'; ?> -->

<header id="header" class="header header--fixed same-padding header1 menu-color admin-header" role="banner">
    <div class="big-container-size hidden-padding" id="top-menu">
        <div class="float-left left-logo-div">
            <a href="index.php"><img src="img/samofa.png" class="logo-img" alt="Logo" title="Logo"></a>
        </div>

        <a href="logout.php" class="pink-hover-text menu-item">
            <?php echo _MAINJS_ALL_LOGOUT ?>
        </a>

        <div id="dl-menu" class="dl-menuwrapper logged-in-dl">
            <button class="dl-trigger">Open Menu</button>
            <ul class="dl-menu">
                <li><a href="adminDashboard.php"><?php echo _ADMIN_DASHBOARD ?></a></li>
                <li><a href="logout.php" ><?php echo _MAINJS_ALL_LOGOUT ?></a></li>
            </ul>
        </div>
    </div>
</header>

<div class="width100 menu-distance75 min-height-with-flower">

    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color">Bonus Details
        <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>">
    </h1>

    <div class="width100 same-padding">

        <?php
            // echo $_POST['user_uid'];
            if(isset($_POST['user_uid']))
            {
                $conn = connDB();
                // $directBonus = getBonusDetails($conn," WHERE referrer_id = ? ", array("referrer_id") ,array($_POST['user_uid']),"s");
                // $otherBonus = getOtherBonus($conn," WHERE receive_uid = ? ", array("receive_uid") ,array($_POST['user_uid']),"s");

                $directBonus = getBonusDetails($conn," WHERE referrer_id = ? ORDER BY date_created DESC ", array("referrer_id") ,array($_POST['user_uid']),"s");
                $otherBonus = getOtherBonus($conn," WHERE receive_uid = ? ORDER BY date_created DESC ", array("receive_uid") ,array($_POST['user_uid']),"s");

                // $directBonus = getBonusDetails($conn," WHERE referrer_id = ? ORDER BY date_created DESC ", array("referrer_id") ,array($_POST['user_uid']),"s");
                // $otherBonus = getOtherBonus($conn," WHERE receive_uid = ? ORDER BY date_created DESC", array("receive_uid") ,array($_POST['user_uid']),"s");
            ?>
                <!-- <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color">Direct Bonus</h1> -->

                <table class="table-css">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Receiver Name</th>
                            <th>Amount</th>
                            <th>Bonus Name</th>
                            <th>Status</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if($directBonus)
                        {
                            for($cnt = 0;$cnt < count($directBonus) ;$cnt++)
                            {
                            ?>    
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>

                                    <td>
                                        <?php 
                                            $referralId = $directBonus[$cnt]->getReferralId();
                                            $conn = connDB();
                                            $userRows = getUser($conn," WHERE uid = ? ", array("uid") ,array($referralId),"s");
                                            echo $userRows[0]->getUsername();
                                        ?>
                                    </td>

                                    <td><?php echo $directBonus[$cnt]->getAmount();?></td>
                                    <td>Direct Bonus</td>
                                    <td><?php echo $directBonus[$cnt]->getStatus();?></td>
                                    <td><?php echo $directBonus[$cnt]->getDateCreated();?></td>
                                </tr>
                            <?php
                            }
                        }
                        ?>  
                    </tbody>
                </table>

                <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"></h1>
                <div class="width100 same-padding"></div>

                <div class="clear"></div>

                <table class="table-css">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Purchaser Name</th>
                            <th>Amount</th>
                            <th>Bonus Name</th>
                            <th>Status</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if($otherBonus)
                        {
                            for($cnt = 0;$cnt < count($otherBonus) ;$cnt++)
                            {
                            ?>    
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>
                                    <!-- <td><?php //echo $otherBonus[$cnt]->getReceiveUsername();?></td> -->

                                    <td>
                                        <?php 
                                            $purchaserUid = $otherBonus[$cnt]->getUid();
                                            $conn = connDB();
                                            $userRows = getUser($conn," WHERE uid = ? ", array("uid") ,array($purchaserUid),"s");
                                            echo $userRows[0]->getUsername();
                                        ?>
                                    </td>

                                    <td><?php echo $otherBonus[$cnt]->getBonus();?></td>
                                    <td><?php echo $otherBonus[$cnt]->getBonusType();?></td>
                                    <td><?php echo $otherBonus[$cnt]->getStatus();?></td>
                                    <td><?php echo $otherBonus[$cnt]->getDateCreated();?></td>
                                </tr>
                            <?php
                            }
                        }
                        ?>  
                    </tbody>
                </table>

        <?php
        }
        ?>

    </div>
</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php
  $tz = 'Asia/Kuala_Lumpur';
  $timestamp = time();
  $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
  $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
  $time = $dt->format('Y');
?>

<div class="footer-div">
	<p class="footer-p">© <?php echo $time;?> <?php echo _JS_FOOTER ?></p>
</div>

</body>
</html>