    <?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/Product.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/phoneFormat.php';

$id = $_POST['id'];

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/userOrdersCompDetails.php" />
    <meta property="og:title" content="Withdrawal Details | Samofa 莎魔髪" />
    <title>Order Details | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/userOrdersCompDetails.php" />
    <!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
    <?php include 'css.php'; ?>
    
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo "Order Details" ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    <div class="width100 same-padding">

        <?php
            if(isset($_POST['id']))
            {
                $conn = connDB();
                $orderDetails = getOrders($conn,"WHERE id = ? ", array("id") ,array($id),"s");
            ?>

                <div class="dual-input">
                    <p class="details-p"><?php echo _JS_USERNAME ?>: </b></p>
                    <p><b><?php echo $orderDetails[0]->getUsername();?></p>
                </div>

                <div class="dual-input second-dual-input">
                    <p class="details-p"><?php echo _JS_FIRSTNAME ?>: </p>
                    <p><b><?php echo $orderDetails[0]->getRecipientFirstName();?></b></p>
                </div>

                <div class="clear"></div>

                <div class="dual-input">
                    <p class="details-p"><?php echo _JS_LASTNAME ?>: </p>
                    <p><b><?php echo $orderDetails[0]->getRecipientLastName();?></b></p>
                </div>

                <div class="dual-input second-dual-input">
                    <p class="details-p"><?php echo _MEMBER_RECEIVER_PHONE_NO ?>: </p>
                    <p><b><?php echo phone_number_format($orderDetails[0]->getContact());?></b></p>
                </div>

                <div class="clear"></div>

                <div class="dual-input">
                    <p class="details-p"><?php echo _CHECKOUT_SHIPPING_ADDRESS ?>:</p>
                    <p><b><?php echo $orderDetails[0]->getAddressLine1().",<br>".$orderDetails[0]->getAddressLine2().",<br>".$orderDetails[0]->getZipcode()." ".$orderDetails[0]->getCity().",<br>".$orderDetails[0]->getState().",<br>".$orderDetails[0]->getCountry().".";?></b></p>
                </div>

                <div class="dual-input second-dual-input">
                    <p class="details-p"><?php echo _PRODUCTDETAILS ?>: <b>
                        <?php $productOrdersDetails = getProductOrders($conn, "WHERE order_id =?",array("order_id"),array($orderDetails[0]->getId()), "s");

                            for ($i=0; $i <count($productOrdersDetails) ; $i++) { 
                                $productDetails = getProduct($conn, "WHERE id =?",array("id"),array($productOrdersDetails[$i]->getProductId()),"s");

                                ?><p><b><?php echo $productDetails[0]->getName().", "; ?></b></p><?php
                            }
                         ?>
                    </b></p>
                </div>

                <div class="clear"></div>

                <div class="dual-input">
                    <p class="details-p"><?php echo _SHIPPING_PAYMENT_METHOD ?>: <b></b></p>
                    <p><b><?php echo $orderDetails[0]->getPaymentMethod(); ?></b></p>
                </div>

                <div class="dual-input second-dual-input">
                    <p class="details-p"><?php echo _SHIPPING_AMOUNT ?>: </p>
                    <p><b><?php echo "RM ".$orderDetails[0]->getPaymentAmount();?></b></p>
                </div>

                <div class="dual-input">
                    <p class="details-p"><?php echo _ADMIN_PICKUP_TYPE ?>: </p>
                    <p><b><?php echo $orderDetails[0]->getPickupType(); ?></b></p>
                    
                </div>

                <?php if ($orderDetails[0]->getPickupType() == 'Courier') {
                    ?>
                    <div id="courierTypeDiv" class="dual-input second-dual-input">
                    <p class="details-p"><?php echo _ADMIN_COURIER_TYPE ?>: </p>
                    <p><b><?php echo $orderDetails[0]->getPickupName(); ?></b></p>
                    </div>

                    <div class="clear"></div>

                    <div id="courierTypeOther" class="dual-input">
                        <p class="details-p"><?php echo _ADMIN_TRACKING_NO ?>: </p>
                        <p><b><?php echo $orderDetails[0]->getTrackingNo(); ?></b></p>
                    </div>
                    <?php
                } ?>

                <div class="clear"></div>
            <?php
        }
        ?>
    </div>
</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>
<script type="text/javascript">
    $(document).ready(function(){
        $("#pickupType").on("change",function(){
            var pickupType = $(this).val();
            // alert(pickupType);
            if (pickupType == 'Courier') {
                $("#courierTypeDiv").fadeIn(function(){
                    $("#courierTypeDiv").show();
                });
                $("#courierType").on("change",function(){
                    var courierType = $("#courierType").val();
                    // alert(courierType);
                    if (courierType == 'Other') {
                        $("#courierTypeOther").fadeIn(function(){
                            $("#courierTypeOther").show();
                        });
                        $("#trackingNo").fadeIn(function(){
                        $("#trackingNo").show();
                        // $("#trackingNo").attr("class","dual-input");
                        });
                        $("#trackingNo").attr("class","dual-input second-dual-input");
                    }else{
                        $("#courierTypeOther").fadeOut(function(){
                        $("#courierTypeOther").hide();
                        });
                        $("#trackingNo").fadeIn(function(){
                        $("#trackingNo").show();
                        $("#trackingNo").attr("class","dual-input");
                        });
                    }
                });
            }else{
                $("#courierTypeDiv,#trackingNo,#courierTypeOther").fadeOut(function(){
                    $("#courierTypeDiv,#trackingNo,#courierTypeOther").hide();
                });
            }
        });
    });
</script>
</body>
</html>