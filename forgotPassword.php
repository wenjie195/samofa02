<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/forgotPassword.php" />
    <meta property="og:title" content="Forget Password | Samofa 莎魔髪" />
    <title>Forget Password | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/forgotPassword.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 same-padding min-height-with-flower menu-distance75">

   <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _JS_FORGOT_PASSWORD  ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

 	<form action="utilities/forgotPasswordFunction.php" method="POST">

        <div class="dual-input">
            <p class="input-top-text"><?php echo _JS_EMAIL ?></p>
            <input class="clean de-input" type="text" placeholder="<?php echo _JS_EMAIL ?>" id="forgot_password" name="forgot_password" required>
        </div>

        <div class="clear"></div>

        <div class="width100 text-center top-bottom-distance">
        	<button class="clean button-width transparent-button dark-pink-button" name="submit"><?php echo _JS_SUBMIT ?></button>
        </div>

    </form>

</div>

	<div class="clear"></div>
    <img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">
    <div class="clear"></div>

<?php include 'js.php'; ?>
<script type="text/javascript">
    var x = location.search.replace('?', '').split('=');
    if (x[1] == 1) {
        alert("Success");
    }else if(x[1] == 2){
        alert("Error");
    }
</script>
</body>
</html>
