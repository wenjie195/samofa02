<?php
if (session_id() == ""){
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/serverMaintenance.php" />
    <meta property="og:title" content="Server Maintenance | Samofa 莎魔髪" />
    <title>Server Maintenance | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/serverMaintenance.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">

<!-- <?php //include 'headerBeforeLogin.php'; ?> -->
<?php //include 'headerAfterLogin.php'; ?>


<div class="width100 same-padding padding-top75 overflow server-bg min-height-only-footer">
	<div class="main-notice-div">
		<div class="main-notice-content-div width100">
			<img src="img/maintenance.png" class="main-img-css" alt="Server Maintenance" title="Server Maintenance">
			<div class="clear"></div>
			<h1 class="dark-pink-text hi-title big-header-color">Server Maintenance <img src="img/feather.png" class="feather-png" alt="Samofa"></h1>
			<p class="quote-p2 ow-min-height0">Samofa will be undergoing maintenance from 2pm to 12pm. Thank you for your understanding.</p>
		</div>
	</div>
</div>

<?php include 'js.php'; ?>
</body>
</html>