<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$productsOrders =  getOrders($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");

$adminProductView =  getOrders($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
	 <meta property="og:url" content="https://samofa.my/purchaseCompleted.php" />

    <meta property="og:title" content="Order History | Samofa 莎魔髪" />
    <title>Order History | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/purchaseCompleted.php" />
	<?php include 'css.php'; ?> 
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 menu-distance75 min-height-with-flower">

    <div class="edit-profile-div2">
                </br></br></br></br></br></br></br></br></br>
                <h3 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color">You have Completed your Purchase!</h3>
                <h3 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color">Please Click Here to View the Purchase History!</h3>
                <div class="width100 text-center top-bottom-distance">
                    <a href="purchaseReport.php" >
                        <button class="clean button-width transparent-button dark-pink-button">Click Here !</button>
                    </a>
                </div>
            </div>
</div>    

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>