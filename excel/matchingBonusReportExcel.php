<?php

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/OtherBonus.php';

require_once dirname(__FILE__) . '/../utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();
$userDetails = getUser($conn);
$no = 0;
$cell = 1;
//call the autoload
require '../vendor/autoload.php';
//load phpspreadsheet class using namespaces
use PhpOffice\PhpSpreadsheet\Spreadsheet;
//call iofactory instead of xlsx writer
use PhpOffice\PhpSpreadsheet\IOFactory;

//make a new spreadsheet object
$spreadsheet = new Spreadsheet();
//get current active sheet (first sheet)
$sheet = $spreadsheet->getActiveSheet();

$spreadsheet->getActiveSheet()->getStyle('A1:G1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('C0C0C0');

    $styleArray = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ],
        'borders' => [
          'allBorders' => [
           'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
           'color' => ['argb' => '000000'],
         ],
      ],
    ];

    $spreadsheet->getActiveSheet()->getStyle('A1:G1')->applyFromArray($styleArray);

//set the value of cell a1 to "Hello World!"
// $sheet->setCellValue('A1', 'Hello World !');

$sheet->setCellValue('A1', 'NO.');
$sheet->setCellValue('B1', 'USERNAME');
$sheet->setCellValue('C1', 'AMOUNT');
$sheet->setCellValue('D1', 'BONUS TYPE');
$sheet->setCellValue('E1', 'FROM');
$sheet->setCellValue('F1', 'STATUS');
$sheet->setCellValue('G1', 'DATE');

if ($userDetails) {
  for ($i=0; $i <count($userDetails) ; $i++) {
    $bonusDetails = getOtherBonus($conn, " WHERE receive_uid =? AND bonus_type = 'Matching'", array("receive_uid"), array($userDetails[$i]->getUid()), "s");
    if ($bonusDetails) {
      for ($j=0; $j <count($bonusDetails) ; $j++) {
        $cell++;
        $no += 1;
        $referralDetails = getUser($conn, "WHERE uid =?",array("uid"),array($bonusDetails[$j]->getUid()), "s");
        
        if ($bonusDetails[$j]->getDateCreated()) {
          $dateCreated = date('d/m/Y',strtotime($bonusDetails[$j]->getDateCreated()));
        }else {
          $dateCreated = "";
        }
        if ($bonusDetails[$j]->getNewUser() == 'Buy Product') {
           $buyProduct =  $bonusDetails[$j]->getNewUser()." (#".$bonusDetails[$j]->getInvoiceNo().")";
          }else{
            $buyProduct = $bonusDetails[$j]->getNewUser();
          }
    
        $sheet->setCellValue('A'.$cell, $no);
        $sheet->setCellValue('B'.$cell, $bonusDetails[$j]->getReceiveUsername());
        $sheet->setCellValue('C'.$cell, $bonusDetails[$j]->getBonus());
        $sheet->setCellValue('D'.$cell, $bonusDetails[$j]->getBonusType());
        $sheet->setCellValue('E'.$cell, $buyProduct);
        $sheet->setCellValue('F'.$cell, $bonusDetails[$j]->getStatus());
        $sheet->setCellValue('G'.$cell, $dateCreated);
      }
    }
  }
}

$styleArray = [
    'borders' => [
      'allBorders' => [
       'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
       'color' => ['argb' => '000000'],
     ],
  ],
    'alignment' => [
      'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
      'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
    ],
];

$spreadsheet->getActiveSheet()->getStyle('A2:G'.$cell.'')->applyFromArray($styleArray);

$range = range('A','G');

foreach ($range as $ranges) {
  $spreadsheet->getActiveSheet()->getColumnDimension($ranges)->setAutoSize(true);
}

$spreadsheet->getActiveSheet()->getStyle('A2:G'.$cell.'')
    ->getAlignment()->setWrapText(true);


//set the header first, so the result will be treated as an xlsx file.
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

//make it an attachment so we can define filename
$date = date('d-m-Y');
header('Content-Disposition: attachment;filename="Matching Bonus Report '.$date.'.xlsx"');

// create IOFactory object
$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
// save into php output
$writer->save('php://output');
