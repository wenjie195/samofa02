<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$allWithdrawal = getWithdrawal($conn, " WHERE status = ? ", array("status"), array("PENDING"), "s");

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/adminViewWithdrawal.php" />
    <meta property="og:title" content="Withdrawal Request | Samofa 莎魔髪" />
    <title>Withdrawal Request | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/adminViewWithdrawal.php" />
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
	<?php include 'css.php'; ?>
    
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMIN_WITHDRAWAL_REQUEST ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    <div class="width100 same-padding container-div1">

    <div class="overflow-scroll-div">
        <table class="table-css">
            <thead>
                <tr>
                    <th><?php echo _PRODUCTDETAILS_NO ?></th>
                    <th><?php echo _MAINJS_INDEX_USERNAME ?></th>
                    <th><?php echo _SHIPPING_AMOUNT ?></th>
                    <th><?php echo _ADMIN_REQUESTED_DATE ?></th>
                    <th><?php echo _ADMIN_ACTION ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($allWithdrawal)
                {
                    for($cnt = 0;$cnt < count($allWithdrawal) ;$cnt++)
                    {
                        $userDetails = getUser($conn, "WHERE uid=?",array("uid"),array($allWithdrawal[$cnt]->getUid()), "s");
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $userDetails[0]->getUsername();?></td>
                            <td><?php echo $allWithdrawal[$cnt]->getAmount();?></td>
                            <td><?php echo date('d/m/Y',strtotime($allWithdrawal[$cnt]->getDateCreated()));?></td>

                            <td>
                                <form method="POST" action="adminVerifyWithdrawal.php" class="hover1">
                                    <button class="clean hover1 transparent-button pointer" type="submit" name="withdrawal_uid" value="<?php echo $allWithdrawal[$cnt]->getWithdrawalUid();?>">
                                        <img src="img/edit.png" class="edit-icon1 hover1a">
                                        <img src="img/edit2.png" class="edit-icon1 hover1b">
                                    </button>
                                </form>                  
                            </td>
                        </tr>
                    <?php
                    }
                }
                ?> 
            </tbody>
        </table>
    </div>

    </div>
</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>