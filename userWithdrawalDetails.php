<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://samofa.my/userWithdrawalDetails.php" />
    <meta property="og:title" content="Withdrawal Details | Samofa 莎魔髪" />
    <title>Withdrawal Details | Samofa 莎魔髪</title>
    <link rel="canonical" href="https://samofa.my/userWithdrawalDetails.php" />
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
	<?php include 'css.php'; ?>
    
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMIN_WITHDRAWAL_DETAILS ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    <div class="width100 same-padding">

        <?php
            if(isset($_POST['withdrawal_uid']))
            {
                $conn = connDB();
                $withdrawalDetails = getWithdrawal($conn,"WHERE withdrawal_uid = ? ", array("withdrawal_uid") ,array($_POST['withdrawal_uid']),"s");
            ?>

                <div class="dual-input">
                    <p class="details-p"><?php echo _EDITPROFILE_BANK_NAME ?>: <b><?php echo $withdrawalDetails[0]->getBankName();?></b></p>
                </div>

                <div class="dual-input second-dual-input">
                    <p class="details-p"><?php echo _EDITPROFILE_BANK_ACC_HOLDER ?>: <b><?php echo $withdrawalDetails[0]->getBankAccHolder();?></b></p>
                </div>

                <div class="clear"></div>

                <div class="dual-input">
                    <p class="details-p"><?php echo _EDITPROFILE_BANK_ACC_NO ?>: <b><?php echo $withdrawalDetails[0]->getBankAccNo();?></b></p>
                </div>

                <div class="dual-input second-dual-input">
                    <p class="details-p"><?php echo _ADMIN_AMOUNT ?>: <b><?php echo $withdrawalDetails[0]->getAmount();?></b></p>
                </div>

                <div class="clear"></div>

                <div class="dual-input">
                    <p class="details-p"><?php echo _PRODUCTDETAILS_STATUS ?>: <b><?php echo $withdrawalDetails[0]->getStatus();?></b></p>
                </div>

                <div class="clear"></div>

                <?php 
                    $status = $withdrawalDetails[0]->getStatus();
                    if($status == "APPROVED")
                    {
                    ?>

                        <div class="dual-input">
                            <p class="details-p"><?php echo _ADMIN_PAYMENT_METHOD ?>: <b><?php echo $withdrawalDetails[0]->getMethod();?></b></p>
                        </div>

                        <div class="dual-input second-dual-input">
                            <p class="details-p"><?php echo _ADMIN_DATE_AND_TIME ?>: <b><?php echo $withdrawalDetails[0]->getDateUpdated();?></b></p>
                        </div>
						<div class="clear"></div>

                    <?php
                    }
                    elseif($status == "REJECTED")
                    {
                    ?>

                        <div class="dual-input">
                            <p class="details-p"><?php echo _ADMIN_REASON ?>: <b><?php echo $withdrawalDetails[0]->getReason();?></b></p>
                        </div>

                        <div class="dual-input second-dual-input">
                            <p class="details-p"><?php echo _ADMIN_DATE_AND_TIME ?>: <b><?php echo $withdrawalDetails[0]->getDateUpdated();?></b></p>
                        </div>
						<div class="clear"></div>

                    <?php
                    }
                    else
                    {}
                ?>

            <?php
        }
        ?>

    </div>
</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>